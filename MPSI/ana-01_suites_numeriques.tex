\chapter{Suites réelles}

\section{Généralités}

\subsection{Vocabulaire et notation}

\begin{definition}
  Soit $E$ un ensemble. On appelle \emph{suite d'éléments de $E$} toute
  application de $\N$ (ou d'une partie de $\N$) dans $E$.
\end{definition}

On dit alors que $u \in E^{\N}$ et on note $u = (u_n)_{n\in\N}$ la suite de
terme général $u_n$.

\subsection{Construction de suites}

\subsubsection{Suites définies de façon explicite}

De telles suites sont définies par $\forall n \in \N, u_n = f(n)$. L'accès à
$u_n$ est direct.

\begin{exemple}
  \begin{itemize}
    \item $u$ de terme général $u_n = 2^n + 1$ ;
    \item $(f_n)_{n \in \N}$ la suite de fonction telle que $\forall n \in
      \N,\ f_n \colon \R \to R, x \mapsto \sin nx$ ;
    \item La suite de droite $(\mathcal{D}_n)_{n\in \N}$ où $\mathcal{D}_n$
      est la droite passant par $\brk*{ 1 + \frac1{n+1} ; \frac1{n+1} }$ et
      de pente $\tan n$.
  \end{itemize}
\end{exemple}

\subsubsection{Suites définies par récurrence}

De telles suites sont définies par leurs premiers termes puis par une
méthode de calcul donnant $u_{n+1}$ en fonction de $u_n,\ u_{n-1},\ \dots$
et éventuellement $n$.

\begin{exemple}
  \begin{itemize}
    \item $u_0 = 1$ et $\forall n \in \N, \ u_n = \mathrm{ppcm}(u_0, u_1 +
      1, \dots , u_n + n)$
    \item $u_0 = u_1 = 1$ et $\forall n \in \N,\ u_{n+2} = u_{n+1} + u_n$.
      On démontrera que $u_n = \frac1{\sqrt{5}}\brk*{ \brk*{\frac{1 +
      \sqrt{5}}2 }^{n+1} - \brk*{\frac{1 - \sqrt{5}}2 }^{n+1}}$. C'est la
      suite dite de Fibonacci.
  \end{itemize}
\end{exemple}

\subsubsection{Suites extraites d'une suite}

\begin{definition}[suite extraite]
  Soit $u$ une suite, $u \in E^{\N}$. On appelle \emph{suite extraite de
  $u$} toute application $u \circ \varphi$ où $\varphi \colon \N \to \N$ est
  strictement croissante.
\end{definition}

\begin{remarque}
  On dit que $\varphi$ est une fonction d'extaction.
\end{remarque}

\begin{exemple}
  Soit $\varphi \colon n \mapsto 2n$. $u \circ \varphi$ est la suite des
  termes de rang pair.
\end{exemple}

\begin{proposition}
  Soit $\varphi$ une fonction d'extraction. On a alors $\forall n \in \N,\
  n ≤ \varphi(n)$.
\end{proposition}
\begin{proof}
  Pour $n = 0$, on a $0 ≤ \varphi(0)$.

  Pour $n ≥ 0$ supposons que $n ≤ \varphi(n)$. Alors $n + 1 ≤ \varphi(n) +
  1$ et comme $\varphi$ est strictement croissante, alors $\varphi(n+1) >
  \varphi(n)$, d'où $\varphi(n+1) ≥ \varphi(n) + 1$ et finalement $n + 1 ≤
  \varphi(n+1)$.
\end{proof}

\subsection{Opération sur les suites réelles}

\begin{definition}
  Soient $u$ et $v$ deux suites réelles. On définit
  \begin{itemize}
    \item $u+v$ comme la suite de terme général $u_n + v_n$.
    \item $uv$ comme la suite de terme général $u_n \times v_n$.
  \end{itemize}
\end{definition}

\begin{proposition}
  $\brk*{\R^{\N}, +, \times}$ est une $\R$-algèbre, c'est-à-dire que
  $\brk*{\R^{\N}, +}$ est un groupe commutatif et que
  $\brk*{\R^{\N},\times}$ est associative et distributive sur $+$.

  Les $\K$-algèbres seront étudiées plus loin, au chapitre \ref{}.
\end{proposition}
\begin{proof}
  $\brk*{\R^{\N}, +}$ est un groupe commutatif de neutre la suite constante
  nulle $(0)_{\N}$ et d'opposé $-u$.

  De la même façon, on démontre les autres propriétés.
\end{proof}

\begin{remarque}
  Si $u v = 0_{\R^{\N}}$, alors $\forall n, \ u_n = 0 \vee v_n = 0$.
  L'anneau des suites n'est donc pas intègre, puisqu'il existe des diviseurs
  de 0.
\end{remarque}

\begin{definition}[suite minorée, majorée ou bornée]
  Une suite est dite \emph{minorée (resp. majorée ou bornée)} lorsque
  $\exists m \in \R$ (resp. $\exists M \in \R$ ou $\exists (m;M) \in \R^2$)
  tel que $\forall n \in \N,\ m ≤ u_n$ (resp. $u_n ≤ M$ ou $m ≤ u_n ≤ M$.)
\end{definition}

\begin{proposition}
  L'ensemble des suites minorées (resp. majorée ou bornée) forme une $\R$
  algèbre.
\end{proposition}
\begin{proof}
  La preuve est analogue à la précédente, en considérant l'$\inf$ des
  minorants comme minorants de l'ensemble des suites.
\end{proof}

\begin{remarque}
  D'autres chapitres viendront préciser ces points.
\end{remarque}

\subsection{Suites réelles et monotonie}

\begin{definition}[croissante]
  Une suite réelle $u_n$ est dite \emph{croissante} lorsque $\forall n \in
  \N, \ u_{n+1} ≥ u_n$.
\end{definition}

\begin{remarque}
  \begin{itemize}
    \item On déduit de cette définition la définition de décroissante, de
      strictement croissante et strictement décroissante.
    \item Par récurrence, on déduit que $\forall (n; m) \in \N^2,\ n ≥ m
      \implies u_n ≥ u_m$.
    \item En pratique, on étudie souvent par récurrence le signe de $u_{n+1}
      - u_n$ et si $u_n > 0$, on étudie la position par rapport à 1 de
      $\frac{u_{n+1}}{u_n}$.
  \end{itemize}
\end{remarque}

\begin{question}
  Soit $u$ définie par $u_n = \frac{n^4}{4^n}\ \forall n \in \N$.

  $u$ est elle monotone à partir d'un certain rang ?
\end{question}
\begin{solution}
  Pour $n ≥ 4, \ \frac{u_{n+1}}{u_n} = \frac14 \brk*{1 + \frac1n}^4$
\end{solution}

On peut compléter cette étude par le cas des suites définies par une
relation de récurrence.

\begin{proposition}
  Soit $f$ une fonction de $\R \to \R$. Soit $u$ une suite définie par $u_0$
  et $\forall n \in \N,\ u_{n+1} = f(u_n)$.

  Si $f$ est croissante, alors $u$ est monotone. Le sens de la suite est
  donné par le signe de $u_1 - u_0$.
\end{proposition}
\begin{proof}
  Si $f$ est croissante, on va par exemple montrer que $u$ est croissante.

  Soit $P_n$ la proposition « $u_n ≤ u_{n+1}$ ».

  $P_0$ est la proposition $u_0 ≤ u_1$, équivalente à $u_1 - u_0 ≥ 0$. Donc
  la récurrence est initialisée.

  Soit $n$ un entier naturel, supposons $P_n$. Comme $f$ est croissante, on
  en déduit $u_{n+1} ≤ u_{n+2}$ et donc la proposition est héréditaire, ce
  qui achève la récurrence.
\end{proof}

\begin{exemple}
  La suite définie par $u_0= -\frac32$ et $\forall n \in , \ u_{n+1} = 1 -
  \frac2n$ est croissante.
\end{exemple}

\begin{proposition}
  Soit $f$ une fonction de $\R \to \R$. Soit $u$ une suite définie par $u_0$
  et $\forall n \in \N,\ u_{n+1} = f(u_n)$.

  Si $f$ est décroissante, alors les suites extraites de termes généraux
  $u_{2n}$ et $u_{2n+1}$ sont monotones et de sens contraires.
\end{proposition}
\begin{proof}
  On pose $v_n = u_{2n}$ et $w_n = u_{2n + 1}$. $v$ est définie par $\left\{
  \begin{array}{l} v_0 = u_0 \\ \forall n \in \N,\ v_n{n+1} = (f \circ
  f)(v_n) \end{array} \right.$ et $w$ par $\left\{
  \begin{array}{l} w_0 = w_1 \\ \forall n \in \N,\ w_n{n+1} = (f \circ
  f)(w_n) \end{array} \right.$

  $f$ étant décroissante, $f \circ f$ est croissante et les suites $v$ et
  $w$ sont donc monotones. Quitte à changer les rôles supposons $v$
  croissante. On a alors $\forall n \in \N,\  v_n ≤ v_{n+1}$ et donc $f(v_n)
  ≥ f(v_{n+1})$ d'où $f(u_{2n}) ≥ f(u_{2n+2})$ c'est-à-dire $u_{2n+1} ≥
  u_{2n + 3}$ ou encore $w_n ≥ w_{n+1}$.
\end{proof}

\subsection{Rappels sur les suites usuelles}

\subsubsection{Suite arithmétique}

\begin{definition}[arithmétique]
  On dit qu'une suite est \emph{arithmétique} lorsqu'il existe $r \in \R$
  tel que $\forall n \in \N,\ u_{n+1} = u_n + r$.
\end{definition}

\begin{proposition}
  La suite $u$ est arithmétique si et seulement si $\exists r \in \R$ tel
  que $\forall n \in \N, \ u_n = u_0 + nr$.
\end{proposition}
\begin{proof}
  Par récurrence pour le sens directe et la réciproque est évidente.
\end{proof}

\begin{proposition}
  La suite $u$ est arithmétique si et seulement si $\forall n \in \N, \
  u_{n+1} = \frac12 \brk*{u_n + u_{n+2}}$
\end{proposition}
\begin{proof}
  On montre par récurrence que $\forall n \in \N, u_{n+2} - u_{n+1} =
  u_{n+1} - u_n$.
\end{proof}

\begin{remarque} Si $u$ est arithmétique de premier terme $u_0$ et de raison
  $r$, on a $ \sum_{k=0}^n u_k = \frac{u_0 + u_n}{2} (n+1) = (n+1) u_0 +
  \frac{n(n+1)}2 r$.
\end{remarque}

\subsubsection{Suite géométrique}

\begin{definition}[géométrique]
  On dit que $u$ est \emph{géométrique} lorsqu'il existe $q \in \R\ \forall
  n \in \N,\ u_{n+1} = q u_n$.
\end{definition}

\begin{proposition}
  $u$ est géométrique si et seulement si $\forall n \in \N, \ u_{n+1} = q^n
  u_0$.
\end{proposition}
\begin{proof}
  Par récurrence.
\end{proof}

\begin{proposition}
  $u$ est géométrique si et seulement si $\forall n \in \N,\  u_{n+1}^2 =
  u_n u_{n+2}$.
\end{proposition}
\begin{proof}
  Si $u$ est géométrique, alors $u_n u_{n+2} = u_0 q^n u_0 q^{n+2} = u_0^2
  q^{n+1} q^{n+1}$.

  Inversement, si $\forall n \in \N,\  u_{n+1}^2 = u_n u_{n+2}$ alors, si
  $u_n = 0$, alors $u_{n+1} = 0$ et donc $\forall n \in \N, u_n = 0$ par
  récurrence et donc soit $u_0 = 0$, soit $q =0$.\\ Si $u_n ≠ 0$ alors
  $u_{n+1} ≠ 0$ et on a $\frac{u_{n+2}}{u_{n+1}} = \frac{u_{n+1}}{u_n}$ et
  ce pour tout $n$. Il existe donc $q \in \R$, tel que $\frac{u_{n+1}}{u_n}
  = q$.
\end{proof}

\begin{remarque}
  Si $u$ est géométrique de premier terme $u_0$ et de raison $q$, alors si
  $q = 1,\ \sum_{k=0}^n u_k = n + 1$, sinon $\sum_{k=0}^n u_k = u_0 \frac{1
  - q^{n+1}}{1 - q}$.
\end{remarque}

\subsubsection{Suite arithmético-géométrique}

\begin{definition}[suite arithmético-géométrique]
  On dit qu'une suite $u$ est \emph{arithmético-géométrique} lorsque
  $\exists (a;b) \in \R^2$, tel que $\forall n \in \N, \ u_{n+1} = au_n +
  b$.
\end{definition}

L'étude d'une telle suite se ramène au cas géométrique : soit $\alpha$ le
point fixe de l'application $f \colon x \mapsto ax + b$. On pose $v_n = u_n
- \alpha$ et on vérifie que $v_n$ est géométrique.

\begin{question}
  Soit $u$ une suite arithmético-géométrique telle que $\forall n \in \N,\
  u_{n+1} = au_n +b$.

  Montrer que $u_n = \brk*{u_0 - \frac{b}{1 - a}}a^n + \frac{b}{1 - a}$.
\end{question}

\subsubsection{Suite à récurrence linéaire double}

\begin{definition}[récurrence linéaire double]
  $u$ est dite \emph{récurrente linéaire double} lorque $\exists (a;b) \in
  \R^2,\ u_{n+2} = a u_{n+1} + bu_n$, $u_0$ et $u_1$ étant fixés.
\end{definition}

\begin{exemple}
  Revoir la suite de Fibonacci.
\end{exemple}

Lorsqu'on veut exprimer $u_n$ en fonction de $n$, on commence par chercher
la suite sous la forme d'une suite géométrique de raison non nulle.

On obtient donc la condition suivante sur $q$ qui doit être solution de $q^2
- aq +b =0$.

\begin{description}
  \item[$a^2 + 4b ≠ 0$]

    Le polynôme en $q$ possède deux solutions distinctes complexes $\lambda$
    et $\mu$ et on peut poser $a = \lambda + \mu$ et $b = -\lambda\mu$

    La relation devient $u_{n+2} = \brk{\lambda + \mu} u_{n+1} - \lambda\mu
    u_n$ et même en factorisant $u_{n+2} - \lambda u_{n+1} = \mu \brk{
    u_{n+1} - \lambda u_n}$ et pareil en échangeant le rôle de $\lambda$ et
    $\mu$. Ainsi, $u_{n+2} - \lambda u_{n+1} = \mu^{n+1} \brk{u_1 - \lambda
    u_0}$ et en combinant les deux expressions, on obtient que $\brk{\lambda
    - \mu} u_{n+1} = (u_1 - \mu u_0) \lambda^{n+1} - (u_1 - \lambda u_0)
    \mu^{n+1}$.

    On montre ainsi qu'il existe deux complexes $A$ et $B$ tels que $u_n =
    A\lambda^n + B \mu^n$.
  \item[$a^2 + 4b = 0$]

    Dans ce cas, on a une racine double $\lambda$ et on peut poser $a = 2
    \lambda$ et $b = - \lambda^2$. La relation de récurrence devient alors
    $u_{n+2} - 2 \lambda u_{n+1} + \lambda^2 u_n = 0$ et comme $b ≠ 0$, 0
    n'est pas racine. En posant $v_n = \frac{u_n}{\lambda^n}$, on se ramène
    à $v_n = An + B$ et donc $u_n = (An + B) \lambda^n$.
\end{description}

\section{Limite d'une suite}

\subsection{Définition}

\begin{definition}
  Soient $u$ une suite à termes réels et $\ell$ un réel. On dit que $\ell$
  est une limite de la suite lorsque \[ \forall \varepsilon > 0, \ \exists
    n_0 \in \N,\ \forall n \in \N,\ n ≥ n_0 \implies \abs{u_n - \ell } ≤
  \varepsilon . \]
\end{definition}

\begin{proposition}
  Si une suite $u \in \R^{\N}$ admet une limite $\ell$, alors celle-ci est
  unique.

  On pourra noter alors $\ell = \lim_{n \to +\infty} u_n = \lim
  u$ et dire que la suite est convergente.
\end{proposition}
\begin{proof}
  Supposons que $\ell_1$ et $\ell_2$ soient deux limites de la suite $u,\
  \ell_1 ≠ \ell_2$. Alors $\forall \varepsilon > 0, \ \exists n_1 \in \N,\
  \forall n \in \N,\ n ≥ n_1 \implies \abs{u_n - \ell_1 } ≤ \varepsilon$ et
  $\forall \varepsilon > 0, \ \exists n_2 \in \N,\ \forall n \in \N,\ n ≥
  n_2 \implies \abs{u_n - \ell_2 } ≤ \varepsilon$

  Prenons $\varepsilon = \frac13 \abs{\ell_1 - \ell_2}$. On a alors, pour $n
  ≥ \max(n_1,n_2),\ \abs{u_n - \ell_1} ≤ \frac13 \abs{\ell_1 - \ell_2}
  \wedge \abs{u_n - \ell_2} ≤ \frac13 \abs{\ell_1 - \ell_2}$ et donc on
  $\ell_1 - \frac13 \abs{\ell_1 - \ell_2} ≤ u_n ≤ \ell_1 +\frac13
  \abs{\ell_1 - \ell_2}$ et de même avec $\ell_2$.

  Supposons $\ell_1 > \ell_2$. On a donc $\frac{2\ell_1 + \ell_2}3 ≤ u_n ≤
  \frac{4\ell_1 - \ell_2}3$ et $\frac{-\ell_1 + 4\ell_2}3 ≤ u_n ≤
  \frac{\ell_1 + 2\ell_2}3$.

  En particulier $\frac{2\ell_1 + \ell_2}3 ≤ u_n$ et $\frac{-\ell_1 +
  4\ell_2}3 ≤ u_n$, qui donne $\frac{\ell_1 - \ell_2}3 ≤ 0$, ce qui est
  absurde.

  Donc $\ell_1 = \ell_2$.
\end{proof}

\begin{question}
  \begin{enumerate}
    \item Montrer que la suite $u$ définie pour tout $n$ par $u_n =
      \frac{n+1}{n+2}$ admet 1 pour limite.
    \item Montrer que la suite $u$ définie pour tout $n$ par $u_n =
      \frac{n+1}{n^2 + 1}$ admet 0 pour limite.
    \item Montrer que la suite $u$ définie pour tout $n$ par $u_n = (-1)^n$
      n'a pas de limite.
  \end{enumerate}
\end{question}
\begin{solution}
  \begin{enumerate}
    \item Soit $\varepsilon > 0$. L'inégalité $\abs{u_n - 1} ≤ \varepsilon
      \iff \frac1{n+2} ≤ \varepsilon \iff n ≥ \frac1{\varepsilon} - 2$. On
      peut donc prendre $n_0 = \max\brk{0; \lfloor \frac1{\varepsilon} - 2
      \rfloor + 1}$
    \item Soit $\varepsilon > 0$. $\abs{u_n - 1} ≤ \varepsilon \iff
      \frac{n+1}{n^2 + 1} ≤ \varepsilon$. Cette dernière inégalité est vraie
      si $\frac{n+1}{n^2} ≤ \varepsilon$ si $n ≥ 1$. Celle-ci est égalgement
      vraie si $\frac{n+n}{n^2} ≤ \varepsilon$ si $n ≥ 1$. On obtient donc
      que $n ≥ 1 \vee n ≥ \frac{2}{\varepsilon}$ et on peut donc poser $n_0
      = \max\brk{1, \lfloor \frac2{\varepsilon} \rfloor +1}$
    \item Pour $\varepsilon = \frac12$, on doit avoir, au moins à partir
      d'un certain rang $\ell - \frac12 ≤ u_n ≤ \ell + \frac12$.

      On a donc \begin{align*}
         \ell - \frac12 & ≤ -1 ≤ l + \frac12 \\
         \ell - \frac12 & ≤ 1  ≤ l + \frac12 \\
      \end{align*} et donc $-1 ≤ 2 ≤ 1$, ce qui est faux.
  \end{enumerate}
\end{solution}

\begin{question}
  Soit $u_n$ le terme général d'une suite d'entiers. On suppose que $u$ est
  convergente. Montrer qu'à partir d'un certain rang, $u$ est stationnaire.
\end{question}
\begin{solution}
  $u$ est convergente donc, $\forall \varepsilon > 0,\ \exists n_0 \in \N,\
  \forall n \in \N, \ n ≥ n_0 \implies \abs{u_n - \ell} ≤ \varepsilon$.

  Si $\ell \notin \N$, alors en posant $\varepsilon = \frac12 \min \brk{\ell
  - \lfloor \ell \rfloor ; \lfloor \ell \rfloor ; 1 - \ell}$, on a
  $\intv{\ell - \varepsilon}{\ell + \varepsilon} \cap \N = ø$. Donc $\ell
  \in \N$.

  De façon générale, l'intervalle $\intv{\ell - \varepsilon}{\ell +
  \varepsilon}$ est réduit à un entier dès que $\varepsilon = \frac12$. Donc
  à partir d'un certain rang $u_n = \ell$.
\end{solution}

\begin{question}
  Soit $(u_n)_{n\in \N*}$ une suite et construisons la suite $v$ de terme
  général $v_n = \frac1n \sum_{k=1}^n u_k$.

  Montrer que si $u$ converge vers $\ell$, alors $v$ converge vers $\ell$.
\end{question}
\begin{solution}
  Observons que pour tout entier naturel non nul,
  \begin{align*}
    v_n - \ell & = \frac1n \sum_{k=1}^n u_k - \frac1n (n\times \ell) \\
               & = \frac1n \sum_{k=1}^n \brk{u_k - \ell}
  \end{align*}
  On peut donc étudier le seul cas $\ell =0$.

  Soit $\varepsilon > 0. \abs{v_n} ≤ \varepsilon \iff \abs{ \frac1n
  \sum_{k=1}^n u_k} ≤ \varepsilon$. Ce qui est vrai si $\frac1n \sum_{k=1}^n
  \abs{u_k} ≤ \varepsilon$.

  Or il existe un rang $n_0$ tel que $\forall p \in \N, \ p ≥ n_0 \implies
  \abs{u_k} ≤ \frac{\varepsilon}2$ et donc pour $n ≥ n_0,\ \frac1n
  \sum_{k=1}^n \abs{u_k} ≤ \frac1n \sum_{k=1}^{n_0-1} \abs{u_k} + \frac{n -
  n_0 +1}n \frac{\varepsilon}2$.

  On pose $K = \sum_{k=1}^{n_0-1} \abs{u_k}$ et on remarque que $\frac{n -
  n_0 +1}n \frac{\varepsilon}2 ≤ \frac{\varepsilon}2$. On a donc une
  inégalité de la forme $\frac{K}n + \frac{\varepsilon}2 ≤ \varepsilon$, qui
  est donc vérifiée dès que $n ≥ \frac{2K}{\varepsilon}$. On peut donc poser
  $n_1 = \lfloor \frac{2K}{\varepsilon} \rfloor + 1$ et pour tout $n ≥ \max
  \brk{n_0,n_1} $, alors $\frac1n \sum_{k=1}^n \abs{u_k} ≤ \varepsilon$ et
  donc $\abs{v_n} ≤ \varepsilon$.
\end{solution}

\subsection{Propriétés}

\begin{proposition}
  Toute suite convergente de réels est bornée. De plus si $u \in \R^{\N}$
  est convergente vérifiant $\forall n \in \N,\ u_n < M$, alors $\lim u ≤
  M$.
\end{proposition}
\begin{proof}
  Soit $u$ une suite convergente. Notons $\ell$ sa limite. Prenons
  $\varepsilon = 1$ par exemple. Alors $\exists n_0 \in \N,\ \forall n \in
  \N,\ n ≥ n_0 \implies \abs{u_n - \ell} ≤ \varepsilon$, ce qui peut
  s'écrire $\ell - 1 ≤ u_n ≤ \ell + 1$.

  Ainsi $\ell + 1$ est un majorant de $\{ u_n \mid n ≥ n_0 \}$ et $\{ u_n
  \mid n < n_0 \}$ est une partie finie de $\R$ donc majorée. On peut donc
  prendre $\max \brk{\ell + 1 ; \max \{ u_n\mid n < n_0 \} }$ comme
  majorant.

  On peut mener un raisonnement analogue sur le minorant et donc la suite
  $u$ est bornée.

  Soit $u$ telle que $\lim u = \ell$ et $\forall n \in \N,\ u_n < M$.
  Supposons que $M > \ell$. Prenons alors $\varepsilon = \frac12 (\ell -
  M)$. Alors, à partir d'un certain rang, on a $\ell - \varepsilon ≤ u_n
  \ell + \varepsilon \implies \ell + \frac{M}2 ≤ u_n$. Comme $\ell > M$, on
  a $\ell + \frac{M}2 > M$ et donc $M ≤ u_n$, ce qui est impossible.

  On en déduit que $\ell ≤ M$.
\end{proof}

\begin{proposition}
  Si $u$ converge vers $\ell$ et $\ell > 0$, alors, $u$ est à termes
  strictement positifs à partir d'un certain rang.
\end{proposition}
\begin{proof}
  Pour $\varepsilon = \frac{\ell}2,\ \exists n_0 \in \N, \ n ≥ n_0 \implies
  \ell - \frac{\ell}2 = \frac{\ell}2 ≤ u_n ≤ \ell + \frac{\ell}2$. Or
  $\frac{\ell}2 > 0$ d'où pour $n ≥ n_0, u_n > 0$.
\end{proof}

\begin{proposition}[comparaison et
  encadrement]\label{01_suites_numeriques:prop:comparaison}
  Si, à partir d'un certain rang, $v_n ≤ u_n ≤ w_n$ et $v$ et $w$
  convvergent vers la même limite $\ell$, alors $u$ convergent vers la même
  limite $\ell$.
\end{proposition}
\begin{proof}
  Les suites $(v_n - \ell)_{n\in \N}$ et $(w_n - \ell)_{n\in \N}$ convergent
  vers 0. Donc les suites $(\abs{v_n - \ell})_{n\in \N}$ et $(\abs{w_n -
  \ell})_{n\in \N}$ aussi.

  On a donc $\abs{u_n - \ell} ≤ \max \brk{ \abs{v_n - \ell} ; \abs{w_n -
  \ell} }$. Or $\forall \varepsilon > 0,\ \exists n_0 \in \N,\ n \in \N,\ n
  ≥ n_0 \implies \abs{v_n - \ell} ≤ \varepsilon \wedge \abs{w_n - \ell} ≤
  \varepsilon$.

  On en déduit donc la convergence de $u$ vers $\ell$.
\end{proof}

\subsection{Convergence des suites extraites}

\begin{proposition}
  Si $u$ converge vers $\ell$ alors toute suite extraite de $u$ converge et
  ademet la même limite.
\end{proposition}
\begin{proof}
  La fonction d'extraction $\varphi$ est une fonction strictement
  croissante et en particulier $\varphi(n) ≥ n$. La condition $n ≥ n_0$
  entraîne donc $\varphi(n) ≥ n_0$ et donc si pour tout $\varepsilon >0$, il
  existe un rang $n_0$ à partir duquel $\abs{u_n - \ell} ≤ \varepsilon$ ceci
  est vrai pour $\abs{u_{\varphi(n)} - \ell} ≤ \varepsilon$.

  $u \circ \varphi$ converge donc vers $\ell$.
\end{proof}

\begin{remarque}
  La contraposée de cette proposition permet de démontrer qu'une suite est
  divergente.
\end{remarque}

\begin{exemple}
  $u$ de terme général $u_n = (-1)^n$. $(u_{2n})_{n\in\N}$ converge vers 1
  et $(u_{2n+1})_{n\in\N}$ converge vers -1, donc $u$ diverge.
\end{exemple}

\begin{question}
  Soient $u$ une suite, $\varphi$ et $\psi$ deux fonctions d'extraction
  telles que :
  \begin{itemize}
    \item $u \circ \varphi$ et $u \circ \psi$ convergent vers la même limite
      $\ell$ ;
    \item $\varphi(\N) \cup \psi(\N) = \N$.
  \end{itemize}
  Montrer que $u$ converge vers $\ell$.
\end{question}
\begin{solution}
  La première condition impose que $\forall \varepsilon > 0,\ \exists n_1
  \in \N \wedge \exists n_2 \in \N \mid \forall n \in \N,\ n ≥ n_ \vee n ≥
  n_1 \implies \abs{u_{\varphi(n)} - \ell} ≤ \varepsilon \wedge
  \abs{u_{\psi(n)} - \ell} ≤ \varepsilon$. On peut poser $n_0 =
  \max(n_1,n_2)$ et $\abs{u_n - \ell} ≤ \varepsilon \iff
  \abs{u_{\varphi(n')} - \ell} ≤ \varepsilon \wedge \abs{u_{\psi(n")} -
  \ell} ≤ \varepsilon$, avec $n' ≥ n_0$ et $n" ≥ n_0$.

  Pour ce $n_0$ fixé, soit $n'_1 = \min \{ \varphi(n') \mid n' ≥ n_0 \}$ et
  $n"_1 = \min \{ \psi(n") \mid n" ≥ n_0 \}$. Alors pour $n ≥ \max(n'_1 ;
  n"_1)$, on a $\abs{u_n - \ell} ≤ \varepsilon$.
\end{solution}

\subsection{Opérations sur les suites convergentes}

\subsubsection{Cas des suites de limites nulles}

\begin{theoreme}
  Soient $u$ et $v$ deux suites.
  \begin{enumerate}[labelsep=0.5pc,label=(\roman*)]
    \item Si $u$ et $v$ sont de limites nulles, alors $u+v$ converge vers 0.
    \item Si $\lim u = 0$ et $v$ bornée, alors $uv$ converge vers 0.
    \item Si $\lim u = 0$, alors $\forall \lambda \in \R,\ \lim \lambda u
      =0$.
  \end{enumerate}
\end{theoreme}
\begin{proof}
  \begin{enumerate}[labelsep=0.5pc,label=(\roman*)]
    \item Soit $\varepsilon > 0$, $\abs{u_n + v_n} ≤ \varepsilon \Leftarrow
      \abs{u_n} + \abs{v_n} ≤ \varepsilon$. Il suffit donc que $\abs{u_n} ≤
      \frac{\varepsilon}2$ et $\abs{v_n} ≤ \frac{\varepsilon}2$.

      Si la première inégalité est vraie pour $n ≥ n_1$ et la seconde pour
      $n ≥ n_2$, en prenant $n_0 = \max(n_1 ; n_2)$ alors on a la premiere
      inégalité pour $n ≥ n_0$ et donc la limite attendue.
    \item Soit $\varepsilon > 0$. $\abs{u_n v_n} ≤ \varepsilon$ est vraie si
      $\abs{u_n M} ≤ \varepsilon \Leftarrow \abs{u_n} M ≤ \varepsilon
      \Leftarrow \abs{u_n} ≤ \frac{\varepsilon}M$.

      Or $\lim u = 0$, donc il existe un rang $n_0$ qui convient. On a donc
      pour ce rang $n_0$ la limite de la suite $uv$.
    \item On se ramène au cas précédent en considérant la suite $v = (
      \lambda)_{n\in \N}$.
  \end{enumerate}
\end{proof}

\begin{corollaire}
  \begin{enumerate}[labelsep=0.5pc,label=(\roman*)]
    \item Si $\lim u = \ell_1$ et $\lim v = \ell_2$, alors $\lim u + v =
      \ell_1 + \ell_2$.
    \item Si $\lim u = \ell_1$ et $\lim v = \ell_2$, alors $\lim uv =
      \ell_1\ell_2$.
    \item Si $\lim u = \ell_1$, alors $\lim \lambda u = \lambda \ell_1$.
  \end{enumerate}
\end{corollaire}
\begin{proof}
  \begin{enumerate}[labelsep=0.5pc,label=(\roman*)]
    \item $(u_n - \ell_1)_{n\in \N}$ et $(v_n - \ell_2)_{n\in \N}$
      convergent vers 0, donc $(u_n - \ell_1 + v_n - \ell_2)_{n\in \N}$
      converge vers 0 et on en déduit que $u + v - (\ell_1 + \ell_2)$
      converge vers 0 et donc $u + v$ converge vers $\ell_1 + \ell_2$.
    \item Soit $n \in \N$, $u_nv_n - \ell_1\ell_2 = (u_n - \ell_1)v_n +
      \ell_1(v_n - \ell_2)$.

      $\lim v - \ell_2 = 0$ donc $\lim \ell_1(v_n - \ell_2) = 0$

      $\lim v_n = l_2 \implies v$ bornée donc $\lim (u_n - \ell_1)v_n = 0$
      et donc $\lim uv - \ell_1\ell_2 = 0$ et finalement $\lim uv =
      \ell_1\ell_2$.
    \item On se ramène au cas précédent.
  \end{enumerate}
\end{proof}

\begin{proposition}
  Soit $u$ une suite convergente telle que $\ell = \lim u ≠ 0$. Alors la
  suite $\frac1u$ est convergente, de limite $\frac1{\ell}$.
\end{proposition}
\begin{proof}
  Si $\ell ≠ 0$ par exemple $\ell > 0$.

  $\lim u = \ell$ entraîne qu'à partir d'un certain rang, $u_n > 0$ et même
  $u_n > \frac{l}2$.

  Pour $n ≥ n_0$, $\frac1{u_n} - \frac1{\ell} = \frac{\ell - u_n}{\ell u_n}$
  d'où $\abs{\frac1{u_n} - \frac1{\ell}} ≤ \varepsilon $ est vrai si $\abs{
  \frac{\ell - u_n}{\ell u_n}} ≤ \varepsilon$ vrai si $\frac{\abs{u_n -
  \ell}}{\frac{\ell^2}2} ≤ \varepsilon \iff \abs{u_n - \ell} ≤ \varepsilon
  \frac{\ell^2}2 = \varepsilon'$.

  Or cette dernière affirmation est vraie si $n ≥ n'_0$, donc en prenant $n
  ≥ \max(n_0,n'_0)$ on obtient l'encadrement et le résultat.
\end{proof}

\subsection{Divergence vers $+\infty$ (ou $-\infty$)}

\begin{definition}
  On dit que $u$ diverge vers $+\infty$ (resp. $-\infty$) lorsque $\forall A
  \in \R,\  \exists n_0 \in \N,\ n \in \N,\ n ≥ n_0 \implies u_n ≥ A \
  (\text{resp. } u_n ≤ A)$.
\end{definition}

\begin{remarque}
  Les théorèmes usuels s'étendent à ce cas là, à l'exception des formes
  indéterminées :
  \begin{itemize}
    \item $\infty - \infty$
    \item $\frac{\infty}{\infty}$
    \item $\frac00$
    \item $0 \times \infty$
    \item $1^{\infty}$
    \item $0^0$
    \item $(+\infty)^0$
  \end{itemize}
\end{remarque}

\section{Comparaison des suites}

\subsection{Domination}

\begin{definition}[domination]
  On dit que la suite de terme général $v_n$ \emph{domine} la suite de terme
  général $u_n$ lorsque $\exists M \in \R_+^*,\ \exists n_0 \in \N,\ \forall
  n \in \N,\ n ≥ n_0 \implies \abs{u_n} ≤ M\abs{v_n}$.
\end{definition}

\begin{remarque}
  On note $u_n = \GO{v_n}$ d'après la notation de Landau (1877-1938)
\end{remarque}

\begin{exemple}
  $n = \GO{n^2}$ ou $3n + 2 = \GO{\frac{n}2}$
\end{exemple}

\begin{proposition}
  \begin{enumerate}[labelsep=0.5pc,label=(\roman*)]
    \item Si $u_n = \GO{v_n}$ et $\lim v_n = 0$, alors $\lim u = 0$.
    \item Si $v$ est à termes tous non nuls, $u_n = \GO{v_n}$ est équivalent
      à «la suite de terme général $\frac{u_n}{v_n}$ est bornée.
    \item Si $u_n = \GO{v_n}$ et $u'_n = \GO{v_n}$, alors $u_n + u'_n =
      \GO{v_n}$.
  \end{enumerate}
\end{proposition}
\begin{proof}
  Le premier point se montre avec la proposition
  \ref{01_suites_numeriques:prop:comparaison} dans le cas de la suite des
  valeurs absolues, et on passe à la suite par l'absurde.

  Le second point est une relecture de la définition.

  Le troisième point se montre avec la définition en prenant $\max(M;M')$.
\end{proof}

\begin{theoreme}
  Si (éventuellement à partir d'un certain rang) $u$ et $v$ sont à termes
  strictement positifs et si, à partir d'un certain rang,
  $\frac{u_{n+1}}{u_n} ≤ \frac{v_{n+1}}{v_n}$, alors $u_n = \GO{v_n}$.
\end{theoreme}
\begin{proof}
  On suppose que $\frac{u_{n+1}}{u_n} ≤ \frac{v_{n+1}}{v_n}$, alors
  $ \prod_{k=0}^{p-1} \frac{u_{n+k+1}}{u_{n+k} ≤
  \prod_{k=0}^{p-1} \frac{v_{n+k+1}}{v_{n+k}}}$ et donc
  $\frac{u_{n_0+p}}{u_{n_0}} ≤ \frac{v_{n_0+p}}{v_{n_0}}$

  On en tire que $u_{n_0 + p} ≤ \frac{u_{n_0}}{v_{n_0}} v_{n_0+p}$ et donc
  pour $n ≥ n_0 + p$, on a bien un majorant $\frac{u_{n_0}}{v_{n_0}}$, ce
  qui achève la démonstration.
\end{proof}

\begin{question}
  Démontrer que $n^2 = \GO{2^n}$.
\end{question}
\begin{solution}
  $\frac{(n+1)^2}{n^2} = \brk*{1 + \frac1n}^2 = 1 + \frac2n + \frac1{n^2}$.

  $\frac{2^{n+1}}{2^n} = 2$.

  Or pour $n ≥ 4,\ \frac2n + \frac1{n^2} ≤ \frac12 + \frac1{16} ≤ 1$.

  Donc $u_n = \GO{v_n}$
\end{solution}

\begin{remarque}
  \begin{itemize}
    \item $u_n = \GO{1}$ signifie $u$ bornée
    \item $\underbrace{u_n = \GO{0}}_{\text{rare}}$ signifie $u$ nulle à
      partir d'un certain rang.
  \end{itemize}
\end{remarque}

\subsection{Prépondérance}

\begin{definition}
  On dit que la suite $u$ \emph{négligeable} devant la suite $v$ (ou $v$
  \emph{prépondérante} devant la suite $u$) lorsque $\forall \varepsilon >
  0, \ \exists n_0 \in \N,\ \forall n \in \N,\ n ≥ n_0 \implies \abs{u_n} ≤
  \varepsilon \abs{v_n}$.
\end{definition}

\begin{remarque} Il est équivalent d'écrire qu'il existe une suite $\alpha$
  de limite nulle telle que $\forall n \in \N, u_n = \alpha_n v_n$, ou
  encore, si (à partir d'un certain rang) $u$ et $v$ sont à termes non nuls,
  alors $\lim_{n\to+\infty} \frac{u_n}{v_n} = 0$.

  On note $u_n = \po{v_n}$ ou $u_n \ll v_n$.
\end{remarque}

\begin{exemple}
  $n = \po{n^2}$ ; $\frac1{n^2} = \po{\frac1n}$ ; si $\alpha < \beta,\
  n^{\alpha} = \po{n^{\beta}}$ ; $(\ln n)^{\beta} = \po{n^{\alpha}} \
  (\alpha > 0)$.
\end{exemple}

\begin{question}
  \begin{enumerate}
    \item Montrer que si $k > 1$ alors $n^{\alpha} = \po{k^n}$.
    \item Montrer que si $k > 1$, alors $k^n = \po{n!}$
  \end{enumerate}
\end{question}
\begin{solution}
  On considère $t_n = \frac{n^{\alpha}}{n^k}$.

  $\frac{t_{n+1}}{t_n} = \brk*{1 + \frac1n}^{\alpha} \frac1k$. Pour $n$
  «assez grand», on a $\brk*{1 + \frac1n}^{\alpha} \frac1k ≤ K < 1$ car
  $\lim \brk*{1 + \frac1n}^{\alpha} = 1$.

  Finalement, il vient que pour $n$ «assez grand», $\frac{t_{n_0+p}}
  {t_{n_0}} ≤ K^p < 1$ et donc $t_{n_0 + p} ≤ t_{n_0} K^p$ qui est de limite
  nulle. Donc $\lim t = 0$ et on a le résultat.

  De la même façon, posons $u_n = \frac{k^n}{n!}$. $\frac{u_{n+1}}{u_n} =
  \frac{k}{n+1}$ qui tend vers 0. D'autre part, à partir d'un certain rang,
  $\frac{u_{n+1}}{u_n} ≤ \frac12$, d'où $u_n ≤ u_{n_0} \frac1{2^{n - n_0}}$
  et donc $\lim u = 0$, ce qui achève la démonstration.
\end{solution}

\begin{remarque}
  \begin{itemize}
    \item On montre que $n! = \po{n^n}$.
    \item $u_n = \po{1}$ signifie que $u$ converge vers 0.
    \item $\underbrace{u_n = \po{0}}_{\text{rare}}$ signifie que $u$ est
      nulle à partir d'un certain rang.
  \end{itemize}
\end{remarque}

Attention : si $u_n = \po{v_n}$ alors $u_n = \GO{v_n}$. La réciproque est
fausse. De plus, $\po{v_n} + \po{v_n} = \po{v_n}$ mais $\po{v_n} - \po{v_n}
= \po{v_n}$.

\subsection{Équivalence}

\begin{definition}
  On dit que $u$ est \emph{équivalente à} $v$ et note $u \sim v$ lorsqu'il
  existe une suite $\alpha$ de limite 1 telle qu'à partir d'un certain rang,
  $u_n = \alpha_n v_n$.
\end{definition}

\begin{remarque}
  Si $v$ est à termes non nuls à partir d'un certain rang, il revient au
  même d'écrire $\lim \frac{u}v = 1$.
\end{remarque}

\begin{exemple}
  Pour $(u_n)$ de \emph{limite nulle}, on a
  \begin{itemize}
    \item $\sin u_n \sim u_n$
    \item $\tan u_n \sim u_n$
    \item $\cos u_n - 1 \sim -\frac12 u_n^2$
    \item $(1 + u_n)^{\alpha} -1 \sim \alpha u_n$
    \item $\ln(1 + u_n) \sim u_n$
    \item $e^{u_n} - 1 \sim u_n$.
  \end{itemize}
\end{exemple}

\begin{proposition}
  L'équivalence est une relation réflexive, symétrique et transitive.
\end{proposition}
\begin{proposition}
  L'équivalence est compatible avec «$\times$», mais pas avec «$+$».
\end{proposition}

\begin{proposition}
  $u_n \sim v_n \iff u_n = v_n + \po{v_n}$.
\end{proposition}
\begin{proof}
  $\lim \frac{u}v = 1 \iff \lim \frac{u - v}v = 0$. On en déduit que $u_n \sim
  v_n \iff u_n - v_n = \po{v_n}$ et donc le résultat.

  À revoir en écrivant la première limite comme $\frac{u_n}{v_n} = 1 +
  \po{1}$.
\end{proof}

\section{Existence de la limite}

\subsection{Convergence monotone}

\begin{theoreme}[convergence monotone]
  \label{ana-01:thm:convergence_monotone}
  Soit $u \in \R^{\N}$ une suite réelle. Si $u$ est croissante majorée,
  alors elle converge. De plus, si $M$ est un majorant de $u$ et $\ell$ la
  limite, $\ell < M$.
\end{theoreme}
\begin{proof}
  $X = \brk[c]{u_n \mid n \in \N} \subset \R$. D'après la construction
  adoptée et les hypothèses, c'est une partie majorée non vide donc elle
  admet une borne supérieure $S = \sup X$. Montrons que $u$ converge vers
  $S$.

  $S$ est la borne supérieure de $X$, donc $\forall \varepsilon > 0,\ S -
  \varepsilon < u_n ≤ S < S + \varepsilon$. On a donc $\abs{S - u_n} <
  \varepsilon$ et donc a fortiori $\abs{S - u_n} ≤ \varepsilon$. Ce qui
  prouve que $\lim u = S$.
\end{proof}

\begin{corollaire}
  Toute suite décroissante minorée admet une limite.
\end{corollaire}
\begin{proof}
  On applique le théorème précédent à la suite $-u$.
\end{proof}

\begin{question}
  Montrer que la suite $u$ définie par $u_0 = 1$ et $u_{n+1} = \frac12 u_n +
  2$ est convergente.

  Montrer également que la suite $v$ définie par $v_0 = -2$ et $v_{n+1} =
  \sqrt{v_n +2}$ convergente.
\end{question}
\begin{solution}
  Soit $f \colon \R \to \R,\ x \mapsto \frac12 x + 2$. $f$ est croissante
  donc $u$ est monotone. $u_1 = 2$ et donc $u_1 - u_0 > 0$, donc $u$ est
  croissante.

  Si $u_n < 6$, alors $u_{n+1} < 3 + 2 < 6$. Par récurrence, on montre que 6
  est un majorant de $u$.

  $u$ converge et sa limite est au plus 6.

  $(u_{n+1})$ est une suite extraite de $u$, donc elle converge vers la même
  limite. Donc $\ell$ est solution de l'équation $\ell = \frac12 \ell + 2
  \iff \ell = 4$.

  Pour la suite $v$ on montre que la suite est croissante majorée par 4, et
  on trouve que la limite éventuelle est solution d'une équation du second
  degré. On élimine la solution $-1$ car la suite est croissante.
\end{solution}

\subsection{Suites adjacentes}

\begin{definition}[suites adjacentes]
  On dit que deux suites $u$ et $v$ sont adjacentes lorsque :
  \begin{enumerate}[label=(\roman*),labelsep=0.5pc]
    \item $u$ et $v$ sont monotones de sens contraire ;
    \item la suite de terme général $u_n - v_n$ a pour limite 0.
  \end{enumerate}
\end{definition}

\begin{exemple}
  $u$ est la suite des valeurs approchées par défaut de $\sqrt{2}$ et $v$ la
  suite des valeurs approchées par excès.
\end{exemple}

\begin{theoreme}\label{01_suites_numeriques:thm:suites_adjacentes}
  Deux suites adjacentes convergent et ont la même limite, qu'elles
  encadrent.
\end{theoreme}
\begin{proof}
  On suppose $u$ croissante et $v$ décroissante.

  Vérifions que $\forall n \in \N,\ \forall m \in \N u_n ≤ v_m$,
  c'est-à-dire que «$\exists n_0 \in \N, \exists m_0 \in \N, \ u_{n_0} >
  v_{m_0}$» est fausse. Cependant, si cette dernière proposition était
  vraie, on aurait pour $n ≥ \max(n_0; m_0),\ u_n ≥ u_{n_0} > v_{m_0} ≥ v_n$
  et donc en particulier ,pour $n≥ \max(n_0; m_0),\ u_n - v_n > 0$ ce qui
  est impossible.

  $u$ est majorée par $v_0$ et est croissante, donc elle converge.

  $v$ est minorée par $u_0$ et est décroissante, donc elle converge.

  On a donc $\lim u = \ell_1$, $\lim v = \ell_2$ et $\ell_1 ≤ \ell_2$.

  Or \begin{align*}
    \forall n \in \N , & u_n ≤ \ell_1 ≤ \ell_2 ≤ v_n      \\
                       & 0 ≤ \ell_2 - \ell_1 ≤ v_n - u_n  \\
                       & 0 ≤ \ell_2 - \ell_1 ≤ \lim v - u \\
  \end{align*}
  d'o* $\ell_1 = \ell_2$.
\end{proof}

\subsection{Segments emboîtés}

\begin{theoreme}[des segments emboîtés]
  Si $(I_n)_{n\in \N}$ est une suite de segments emboîtés (c'est-à-dire
  $\forall n \in \N,\  I_{n+1} \subset I_n$ et $\lim (\mathrm{diam} I_n) =
  0$), alors $\bigcap_{n\in \N} I_n = \{ \alpha \}$
\end{theoreme}
\begin{proof}
  Les segments $I_n$ sont de la forme $I_n = \intv{a_n}{b_n}$ où $a$ et $b$
  sont deux suites, $a$ croissante et $b$ décroissante. L'hypothèse $\lim
  (\mathrm{diam} I_n) = 0$ entraîne que $\lim b - a =0$. Le théorème
  \ref{01_suites_numeriques:thm:suites_adjacentes} permet de conclure
  qu'elles convergent toutes deux vers un même réel $\alpha$.

  Comme $\forall n \in \N,\ a_n ≤ \alpha ≤ b_n$, on a $\alpha \in
  \bigcap_{n\in \N} I_n$. S'il existe $\beta ≠ \alpha,\ \beta \in
  \bigcap_{n\in \N} I_n$, alors $\forall n \in \N,\ a_n ≤ \beta ≤ b_n$ et
  donc $b_n - a_n ≥ \abs{b - a} > 0$, ce qui est contradictoire avec $\lim
  b-a = 0$. Donc $\beta = \alpha$.
\end{proof}

\begin{exemple}
  $I_n = \intv*{0}{\frac1{n+1}} ; J_n = \intv*{\frac{-1}{n+1}}{\frac1{n+1}}$
\end{exemple}

\subsection{Théorème de Bolzano-Weierstrass}

Énoncé par Bolzano (1780-1848) et démontré par Weierstrass (1815-1897)

\begin{theoreme}[Bolzano-Weierstrass] \label{ana-01:thm:BolzanoWeierstrass}
  Soit $u$ une suite bornée de réel. Alors il existe une fonction
  d'extraction $\varphi$ telle que $u \circ \varphi$ converge.
\end{theoreme}
\begin{proof}
  Soit $(u_n)$ une suite bornée de réels. Il existe donc $a$ et $b$ deux
  réels tels que $\forall n \in \N,\ a ≤ u_n ≤ b$.

  Il existe une infinité d'indice «$n$» tels que $\intv*{a}{\frac{a+b}2}$ ou
  $\intv*{\frac{a+b}2}{b}$ contient une infinité de termes de la suite.

  Posons alors $a_0 = a$ et $b_0=b$. On construit les deux suites $(a_n)$ et
  $(b_n)$ en posant $a_{n+1} = a_n$ et $b_{n+1} = \frac{a_n+b_n}2$ si
  $\intv*{a_n}{\frac{a_n+b_n}2}$ contient une infinité de termes de la suite
  et $a_{n+1} = \frac{a_n+b_n}2$ et $b_{n+1} = b_n$ si
  $\intv*{\frac{a_n+b_n}2}{b_n}$ contient une infinité de termes de la suite.

  On définit ainsi une suite de segments emboîtés $\intv{a_n}{b_n}$. Ils
  définissent donc un réel $\ell$.

  Construisons désormais $\varphi$. On pose $\varphi(0) = 0$.
  $\intv{a_1}{b_1}$ contient une infinité de termes de la suite, donc il
  existe $n ≠ 0$ tel que $u_n \in \intv{a_1}{b_1}$. Posons alors $\varphi(1)
  = n$.

  Supposons connu $\varphi(k)$ pour $0 ≤ k ≤ n$ tel que $\varphi(k-1) <
  \varphi(k)$. Par construction, $\intv{a_{n+1}}{b_{n+1}}$ contient une
  infinité de termes de la suite.

  Pour $\varphi(n+1)$ prenons le plus petit indice $p$ tel que $p >
  \varphi(n)$ et $u_p \in \intv{a_{n+1}}{b_{n+1}}$. Alors $\varphi(n+1) >
  \varphi(n)$.

  On vient ainsi de construire $\varphi$ telle que $u \circ \varphi (n) \in
  \intv{a_n}{b_n}$.

  On a $\abs{u_{\varphi(n)} - \ell} ≤ b_n - a_n = \frac1{2^n}(b_0 - a_0)$
  d'où $\lim_{n\to + \infty} u_{\varphi(n)} - \ell = 0$ et donc le résultat.
\end{proof}
