\chapter{Fonctions de $\R^2$ vers $\R$}

\section{Topologie dans $\R^2$}

\subsection{Rappel de la notion de norme dans $\R^2$}

On rappelle la définition \ref{alg13:def:norme} d'une norme :\\
Soit $E$ un espace. On appelle \emph{norme} toute application $N \colon E
\to \R_+$ telle que
\begin{itemize}
  \item $\forall u \in E,\ N(u) = 0 \implies u = 0$ ;
  \item $\forall \lambda \in \R,\ \forall u \in E,\ N(\lambda u) =
    \abs{\lambda} N(u)$ ;
  \item $\forall (u,v) \in E^2,\ N(u+v) ≤ N(u) + N(v)$.
\end{itemize}

\begin{exemple} Soit $u = (x,y) \in \R^2$.
  \begin{itemize}
    \item $N_1(u) = \abs{x} + \abs{y}$
    \item $N_2(u) = \sqrt{x^2 + y^2}$
    \item $N_p(u) = \brk*{ \abs{x}^p + \abs{y}^p }^{\frac1p}$
    \item $N_{\infty} = \max \brk*{ \abs{x}, \abs{y}}$
  \end{itemize}
\end{exemple}

\begin{definition}
  Deux normes $N$ et $N'$ sont dites équivalentes lorsque $\exists
  (\lambda_1, \lambda_2) \in (\R_+^*)^2,\ \forall u \in \R^2,\ \lambda_1
  N(u) ≤ N'(u) ≤ \lambda_2 N(u)$.
\end{definition}

\begin{exemple}
  Dans $\R^2, N_1,\ N_2$ et $N_{\infty}$ sont équivalentes.
  \begin{itemize}
    \item $\forall u \in \R^2,\ N_{\infty}(u) ≤ N_1(u) ≤ 2 N_{\infty}(u)$.
    \item $\forall u \in \R^2,\ N_{\infty}(u) ≤ N_2(u) ≤ \sqrt{2} N_{\infty}(u)$.
    \item La relation étant une relation
      d'équivalence\footnote{démonstration aisée laissée au lecteur} on a
      $N_1$ équivalent à $N_2$.
  \end{itemize}
\end{exemple}

On rappelle aussi la définition \ref{alg13:def:distance} d'une distance :\\
On dit que $d \colon E×E \to \R_+$ est une \emph{distance} lorsque
\begin{itemize}
  \item $\forall (u,v) \in E^2,\ d(u,v) = 0 \implies u = v$
  \item $\forall (u,v) \in E^2,\ d(u,v) = d(v,u)$
  \item $\forall (u,v,w) \in E^3,\ d(u,v) ≤ d(u,w) + d(w,v)$.
\end{itemize}

\begin{definition}
  Deux distances $N$ et $N'$ sont dites équivalentes lorsque $\exists
  (\lambda_1, \lambda_2) \in (\R_+^*)^2,\ \forall (x,y) \in (\R^2)^2,\
  \lambda_1 d(x,y) ≤ d'(x,y) ≤ \lambda_2 d(x,y)$.
\end{definition}

\subsection{Boule ouverte, boule fermée}

$\R^2$ est désormais muni d'une distance $d$.

\begin{definition}[boule ouverte (resp. fermée)]
  On appelle \emph{boule ouverte} (resp. \emph{fermée}) de centre $u$ et de
  rayon $R$ l'ensemble des $v \in \R^2$ tels que $d(u,v) < R$ (resp. $d(u,v)
  ≤ R$.)

  On note $\B(u,R)$ (resp. $\overline{\B}(u,R)$.)
\end{definition}

\begin{question}
  Dessiner les boules $\B(0,1)$ pour $d_1, d_2$ et $d_{\infty}$
\end{question}
\begin{solution}
  ~\hfill
  \begin{tikzpicture}[>=latex]
    \draw [->] (-1.2,0) -- (1.2,0) ;
    \draw [->] (0,-1.2) -- (0,1.2) ;

    \draw (1,0) -- (0,1) -- (-1,0) -- (0,-1) -- cycle ;
  \end{tikzpicture}
  \hfill
  \begin{tikzpicture}[>=latex]
    \draw [->] (-1.2,0) -- (1.2,0) ;
    \draw [->] (0,-1.2) -- (0,1.2) ;

    \draw plot [domain=0:360, smooth] ({cos(\x)}, {sin(\x)}) ;
  \end{tikzpicture}
  \hfill
  \begin{tikzpicture}[>=latex]
    \draw [->] (-1.2,0) -- (1.2,0) ;
    \draw [->] (0,-1.2) -- (0,1.2) ;

    \draw (1,1) -- (1,-1) -- (-1,-1) -- (-1,1) -- cycle ;
  \end{tikzpicture}
  \hfill ~
\end{solution}

\subsection{Parties ouvertes de $\R^2$}

\begin{definition}
  Une partie $\U$ de $\R^2$ est dite ouverte lorsque $\forall x \in \U,\
  \exists r \in \R_+^*,\ \B(x,r) \subset \U$.
\end{definition}

\begin{exemple}
  Les boules ouvertes sont des parties ouvertes.
\end{exemple}

Parmi les parties ouvertes usuelles, on peut citer $\R^2$ ou encore $ø$.

\begin{proposition}\label{ana13:prop:union_quelconque_ouvert}
  Si $\brk*{\U_i}_{i\in I}$ est une famille d'ouverts de $\R^2$, alors
  $\bigcup_{i\in I} \U_i$ est un ouvert de $\R^2$.
\end{proposition}
\begin{proof}
  Soit $x \in \bigcup_{i\in I} \U_i,\ \exists i_0 \in I,\ x \in \U_{i_0}$ et
  donc $\exists r \in \R_+^*,\ \B(x,r) \subset \U_{i_0}$, d'où $\B(x,r)
  \subset \bigcup_{i\in I} \U_i$.
\end{proof}

\begin{proposition}\label{ana13:prop:intersection_finie_ouvert}
  Pour toute famille finie d'ouverts $\brk*{\U_i}_{1≤i≤n},\ \bigcap_{i=1}^n
  \U_i$ est un ouvert.
\end{proposition}
\begin{proof}
  Par récurrence sur $n$ en utilisant le fait que l'intersection de deux
  ouverts est un ouvert.

  Soit $\U_1$ et $\U_2$ deux ouverts.
  \begin{description}
    \item[premier cas] $\U_1 \cap \U_2 = ø$, c'est un ouvert.
    \item[deuxième cas] $\U_1 \cap \U_2 ≠ ø$. Soit $x \in \U_1 \cap \U_2$\\
      $\exists r_1 >0, \B(x,r_1) \subset \U_1$ et $\exists r_2 >0, \B(x,r_2)
      \subset \U_2$ et donc pour $r = \min(r_1,r_2),\ \B(x,r) \subset \U_1
      \cap \U_2$
  \end{description}
\end{proof}

\begin{remarque}
  La proposition est fausse pour les familles infinies.

  Pour s'en convraincre : $\U_i = \intv*{x - \frac1{i}}{x + \frac1{i}}$ est
  une famille infinie d'ouverts de $\R$ et $\bigcap_{i=1}^{+\infty} U_i =
  \set{x}$ qui n'est pas ouvert.
\end{remarque}

\begin{proposition}
  La notion d'ouvert ne dépend pas de la distance choisie.
\end{proposition}
\begin{proof}
  En effet, dans $\R^2$, pour deux distances choisies (qui sont
  équivalentes), on peut noter $\B_d(x,r)$ la boule ouverte pour la distance
  $d$ et $\B_{d'}(x,r)$ la boule ouverte pour la distance $d'$.

  On a $\B_(x,r_1) \subset \B_{d'}(x,r) \subset \B_d(x,r_2)$
\end{proof}

\subsection{Parties fermées de $\R^2$}

\begin{definition}[partie fermée]
  Une partie est dite \emph{fermée} lorsque son complémentaire dans $\R^2$
  $\overline{F} = \R^2 \setminus F$ est ouvert.
\end{definition}

Parmi les parties usuelles, on peut noter que $\R^2$ et $ø$ sont fermées.

\begin{proposition}
  Toute intersection de fermées est fermée.
\end{proposition}
\begin{proof}
  Le complémentaire d'une intersection de fermées est une union d'ouverts
  qui est ouvert (voir proposition \ref{ana13:prop:union_quelconque_ouvert}.)
\end{proof}

En particulier, on montre ainsi que les singletons sont fermés.

\begin{proposition}
  Toute union finie de fermée est fermée.
\end{proposition}
\begin{proof}
  Démonstration analogue à la précédente en utilisant la proposition
  \ref{ana13:prop:intersection_finie_ouvert}.
\end{proof}

\subsection{Aspects séquentiels des fermés}

On peut rappeller qu'une suite de points de $\R^2$ admet $\ell$ pour limite
s'écrit $\forall \varepsilon > 0, \exists n_0 \in \N,\ n ≥ n_0 \implies
\norm{u_n - \ell} ≤ \varepsilon$. On peut remplacer le second membre de
l'implication par $u_n \in \overline{\B}(\ell, \varepsilon)$

\begin{theoreme}
  Une partie de $X \subset \R^2$ est fermée si et seulement si toute suite
  convergente d'éléments de $X$ a sa limite dans $X$.
\end{theoreme}
\begin{proof}
  Soit $X \in \R^2$ une partie fermée de $\R^2$ et $u$ une suite convergente
  d'élements de $X$, de limite $\ell$ dans $\R^2$.\\
  Supposons que $\ell \not\in X$. Dans ce cas, $\ell \in \overline{X}$ qui
  est ouvert. Il existe donc $\alpha >0, \B(\ell,\alpha) \subset
  \overline{X}$. Or il existe $n_0 \in \N,\ n ≥ n_0 \implies u_n \in
  \B(\ell,\alpha)$. Donc $u_n \in X \cap \overline{X}$, ce qui est
  impossible.\\
  Donc $X$ fermé $\implies \ell \in X$.

  Inversement, supposons que toute suite $u$ convergente dans $X$ possède sa
  limite dans $X$. Supposons de plus que $X$ n'est pas fermé. Dans ce cas,
  $\overline{X}$ n'est pas ouvert et donc il existe $x \in \overline{X}$
  tel que $\forall \varepsilon > 0, \B(x,\varepsilon) \cap X ≠ ø$. Alors
  pour tout $n \in \N^*$, on peut associer $x_n \in X$ tel que $x_n \in
  \B\brk*{x,\frac1n}$. On définit ainsi une suite $(x_n)_{n\in\N^*}$ de
  points de $X$ de $\lim_{n\to +\infty} = x \in \overline{X}$, ce qui est
  contradictoire.
\end{proof}

Ce théorème est utile, à la fois pour montrer qu'une partie est fermée ou
non et pour, dans certains cas, caractériser l'appartenance de la limite
d'une suite.

\begin{theoreme}[Bolzano-Weierstrass version $\R^2$]
  De toute suite bornée de points de $\R^2$, on peut extraire une suite
  convergente.
\end{theoreme}
\begin{proof}
  À revoir.
\end{proof}

\subsection{Parties compactes de $\R^2$}

\begin{definition}
  Une partie $X$ de $\R^2$ est dite compacte lorsque de toute suite de
  points de $X$, on peut extraire une suite convergente ayant sa limite dans
  $X$.
\end{definition}

\begin{exemple}
  $\set{a}$ et $ø$ sont compacts. $\R^2£$ ne l'est pas : la suite de terme
  général $(1,n)$ diverge.
\end{exemple}

\begin{proposition}
  Une partie de $X$ de $\R^2$ est compacte si et seulement si elle est
  fermée et bornée.
\end{proposition}
\begin{proof}
  Rappellons déjà que $X$ bornée signifie que $\exists R > 0,\ X \subset
  \B(0,R)$. On peut, dans ce cas, définir le diamètre de $X$ comme
  $\sup_{(x_1,x_2) \in X^2}(x_1,x_2)$.

  Supposons $X$ compacte. Alors si $u$ est une suite convergente de $X$, sa
  limite est dans $X$ et donc $X$ est fermée.\\
  Si $X$ n'est pas bornée, pour tout $n \in \N$, on pourrait trouver $x_n$
  tel que $\norm{x_n} ≥ n$ et alors il est impossible d'extraire une suite
  suite convergente dans $\R^2$.\\
  $X$ compacte $\implies X$ fermée bornée.

  Réciproquement, supposons $X$ fermée bornée dans $\R^2$.\\
  Soit $(x_n)_{n\in\N}$ une suite de $X$. Comme, $X$ est bornée, on peut
  extraire une suite convergente. Or $X$ étant fermée, $(u_{\varphi(n)}) \in
  X^{\N}$ converge dans $X$. Donc $\ell \in X$.\\
  Donc $X$ est compacte.
\end{proof}

\subsection{Voisinage}

\begin{definition}
  Une partie $\V$ de $\R^2$ est un voisinage $a \in \R^2$ lorsqu'il existe un
  ouvert $\O$ de $\R^2$ tel que $a \in \O \subset \V$.
\end{definition}

\begin{remarque}
  Par construction, un voisinage n'est jamais vide.
\end{remarque}

\begin{proposition}
  Un ouvert est un voisinage de chacun de ses points.
\end{proposition}
\begin{proof}
  Soit $x \in \O$, alors $\exists r > 0,\ \B(x,r) \subset \O$.
\end{proof}

\begin{definition}\label{ana13:def:frontiere}
  On définit la frontière de $X$ comme $ \bigcap_{\substack{X \subset
  F\\ F\text{ fermé}}} F \cap \overline{\bigcap_{\substack{X \subset F\\
  F\text{ fermé}}} F}$.
\end{definition}

\section{Limite et continuité des fonctions de $\R^2$ dans $\R$}

\subsection{Exemples}

% Insérer ici des exemples

On peut vérifier que les fonctions affines $(x,y) \mapsto ax + by + c$ sont
continues. On peut vérifier de même que les fonctions lipschitziennes sont
également continues.

Si $f$ n'est pas définie en un point $a$ adhérant à l'ensemble de définition
et admet une limite en $a$, on peut prolonger par continuité.

\begin{theoreme}
  $f$ est continue en $a \in \D_f$ si et seulement si $\forall
  (x_n)_{n\in\N} \in \D_f^{\N},\ \lim_{n \to +\infty} x_n = a \implies
  \lim_{x\to +\infty} f(x_n) = f(a)$.
\end{theoreme}
\begin{proof}
  La démonstration est analogue à celle des fonctions de $\R$ dans $\R$.
  % Retrouver le théorème et insérer la référence.
\end{proof}

On peut étudier le cas des fonctions partielles.

\begin{proposition}
  Si $f$ est $\Cl^0$ en $a \in \D_f,\ a =(x_0,y_0)$ alors les fonctions
  partielles $x \mapsto f(x,y_0)$ et $y \mapsto f(x_0,y)$ sont continues.
\end{proposition}
\begin{proof}
  $\forall \varepsilon > 0,\ \exists \alpha > 0,\ \forall (x,y) \in \D_f,\
  \norm{a - (x,y)} ≤ \alpha \implies \abs{f(x,y) - f(a)} ≤ \varepsilon$. En
  particulier, pour $y = y_0$,\\
  $\forall \varepsilon > 0,\ \exists \alpha > 0,\ \forall x \in p_1(D_f),\
  \norm{a - (x,y_0)} ≤ \alpha \implies \abs{f(x,y_0) - f(a)} ≤ \varepsilon$,
  ce qui n'est que la continuité de la fonction partielle.
\end{proof}

\begin{remarque}
  La réciproque est fausse.
\end{remarque}

\begin{proposition}
  Les fonctions continues sur un ouvert $\U \subset \R^2$ à image dans $\R$
  forment une $\R$-algèbre.
\end{proposition}
\begin{proof}
  Aisée.
\end{proof}

\begin{exemple}
  La fonction $(x,y) \mapsto 2\arctan\brk*{\frac{y}{1+x}}$ est continue sur
  $\U = \intv[o]{-1}{+\infty} × \R$.
\end{exemple}

\begin{proposition}
  Si $f$ est continue sur un compact $K \subset \R^2$ (fermé, borné), alors
  $f(K)$ est une partie fermée bornée (compacte) de $\R$.
\end{proposition}
\begin{proof}
  Soit $f$ continue sur $\K$. Supposons $f(K)$ non fermée. Alors $\exists
  (x_n)_{n_\in\N} \in K^{\N}$ telle que $\lim x = \ell_1$ et $\lim f(x) =
  \ell \not\in f(K)$. La suite $(x_n)$ est à valeurs dans $\K$, on peut donc
  extraire une suite comvergente dans $K$ et $\lim x_{\varphi(n)} = \ell_2
  \in \K$.\\
  La suite $(f(x_{\varphi(n)}))$ est extraite de $(f(x_n))$ et converge
  aussi vers $\ell$, mais $f(x_{\varphi(n)})$ à pour limite $f(\ell_2)$ donc
  $f(\ell) = \ell_2$.\\
  Donc $f(K)$ est fermée.

  Montrons que $f(K)$ est bornée.\\
  Si $f(K)$ non bornée, alors par exemple, $f(K)$ est non majorée. On
  pourrait alors trouver une suite $x_n$ d'éléments de $f(K)^{\N}$ telle que
  $\lim_{n\to+\infty} f(x_n) = +\infty$.\\
  De $(x_n)$ on extrait une suite $(x_{\varphi(n)})$ telle que $\lim
  x_{\varphi(n)} = \ell \in \K$. Ainsi, $\lim f(x_{\varphi(n)}) = f(\ell)$
  et donc $f(\ell) = +\infty$. Contradiction.
\end{proof}

\begin{theoreme}
  Si $f$, à image dans $\R$, est continue sur $\R^2$, alors l'image
  réciproque d'un intervalle fermé de $\R$ est une partie fermée de $\R^2$.
\end{theoreme}
\begin{proof}
  Soit $I = \intv[r]{a}{+\infty}$ un intervalle fermé de $\R$. On pose $X =
  \overset{-1}{f}(I)$.\\
  Soit $(x_n)_{n\in\N} \in X^{\N}$ une suite convergente de points de $I$ et
  comme $f$ est continue, $f(\ell) \in I$, donc $\ell \in \overset{-1}{f}(I)
  = X$.\\
  $X$ est donc une partie fermée.
\end{proof}

\section{Calcul différentiel}

Ici $f$ est une fonction définie sur un ouvert $\U \subset \R^2$, à image
dans $\R$.

\subsection{Dérivabilité en $a$}

\begin{definition}
  Soit $h = (\alpha,\beta)$ un vecteur non nul de $\R^2$. On dit que $f$
  admet une dérivée en $a \in \U$ selon $k$ lorsque la fonction $\varphi
  \colon t \mapsto f(a + th) = f(x_0 +t\alpha, y_0 + t\beta)$ est dérivable
  en 0. On note $D_k(f)(a)$ le nombre $\varphi'(0)$.
\end{definition}

\begin{remarque}
  $\U$ est un ouvert et $a \in \U$, donc $f$ est bien définie sur un
  voisinage $a$ et $\varphi$ est bien définie au voisinage de 0.
\end{remarque}

\subsection{Dérivées partielles en $a$}

\begin{definition}
  Lorsque $h = (1,0)$ (resp. $(0,1)$), le nombre $D_k(f)(a)$ est appellé nombre
  dérivé partiel par rapport $x$ (resp. $y$) et est noté $D_x(f)(a)$ ou
  $\frac{\partial f}{\partial x}(a)$ ou $f'_x(a)$.
\end{definition}

\begin{definition}
  On dit que la fonction $f$ est de classe $\Cl^1$ sur un ouvert $\U$ de
  $\R^2$ lorsque $\forall k \in \R^2$, la fonction $M\mapsto D_k(f)(M)$ est
  continue sur $\U$.
\end{definition}

\begin{remarque}
  Si $f$ est de classe $\Cl^1$ sur $\U$, ses dérivées partielles
  $\frac{\partial f}{\partial x}$ et  $\frac{\partial f}{\partial y}$ sont
  continues sur $\U$.
\end{remarque}

\begin{theoreme}
  Si les dérivées partielles de $f$ existent et sont continues en tout point
  de $\U$, alors $f$ admet un développement limité d'ordre 1 en tout point
  de $\U$ : \\
  $f(x_0 + u, y_0 + v) = f(x_0,y_0) + u \frac{\partial f}{\partial
  x}(x_0,y_0 + v) + v \frac{\partial f}{\partial y}(x_0 + u,y_0) +
  \po{\norm{(u,v)}}$.\\
  $f$ admet alors un nombre dérivé selon tout vecteur $k$ tel que\\
  $D_k(f)(a) = \alpha \frac{\partial f}{\partial x}(a) + \beta
  \frac{\partial f}{\partial y}(a)$.\\
  L'application $k \mapsto D_k(f)(a)$ est une forme linéaire.
\end{theoreme}
\begin{proof}
  $f(x_0 + u, y_0 + v) - f(x_0, y_0) = f(x_0 + u, y_0 + v) - f(x_0 + u, y_0)
  + f(x_0 + u, y_0) - f(x_0, y_0)$.\\
  $\U$ étant un ouvert, $y \mapsto f(x_0 + u, y_0)$ est $\Cl^0$ sur
  $\intv{y_0}{y_0 + v}$ et dérivable sur $\intv[o]{y_0}{y_0 + v}$.\\
  $\exists \theta_1 \in \intv[o]{0}{1},\ f(x_0 + u, y_0 + v) - f(x_0 + u,
  y_0) = v f'_y(x_0 + u, y_0 + \theta_1 v)$ et de même, $\exists \theta_2
  \in \intv[o]{0}{1},\ f(x_0 + u, y_0) - f(x_0, y_0) = uf'_x(x_0 + \theta_2
  u, y_0)$.\\
  $\Delta(u,v) = (f(x_0 + u, y_0 + v) - f(x_0, y_0) - uf'_x(x_0,y_0) -
  vf'_y(x_0,y_0)) × \frac1{\norm{(u,v)}}$\\
  $\Delta(u,v) = (u(f'_x(x_0 + \theta_2 u, y_0) - f'_x(x_0,y_0))+v(f'_y(x_0
  + u, y_0 + \theta_1 v) - f'_y(x_0, y_0))) × \frac1{\norm{(u,v)}}$\\
  $\abs{\Delta(u,v)} ≤ \frac1{\norm{(u,v)}} (\abs{u} \abs{f'_x(x_0 +
    \theta_2 u, y_0) - f'_x(x_0,y_0)} + \abs{v} \abs{f'_y(x_0 + u, y_0 +
  \theta_1 v) - f'_y(x_0, y_0)})$\\
  Comme $\frac{\abs{u}}{\norm{(u,v)}} ≤ 1$, on a \\
  $\abs{\Delta(u,v)} ≤ \abs{f'_x(x_0 + \theta_2 u, y_0) - f'_x(x_0,y_0)} +
  \abs{f'_y(x_0 + u, y_0 + \theta_1 v) - f'_y(x_0, y_0)}$.\\
  On aura donc $\abs{\Delta(u,v)} ≤ \varepsilon$ dès que $\norm{(u,v)}$ sera
  suffisament petit.

  À partir du développement limité, on déduit le reste.
\end{proof}

On peut désormais noter, de façon très abusive $\diff f = \frac{\partial
f}{\partial x}\diff x + \frac{\partial f}{\partial y}\diff y$.

\begin{proposition}
  Si $f$ est de classe $\Cl^1$ sur $\U$, alors elle est de classe $\Cl^0$
  sur $\U$.
\end{proposition}
\begin{proof}
  Démonstration analogue à celle sur les fonctions de $\R \to \R$.
\end{proof}

On considère la forme linéaire $\diff f \colon (\alpha,\beta) \mapsto f'_x
\alpha + f'_y \beta$. Comme forme linéaire, celle-ci peut s'exprimer comme
un produit scalaire et on a, pour tout $(\alpha, \beta),\ \diff f =
\braket{(f'_x,f'_y)}{(\alpha, \beta)}$.

\begin{definition}[gradient d'une fonction]
  Le \emph{gradient} de $f$ est le vecteur canoniquement associé à la forme
  linéaire $\diff f$ : $\vv{\grad}f = (f'_x,f'_y)$.
\end{definition}

\begin{proposition}
  $\grad$ est linéaire et $\grad (fg) = (\grad f)g +f(\grad g)$\\
  Pour $g$ continue de $\R \to \R,\ \grad g\circ f = g' \circ f \grad f$.
\end{proposition}
\begin{proof}
  Aisée et similaire à celle sur les fonctions de $\R$ dans $\R$.
\end{proof}

On peut s'intéresser à la composition de fonction à droite.

\begin{proposition}
  Soient $f \colon \U \to \R$ et $\varphi \colon I \to \U$ telle que
  $\varphi(t) = (\varphi_1(t), \varphi_2(t))$, $I = \intv[o]{a}{b} \subset
  \R$ et $\U$ un ouvert de $\R^2$.\\
  Si $f$ et $\varphi$ sont des fonctions de classe $\Cl^1$ sur $\U$ (resp.
  $I$), alors $f \circ \varphi$ est de classe $\Cl^1$ et \\
  $\forall t_0 \in I,\ (f \circ \varphi)' = \frac{\partial f}{\partial
  x}(\varphi(t_0)) × \frac{\diff \varphi_1}{\diff t}(t_0) + \frac{\partial
  f}{\partial y}(\varphi(t_0)) × \frac{\diff \varphi_2}{\diff t}(t_0)$.
\end{proposition}
\begin{proof}
  \begin{align*}
    (f \circ \varphi)(t_0+u) & = f(\varphi(t+u))                  \\
                           & = f(\varphi_1(t_0+u),\varphi_2(t+u)) \\
                           & = f(\varphi_1(t_0) + \varphi'_1(t) + \po{u},\varphi_2(t) + \varphi'_2(t) + \po{u}) \\
                           & = f(\varphi_1(t_0) + \alpha,\varphi_2(t) + \beta) \\
                           & = f(\varphi_1(t_0),\varphi_2(t)) + \alpha f'_x(\varphi_1(t),\varphi_2(t)) + \beta f'_y(\varphi_1(t),\varphi_2(t)) + \po{\norm{(a,b)}} \\
                           & = (f \circ \varphi)(t_0) + \alpha
    \frac{\partial f}{\partial x}(\varphi(t_0)) + \beta \frac{\partial
    f}{\partial y}(\varphi(t_0)) + \po{u} \\
  \end{align*}
  On retrouve ainsi un développement limité à l'ordre 1, d'où la
  dérivabilité et la continuité.
\end{proof}

\begin{theoreme}
  Soient $f$ de classe $\Cl^1$ sur un ouvert $\U$ de $\R^2$ à images dans
  $\R$, $\varphi$ de classe $\Cl^1$ sur un ouvert $\V$ à images dans $\U$.
  Alors $f \circ \varphi$ est de classe $\Cl^1$ de $\V$ dans $\R$.
\end{theoreme}
\begin{proof}
  Analogue à la précédente.
\end{proof}

On peut désormais donner une caractérisation des extremums.

\begin{theoreme}
  Si $f$ est de classe $\Cl^1$ sur un ouvert de $\R^2$ admet un extremum en
  $(x_0,y_0) \in \U$, alors les dérivées partielles s'annulent en ce point.
\end{theoreme}
\begin{proof}
  Puisque c'est un maximum pour la fonction $f$, c'est un maximum pour les
  fonctions partielles et donc les dérivées partielles s'annulent.
\end{proof}

On peut également s'intéresser aux dérivées d'ordre supérieur ou égal à 2.

\begin{definition}
  Si $f$ admet des dérivées partielles $\frac{\partial f}{\partial x_i},\ i
  \in \set{1,2}$ sur un ouvert $\U$ de $\R^2$ et si les dérivées partielles
  sont elles-mêmes dérivables, on note \\
  $\frac{\partial^2 f}{\partial x_i \partial x_j} = \frac{\partial
  \frac{\partial f}{\partial x_i}}{\partial x_j}$
\end{definition}

\begin{theoreme}[de Schwarz]
  Si $\frac{\partial^2 f}{\partial x_i \partial x_j}$ et $\frac{\partial^2
  f}{\partial x_j \partial x_i}$ existent et sont continues sur $\U$, alors
  elles sont égales. On dit que la fonction est de classe $\Cl^2$.
\end{theoreme}
\begin{proof}
  Admis, mais la preuve repose sur le théorème des accroissements finis.
\end{proof}

Des fonctions comme le lemniscate de Bernoulli sont défines par $(x^2 +
y^2)^2 = a(x^2 - y^2)$ et plus généralement par des équations du type de
$f(x,y) = 0$.

\begin{theoreme}[des fonctions implicites, version locale]
  Soit $f$ une fonction de classe $\Cl^1$ sur un ouvert $\U$ de $\R^2$, $a
  \in U$ tel que $f(a) = 0$ et $\frac{\partial f}{\partial y}(a) ≠ 0$.
  Alors, $\exists I = \intv[o]{x_0 - \alpha}{x_0 + \alpha}$ et $J =
  \intv{y_0 - \beta}{y_0 + \beta}$ tels que $I × J \subset \U$ et $\exists
  \varphi \colon I \to J$, de classe $\Cl^1$ pour laquellle, $\forall M \in
  I×J,\ f(M) = 0 \iff y = \varphi(x)$, de plus, $\varphi'(x_0) = -
  \frac{f'_x(a)}{f'_y(a)}$.
\end{theoreme}
\begin{proof}
  Admis
\end{proof}

Une conséquence de ce théorème est une propriété des lignes de niveaux. Soit
$C_k$ la ligne de niveau définie par $f(x,y) = k$. On pose alors $g \colon
(x,y) \mapsto f(x,y) - k$ et on s'intéresse à la courbe d'équation $g(x,y) =
0$.\\
$\vv{\grad}g = \begin{pmatrix} \frac{\partial g}{\partial x} \\
\frac{\partial g}{\partial y} \end{pmatrix}$ et de plus, d'après le théorème
des fonctions implicites, on $\frac{\diff y}{\diff x} = -
\frac{\frac{\partial g}{\partial x}}{\frac{\partial g}{\partial y}}$.\\
Le vecteur directeur de la tangente s'exprime par $\begin{pmatrix} 1 \\
-\frac{f'_x}{f'_y} \end{pmatrix}$ et il est colinéaire à $\begin{pmatrix}
f'_y \\ -f'_x \end{pmatrix}$ et donc orthogonal à $\vv{\grad}g$.
