\chapter{Étude des fonctions usuelles}

\section{Trigonométrie circulaire}

\subsection{Les fonctions élémentaires}
Les fonctions $\sin$ et $\cos$ sont des fonctions de $\R \to \R$,
périodiques, de période $2\pi$, respectivement impaire et paire.

On peut revoir le formulaire de trigonométrie et en particulier, \[ \forall
  x \in \R, \sin \frac{\pi}2 - x = \cos x ; \cos \frac{\pi}2 - x = \sin x ;
\sin \frac{\pi}2 + x = \cos x ; \cos \frac{\pi}2 + x = - \sin x . \]

\subsubsection{Continuité}

$\sin x - \sin a = 2 \sin \dfrac{x -a}2 \cos \dfrac{x + a}2
\underset{a}{\sim} 2 \dfrac{x -a}2 \cos \dfrac{x + a}2$. Comme $\cos
\dfrac{x + a}2$ est borné, on en déduit que $\lim_{x \to a}
\sin x - \sin a = 0$ et donc que $\sin$ est continue et donc $\cos$ aussi.

\subsubsection{Dérivabilité}

$\dfrac{\sin x - \sin a}{x - a} = \dfrac{2 \sin \dfrac{x -a}2 \cos \dfrac{x
+ a}2}{x - a} \underset{a}{\sim} 2 \cos \dfrac{x + a}2$ On a donc que
$\lim_{x \to a} \dfrac{\sin x - \sin a}{x - a} = \cos a$ et
donc $\sin$ est dérivable et sa dérivée est $\cos$.

De même, on en déduit que la dérivée de $\cos$ est $-\sin$.

\subsection{Réciproques $\arccos$ et $\arcsin$}

\subsubsection{$\arcsin$}

On considère la restriction de $\sin$ à $\intv{-\frac{\pi}2}{\frac{\pi}2}$.
Ainsi, la fonction est strictement croissante, continue sur
$\intv{-\frac{\pi}2}{\frac{\pi}2}$, dérivable sur
$\intv{-\frac{\pi}2}{\frac{\pi}2}$. Elle établit ainsi une bijection de
$\intv{-\frac{\pi}2}{\frac{\pi}2}$ sur $\intv{-1}{1}$, et on note sa
réciproque $\arcsin$. On a ainsi $\forall x \in
\intv{-\frac{\pi}2}{\frac{\pi}2}, \ \forall y \in \intv{-1}{1} \ y = \sin x
\iff x = \arcsin y$.

\begin{question}
  Calculer :
  \begin{enumerate}
    \item $\sin \arcsin x$
    \item $\arcsin \sin x$
    \item $\cos \arcsin x$
  \end{enumerate}
\end{question}
\begin{solution}
  \begin{enumerate}
    \item Pour $x \in \intv{-1}{1}$, $\sin \arcsin x = x$
    \item $\begin{array}{ccc}
        \arcsin \sin x = x_1 & \implies & \sin x = \sin x_1,\ x_1 \in
        \intv{-\frac{\pi}2}{\frac{\pi}2} \\
        & \iff & \left\{ \begin{array}{c} x_1 \equiv x \mod 2\pi \\ x_1
        \equiv \pi - x \mod 2\pi \\ x_1 \in \intv{-\frac{\pi}2}{\frac{\pi}2}
      \end{array} \right. \end{array}$
    \item $\cos \arcsin x = \sqrt{1 - \sin^2 \arcsin x} = \sqrt{1 - x^2}$.
  \end{enumerate}
\end{solution}

\begin{proposition}
  $\arcsin$ est $\Cl^0$ sur $\intv{-1}{1}$, strictement croissante,
  dérivable sur $\intv[o]{-1}{1}$.
\end{proposition}
\begin{proof}
  Il s'agit d'une application du théorème
  \ref{ana04:prop:continue_derivable_reciproque}
\end{proof}

On a de plus $\arcsin' = \dfrac1{\sin' \circ \arcsin} = \dfrac1{\cos \circ
\arcsin}$ et donc $\forall x \in \intv[o]{-1}{1}$, $\arcsin' x =
\dfrac1{\sqrt{1 - x^2}}$.

\begin{center}
  \begin{tikzpicture}
    \draw plot [smooth,domain=-1.52:1.52] (\x, {sin(\x r)}) ;
    \draw plot [smooth,domain=-1:1] ({sin(\x r)}, \x) ;
  \end{tikzpicture}
\end{center}

\begin{remarque}
  $\arcsin x \sim_0 x$
\end{remarque}

\subsubsection{$\arccos$}

On considère désormais la restriction de $\cos$ à $\intv{0}{\pi}$. On se
retrouve dans les mêmes conditions que précédement, et on peut ainsi définir
la fonction.

\begin{proposition}
  $\arccos$ est $\Cl^0$ sur $\intv{-1}{1}$, dérivable sur $\intv[o]{-1}{1}$
  et on a $\forall x \in \intv[o]{-1}{1}, \arccos' x = \dfrac{-1}{\sqrt{1 -
  x^2}}$.
\end{proposition}
\begin{proof}
  Démonstration analogue à la précédente.
\end{proof}

\begin{center}
  \begin{tikzpicture}
    \draw plot [smooth,domain=0:3.14] (\x, {cos(\x r)}) ;
    \draw plot [smooth,domain=-1:1] ({cos(\x r)}, \x) ;
  \end{tikzpicture}
\end{center}

\begin{proposition}
  $\forall x \in \intv{-1}{1},\ \arcsin x + \arccos x = \dfrac{\pi}2$.
\end{proposition}
\begin{proof}
  Posons, pour $x  \in \intv{-1}{1}$, $\varphi(x) = \arcsin x + \arccos
  x$.\\
  $\varphi$ est $\Cl^0$ sur $\intv{-1}{1}$, dérivable sur $\intv[o]{-1}{1}$
  et pour tout $x \in \intv[o]{-1}{1},\ \varphi'(x) = \dfrac{1}{\sqrt{1
  -x^2}} + \dfrac{-1}{\sqrt{1 -x^2}} = 0$. \\
  $\varphi$ est donc constante sur $\intv[o]{-1}{1}$ et y vaut $\varphi(0) =
  \arcsin 0 + \arccos 0 = \dfrac{\pi}2$. Par prolongement continue, on a le
  résultat sur $\intv{-1}{1}$.

  On peut obtenir ce même résultat en développant $\sin(\arcsin x + \arccos
  x)$.
\end{proof}

\subsection{Fonctions $\tan$ et $\arctan$}

\begin{definition}
  On définit la fonction tangente, notée $\tan$, de la façon suivante
  $\forall x \in \R \setminus \left\{ \dfrac{\pi}2 + k \pi, k \in \Z
  \right\},\ \tan x = \dfrac{\sin x}{\cos x}$
\end{definition}

La fonction ainsi définie est impaire, de péirode $\pi$, $\Cl^0$ et
dérivable sur $\R \setminus \left\{ \dfrac{\pi}2 + k \pi, k \in \Z
\right\}$.

\begin{proposition}
  $\forall x \in \D_{\tan},\ \tan' x = 1 + \tan^2 x = \dfrac1{\cos^2 x}$
\end{proposition}

\begin{center}
  \begin{tikzpicture}[yscale=0.7]
    \draw plot [domain=-1.40:1.40,smooth] (\x, {tan(\x r)}) ;
  \end{tikzpicture}
\end{center}

\begin{proposition}
  La restriction de la fonction $\tan$ à
  $\intv[o]{-\frac{\pi}2}{\frac{\pi}2}$ est une bijection de
  $\intv[o]{-\frac{\pi}2}{\frac{\pi}2}$ sur $\R$. On note $\arctan$ sa
  bijection réciproque.
\end{proposition}
\begin{proof}
  On utilise les mêmes arguments que pour les bijections réciproques des
  fonctions $\sin$ et $\cos$.
\end{proof}

On a, $\forall x \in\intv[o]{-\frac{\pi}2}{\frac{\pi}2},\ \forall y \in \R,\
y = \tan x \iff x = \arctan y$.

\begin{proposition}
  $\arctan$ est $\Cl^0$ sur $\R$ et dérivable et $\forall x \in \R,\
  \arctan' x = \frac{1}{1 + x^2}$
\end{proposition}

On a les équivalences suivantes :
\begin{itemize}
  \item $\tan x \underset{0}{\sim} x$
  \item $\arctan x \underset{0}{\sim} x$
\end{itemize}

\begin{question}
  Montrer que, pour tout $x \in \R_+^*$, $\arctan x + \arctan \frac1x =
  \dfrac{\pi}2$
\end{question}
\begin{solution}
  Étudier $\phi(x) = \arctan x + \arctan \frac1x$ sur $\intv[o]{-\infty}{0}$
  et sur $\intv[o]{0}{+\infty}$ et montrer qu'elle est constante sur ces
  deux intervalles et qu'elle vaut $\dfrac{\pi}2$.
\end{solution}

On peut pour finir cette partie, définir la fonction $\cot =
\dfrac{\cos}{\sin}$

\section{Logarithme et exponentielle}

\subsection{Logarithme népérien}

\begin{definition}
  $\forall x \in \R_+^*,\ \ln x = \int_1^x \dfrac1t \diff t$.
\end{definition}

\begin{remarque}
  Cette définition est justifiée par le fait que $x \mapsto \frac1x$ est
  dérivable sur $\R_+^*$. Le chapitre \ref{} permettra de justifier que la
  continuité était suffisante.
\end{remarque}

\begin{proposition}
  \begin{itemize}
    \item $\ln$ est $\Cl^{\infty}$ sur $\R_+^*$.
    \item $\ln$ est strictement croissante sur $\R_+^*$
    \item $\ln$ établit une bijection sur $\R_+^*$.
    \item $\lim_{x \to +\infty} \ln x = +\infty$
    \item $\lim_{\substack{x \to 0\\ x > 0 }} \ln x = -\infty$
  \end{itemize}
\end{proposition}
\begin{proof}
  La preuve est renvoyé au chapitre \ref{} sur l'intégration.
\end{proof}

\begin{proposition}
  Pour tous $x$ et $y$ réels strictement positifs, $\ln(xy) = \ln(x) +
  \ln(y)$.
\end{proposition}
\begin{proof}
  La preuve repose sur le fait que la dérivée de $\ln$ est la fonction
  inverse :\\
  Soit $\Phi_y \colon x \mapsto \ln(xy)$ pour $y > 0$ fixé.\\
  $\Phi_y$ est dérivable et $\Phi'_y = \dfrac1x$. On a donc que $\Phi'_y$ et
  $\ln'$ sont égales et donc $\Phi_y$ et $\ln$ ne diffèrent que d'une
  constante : $\Phi_y(x) = \ln x + K_y$. Or $\Phi_y(1) = \ln(y) + K_y =
  \ln(1)$ donc $K_y = \ln(y)$.\\
  $\ln$ est donc un morphisme de $(\R_+^*,\times) \to (\R,+)$.
\end{proof}

%Note : ln croissante + le point précédent permettent de montrer que ln est
%non majorée et donc la suite ln 2^n = n \ln 2 diverge et donc ln diverge.

\begin{question}
  Montrer que :
  \begin{enumerate}
    \item
      \begin{enumerate}
        \item $\lim_{x \to 0} \dfrac{\ln 1 + x}x = 1$
        \item $\ln 1+x \underset{0}{=} x$
      \end{enumerate}
    \item
      \begin{enumerate}
        \item $\lim_{x \to +\infty} \dfrac{\ln x}x = 0$
        \item $\ln x \underset{+\infty}{=} \po{x}$
      \end{enumerate}
  \end{enumerate}
\end{question}
\begin{solution}
  \begin{enumerate}
    \item
      \begin{enumerate}
        \item Interpréter comme unn nombre dérivé
        \item Revoir la définition d'équivalent
      \end{enumerate}
    \item
      \begin{enumerate}
        \item Comparer $t$ et $\sqrt{t}$ puis leurs inverses, à un
          coefficient $\frac1X$ près. Intégrer …
        \item Revoir la définition d'équivalent
      \end{enumerate}
  \end{enumerate}
\end{solution}

\subsection{Exponentielle de base $e$}

\begin{definition}
  $\forall x \in \R,\ \forall y \in \R_+^*,\ y = \exp(x) \iff x = \ln y$.
\end{definition}

\begin{proposition}
  \begin{itemize}
    \item $\exp$ est de classe $\Cl^{\infty}$ sur $\R$ ;
    \item $\exp' = \exp$ ;
    \item $\exp$ est un isomorphisme strictement croissant de $(\R, +)$ sur
      $(\R^*,\times)$ ;
    \item $\lim_{x \to +\infty} \exp x = + \infty$ \hfill
      $\lim_{x \to -\infty} \exp x = 0$ ; \hfill ~
    \item $\exp(x) - 1 \underset{0}{\sim} x$ \hfill $\lim_{x
      \to +\infty} \dfrac{\exp{x}}x = + \infty$ \hfill $\lim_{x
    \to +\infty} x\exp x = 0$ \hfill ~
  \end{itemize}
\end{proposition}
\begin{proof}
  Soit $x \in \R,\ \exp'(x) = \dfrac1{\ln'(\exp(x))} = \dfrac1{\frac1{\exp
  x} } = \exp x$.
\end{proof}

\section{Trigonométrie hyperbolique}

\subsection{Définition}

\begin{definition}
  $\forall x \in \R,\ \cosh x = \dfrac{\exp x + \exp -x}2$ et $\sinh x =
  \dfrac{\exp x - \exp -x}2$.
\end{definition}

%% Insérer ici l'étude complète de ces deux fonctions

\begin{question}
  Après avoir démontrer que $\sinh$ est bijective sur $\R$, donner
  l'expression de sa réciproque.
\end{question}
\begin{solution}
  $\sinh^{-1} x = \ln\brk{x + \sqrt{x^2 + 1}}$
\end{solution}

% insérer ici la même question sur $cosh sur $R+$

\subsection{Tangente hyperbolique}

\section{Fonctions puissances}

\subsection{Exposant dans $\N$}

\subsection{Exposant dans $\Z$}

\subsection{Exposant dans $\Q$}

\subsubsection{Exposant $\frac1n,\ n \in \N^*$}

\subsubsection{Exposant $\frac{p}q,\ p\in \Z, q \in \N^*$}

\subsection{Puissances dans $\R$}

\section{Équations fonctionnelles}

\subsection{Fonctions linéaires}

\subsection{Fonctions exponentielles}

\subsection{Fonctions logarithmiques}

\subsection{Fonctions puissances}


