\chapter{Dérivation des fonctions de $\R \to \R$}

\section{Défintions et calculs}

$f$ est continue en $x_0$ signifie qu'au voisinage de $x_0$, $f(x)
\underset{x_0}{=} f(x_0) + \po(1)$ ou encore $f(x_0 + h) \underset{h \to
0}{=} f(x_0) + \po(1)$.

\subsection{Différentiabilité}

\begin{definition}
  On dit que $f$ est \emph{différentiable} en $x_0 \in I = \D_f$ lorsque $f$
  admet en $x_0$ un développement limité d'ordre 1, c'est à dire qu'il
  existe $A$ et $B$ réels tels que \[ f(x_0 + h) \underset{h \to 0}{=} A +
  Bh + \po{h} .\]
\end{definition}

\begin{remarque}
  Si $f$ est différentiable en $x_0$, alors $\lim_{h\to 0}
  f(x_0 + h) = A$. Or $x_0 \in I$, donc $A = f(x_0)$ et $f$ est continue en
  $x_0$.
\end{remarque}

\begin{definition}[nombre dérivé]
  Le nombe $B$ est appellé \emph{nombre dérivé} de $f$ en $x_0$.
\end{definition}

\begin{center}
  \begin{tikzpicture}[>=latex]
    \draw (-1,0) -- (5,0) ;
    \draw (0,-1) -- (0,5) ;
    \draw [dashed] (3,0) node [below] {$x_0$} -- (3,2) -- (0,2) node [left]
      {$f(x_0)$};
    \draw [dashed] (4,0) node [below] {$x_0 + h$} -- (4,3) -- (0,3) node
      [left] {$f(x_0 + h)$};
    \draw[smooth]
      (1,1) .. controls (2,3/2) ..
      (3,2) .. controls (4,3) ..
      (4,5) ;
    \draw (2,1) -- (4,3) ;
  \end{tikzpicture}
\end{center}

\begin{proposition}
  La droite d'équation $y = f(x_0) + B (x -x_0)$ est la tangente à la courbe
  représentative $\Cf_f$ de $f$ en $M_0 : (x_0,f(x_0)$.
\end{proposition}
\begin{proof}
  Voir le dessin ci-dessus.
\end{proof}

\begin{remarque}
  $f$ peut être continue et $x_0$, mais non différentiable en $x_0$, comme
  par exemple $\abs{⋅}$.
  Ainsi, différentiable $\implies$ continue, mais continue $\not\implies$
  différentiable.
\end{remarque}

\subsection{Dérivabilité}

\begin{definition}[dérivable]
  On dit que $f$ est \emph{dérivable} en $x_0 \in \overset{\circ}{I}$
  lorsque la fonction $h \mapsto \dfrac{f(x_0 + h) - f(x_0)}h$ admet une
  limite réelle en 0.\\
  Cette limite est appelée \emph{nombre dérivé} de $f$ en $x_0$ et est notée
  $f'(x_0)$.
\end{definition}

\begin{proposition}
  $f$ est dérivable en $x_0$ si et seulement si $f$ est différentiable en
  $x_0$.
\end{proposition}
\begin{proof}
  Si $f$ est dérivable en $x_0$, alors $\dfrac{f(x_0 + h) - f(x_0)}h
  \underset{h \to 0}{\longrightarrow} f'(x_0)$, ce qui s'écrit $\dfrac{f(x_0
  + h) - f(x_0)}h \underset{h \to 0}{=} f'(x_0) + \po{1} \iff f(x_0 + h)
  \underset{h \to 0}{=} f(x_0) + hf'(x_0) + \po{h}$. Donc $f$ est
  différentiable en $x_0$.

  Réciproquement, si $f(x_0 + h) \underset{h \to 0}{=} f(x_0) + Bh + +
  \po{h}$, alors $\dfrac{f(x_0 + h) - f(x_0)}h \underset{h \to 0}{=} B  +
  \po{1}$ et donc on a $\lim_{h\to 0} \dfrac{f(x_0 + h) -
  f(x_0)}h = B$ d'où la dérivabilité.
\end{proof}

\begin{definition}
  On dit que $f$ est dérivable à droite (resp. à gauche) lorsque le rapport
  $\dfrac{f(x_0 + h) - f(x_0)}h$ admet une limite à droite (resp. à gauche).
\end{definition}

\begin{proposition}
  Une fonction est dérivable lorsqu'elle est dérivable à droite et à gauche
  et que $f'_d(x_0) = f'_g(x_0)$.
\end{proposition}
\begin{proof}
  Voir démonstration sur la continuité.
\end{proof}

\subsection{Dérivabilité sur un intervalle}

\begin{definition}
  $f$ est dérivable sur $I = \intv{a}{b}$ lorsque $f$ est dérivable en tout
  $x \in \intv[o]{a}{b}$ et que $f'_d(a)$ et $f'_g(b)$ existent.\\
  On écrit alors $f \in \D(I,\R)$ et on note $f'$ la fonction $f' \colon I
  \to \R,\ x \mapsto f'(x)$.
\end{definition}

\subsection{Calcul de dérivées}

\subsubsection{Linéarité de la dérivée}

Soit $I$ un intervalle de $\R$.\\
On considère ici $D \colon \D(I,\R) \to \mathscr{F}(\R,\R),\ f \mapsto f'$.

\begin{proposition}
  $D \in \L(\D(I,\R),\mathscr{F}(\R,\R))$.
\end{proposition}
\begin{proof}
  $\D(I,\R)$ est non vide car les fonctions constantes sont dérivables.

  Soient $f$ et $g$ deux fonctions dérivables. Étudions $f + \lambda g$.\\
  $\forall x_0 \in I,\ \dfrac{(f+ \lambda g)(x_0 + h) - (f + \lambda
  g)(x_0)}h = \dfrac{f(x_0 + h) - f(x_0)}h + \lambda \dfrac{g(x_0 + h) -
  g(x_0)}h$, ce qui reste vrai par passage à la limite et donc fournit le
  résutlat $(f + \lambda g)' = f' + \lambda g'$.
\end{proof}

\subsubsection{Dérivée d'un produit}

\begin{proposition}
  Soient $f$ et $g$ deux fonctions dérivables sur $I$. On a $D(fg) = D(f)g +
  fD(g)$.
\end{proposition}
\begin{proof}
  Soit $x_0 \in I$, on a
  \begin{align*}
    fg(x_0 + h) & = f(x_0 + h)g(x_0 + h) \\
                & \underset{0}{=} (f(x_0) + f'(x_0)h + po{h})(g(x_0) + g'(x_0)h + po{h}) \\
                & \underset{0}{=} f(x_0) g(x_0) + (f'(x_0)g(x_0) + f(x_0)g'(x_0)) h + \po{h} \\
                & \underset{0}{=} f(x_0) g(x_0) + (f'g + fg')(x_0)h + \po{x_0}
  \end{align*}
\end{proof}

On en déduit par récurrence que les fonctions $x \mapsto x^n$ sont
dérivables et que la dérivée est $x \mapsto nx^{n-1}$ et que les fonctions
polynomiales sont dérivables et qu'on les dérive termes à termes.

\subsubsection{Dérivée de l'inverser}

\begin{proposition}
  Soit $f$ définie sur $I$, dérivable sur $I$ et telle que $\forall x \in
  I,\ f(x) ≠ 0$. Alors $\frac1f$ est définie sur $I$ et $\brk*{\frac1f}' =
  \frac{-f'}{f^2}$
\end{proposition}
\begin{proof}
  Soit $x_0 \in I$. On a $\dfrac{\frac1{f(x_0 + h)} - \frac1{f(x_0)}}{h} = -
  \dfrac{f(x_0 + h) - f(x_0)}{h} × \dfrac{1}{f(x_0 + h)f(x_0)}$\\
  Par passage à la limite, on obtient le résulat.
\end{proof}

\begin{proposition}
  Soient $f$ et $g$ deux fonctions dérivables sur $I$. On a
  $\brk*{\frac{f}g}' = \frac{fg' - f'g}{f^2}$
\end{proposition}
\begin{proof}
  Appliquer les deux propositions précédentes à $f × \frac1g$
\end{proof}

\subsubsection{Dérivée des fonctions composées}

\begin{proposition}
  Soient $f$ et $g$ deux fonctions. $g$ est dérivable en $x_0$ et $f$
  dérivable en $y_0 = g(x_0)$, alors $f \circ g$ est dérivable en $x_0$ et
  $(f \circ g)'(x_0) = g'(x_0)×(f'\circ g)(x_0)$.
\end{proposition}
\begin{proof}
  \begin{align*}
    (f \circ g)=(x_0 + h) = f(g(x_0 + h)) & \underset{h \to 0}{=} f(g(x_0) + g'(x_0)h + \po{h}) \\
                                         & \underset{h \to 0}{=} f(g(x_0) +k(h) ) \\
                                         & \underset{h \to 0}{=} f(g(x_0)) + f'(g(x_0))×k(h) + \po{k(h)} \\
                                         & \underset{h \to 0}{=} (f\circ g)(x_0) + (f'\circ g)(x_0) × (g'(x_0)h  + \po{h}) + \po{g'(x_0)h  + \po{h}} \\
                                         & \underset{h \to 0}{=} (f\circ g)(x_0) + (f'\circ g)(x_0) × g'(x_0)h + \po{h}
  \end{align*}
\end{proof}

\subsubsection{Dérivée de la réciproque}

On rappelle qu'une fonction $f$ continue sur $I$, strictement monotone est
un homéomorphisme de $I$ sur $J = f(I)$.

\begin{proposition}
  Soit $f$ une telle fonction. $f$ est dérivable en $x_0$ et $f'(x_0)$ ne
  s'annule pas, si et seulement si $f^{-1}$ est dérivable et $(f^{-1})' =
  \frac{1}{f'\circ f^{-1}}$.
\end{proposition}
\begin{proof}
  Si $f$ et dérivable en $x_0$ et $f^{-1}$ dérivable en $y_0 = f(x_0)$, on a
  alors $(f^{-1} \circ f)'(x_0) = \Id'(x_0) = 1$ d'où $((f^{-1})' \circ
    f)(x_0)f'(x_0) = 1$ et donc $f'(x_0) ≠ 0$. On en déduit que $(f'\circ
  f^{-1})(x_0) (f^{-1})'(x_0) = 1$ d'où l'égalité.

  Réciproquement, si $f$ est dérivable et $f'(x_0) ≠ 0$, on a
  \begin{align*}
    f'(x_0) & = \lim_{h \to 0} \frac{f(x_0 + h) - f(x_0)}h \\
            & = \lim_{h \to 0} \frac{f(x_0 + h) - y_0}{x_0 + h - x_0}
  \end{align*}
  $f$ étant bijective, elle est donc injective et pour $h ≠ 0,\ f(x_0 + h) ≠
  f(x_0)$ et donc\\$\dfrac{1}{f'(x_0)} = \lim_{h \to 0}
  \frac{f^{-1}(y_0) + h - f^{-1}(y_0)}{f(x_0 + h) - y_0}$ Posons $k = f(x_0
  + h) - f(x_0)$. l'égalité précédente devient\\$\dfrac{1}{f'(x_0)} =
  \lim_{h \to 0}\frac{f^{-1}(y_0) + h - f^{-1}(y_0)}{k}$ avec
  $h \to 0 \iff k \to 0$ et $f(x_0 +h) = y_0 + k \implies x_0 + h =
  f^{-1}(y_0 +k)$ et donc $h = f^{-1}(y_0 +k) - f^{-1}(y_0)$. On a donc\\
  $\dfrac{1}{f'(x_0)} = \lim_{k \to 0} \dfrac{f^{-1}(y_0 +k) -
  f^{-1}(y_0)}{k}$. Or $x_0 = f^{-1}(y_0)$ et donc on a \\ $
  \lim_{k \to 0} \dfrac{f^{-1}(y_0 +k) - f^{-1}(y_0)}{k} =
  \dfrac{1}{f'(f^{-1}(y_0))}$.
\end{proof}

\subsubsection{Dérivée logarithmique}

\begin{definition}
  Soit $f$ dérivable sur $I$ et $a \in I$. On appelle \emph{dérivée
  logarithmique} de $f$ en $a$ le nombre $\frac{f'(a)}{f(a)} = g'(a)$ où
  $g$ est définie par $g(x) = \ln \abs{f(x)}$ sur $I$.
\end{definition}

\section{Variations de fonctions, accroissements finis}

\subsection{Extremum}

\begin{proposition}
  Soit $f$ une fonction. Si $f$ est dérivable en $a \in \overset{\circ}{I}$
  et présente un maximum (ou un minimum) en $a$, alors $f'(a) = 0$.
\end{proposition}
\begin{proof}
  Si $f$ admet un maximum en $a$, alors sur $I \cap \intv[o]{-\infty}{a},\
  f(x) ≤ f(a)$ et donc $f(x) - f(a)$ est du signe opposé à $x - a$ et
  $f'_g(a) ≤ 0$. De même, on trouve $f'_d(a) ≥ 0$ et comme $f'(a)$ existe,
  on a $f'_g(a) = f'_d(a)$ donc $f'(a) = 0$.
\end{proof}

\begin{remarque}
  On démontre que si $f$ est croissante (resp. décroissante), alors $f'(x) ≥
  0$ (resp. $f'(x) ≤ 0$.)
\end{remarque}

\subsection{Lemme de Rolle}

\begin{lemme}\label{ana04:lem:Rolle}
  Soit $f$ une fonction continue sur $\intv{a}{b}$, dérivable sur
  $\intv[o]{a}{b}$ et telle que $f(a) = f(b)$. Alors il existe $c \in
  \intv[o]{a}{b}$ tel que $f'(c) = 0$.
\end{lemme}
\begin{proof}
  Si $f$ est constante, alors $\forall x \in \intv[o]{a}{b};\ f'(x) = 0$.

  Si $f$ est non constante, alors l'image de $\intv{a}{b}$ est
  $\intv{\alpha}{\beta}$ avec $\alpha ≠ f(a)$ ou $\beta ≠ f(b)$. Par
  exemple, $\beta = f(c) ≠ f(a)$ et $c ≠ a$ et $c ≠ b$.\\
  $f$ admet donc un maximum en $c$ d'où, comme $c \in \intv[o]{a}{b},\ f'(c)
  = 0$.
\end{proof}

\subsection{Théorème des accroissements finis}

\begin{theoreme}\label{ref:ana04:thm:TAF}
  Soit $f$ une fonction continue sur $\intv{a}{b}$, dérivable sur
  $\intv[o]{a}{b}$. Alors il existe $c \in \intv[o]{a}{b}$ tel que $f'(c) =
  \dfrac{f(b) - f(a)}{b - a}$
\end{theoreme}
\begin{proof}
  Considérons $g \colon \intv{a}{b} \to \R, x \mapsto f(x) - \brk*{
  \dfrac{f(b) - f(a)}{b - a}(x -a) + f(a)}$.\\
  $g$ ainsi construite possède les même propriétés que $f$ et $g(a) = 0 =
  g(b)$. On peut donc appliquer les hypothèses du théorème
  \ref{ana04:lem:Rolle} et donc il existe $c \in \intv[o]{a}{b}$ tel que
  $g'(c) = 0$.

  Calculons $g'(x)$.\\
  $g'(x) = f'(x) - \dfrac{f(b) - f(a)}{b - a}$. La condition $g'(c) = 0$
  équivaut à $f'(c) = \dfrac{f(b) - f(a)}{b - a}$.
\end{proof}

\begin{proposition}[Formule de Taylor-Lagrange]
  Soit $f$ une fonction continue jusqu'à l'ordre $n$ sur $\intv{a}{b}$ et
  telle que $f^(n+1)$ existe sur $\intv[o]{a}{b}$. Alors $\exists c \in
  \intv[o]{a}{b},\ f(b) = \sum_{k=0}^n f^{(k)}(a)
  \dfrac{(b-a)^k}{k!} + f^{(n+1)}(c) \dfrac{(b-a)^{n+1}}{(n+1)!}$
\end{proposition}
\begin{proof}
  On construit $g$ sur $\intv{a}{b}$ en posant pour tout $x \in \intv{a}{b},
  \ g(x) = f(b) - \sum_{k=0}^n f^{(k)}(x)\dfrac{(b-x)^k}{k!} +
  A \dfrac{(b-x)^{n+1}}{(n+1)!}$. On prends $A$ tel que $g(a) = g(b) = 0$.\\
  Ainsi construite $g$ satisfait aux conditions du théorème de Rolle
  (\ref{ana04:lem:Rolle}) et donc il existe $c \in \intv[o]{a}{b}$ tel que
  $g'(c) = 0$.
  \begin{align*}
    \forall x \in \intv[o]{a}{b},\ g'(x) & = - \sum_{k=0}^n
    f^{(k+1)}(x)\dfrac{(b-x)^k}{k!} + \sum_{k=0}^n
    f^{(k)}(x)\dfrac{k(b-x)^{k-1}}{k!} - A \dfrac{(b-x)^n}{n!} \\
    & = - \sum_{k=1}^{n+1}
    f^{(k)}(x)\dfrac{(b-x)^{k-1}}{(k-1)!} + \sum_{k=1}^n
    f^{(k)}(x)\dfrac{(b-x)^{k-1}}{(k-1)!} - A \dfrac{(b-x)^n}{n!} \\
    & = - f^{(n+1)}(x)\dfrac{(b-x)^n}{n!} - A \dfrac{(b-x)^n}{n!} \\
    & = - \dfrac{(b-x)^n}{n!} \brk{A + f^{(n+1)}(x)}
  \end{align*}
  En prenant $x = c$, on obtient que $A = f^{(n+1)}(c)$ et on a donc le
  résultat final.
\end{proof}

\subsection{Corollaires}

\subsubsection{Lien dérivée variation}

\begin{proposition}
  Si $f \in \Cl^0(\intv{a}{b})$ est dérivable sur $\intv[o]{a}{b}$, à
  dérivée nulle (resp. positive, resp. négative) sur $\intv[o]{a}{b}$, alors
  $f$ est constante (resp. croissante, resp. décroissante) sur
  $\intv[o]{a}{b}$.
\end{proposition}
\begin{proof}
  Soit $f \in \Cl^0(\intv{a}{b})$, dérivable sur $\intv[o]{a}{b}$. Pour tout
  $(x,y) \in \intv{a}{b}^2$, on peut appliquer le théorème
  \ref{ref:ana04:thm:TAF} et on obtient que, comme il existe $c \in
  \intv[o]{a}{b}$ tel que $f'(c) = \dfrac{f(y) - f(x)}{y - x} = f'(c) ≥ 0$,
  on a alors $\dfrac{f(y) - f(x)}{y - x} ≥ 0$ et donc la fonction est
  croissante.

  La démonstration se mène de la même façon pour les autres cas.
\end{proof}

\begin{remarque}
  La démonstration montre que si $f'$ est strictement positive, alors $f$
  est strictement croissante. Cependant, si $f$ est strictement croissante,
  $f'$ n'est pas strictement positive : il suffit d'étudier la fonction cube
  pour s'en convaincre.
\end{remarque}

\subsubsection{Dérivabilité par passage à la limite de la dérivée}

\begin{proposition}
  Si $f \in \Cl^0(\intv{a}{b})$, dérivable sur $\intv[o]{a}{b}$ et si
  $\lim_{x \to a}f'(x) = \ell \in \R$ alors $f$ est dérivable
  en $a$ à droite et $f'_d(a) = \ell$.
\end{proposition}
\begin{proof}
  Soit $\varepsilon > 0, \abs*{\dfrac{f(x) - f(a)}{x - a} - \ell} ≤
  \varepsilon$ pour $x \in \intv[l]{a}{b}$.\\
  Le théorème des accroissements finis peut être appliqué à $f$ sur
  $\intv{a}{x}$ et donc $\exists c_x \in \intv[o]{a}{x}$ tel que
  $\dfrac{f(x) - f(a)}{x - a} = f'(c_x)$.

  On s'intéresse alors $\abs{f'(c_x) - \ell} ≤ \varepsilon$.\\
  Or $\forall \varepsilon > 0, \exists \alpha > 0,\ 0 < x - a ≤ \alpha
  \implies \abs{f(x) - b} ≤ \varepsilon$.\\
  Or $c_x \in \intv[o]{a}{x}$, on a donc $0 < c_x - a < x - a ≤ \alpha$. \\
  Donc pour $0 < x -a ≤ \alpha$, on a $\abs{f(x) - \ell} ≤ \varepsilon$,
  d'où $\lim_{\substack{x \to a \\ x > a}} f'(c_x) = \ell$ et
  donc $f'_d(a) = \ell$.
\end{proof}

\begin{remarque}
  Dans ce cas $f$ est continue à droite, mais en général, l'existence de la
  dérivée ne prouve pas sa continuité. Par exemple, $f$ définie par $f(x) =
  x^2 \sin\frac1x$ prolongée par 0 en 0 est continue sur $\R$, dérivable sur
  $\R^*$.
\end{remarque}

\begin{remarque}
  Si la limite du taux d'accroissement est infinie, on a une demi-tangente
  parallèle à l'axe des ordonnées.
\end{remarque}

\section{Dérivées successives}
\subsection{Fonctions de classe $\Cl^k$}

\begin{definition}
  Une fonction est dite de classe $\Cl^k$ sur $I$ lorsque $f$ admet des
  dérivées successives $f^{(i)}$ pour $i \in \{1,…,k\}$, ces fonctions étant
  toutes continues.
\end{definition}

\begin{remarque}
  Si pour tout $k \in \N$, $f$  est de classe $\Cl^k$, on note alors
  $\Cl^{+\infty}$.
\end{remarque}

\subsection{Formule de Leibniz}

\begin{proposition}
  Si $f$ et $g$ sont de classe $\Cl^n$ sur $I$ alors $fg$ est de classe
  $\Cl^n$ et \[ (fg)^{(n)} = \sum_{k=0}^n \binom{n}{k} f^{(k)}g^{(n-k)} \]
  avec la convention $f^{(0)} = f$.
\end{proposition}
\begin{proof}
  Preuve par récurrence.
\end{proof}

\subsection{Dérivée $n$-ième d'une fonction composée}

\begin{proposition}
  Si $f$ est de classe $\Cl^n$ de $I \to J$ et $g$ de classe $\Cl^n$ sur
  $J$, alors $g \circ f$ est de classe $\Cl^n$ sur $I$.
\end{proposition}
\begin{proof}
  Par récurrence sur $n$ \\
  Pour $n = 0$, on $f$ et $g$ continues entraine $g \circ f$ continue.

  Soit $n \in \N$, supposons que $[ f \in \Cl^n(I,J) \wedge g \in \Cl^n
  (J,\R) ] \implies g \circ f \Cl^n(I,\R)$. Considérons alors $F \in
  \Cl^{n+1} (I,J) \wedge G \in \Cl^{n+1} (J,\R)$.\\ $G\circ F$ est dérivable
  et $(G \circ F) ' = F' \times (G'\circ F)$, où $F'$ et $G'$ sont de classe
  $\Cl^n$. On en déduit alors le résultat.
\end{proof}

\subsection{Fonctions réciproques}

\begin{proposition} \label{ana04:prop:continue_derivable_reciproque}
  Soit $n \in \N,\ n ≥ 2$, supposons que $f \in \Cl^n(I,J)$ et $\forall x
  \in I,\ f(x) ≠ 0$, alors $f$ est un $\Cl^n$ diffémorphisme\footnote{une
    bijection dérivable, à dérivée continue et dont la bijection est aussi
  dérivable, à dérivée continue.}
\end{proposition}
\begin{proof}
  Soit $n ≥ 2$. On a alors $f'$ continue sur $I$ et $\forall x \in I,\ f(x)
  ≠ 0$, $f'(x)$ est de signe constant, donc $f$ est monotone. Elle établit
  ainsi une bijection de $I \to J$ et même un homéomorphisme dont on peut
  donner la dérivée : $(f^{-1})' = \dfrac{1}{f' \circ f^{-1}}$ et $f' \circ
  f^{-1}$ est au moins de classe $\Cl^1$, pour $f$ de classe $\Cl^2$.
\end{proof}
