\chapter{Étude affine des arcs paramétrés du plan}

Dans ce chapitre, on confond $\underbrace{\text{point}}_{\in \R^2}$,
$\underbrace{\text{vecteur}}_{\in \R^2}$ et $\underbrace{\text{nombres
complexes}}_{\in \C = \R^2}$.

\section{Fonctions de $\R$ vers $\R^2$ (ou $\C$)}

Soit $f \colon t \mapsto f(t) = (x(t),y(t)) = x(t) + iy(t)$.

\subsection{Limite et continuité}

\begin{definition}
  Soit $f$ définie sur un intervalle $I$ de $\R$ vers $\R^2$ et $a$ un point
  de $I$ ou de son adhérence. On dit que $\ell$ est limite de $f$ en $a$
  lorsque \\
  $\forall \varepsilon > 0,\ \exists \alpha > 0,\ \forall t \in I,\
  \abs{t-a} ≤ \alpha \implies \norm{f(t) - \ell} ≤ \varepsilon \ \text{ou}\
  \abs{f(t) - \ell} ≤ \varepsilon$.
\end{definition}

\begin{proposition}
  Si $\ell$ existe, alors elle est unique.
\end{proposition}
\begin{proof}
  Même démonstration que pour les fonctions de $\R$ dans $\R$.
\end{proof}

\begin{proposition}
  $f$ admet la limite $\ell = \pair{\ell_1}{\ell_2}$ en $a$ si et seulement
  si $x$ admet pour limite $\ell_1$ en $a$ et $y$ admet pour limite $\ell_2$
  en $a$.
\end{proposition}
\begin{proof}
  Si $\lim_a f = \ell = (\ell_1,\ell_2)$, alors \\ $\forall
  \varepsilon > 0,\ \exists \alpha > 0,\ \forall t \in I,\ \abs{t-a} ≤
  \alpha \implies \sqrt{(x(t) - \ell_1)^2 + (y(t) - \ell_2)^2} ≤
  \varepsilon$. Alors, à plus forte raison, $\abs{x(t) - \ell_1} ≤
  \varepsilon$ et $\abs{y(t) - \ell_2} ≤ \varepsilon$.\\ D'où $
  \lim_a f = \ell \implies \lim_a x = \ell_1$ et
  $\lim_a y = \ell_2$.

  Inversement, si $\lim_a x = \ell_1$ et $ \lim_a
  y = \ell_2$, alors $\forall \varepsilon > 0,\ \exists \alpha_1 > 0,\
  \forall t \in I,\ \abs{t-a} ≤ \alpha_1 \implies \abs{x(t) - \ell_1} ≤
  \dfrac{\varepsilon}{\sqrt{2}}$ et $\forall \varepsilon > 0,\ \exists
  \alpha_2 > 0,\ \forall t \in I,\ \abs{t-a} ≤ \alpha_2 \implies \abs{y(t) -
  \ell_2} ≤ \dfrac{\varepsilon}{\sqrt{2}}$. Alors, pour $\alpha =
  \min(\alpha_1,\alpha_2)$, on a $\lim_a f = \ell =
  (\ell_1,\ell_2)$, alors $\forall \varepsilon > 0,\ \exists \alpha > 0,\
  \forall t \in I,\ \abs{t-a} ≤ \alpha \implies \sqrt{(x(t) - \ell_1)^2 +
    (y(t) - \ell_2)^2} ≤ \sqrt{ \dfrac{\varepsilon^2}2 +
  \dfrac{\varepsilon^2}2} = \varepsilon$. D'où la conclusion.
\end{proof}

La conséquence est que les notions utilisant le concept de limite comme la
continuité ou la dérvabilité deviennent des conséquences.

\begin{exemple}
  $\varphi \colon t \mapsto e^{it}$. Pour $t \in \R$, on a $\varphi(t) =
  \cos t + i\sin t$ et $\varphi'(t) = - \sin t +i \cos t = i(i \sin t+ \cos
  t) = ie^{it}$.
\end{exemple}

\begin{remarque}
  Attention : l'absence d'ordre dans $\C$ entraine qu'il n'ya ni signe de la
  dérivée ni fonction croissante, ni décroissante. En particulier, pas de
  théorème de Rolle.
\end{remarque}

On peut étendre l'intégration en posant la définition suivante :

\begin{definition}
  $\int_a^b f(t) \diff t =  \int_a^b x(t) \diff t
  + i \int_a^b y(t) \diff t$.
\end{definition}

En particulier, la formule de Taylor avec reste intégral (proposition
\ref{08ana:prop:taylor_reste_integral}) reste valable. Cela est
essentiellement du à la proposition permettant de majorer une intégrale.

\begin{proposition}
  $\abs*{\int_a^b f(t) \diff t} ≤  \int_a^b
  \abs{f(t)} \diff t$.
\end{proposition}
\begin{proof}
  $\int_a^b f(t) \diff t = e^{i\alpha} \abs*{
  \int_a^b f(t) \diff t}$ (forme trigonométrique) \\
  $\abs*{\int_a^b f(t) \diff t} = e^{-i\alpha}
  \int_a^b f(t) \diff t \in \R_+$ \\
  $\abs*{\int_a^b f(t) \diff t} =  \int_a^b \Re(
  e^{-i\alpha} f(t) ) \diff t$\\
  Or $\Re(e^{-i\alpha} f(t)) ≤ \abs*{e^{-i\alpha} f(t)} ≤ \abs*{f(t)}$.\\
  D'où $\abs*{\int_a^b f(t) \diff t} ≤  \int_a^b
  \abs{f(t)} \diff t$.
\end{proof}

Une conséquence de cette formule de Taylor avec reste intégral est que pour
tout $t \in \R, \abs*{e^{it} - \sum_{k=0}^n \dfrac{i^k}{k!}t^k
} ≤ \dfrac{\abs{t}^{n+1}}{(n+1)!}$ et donc que $\lim_{n \to
+\infty} \sum_{k=0}^n \dfrac{(it)^k}{k!} = e^{it}$.

\section{Arcs paramétrés}

\subsection{Définition}

\begin{definition}
  On appelle arc paramétré de classe $\Cl^k$ tout couple $(I,f)$ où $I$ est
  un intervalle de $\R$ et $f \colon I \to \R^2$ (ou $\R^3$) de classe
  $\Cl^k$.
\end{definition}

Du point de vue cinématique, $I$ est l'intervalle de temps et $f$ donne la
position en fonction de la date. On écrit parfois $f(t) = M(t)$.

L'ensemble $\setcond{f(t)}{t\in I}$ est le support de l'arc paramétré. On
parle aussi de trajectoire.

\begin{exemple}
  \leavevmode
  $f \colon \begin{cases} x(t) = \cos t \\ y(t) = \sin t\end{cases}, t \in
  \intv{0}{\pi}$.

  $g \colon \begin{cases} x(u) = \dfrac{1 - u^2}{1+u^2} \\ y(u) =
  \dfrac{2u}{1+u^2} \end{cases}, u \in \R_+$.
\end{exemple}

\begin{definition}
  On dit que $(J,g)$ est un (re)paramétrage de classe $\Cl^k$ admissible de
  $(I,f)$ lorsqu'il existe $\varphi$, une bijection de classe $\Cl^k$ telle
  que $\varphi(I) = J$ et $f = g \circ \varphi$
\end{definition}

\begin{exemple}
  Dans l'exemple précédent, on passe de $f$ à $g$ en posant $u = \tan
  \dfrac{t}2$.
\end{exemple}

\subsection{Propriétés de symétries}

L'étude des symétries est en fait l'étude des reparamétrage par $t \to -t$.

Dans le cas de $f$ périodique, on peut limiter l'étude à une période.

\subsection{Étude affine des arcs}

En particulier dans cette section, on peut parler des points d'inflexion et
des points de rebroussement de première et deuxième espèce.

% voir pdf Étude affine des arcs paramétrés du plan page 7

\subsection{Branches infinies}

\subsection{Point double}

\begin{definition}
  Un point $A$ est dit \emph{point double} de l'arc paramétré s'il existe
  $t_1$ et $t_2$ dans $I$ distincts tels que $M(t_1) = M(t_2) = A$
\end{definition}

\begin{exemple}
  $x(t) = t^2 - 2t$ et $y(t) = t^2 + \dfrac1{t^2}$, $t\in \R^*$.
\end{exemple}

\section{Quelques notions sur les coniques}

\subsection{Définition bifocale de l'ellipse}

\section{Coordonnées polaires}

\subsection{Notions sur les coordonnées polaires}

\subsection{Arcs paramétrés définis par une équation polaire}

\subsection{Coniques dont un foyer est placé au pôle}

\subsection{Étude générale de $r = f(\theta)$}
