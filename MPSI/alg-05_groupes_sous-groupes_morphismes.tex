\chapter{Groupes, sous-groupes et morphismes}
\label{alg05}

Soit $E$ un ensemble, $*$ une loi de composition interne, c'est-à-dire une
application de $E × E \to E,\ (x,y) \mapsto *(x,y) = x*y$.

\section{Généralités}

\subsection{Définition}

\begin{definition}
  On appelle \emph{magma} la donnée d'un ensemble et d'une loi de
  composition interne. On note $(E,*)$.
\end{definition}

\begin{definition}[groupe]
  Un \emph{groupe} est un magma $(E,*)$ tel que
  \begin{itemize}
    \item $\forall (a,b,c) \in E^3,\ (a*b)*c = a*(b*c)$ (associativité)
    \item $\exists e \in E \mid \forall a \in E,\ e * a = a * e = a$
      (neutre)
    \item $\forall a \in E, \exists b \in E \mid a * b = b * a = e$
      (symétrique)
  \end{itemize}
\end{definition}

\begin{remarque}
  Le symétrique s'appelle
  \begin{itemize}
    \item opposé pour la  loi +
    \item inverse pour la loi $×$
    \item réciproque pour la loi $\circ$
  \end{itemize}
\end{remarque}

\begin{definition}[commutativité]
  La loi $*$ est dite \emph{commutative} lorsque $\forall (a,b) \in E^2,\
  a*b = b*a$.
\end{definition}

\begin{remarque}
  Un groupe commutatif est dit \emph{abélien}.
\end{remarque}

\begin{exemple}[usuels]
  \begin{itemize}
    \item $(E,+)$ avec $E \in \{ \R,\Q,\Z,\C \}$
    \item $(E,×)$ avec $E \in \{ \R^*,\Q^*,\C^* \}$
    \item $(P(E),\Delta)$
    \item $(\mathscr{T},\circ)$ le groupe des translations du plan
  \end{itemize}
\end{exemple}

\subsection{Propriétés}

\begin{proposition}[unicité du neutre]
  Si $e$ et $f$ sont neutres pour $(E,*)$, alors $e = f$.
\end{proposition}
\begin{proof}
  $e*f = e$ et aussi $e*f = f$ donc $e = f$.
\end{proof}

\begin{proposition}[régularité]
  Dans un groupe $(G,*)$ tout élement est régulier (simplifiable.)
  $\forall (x,y,z) \in G^3, x * y = x * z \implies y = z$ et $y * x = z = *x
  \implies y = z$.
\end{proposition}
\begin{proof}
  Supposons que pour $(x,y,z) \in G^3$, on ait $x*y = x*z$. En composant à
  gauche par $x^{-1}$, on a $x^{-1} * x * y = x^{-1} * x * z$ et donc $x =
  z$.
\end{proof}

\begin{proposition}
  Soient $(a,b) \in G^2$. L'équation $a *x = b$ d'inconnue $x \in G$ possède
  une unique solution $x = a^{-1}b$.
\end{proposition}
\begin{proof}
  Par composition à gauche par $a^{-1}$
\end{proof}

\begin{proposition}[symétrique de $a*b$] \label{alg-05:prop:symetrique}
  $\forall (a,b) \in (G,*)^2,\ (a*b)^{-1} = b^{-1} * a^{-1}$
\end{proposition}
\begin{proof}
  Calculons $(a*b) * (b^{-1} * a^{-1})$. On a $(a*b) * (b^{-1} * a^{-1}) =
  a*b * b^{-1} * a^{-1} = a * e * a^{-1} = e$
\end{proof}

\begin{remarque}
  On déduit de cette dernière proposition (\ref{alg-05:prop:symetrique})
  l'unicité du symétrique.
\end{remarque}

\section{Sous groupes}

On considère ici, que $(G,*)$ est un groupe et que $H \subset G)$.

\subsection{Définition}

\begin{definition}
  On dit que $H$ est un sous groupe de $(G,*)$ lorsque :
  \begin{itemize}
    \item $H$ est stable par $*$
    \item $(H,*_{|_H})$ est un groupe
  \end{itemize}
\end{definition}

\begin{exemple}
  \begin{itemize}
    \item $(G,*)$ est un sous groupe de $(G,*)$
    \item $(\{e\},*)$ est un sous groupe de $(G,*)$ ($G ≠ ø$)
    \item $Z$ est un sous groupe de $\Q$ pour l'addition, $\Q$ de $\R$, et
      $\R$ de $\C$.
  \end{itemize}
\end{exemple}

\begin{proposition}
  Le neutre de $(H,*)$ est le neutre de $(G,*)$
\end{proposition}
\begin{proof}
  Si $f$ est neutre de $(H,*)$, alors $f*f = f$ dans $H$ et donc dans $G$,
  $f^{-1}*(f*f) = f^{-1}*f \iff f = e$.
\end{proof}

\subsection{Caractérisation}

\begin{proposition}
  Une partie $H$ de $(G,*)$ est un groupe  de $(G,*)$ est un sous groupe si
  et seulement si
  \begin{itemize}
    \item $e \in H$
    \item $H$ est stable par $*$
    \item tout élement de $H$ a son symétrique dans $H$.
  \end{itemize}
\end{proposition}
\begin{proof}
  Dans le sens direct OK

  Sens réciproque \\
  Supposons les 3 points. $H$ est donc stable, $e$ est le s neutre dans $H$
  et tout élément de $H$ possède un symétrique.\\
  Soit $(a,b,c) \in H^3$. On a donc dans $G,\ (a*b)*c = a*(b*c)$. Or la
  stabilité de $H$ fait que le résultat est également vrai dans $H$, car
  $(a*b) \in H$ et $(b*c) \in H$.
\end{proof}

\begin{exemple}
  Dans $(\R,+)$, on considère $G = \{ a + b\sqrt{2} , (a,b) \in \Z^2 \}$.
  $G$ est un groupe. En effet,
  \begin{itemize}
    \item $0 \in G$
    \item $\forall (a,b,a',b') \in Z^4, \ a + b \sqrt{2} + a' + b'\sqrt{2} =
      a + a' + (b+b')\sqrt{2}$
    \item $-(a + b \sqrt{2}) = -a + (-b)\sqrt{2} \in G$
  \end{itemize}
  $(G,+)$ est donc bien un sous groupe de $(\R,+)$.
\end{exemple}

\begin{proposition}
  Une partie $H$ est un sous groupe de $G$ si et seulement si
  \begin{enumerate}
    \item $H ≠ ø$
    \item $\forall (a,b) \in H^2,\ a*b^{-1} \in H$
  \end{enumerate}
\end{proposition}
\begin{proof}
  On suppose 1. et 2.\\
  Soit $a \in H$, ce qui est possible par 1. On a, d'après 2. $a*a^{-1} = e
  \in H$.\\
  D'après 2. à  nouveau, $\forall b \in H,\ e*b^{-1} = b^{-1}$ et donc
  $b^{-1} \in H$.\\
  Enfin, comme $b^{-1} \in H$, on a $a * \brk{b^{-1}}^{-1} = a*b \in H$. et
  donc $H$ est stable.
\end{proof}

\begin{question}
  Soit $E = \{ x + i y, (x,y) \in Q^2 \}$.

  Montrer que $(E,+)$ est un groupe.
\end{question}
\begin{solution}
  \begin{itemize}
%   \item $E \subset \C$ et $(\C,+)$ est un groupe.
    \item $0 = 0 + 0i$ donc $E ≠ ø$.
    \item Soient $z$ et $z'$ dans $E$, $z = x + iy, \ z' = x' + iy'$,
      étudions $z - z'$\\
      $z - z' = x - x' + (y - y')i$ et $x - x \in Q$ et $y - y' \in Q$, donc
      $z - z' \in E$.
  \end{itemize}
\end{solution}

\begin{question}
  Soit $E = \{ a + b\sqrt{2} , (a,b) \in \Q^2 \setminus \{(0,0)\} \}$.

  Montrer que $(E,×)$ est un groupe.
\end{question}
\begin{solution}
  Même méthode que le précédent.
\end{solution}

\subsubsection{Sous groupes de $\Z$}

\begin{proposition}\label{05alg:prop:sousgroupes_Z}
  Les seuls sous groupes de $\Z$ sont les $a\Z$, pour $a \in \Z$.
\end{proposition}
\begin{proof}
  Le fait que les $a\Z$ soient des sous groupes de $Z$ se vérifie avec la
  définition. Montrons que ce sont les seuls.

  Soit $H$ un sous groupe de $\Z$.\\
  Si $H = \{ 0 \}$, alors $H = 0\Z$ convient.\\
  Si $H ≠ \{ 0 \}$, alors il existe $h \in H$. Si $h$ est négatif, alors
  $-h$ est positif et dans tous les cas $H \cap \N^*$ est non-vide. D'après
  la proposition (\ref{}), on a l'existe de $a = \min(H \cap \N^*)$.

  Soit $h \in H \cap \N^*$, divisons $h$ par $a$ : $h = aq + r, \ 0 ≤ r <
  a$.\\
  $r = h - aq$ et donc, comme $h \in H$, $a \in H$, $aq \in H$ et par
  récurrence, $r = 0$ et donc $h = aq$. Donc $h = a\Z$.\\
  On peut procéder de même pour les nombres négatifs en considérant $-h$.

  On a donc $H \subset a\Z$. Mais comme $a \in H,\ a\Z \subset H$, donc $H =
  a\Z$.
\end{proof}

\subsubsection{Sous groupes additifs de $\R$}

\begin{proposition} Dans $(\R,+)$ les sous groupes sont soit :
  \begin{itemize}
    \item discret de la forme $m\Z,\ m \in \R_+$
    \item dense dans $\R$
  \end{itemize}
\end{proposition}
\begin{proof}
  Soit $H$ un sous groupe de $\R$.\\
  Si $H = \{ 0 \}$, alors $H = 0\Z$ convient.\\
  Si $H ≠ \{ 0 \}$, $H \cap \R_+^*$ est non vide. Soit $m = \inf(H \cup
  \R_+^*)$. On a $m ≥ 0$, d'après ce qui précède, car $H \cap \R_+^*$ est
  non vide minoré par 0.
  \begin{enumerate}[label=\alph*)]
    \item Si $m > 0$, montrons que $H = m\Z$.

      Si $m \notin H,\ \exists h \in H,\ m < h < \frac32h$ et si on pose
      $\varepsilon = h - m,\ \exists h' \in H,\ m < h' < m + \varepsilon =
      h$.\\
      Considérons $h - h' > 0$.\\
      $0 < h - h' < \frac12m$, or $h - h' \in H \cap \R_+^*$, cela contredit
      l'hypothèse $m = \inf H \cap \R_+^*$. Do'ù $m \in H$.

      On a donc $m\Z \in H$.

      Inversement, soit $h \in H \cap \R_+^*$.\\
      $h = mq + r,\ 0 ≤ r < m \implies r = h - mq$ et comme $h \in H$ et $m
      \in H$, on en déduit par récurrence que $mq \in H$ et donc $r \in H$
      et donc $r = 0$. Par un raisonnement analogue puor $h$ négaitf, on
      aboutit à $H \subset m\Z$ et donc $H = m\Z$.
    \item Si $m = 0$, montrons que $H$ est dense dans $\R$.

      Soient $(x,y) \in \R^2,\ x < y$. Il s'agit de montrer l'existence de
      $h$ tel que $x < h <y$.\\
      Comme $y - x > 0,\ \exists h_1 \in H$ tel que $0 < h_1 < y - x$.
      Posons $n = \lfloor \frac{x}{h_1} \rfloor$. Par définition, on a $n ≤
      \frac{x}{h_1} < n + 1$ et donc $nh_1 ≤ x < (n+1)h_1$. Or $y - nh_1
      -h_1 ≥ y - w - h_1 > 0$, d'où $x  < (n+1)h < y$. Comme $h_1 \in H,\
      (n+1)h_1 \in H$.

      $H$ est donc dense dans $\R$.
  \end{enumerate}
\end{proof}

\begin{exemple}
  Il existe des fonctions numériques réelles dont le groupe des périodes est
  dense dans $\R$, comme $\mathbf{1}_{\Q}$ la fonction caractéristique des
  rationnels. En effet, $\forall x \in \R,\ \forall r \in Q,\
  \mathbf{1}_{\Q}(x + r) = \mathbf{1}_{\Q}(x)$.
\end{exemple}

\subsubsection{Intersection de sous groupes}

\begin{theoreme}\label{05alg:thm:intersection_ss_grpes}
  Si $H_1$ et $H_2$ sont des sous groupes de $(G,*)$, alors $H_1 \cap H_2$
  est un sous groupe de $(G,*)$.

  De plus, si $(H_i)_{i\in I}$ est une famille de sous groupes de $(G,*)$,
  alors $\bigcap_{i \in I} H_i$ est un sous groupe.
\end{theoreme}
\begin{proof}
  $e \in H_1 \wedge e \in H_2 \implies e \in H_1 \cap H_2$ et donc $H_1 \cap
  H_2 ≠ ø$.\\
  Soient $(x,y) \in (H_1 \cap H_2)^2$. $x*y^{-1} \in H_1$ et $x*y^{-1} \in
  H_2$ donc $x*y^{-1} \in H_1 \cap H_2$. On en déduit que $H_1 \cap H_2$ est
  un sous groupe de $G$.

  La propriété est vraie lorsqu'on passe à une intersection infinie, car
  $(x,y) \in \brk*{\bigcap_{i \in I} H_i}^2 \implies \forall i
  \in I,\ x*y^{-1} \in H_i$.
\end{proof}

\begin{exemple}
  $2\Z \cap 3\Z = 6\Z$.
\end{exemple}

\subsubsection{Sous groupe engendré par une partie}

\begin{definition}[sous groupe engendré par une partie]
  Soient $(G,*)$ un groupe et $A$ une partie de $G$. On appelle le sous
  groupe de $G$ engendré par $A$, noté ($\gr A$) l'intersection de tous les
  sous groupes contenant $A$. \[ \gr A = \bigcap_{\substack{H\
  \text{sous groupe de}\ G \\ A \subset H}} H . \]
\end{definition}

\begin{remarque}
  $\gr A$ est un bien un sous groupe, car c'est une intersection de sous
  groupes.
\end{remarque}

\begin{exemple}
  \begin{itemize}
    \item $\gr(\{2,3\}) = \Z$, car $3 - 2 = 1 \implies 1\Z \subset \Z$.
    \item $\gr(\{2,4\}) = 2\Z$
  \end{itemize}
\end{exemple}

\begin{theoreme}
  $\gr(A)$ est l'ensemble des composés finis des éléments de $A$ et de leur
  symétrique.
\end{theoreme}
\begin{proof}
  Provisoirement admis.
\end{proof}

\section{Morphismes de groupes}

\subsection{Définition}

\begin{definition}[morphisme de groupe] \label{alg05:def:morphisme}
  Soient $(E,*)$ et $(F,\top)$ deux magmas et $\varphi \colon E \to F$.

  On dit que $\varphi$ est un morphisme de $(E,*)$ vers $(F,\top)$ lorsque :
  $\forall (x,y) \in E^2,\ \varphi(x*y) = \varphi(x) \top \varphi(y)$.

  Si de plus, $E = F$, on parle d'endomorphisme, si $\varphi$ est bijective,
  on parle d'isomorphisme et si les deux conditions sont remplies, on parle
  d'automorphisme.
\end{definition}

\begin{proposition}
  Si $\varphi$ est un isomorphisme de $(E,*)$ vers $(F,\top)$, alors
  $\varphi^{-1}$ est un isomorphisme de $(F,\top)$ vers $(E,*)$.
\end{proposition}
\begin{proof}
  Soient $(x',y') \in F^2,\ \exists (x,y) \in E^2,\ x' = \varphi(x) \wedge
  y' = \varphi(y)$.

  $\varphi^{-1}(x' \top y') = \varphi^{-1}(\varphi(x) \top \varphi(y)) =
  \varphi^{-1}(\varphi(x * y)) = x * y = \varphi^{-1}(x') *
  \varphi^{-1}(y')$.
\end{proof}

\begin{proposition}
  Soient $G_1$ et $G_2$ deux groupes et $\varphi$ un morphisme de $G_1 \to
  G_2$. Les assertions suivantes sont vraies :
  \begin{enumerate}[labelsep=0.7pc,label=(\roman*)]
    \item L'image du neutre de $G_1$ par $\varphi$ est le neutre de $G_2$.
    \item L'image du symétrique est le symétrique de l'image.
    \item $\forall x \in G_1, \forall n \in \Z,\ \varphi(x^n) =
      \brk*{\varphi(x)}^n$.
  \end{enumerate}
\end{proposition}
\begin{proof}
  \begin{enumerate}[labelsep=0.7pc,label=(\roman*)]
    \item $e_1*e_1 = e_1 \implies \varphi(e_1) \top \varphi(e_1) =
      \varphi(e_1)$ et en simplifiant $\varphi(e_1) = e_2$.
    \item Dans $G_1$, $x * x^{-1} = e_1$ d'où $\varphi(x * x^{-1}) =
      \varphi(e_2) \iff \varphi(x) \top \varphi(x^{-1}) = e_2$. On en déduit
      donc que $\varphi(x^{-1}) = (\varphi(x))^{-1}$.
    \item Par récurrence pour $n \in \N$, puis on se ramène à ce cas.
  \end{enumerate}
\end{proof}

\subsection{Noyau et image}

\begin{proposition}
  Soient $(G_1,*)$ et $(G_2,\top)$ deux groupes et $\varphi \colon G_1 \to
  G_2$ un morphisme.

  Si $H$ est un sous groupe de $G_1$, alors $\varphi(H)$ est un sous groupe
  de $(G_2,\top)$.

  Pour tout $F$ sous groupe de $(G_2,\top)$, $\overset{-1}{\varphi}(F)$ est
  un sous groupe de $(G_1,*)$.
\end{proposition}
\begin{proof}
  Soit $H$ un sous groupe de $G_1$. $e_1 \in H$ donc $e_2 = \varphi(e_1) \in
  \varphi(H)$ et donc $\varphi(H) ≠ ø$.\\
  Soient $(x',y') \in \varphi(H)^2,\ \exists (x,y) \in H^2,\ x' = \varphi(x)
  \wedge y' = \varphi(y)$.

  \begin{align*}
    x' \top (y')^{-1} & = \varphi(x) \top \varphi(y)^{-1} \\
                      & = \varphi(x) \top \varphi(y^{-1})  \\
                      & = \varphi(x * y^{-1})
  \end{align*}
  Or $x * y^{-1} \in H$ donc $x' \top (y')^{-1} \in \varphi(H)$.

  Soit $F(,\top)$ un sous groupe de $(G_2,\top)$. $e_2 \in F$ donc $e_1 \in
  \overset{-1}{\varphi}(F)$ et donc $\overset{-1}{\varphi}(F) ≠ ø$.\\
  \begin{align*}
    \varphi(x * y^{-1}) & = \varphi(x) \top \varphi(y^{-1}) \\
                        & = \varphi(x) \top \varphi(y)^{-1}
  \end{align*}
  Comme $\varphi(x) \in F$ et $\varphi(y) \in F$, on a que $\varphi(x) \top
  \varphi(y)^{-1} \in F$ et donc $x * y^{-1} \in \overset{-1}{\varphi}(F)$.

  Conclusion, $\overset{-1}{\varphi}(F)$ est un groupe.
\end{proof}

\subsubsection{Cas particulier}

\begin{definition}[image]
  $\varphi(G_1)$ est l'\emph{image} de $\varphi$. On note $\Im \varphi = \{
  \varphi(x), x \in G_1 \}$.
\end{definition}

\begin{definition}[noyau]
  $\overset{-1}{\varphi}(\{e_2\})$ est le \emph{noyau} de $\varphi$. On
  note $\ker \varphi = \{ x \in G_1 \mid \varphi(x) = e_2 \}$.
\end{definition}

\begin{proposition}\label{alg05:prop:injectivite}
  Soit $\varphi$ un morphisme de $(G_1,*)$ vers $(G_2,\top)$. $\varphi$ est
  injectif si et seulement si $\ker \varphi = \{ e_1 \}$.
\end{proposition}
\begin{proof}
  Si $\varphi$ est injectie, on sait que $\varphi(e_1) = e_2$ et que $e_2$
  est la seule solution. On a donc $\ker \varphi = \{ e_1 \}$.

  Si $\ker \varphi = \{ e_1 \}$, démontrons que $\varphi$ est injective.\\
  Soit $(x,y) \in G_2^2$. Supposons que $\varphi(x) = \varphi(y)$. Alors on
  a successivement $\varphi(x) = \varphi(y) \iff \varphi(x) \top
  \varphi(y)^{-1} = e_2 \iff \varphi(x * y^{-1}) = e_2$ et donc $x * y^{-1}
  = e_1 \iff x = y$ et donc $\varphi$ est injective.
\end{proof}

\begin{remarque}
  Lorsque $\ker \varphi ≠ \{ e_1 \}$, l'équation $\varphi(x) = y$ pour $y$
  donné possède soit aucune solution, soit une solution $x_0$ et toutes les
  solutions de la forme $n * x_0,\ n \in \ker \varphi$.
\end{remarque}

\subsection{Transport de structures}

\begin{proposition}
  Soit $\varphi$ un morphisme de $(G_1,*) \to (G_2,\top)$. Si $\varphi$ est
  surjective et $(G_1,*)$ un groupe, alors $(G_2,\top)$ est un groupe.
\end{proposition}
\begin{proof}
  \begin{itemize}
    \item Soit $(x',y',z') \in G_2^3$. $\exists (x,y,z) \in G_1^3,\ x' =
      \varphi(x),\ y' = \varphi(y),\ z' = \varphi(z)$.\\
      $x' \top (y' \top z') = x' \top \varphi(y * z) = \varphi(x * y * z) =
      \varphi(x * y) \top z' = (x' \top y') \top z'$
    \item Les deux autres points de démontrent aisément.
  \end{itemize}
\end{proof}

\begin{question}
  Étudier la structure de $\R$ muni de $x*y = \sqrt[3]{x^3 + y^3}$ et $\R
  \setminus \{ -1 \}$ de $a*b = a + b + ab$.
\end{question}
\begin{solution}
  $x \mapsto \sqrt[3]{x}$ est un bijection et $x \mapsto x + 1$ aussi en
  remarquant que $a + b + ab + 1 = (a + 1)(b+1)$.
\end{solution}
