\chapter{Propriétés métriques des courbes planes}

Ici $\R^2$ est muni d'une structure euclidienne et l'espace $\R^3$ est
orienté par le choix d'une base orthonormée directe.

\section{Exponentielle complexe, théorème de relèvement}

\subsection{Rappel}

\begin{proposition}
  Pour $z \in \C,\ z = x + iy,\ (x,y) \in \R^2$.\\
  $\exp(z) = e^{x}(\cos y + i \sin y)$\\
  $\exp$ est un morphisme de $(\C,+) \to (\C,×)$.
\end{proposition}
\begin{proof}
  \begin{align*}
    \exp(z + z') & = \exp( x + x' + i(y +y'))                      \\
                 & = e^{x+x'}(\cos(y+y') +i\sin(y+y'))             \\
                 & = e^{x+x'}(\cos y + i\sin y)(\cos y' +i\sin y') \\
                 & = \exp{z} × \exp(y')                            \\
  \end{align*}
\end{proof}

Pour $z \in \R,\ \exp(z) = e^z$.\\
Pour $z \in \C\setminus\R,\ \exp(z) = \exp(iy) = e^{iy}$.\\
$\abs*{e^z} = e^{\Re(z)}$.\\
$\arg(e^z) \equiv \Im z \pmod{2\pi} $.

\begin{theoreme}
  L'application $\varepsilon \colon \R \to \C,\ \theta \mapsto e^{i\theta}$
  établit une bijection de $\intv[o]{-\pi}{\pi}$ sur $\U\setminus\set{-1}$.
  $\varepsilon$ est continue et sa réciproque est continue.
\end{theoreme}
\begin{proof}
  $\varepsilon(\theta) = \cos\theta + i\sin\theta$. $\varepsilon$ est
  $\Cl^0$ sur $\intv[o]{-\pi}{\pi}$ et surjective sur $\U\setminus\set{-1}$.

  Soit $z \in \U\setminus\set{-1}$.\\
  $z = x + iy,\ (x,y) \in \R^2,\ x^2 + y^2 = 1$\\
  $\exists \theta$ défini à $2\pi$ près tel que $\cos\theta = x$ et
  $\sin\theta = y$. Comme $z ≠ -1$, $\theta$ est unique dans
  $\intv[o]{-\pi}{\pi}$.\\
  $\varepsilon$ est donc une bijection.
  \begin{center}
    \begin{tikzpicture}[>=latex,scale=2]
      \draw [->] (-1,0) -- (1,0) ;
      \draw [->] (0,-1) -- (0,1) ;
      \draw plot [domain = 0:360,smooth] ({cos(\x)},{sin(\x)}) ;

      \draw (0,0) -- (0.866,0.5) ;
      \draw (-1,0) -- (0.866,0.5) ;
    \end{tikzpicture}
  \end{center}
  On peut poser $\theta = 2\arctan \frac{y}{1+x} \in \intv[o]{-\pi}{\pi}$.
  Les fonctions $\arctan$ et $(x,y) \mapsto \frac{y}{1+x}$ étant continues,
  la fonction $\varepsilon^{-1}$ est continue.
\end{proof}

\begin{theoreme}[de relèvement]
  Soit $I \subset \R,\ f \colon I \to \U$ de classe $\Cl^p,\ p≥ 1$.\\
  Alors \begin{enumerate}
    \item il existe $\varphi \colon I \to \R$ de classe $\Cl^p$ telle que
      $\forall t \in I,\ f(t) = e^{\varphi(t)}$ et on dit que $\varphi$ est
      \emph{un relèvement} de $f$.
    \item Si $\varphi_1$ et $\varphi_2$ sont deux relèvements de $f$, alors
      $\exists k \in \Z$ tel que $\forall t \in I, \varphi_2(t) -
      \varphi_1(t) = k2\pi$.
  \end{enumerate}
\end{theoreme}
\begin{proof}
  Admis
\end{proof}

\begin{theoreme}
  Soit $\Gamma = (I,f)$ un arc paramétré de classe $\Cl^k,\ k ≥ 1$ tel que
  $f \colon I \to \R^2 \setminus \set{\pair{0}{0}}$.\\
  Alors il existe un couple $(\rho,\theta)$ de fonctions de classe $\Cl^k$
  telles que $\forall t \in I,\ f(t) = \rho(t)e^{i\theta(t)}$.
\end{theoreme}
\begin{proof}
  Soit $g \colon t \mapsto \frac1{\norm{f(t)}}f(t)$. $g$ est de $I$ dans
  $\U$ et par récurrence, on montre que $g$ est de classe $\Cl^k$. D'après
  le théorème de relèvement, $\exists \theta$ tel que $\forall t \in I, g(t)
  = e^{i\theta(t)}$ et donc $\forall t \in I, f(t) = \norm{f(t)}
  e^{i\theta(t)}$.

  L'application $t \mapsto \theta(t)$ est à dérivée non nulle, donc
  monotone. C'est alors un $\Cl^k$ difféomorphisme et donc on peut
  considérer $\theta$ comme paramètre.
\end{proof}

\section{Longueur d'un arc, abscisse curviligne}

\subsection{Abscisse curviligne}

\begin{definition}
  Soit $\Gamma = (I,f)$ un arc paramétré de classe $\Cl^k,\ k ≥ 1$.\\
  On dit que $s$ est une abscisse curviligne de $\Gamma$ lorsque $s$ est une
  application de $I$ vers $\R$ telle que $\forall t \in I, s'(t) =
  \norm{f'(t)}$
\end{definition}

En cinématique, on écrit souvent $s(t) = \int_{t_0}^t \norm{f'(t)} \diff t$.

\begin{exemple}[L'astroïde (hypocycloïde à 4 points de rebroussement)]
  $f(t) = \begin{cases}x(t) = a \cos^3 t \\ y(t) = a \sin^3 t \end{cases}$
  admet pour dérivée $\begin{cases}x'(t) = -3a \cos^2 t \sin t \\ y(t) = 3a
  \sin^2 t \cos t \end{cases}$ et donc $\norm{f'(t)}^2 = 9a^2 \sin^2 t +
  \cos^2 t$. On peut donc poser $s(t)$ comme primitive de $3a \abs{\cos t
  \sin t}$.

  On peut enfin calculer la longueur en remarquant que la longueur est
  quatre fois celle obtenue en parcourant $\intv*{0}{\frac{\pi}2}$.
  \begin{align*}
    \frac{\ell}4 & = \frac{3a}2 \int_0^{\frac{\pi}2} \sin 2t \diff t \\
                 & = -\frac{3a}4 \brk[s]*{\cos 2t}_0^{\frac{\pi}2}   \\
                 & = -\frac{3a}4 × -2                                \\
  \end{align*}
  On en déduit que $\ell = 6a$.
\end{exemple}

\subsection{Le repère de Freinet}

On suppose que $\Gamma = (I,f)$ est un paramètré de classe $\Cl^k,\ k ≥ 1$
régulier, c'est-à-dire que $\forall t \in I,\ \norm{f(t)} ≠ 0$. Dans ce cas,
$s'(t)$ est strictement positive et $s$ est strictement croissante.\\
$g = f \circ s^{-1}$ est un reparamétrage admissible de de classe $\Cl^k$ et
on dit que c'est un paramètrage normal de $\Gamma$.\\
On a $\frac{\diff g}{\diff s} = \frac{\diff f \circ s^{-1}}{\diff s^{-1}} ×
\frac{\diff s^{-1}}{\diff s}$ d'où
\begin{align*}
  \norm*{\frac{\diff g}{\diff s}} &= \norm*{ f'(s^{-1})  \frac{\diff s^{-1}}{\diff s}} \\
                                  &= \norm*{ f'(t) \frac{\diff t}{\diff s}} \\
                                  &= \abs*{\frac{\diff t}{\diff s}} × \norm*{ f'(t)} \\
                                  &= \abs*{\frac{\diff t}{\diff s}} × \abs*{\frac{\diff s}{\diff t}} = 1 \\
\end{align*}
On pose alors $\vv{T} = \frac{\diff \vv{g}}{\diff s}$ et on a $\norm{\vv{T}}
= 1$. On choisit ensuite $\vv{N}$ orthogonal à $\vv{T}$.

\begin{definition}[Repère de Freinet]
  Le repère mobile $(M,\vv{T},\vv{N})$ où $(\vv{T},\vv{N})$ est une base
  orthonormale directe s'appelle repère de Freinet.
\end{definition}

\begin{theoreme}
  Soit $g$ de $J \to \R^2$ un paramètrage normal de $\Gamma$, de classe
  $\Cl^k,\ k ≥ 2$. Il existe une fonction $\alpha$ de $J \to \R$, de classe
  $\Cl^{k-1}$ telle que $\forall s \in J,\ \vv{T}(s) = \cos \alpha(s)
  \vv{\imath} + \sin \alpha(s) \vv{\jmath} = e^{i\alpha(s)}$.
\end{theoreme}
\begin{proof}
  $\alpha$ est un relèvement de la fonction $s$
\end{proof}

\subsection{Courbure}

Localement, la courbe ressemble à un arc de cercle.

Ici, $\Gamma = (I,f)$ est un arc paramétré birégulier, de classe $\Cl^k,\ k
≥ 2$.

\begin{definition}[courbure et rayon de courbure]
  La \emph{courbure} en $M(s)$ est $\gamma(s) = \frac{\diff \alpha}{\diff
  s}$ et le \emph{rayon de courbure} $R(s) = \frac{\diff s}{\diff \alpha}$.
\end{definition}

\begin{proposition}[Première et deuxième formules de Freinet]
  $\frac{\diff \vv{T}}{\diff s} = \gamma \vv{N}$

  $\frac{\diff \vv{N}}{\diff s} = -\gamma \vv{T}$
\end{proposition}

\begin{definition}[centre de courbure et développée]
  Le \emph{centre de courbure} est le point $I$ défini par $I = M +
  R\vv{N}$.\\
  L'ensemble des points $I$ est la \emph{développée de $\Gamma$}.
\end{definition}
