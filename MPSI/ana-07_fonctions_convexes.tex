\chapter{Fonctions convexes}

\section{Cas général}

\subsection{Premières définitions}

\begin{definition}
  Une partie $U$ de $\R^2$ est dite convexe lorsque $\forall (A,B) \in U^2,\
  [AB] \subset U$.
\end{definition}

\begin{definition}[épigraphe]
  Soit $f$ une fonction définie sur $I$ un intervalle de $\R$. On appelle
  \emph{épigraphe} de $f$ l'ensemble $\mathscr{E}_f = \{ (x,y) \in \R^2 \mid
  x \in I,\ y ≥ f(x) \}$.
\end{definition}

\begin{definition}[fonction convexe]
  Soit $I$ un intervalle de $\R$ et $f \colon I \to \R$. On dit que $f$ est
  \emph{convexe} lorsque son épigraphe est convexe.
\end{definition}

\subsection{Position des cordes}

\begin{proposition}
  $f$ est convexe si et seulement si $\forall (x_1,x_2) \in I^2,\ \forall t
  \in \intv{0}{1},\ f(tx_1 + (1-t)x_2) ≤ tf(x_1) + (1-t)f(x_2)$.
\end{proposition}
\begin{proof}
  Si l'épigraphe est convexe, alors prenons $A (x_1;f(x_1))$ et $B
  (x_2;f(x_2))$, $[AB] \subset \mathscr{E}_f$ et donc $P(x;tf(x_1) +
  (1-t)f(x_2)) \in \mathscr{E}_f$ car $P \in [AB]$. Ainsi, $tf(x_1) +
  (1-t)f(x_2) ≥ f(x) = f(tx_1 + (1-t)x_2)$.

  Si l'inégalité de convexité est vérifiée, alors soit $(A,B) \in
  \mathscr{E}_f^2,\ A (x_1;y_1)$ et $B (x_2;y_2)$. On a en particulier $y2 ≥
  f(x_2)$ et $y_1 ≥ f(x_1)$.\\
  Soit $Q \in [AB]$, $Q (x;y)$ est un barycentre «$tA +(1-t)B$. On a donc $x
  = tx_1 + (1-t)x_2$ et $y = ty_1 + (1-t)y_2$. Or $y_1 ≥ f(x_1)$ et $y_2 ≥
  f(x_2) \implies y ≥ tf(x_1) + (1-t) f(x_2)$ or $tf(x_1) + (1-t) f(x_2) ≥
  f(tx_1 + (1-t)x_2) = f(x)$ d'où $y ≥ f(x)$ et donc $Q \in \mathscr{E}_f$.
\end{proof}

\begin{question}
  Montrer que $f \colon x \mapsto x^2$ est convexe sur $\R$.
\end{question}
\begin{solution}
  Étudier le signe de $tx_1^2 + (1-t)x_2^2 - (tx_1 + (1-t)x_2)^2$
\end{solution}

\begin{definition}[concave]
  On dit que $f$ est \emph{concave} quand $-f$ est convexe.
\end{definition}

\subsection{Pente des cordes}

\begin{definition}
  Soient $f$ une fonction définie sur $I$ un intervalle de $\R$ et $x$ et
  $y$ deux éléments distincts de $I$. On appelle pente entre $M(x;f(x))$ et
  $N(y;f(y))$ la valeur de $p_x(y) = \dfrac{f(y) - f(x)}{y - x}$.
\end{definition}

\begin{lemme}
  Si $x,y,z$ sont trois points de $I$ tels que $x < y < z$, alors $p_x(y) -
  p_x(z)$, $p_y(x) - p_y(z)$ et $p_z(x) - p_z(y)$ sont de même signe.
\end{lemme}
\begin{proof}
  \begin{align*}
    p_x(y) - p_x(z) & = \dfrac{f(y) - f(x)}{y - x} - \dfrac{f(z) - f(x)}{z - x} \\
                    & = \dfrac{(f(y) - f(x))(z - x) - (f(z) - f(x))(y - x)}{(z - x)(y - x)} \\
                    & = \dfrac{(y-z)f(x) + (z-x)f(y) + (x-y)f(z)}{(z - x)(y- x)} \\
                                    & = \dfrac{N}{(z - x)(y- x)} \\
                    p_y(z) - p_y(z) & = \dfrac{N}{(x-y)(z-y)}    \\
                    p_z(x) - p_z(y) & = \dfrac{N}{(z-x)(y-z)}    \\
  \end{align*}
  On a $\dfrac1{(y-x)(z-x)} > 0$, $\dfrac1{(y-x)(z-y)} > 0$ et $\dfrac1{(x
  - z)(y-z)} > 0$ d'où le résultat.
\end{proof}

Ce lemme est connu comme «inégalité des trois pentes» lorsque la fonction
est convexe.

\begin{theoreme}
  Soit $f$ définie sur $I \subset \R$. $f$ est convexe sur $I$ si et
  seulement si pour tout $a \in I$, la fonction $p_a$ associée à $f$ est
  croissante sur $I \setminus \{ a \}$.
\end{theoreme}
\begin{proof}
  Si $f$ est convexe, alors pour $x < a < y$,\\
  $p_a(y) - p_a(x) = - \dfrac{(x -y)f(a) (y-a)f(x) + (a -
  x)f(y)}{(x-a)(a-y)}$\\ Or $f$ est convexe et on peut écrire $a = \dfrac{(y
  - a)x + (a-x)y}{y-x} \iff $.\\
  $f(a) = f\brk*{\dfrac{(y-a)x + (a-x)y}{y-x}} ≤ \dfrac{(y-a)f(x) +
  (a-x)f(y)}{y-x} \iff$\\
  $(y -x)f(a) ≤ (y-a)f(x) + (a-x)f(y) \iff (x -y)f(a) + (y-a)f(x) +
  (a-x)f(y) ≥ 0$\\
  On obtient donc que $p_a(y) - p_a(x) ≥ $ et donc que $p_a$ est une
  fonction croissante.
\end{proof}

\subsection{Position de la courbe par rapport aux tangentes}

\begin{theoreme}
  Soit $f$ dérivable sur $I$, intervalle ouvert de $\R$. $f$ est convexe sur
  $I$ si et seulement si $\forall A \in \mathcal{C}_f$, tous les points de
  $\mathcal{C}_f$ sont «au-dessus» de la tangente à $\mathcal{C}_f$ en $A$.
\end{theoreme}
\begin{proof}
  On peut déjà remarquer que, si $f$ est convexe et dérivable, alors pour
  $x < a,\ p_a(x) ≤ f'(a)$ et que pour $x > a,\ p_a(x) > f'(a)$. En effet,
  pour $x < a,\ x- a < 0$ et donc $\dfrac{f(x) - f(a)}{x -a} ≤ f'(a) \iff
  f(x) - f(a) ≥ (x-a)f'(a) \iff f(x) ≥ f'(a)(x-a) + f(a)$, alors que pour $x
  > a$, on a directement $\dfrac{f(x) - f(a)}{x -a} ≥ f'(a) \iff f(x) ≥
  f'(a)(x-a) + f(a)$. Donc $\mathcal{C}_f$ est au dessus de sa tangente en
  $A(a;f(a))$.

  On suppose $f$ dérivable sur $I$ et $\forall a \in I, \ f(x) ≥ f'(a)(x-a)
  + f(a)$.\\
  Soit $(x_1,x_2) \in I^2,\ x_1 < a < x_2$. Ainsi, on a $f(x_1) ≥ f'(a)(x_1
  -a) + f(a)$ et $f(x_2) ≥ f'(a)(x_2 -a) + f(a)$, ainsi $\dfrac{f(x_1) -
  f(a)}{x-a} ≤ f'(a)$ et $\dfrac{f(x_2) - f(a)}{x-a} ≥ f'(a)$.\\
  On a donc $\forall (x_1,a,x_2) \in I^3,\ p_a(x_1) ≤ p_a(x_2)$ et donc
  $\forall a \in I, p_a$ est croissante sur $I \setminus \{ a \}$.
\end{proof}

\section{Quelques applications : inégalités de convexité}

\subsection{$f\colon x \mapsto x^2$ sur $\R_+$}

$\forall (a,b) \in \R_+^2$, et pour $\lambda = \frac12$, on peut considérer
$f(\lambda (a+b))$. En particulier, on a $ab ≥ 0$. $f$ est convexe et on a
donc $f(\lambda (a+b)= ≤ \lambda f(a) + \lambda f(b)$, ce qui s'écrit
$\brk*{\dfrac{a+b}2}^2 ≤ \dfrac{a^2}2 + \dfrac{b^2}2$, ce qui permet de
conclure que la moyenne arithmétique $\frac{a+b}2$ est inférieure à la
moyenne quadratique $\sqrt{\dfrac{a^2+b^2}2}$.

On obtient aussi que $ab ≤ \frac14\brk{a^2 + b^2}$

\subsection{Avec la fonction $\ln$}

La fonction $\ln$ est concave et on obtient $\ln \dfrac{a+b}2 ≥ \dfrac12 \ln
a + \dfrac12 \ln b$, ce qui s'écrit aussi $\ln \dfrac{a+b}2 ≥ \ln
\sqrt{ab}$. On en déduit ainsi que $\dfrac{a+b}2 ≥ \sqrt{ab}$ et donc que la
moyenne arithmétique est supérieure à la moyenne géométrique.

Enfin, remplaçons $a$ et $b$ par $\frac1a$ et $\frac1b$.\\
$\dfrac12 \brk*{ \dfrac1a + \dfrac 1b} ≥ \sqrt{\dfrac1a \dfrac1b}$ et donc
$\dfrac{1}{\dfrac12 \brk*{ \dfrac1a + \dfrac 1b}} ≤ \sqrt{ab}$. Ainsi la
moyenne harmonique est sous la moyenne géométrique.

\subsection{Inégalité de convexité}

\begin{proposition}
  Si $f$ est convexe sur $I$ et $(a_1,…,a_n) \in I^n$ et $\forall (\alpha_i)
  \in \intv{0}{1}^n,\ \sum_{i=1}^n \alpha_i = 1,\ f\brk*{
  \sum_{i=1}^n \alpha_i a_i } ≤ \displaystyle\sum_{i=1}^n
  \alpha_i f(a_i)$.
\end{proposition}
\begin{proof}
  L'inégalité se montre par récurrence.
\end{proof}
