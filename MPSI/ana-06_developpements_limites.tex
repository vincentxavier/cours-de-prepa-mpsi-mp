\chapter{Développements limités}

On peut réécrire la notion de continuité d'une fonction en $a$, en
utilisant la notion de voisinage : \[ f(a + h) \underset{h \to 0}{=} f(a) +
\po{1} . \]
De la même façon, pour la dérivabilité en $a$, on obtient $f(a + h)
\underset{h \to 0}{=} f(a) + f'(a)h + \po{h}$.

On appelle ces deux égalités «développement limité à l'ordre 0 (resp. 1) en
$a$ de la fonction $f$».

\section{Généralités}

Soit $f \colon I \to \R$, $I$ un intervalle d'intérieur non vide
($\overset{\circ}{I} ≠ ø$) et $a$ un élément de $I$, éventuellement une
borne.

\subsection{Définition}

\begin{definition}
  $f$ admet un développement limité (polynomial) d'ordre $n$ en $a$
  lorsqu'il existe une suite $(a_0,…,a_n) \in \R^{(\N)}$ et il existe
  $\alpha > 0$ tel que pour tout $x \in \intv[o]{a - \alpha}{a+\alpha} \cup
  I$, on ait \[ f(x) = a_0 + a_1 (x- a) + a_2 (x - a)^2 + … + a_n (x-a)^n +
  \po{(x-a)^n} . \]
\end{definition}

Un changement de variable affine permet de réécrire, pour $h$ «petit»
l'égalité précédente sous la forme $f(a + h) = a_0 + a_1 h + a_2 h^2 + … +
a_n h^n + \po{h^n}$.

\begin{remarque}
  \begin{itemize}
    \item Pour $n \in \{0;1\}$ on retrouve les notions présentées en
      exemple.
    \item Il existe une fonction $\varepsilon$ telle que $\varepsilon(h)
      \underset{h\to 0}{\longrightarrow} 0$ et $\po{h^n}$ peut s'écrire $h^n
      \varepsilon(h)$.
    \item $a_0 + a_1 h + a_2 h^2 + … + a_n h^n$ est la partie entière du
      développement limité d'ordre $n$.
    \item Cette partie entière (ou polynomiale) est de degré au plus $n$.
    \item Comme on peut se remplacer, en posant $x = a + h$, $x \to a$ par
      $h\to 0$, on étudiera surtout le cas où $a = 0$.
  \end{itemize}
\end{remarque}

\begin{exemple}
  On cherche le développement limité de $\dfrac1{1-x}$ au voisinage de 0.

  Comme $(1-x)(1 + x + … + x^n) = 1 - x^{n+1}$, on a $1 + x + … + x^n =
  \dfrac1{1-x} - \dfrac{x^{n+1}}{1 - x}$. Ce qui entraine que $\dfrac1{1-x}
  = 1 + x + … + x^n + \dfrac{x^{n+1}}{1 - x}$ d'où \[ \dfrac1{1-x} = 1 + x +
  … + x^n + \po{x^n} .\]
\end{exemple}

\begin{question}
  Donner le développement limité en 0 de la fonction définie par $\left\{
  \begin{array}{l} f(x) = x^2 \ln x \\ f(0) = 0 \end{array} \right.$
\end{question}
\begin{solution}
  On peut déjà écrire à l'ordre 0 et 1 :
  \begin{itemize}
    \item $f(x) \underset{0}{=} 0 + \po{1}$
    \item $f(x) \underset{0}{=} 0 + 0 x\po{x}$
  \end{itemize}
  Écrivons $f(x)$ sous la forme $f(x) = a_0 + a_1 x + a_2x^2 + \po{x^2}$.
  Par identification, on a $a_0 = 0$, et donc $f(x) = a_1 x + a_2x^2 +
  \po{x^2}$.\\
  $\dfrac{f(x)}x = a_1 + a_2 x + \po{x}$. Or $\lim_{x\to
  0}\dfrac{f(x)}x = 0$, d'où $a_1 = 0$. Donc $f(x) = a_2 x^2 + \po{x^2}$.\\
  $\dfrac{f(x)}{x^2} = a_2 + \po{1} \ln \abs{x}$ qui ne possède pas de
  limite réelle en 0.
\end{solution}

\begin{question}
  Même question avec $f$ définie par $\left\{ \begin{array}{l} f(x) = x^3
  \sin \frac1x \\ f(0) = 0 \end{array} \right.$
\end{question}
\begin{solution}
  Même méthode : on écrit le développement limité a priori, et on élimine
  les termes un à un, jusqu'à montrer qu'il y a une impossibilité.
\end{solution}
\begin{question}
  Même question avec $f$ définie par $\left\{ \begin{array}{l} f(x) =
  e^{-\frac1{x^2}} \\ f(0) = 0 \end{array} \right.$
\end{question}
\begin{solution}
  On montre (en étudiant la limite du quotient) que $\forall n \in \N,\
  e^{-\frac1{x^2}} = \po{x^n}$.
\end{solution}

\begin{proposition}
  Si $f$ admet un développement limité à l'ordre $n$ en 0, alors celui-ci
  est unique.
\end{proposition}
\begin{proof}
  Supposons que $f(x)$ s'écrive $f(x) = a_0 + a_1 x + … + a_n x^n +
  \po{x^n}$ et $f(x) = b_0 + b_1 x + … + b_n x^n + \po{x^n}$.\\
  Alors $(a_0 - b_0) + (a_1 - b_1) x + … + (a_n - b_n) x^n \underset{0}{=}
  \po{x^n}$.\\
  Soit $k$ le plus petit entier tel que $a_k ≠ b_k$.\\
  $(a_k - b_k)x^k + \po{x^k} = \po{x^n}$, d'où $(a_k - b_k)x^k =\po{x^k}$ et
  donc $a_k - b_k = \po{1}$, ce qui n'est possible que si $a_k = b_k$.
\end{proof}

\begin{proposition}
  Si $f$ est paire (respectivement impaire), la partie entière du
  développement limité est paire (respectivement impaire)
\end{proposition}
\begin{proof}
  Si $f$ est paire, alors pour tout $x\in I,\ f(x) = f(-x)$.\\
  On a donc $a_0 + a_1 x + a_2 x^2 + … + a_n x^n +\po{x^n} = a_0 - a_1 x +
  a_2 x^2 + … + (-1)^n a_n x^n + \po{x^n}$. L'unicité du développement
  limité permet d'écrire $a_k = (-1)^ka_k$. Si $k$ est pair, alors ok,
  sinon, $a_k = - a_k \implies a_k = 0$.
\end{proof}

\begin{exemple}
  $\cos x \underset{0}{=} 1 - \dfrac{x^2}2 + \po{x^3}$.
\end{exemple}

\begin{proposition}
  Si $f$ admet un développment limité en 0 à l'ordre $n$, alors pour tout $m
  ≤ n$, $f$ admet un développement limité en 0 à l'ordre $m$, dont la partie
  entière s'obtient par troncature.
\end{proposition}
\begin{proof}
  \begin{align*}
    f(x) & \underset{0}{=} a_0 + a_1 x + a_2 x^2 + … + a_m x^m  +
    \underbrace{… + a_n x^n +\po{x^n}}_{\po{x^m}} \\
    f(x) & \underset{0}{=} a_0 + a_1 x + a_2 x^2 + … + a_m x^m  + \po{x^m}
  \end{align*}
  en utilisant l'unicité du dévelppement limité.
\end{proof}

\subsection{Opérations sur les développements limités}

\subsubsection{Combinaisons linéaires}

\begin{proposition}
  Si $f$ et $g$ admettent des développements limités en 0 à l'ordre $n$,
  alors pour tout $\lambda \in \R,\ f + \lambda g$ admet un développement
  limité en 0 dont la partie entière est la combinaison linéaire des parties
  entières.
\end{proposition}
\begin{proof}
  $f(x) \underset{0}{=} a_0 + … + a_n x^n + \po{x^n}$ \\
  $g(x) \underset{0}{=} b_0 + … + b_n x^n + \po{x^n}$ \\
  $f(x) + \lambda g(x) \underset{0}{=} (a_0 + \lambda b_0) + … + (a_n +
  \lambda b_n) x^n + \po{x^n}$.
\end{proof}

\begin{remarque}
  Si $f$ et $g$ admettent des ordres différents, on obtient un développement
  limité au plus petits des deux ordres.
\end{remarque}

\subsubsection{Multiplication}

\begin{exemple}
  Au voisinage de 0, supposons que $f(x) \underset{0}{=} x^2 + 3x^4 + x^5 +
  \po{x^5}$ et $g(x) \underset{0}{=} x - x^3 + x^4 + \po{x^4}$.\\
  \begin{align*}
    f(x)g(x) & = (x^2 + 3x^4 + x^5 + \po{x^5})(x - x^3 + x^4 + \po{x^4}) \\
             & = x^3 + 2x^5 + 2x^6 + 3x^7 + 2x^8 + x^9 + \po{x^6}        \\
             & = x^3 + 2x^5 + 2x^6 + \po{x^6}
  \end{align*}
\end{exemple}

\begin{proposition}\label{ana06:prop:multiplication}
  Si $f$ et $g$ admettent des développements limités en 0 aux ordres $n$ et
  $m$ respectivement, alors $fg$ admet un développement limité d'ordre au
  moins $\min(n,m)$
\end{proposition}

\begin{remarque}
  Si les valuations des parties entières des $f$ et $g$ sont non nulles,
  alors cette ordre peut-être augmenté.
\end{remarque}

\subsubsection{Composition}

\begin{proposition}\label{ana06:prop:composition}
  Soient $f$ et $g$ des fonctions admettant un développement limité d'ordre
  $n$ au voisinage de 0 et si $f(0) = 0$, alors, $g \circ f$ admet un
  développement limité d'ordre $n$ au voisinage de 0.
\end{proposition}
\begin{proof}
  $f$ au voisinage de 0 s'écrit $f(x) \underset{0}{=} a_1 x + … + a_n x^n +
  \po{x^n} = P_n(x) + \po{x^n} = xf_1(x)$ car $f(0) = 0$.\\
  $g(x) \underset{0}{=} Q_n(x) + \po{x^n}$.\\
  Calculons $(g \circ f)(x) - (Q_n \circ f)(x)$.
  \begin{align*}
    (g \circ f)(x) - (Q_n \circ f)(x) & = Q_n(f(x)) + \po{(f(x))^n} - Q_n(f(x)) \\
                                      & = \po{x^n(f_1(x))^n} \\
                                      & = \po{x^n}
  \end{align*}
  De plus, $Q_n \circ f(x) - Q_n \circ P_n(x) = \sum_{k=0}^n
  b_k ( f(x)^k - P_n(x)^k)$. On démontre par récurrence que, comme $f(x) =
  P_n(x) + \po{x^n}$, alors $f(x)^k = P_n(x)^k + \po{x^n}$ et donc $Q_n
  \circ f(x) - Q_n \circ P_n(x) = \sum_{k=0}^n b_k \po{x^n} = \po{x^n}$.\\
  On finit donc par obtenir que $g\circ f (x) - Q_n \circ P_n(x) =
  \po{x^n}$.
\end{proof}

\begin{exemple}
  On admet que $\sin x \underset{0}{=} x - \dfrac{x^3}{6} + \dfrac{x^5}{120}
  + \po{x^6}$. Donner le développement limité ) l'ordre 5 de $\sin
  \dfrac{x}{1-x}$.

  On a $\dfrac{x}{1-x} \underset{0}{=} x(1 + x + x^2 + x^3 + x^4 + \po{x^4})
  \underset{0}{=} x + x^2 + x^3 + x^4 + x^5 + \po{x^5}$.\\
  $\sin \dfrac{x}{1-x} \underset{0}{=} x + x^2 + x^3 + x^4 + x^5 - \dfrac16
  (x + x^2 + x^3 + x^4 + x^5)^3 + \dfrac{1}{120} (x + x^2 + x^3 + x^4 +
  x^5)^5 + \po{x^5}$\\
  $\sin \dfrac{x}{1-x} \underset{0}{=} x + x^2 + \brk*{1 - \frac16}x^3 +
  \brk*{1 - \frac36}x^4 + \brk*{1 - \dfrac{3+3}6 + \dfrac{1}{120}}x^5 + +
  \po{x^5}$.
\end{exemple}

\begin{question}
  Donner le développement limité de $\dfrac1{1-\sin x}$ en 0 à l'ordre 3.
\end{question}
\begin{solution}
  $\dfrac1{1-\sin x} \underset{0}{=} 1 +x +x^2 + \dfrac56 x^3 + \po{x^3}$
\end{solution}

\begin{corollaire}
  Si $f$ et $g$ admettent des développements limités à l'ordre $n$ en 0,
  et si $g(0) ≠ 0$, alors $\dfrac{f}g$ admet un développement limité à
  l'ordre $n$ en 0.
\end{corollaire}
\begin{proof}
  $\dfrac{f(x)}{g(x)} \underset{0}{=} \dfrac{f(x)}{g(0) - h(x)} =
  \dfrac1{g(0)} \dfrac{f(x)}{1 - \frac{h(x)}{g(0)}} = \dfrac1{g(0)} \times
  f(x) \times \dfrac{f(x)}{1 - l(x)}$ où $l(0) = 0$. On peut donc appliquer
  les propositions \ref{ana06:prop:multiplication} et
  \ref{ana06:prop:composition} pour conclure.
\end{proof}

\begin{exemple}
  $\tan x \underset{0}{=} \dfrac{\sin x}{\cos x} \underset{0}{=} \dfrac{x -
    \frac{x^3}6 + \po{x^4}}{1 - \frac{x^2}2 + \po{x^3}} = \brk*{x -
  \frac{x^3}6 + \po{x^3}} \brk*{1 - \frac{x^2}2 + \po{x^3}}$\\
  $\tan x \underset{0}{=} x + \dfrac13 x^3 + \po{x^3}$.
\end{exemple}

\subsubsection{«Primitivation» d'un développement limité}

\begin{proposition}\label{ana06:prop:integration}
  Soit $f$ une fonction de classe $\Cl^0$ au voisinage de 0 et admettant un
  développement limité d'ordre $n$ au voisinage de 0.\\
  $f(x) \underset{0}{=} P_n(x) + \po{x^n}$. Alors pour tout primitive $F$
  de $f$ au voisinage de 0, on a \[ F(x) = F(0) + \int_0^x P_n(t) \diff t +
  \po{x^{n+1}} . \]
\end{proposition}
\begin{proof}
  On a $f(t) - P_n(t) = \po{t^n}$, ce qui s'écrit aussi \\
  $\forall \varepsilon > 0,\ \exists \alpha > 0,\ \abs{t} ≤ \alpha \implies
  \abs{f(t) - P_n(t)} ≤ \varepsilon \abs{t^n}$. Cette dernière condition
  s'écrit aussi \\
  $- \varepsilon \abs{t^n} ≤ f(t) - P_n(t) ≤ \varepsilon \abs{t^n}$.\\
  $\left\{ \begin{array}{l} f(t) - P_n(t) - \varepsilon \abs{t^n} ≤ 0 \\
  f(t) - P_n(t) + \varepsilon \abs{t^n} ≥ 0 \end{array} \right.$\\
  Pour $t ≥ 0$, on a $\left\{ \begin{array}{l} f(t) - P_n(t) - \varepsilon
  t^n ≤ 0 \\ f(t) - P_n(t) + \varepsilon t^n ≥ 0 \end{array} \right.$\\
  Pour $x \in \intv{0}{\alpha}$, on peut intégrer sur $\intv{0}{x}$ et on
  obtient : \[ \left\{ \begin{array}{l} F (x) - F(0) - \int_0^x
  P_n(t) \diff t - \varepsilon \frac{x^{n+1}}{n+1} \\ F (x) - F(0) -
  \int_0^x P_n(t) \diff t + \varepsilon \frac{x^{n+1}}{n+1}
  \end{array} \right. . \] D'où $\abs*{F (x) - F(0) - \int_0^x
  P_n(t) \diff t} ≤ \dfrac{\varepsilon}{n+1} x^{n+1}$.\\
  On a donc $F (x) - F(0) - \int_0^x P_n(t) \diff t =
  \po{x^{n+1}}$.
\end{proof}

\begin{exemple}
  On sait que $\tan 0 = 0$ et que $\tan' = 1 + \tan^2$.\\
  \begin{align*}
    \tan x  & \underset{0}{=} \po{1}                                       \\
    \tan' x & \underset{0}{=} 1 + \po{1}                                   \\
    \tan x  & \underset{0}{=} \tan 0 + x \po{x}                            \\
    \tan' x & \underset{0}{=} 1 + x^2 + \po{x^2}                           \\
    \tan x  & \underset{0}{=} x + \dfrac{x^3}3 + \po{x^3}                  \\
    \tan' x & \underset{0}{=} 1 + \brk*{x + \dfrac{x^3}3 + \po{x^3}}^2     \\
    \tan' x & \underset{0}{=} 1 + x^2 + \dfrac{2x^4}3 + \po{x^4}           \\
    \tan x  & \underset{0}{=} x + \dfrac{x^3}3 + \dfrac2{15}x^5 + \po{x^5} \\
  \end{align*}
\end{exemple}

\begin{proposition}
  Soit $f$ une fonction de classe au moins $\Cl^1$ telle que $f$ admet un
  développement limité d'ordre $n+1$ en 0 et $f'$ admet un développement
  limité d'ordre $n$ en 0. Alors la partie entière du développement de $f'$
  à l'ordre $n$ est la dérivée de la partie entière du développement limité
  de $f$.
\end{proposition}
\begin{proof}
  On utilise le fait qu'une primitive de $f'$ est $f$ et la proposition
  \ref{ana06:prop:integration} pour conclure.
\end{proof}

\subsubsection{Existence d'un développement limité}

\begin{proposition}[Formule de Taylor-Young]
  Soit $f$ de classe $\Cl^n$ au voisinage de $a$. Alors il existe $\alpha >
  0$ tel que $\forall h ≤ \alpha,\ f(a+h) = f(a) + h f'(a) + f'(a)
  \dfrac{h^2}{2!} + … + → \dfrac{f^{(n)}(a)}{n!}h^n + \po{h^n} =
  \sum_{k=0}^n \dfrac{f^{(k)}(a)}{n!} h^k + \po{h^n}$.
\end{proposition}
\begin{proof}
  Par récurrence.\\
  À l'ordre $n = 0$, la continuité de $f$ permet d'écrire que $f(a+h) = f(a)
  +\po{1}$.\\
  On suppose que pour $n$ fixé, on a $f\in \Cl^n$ et qu'elle possède un
  développement limité à l'ordre $n$ tel que $f(a+h) =
  \sum_{k=0}^n \dfrac{f^{(k)}(a)}{n!} h^k + \po{h^n}$. Soit $g$
  de classe $\Cl^{n+1}$ au voisinage de $a$. $g'$ est de classe $\Cl^n$ au
  voisinage de $a$ et $g'(a+h) = \sum_{k=0}^n \dfrac{g'(a)}{n!}
  h^k + \po{h^n}$. En intégrant, on obtient que $g(a+h) =
  \sum_{k=0}^{n+1} \dfrac{f^{(k)}(a)}{n!} h^k + \po{h^{n+1}}$.
\end{proof}

\begin{exemple}
  $f\colon x \mapsto (1+x)^{\alpha}$ est $\Cl^{\infty}$ pour $x > 1$,
  quelque soit $\alpha \in \R$ et on a\\
  $(1+x)^{\alpha} \underset{0}{=} 1 + \alpha x + \dfrac{\alpha (\alpha -
  1)}{2!}x^2 + … + \dfrac{\alpha (\alpha -1) … (\alpha - k + 1)}{k!} x^k +
  \po{x^k}$
\end{exemple}

\begin{remarque}
  L'exemple précendent peut s'appliquer à des $\alpha \in \{-1; \frac12 ;
  -\frac12 \}$.\\
  Par intégration, on trouve aussi $\ln(1+x) = x - \dfrac{x^2}2 +
  \dfrac{x^3}{3} + … + (-1)^{k-1} \dfrac{x^k}{k} + \po{x^k}$.
\end{remarque}

On peut également déduire, par composition, $e^x = \sum_{k=0}^n
\dfrac{x^k}{k!} + \po{x^k}$ et donc les développements limités du sinus et
du cosinus, en considérant les parties paires et impaires de l'exponentielle
complexe.

\subsection{Utilisation des développements limités}

\subsubsection{Recherche de limites}

En effectuant un développement limité, on trouve ainsi des prolongements
continus en 0, voir des équivalents aux fonctions.

Pour les limites en $± \infty$, on pose $x = \frac1h$ et on trouve en
prenant le développement limité en 0.

\subsubsection{Recherche de tangente}

Mieux encore, les termes de rangs 0 et 1 donnent l'équation de la tangente
en 0 et le terme non nul suivant permet de déterminer la position relative
de la courbe par rapport à sa tangene.

\subsubsection{Comportement asymptotique}

Le changement de variable proposé permet de déterminer une direction
asymptotoque à la courbe.

\subsubsection{Développement limité de la réciproque}

L'existence d'un tel développement est garantie par la formule de
Taylor-Young et on l'obtient en composant par des coefficients indéterminés
: on développe $f^{-1} \circ f = \Id$.
