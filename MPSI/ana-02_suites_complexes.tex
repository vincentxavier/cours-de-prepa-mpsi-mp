\chapter{Suites complexes}

\section{Généralités}

Soit $u$ une suite complexe. On note $u \in \C^{\N}$.

\begin{definition}
  Une suite complexe est bornée lorsque la suite des modules est bornée
  : \[ \exists M \in \R, \ \forall n \in \N, \abs{u_n} ≤ M . \]
\end{definition}

\begin{proposition}
  Les suites complexes forment une $\C$-algèbre.
\end{proposition}
\begin{proof}
  Il suffit de vérifier les propositions.
\end{proof}

\begin{definition}
  Une suite complexe $u$ est dite convergente lorsqu'il existe $\lambda
  \in \C,\ \forall \varepsilon > 0,\ \exists n_0 \in \N, \ \forall n \in
  \N, n ≥ n_0 \implies \abs{u - \lambda} ≤ \varepsilon$.
\end{definition}

\begin{exemple}
  Soient 3 points d'affixes $a,b,c$. On pose $u_0 = a$, $u_1= b$ et $u_2
  = c$ et pour tout $n$, $u_{n+3}$ est l'isobarycentre des points
  d'affixes $u_n, u_{n+1}, u_{n+2}$.

  Cette suite est convergente.
\end{exemple}

\begin{proposition}
  Le nombre complexe $\lambda$, s'il existe est unique.
\end{proposition}
\begin{proof}
  Soient $\lambda$ et $\lambda'$ deux limites.

  $0 ≤ \abs{\lambda - \lambda'} ≤ \abs{\lambda - u} + \abs{u -
  \lambda'}$ Or par définition, on a, pour $n ≥ \max(n_0, n_0'),\ 0 ≤
  \abs{\lambda - \lambda'} ≤ 2\varepsilon$ et ce pour tout $\varepsilon
  > 0$. Donc $\lambda = \lambda'$.
\end{proof}

On peut donc écrire $\lambda = \lim_{n \to + \infty} u_n$.

\begin{proposition}
  Toute suite convergente est bornée.
\end{proposition}
\begin{proof}
  Pour $n ≥ n_0,\ \abs{\lambda - u} ≤ \varepsilon$ et pour $0 ≤ n <
  n_0$, il existe $R = \max \abs{ \lambda - u}$. D'où $\forall n \in
  \N,\ \abs{\lambda - u } ≤ \max(R,\varepsilon) = R_1$.

  Or $\abs{\abs{\lambda} - \abs{u}} ≤ \abs{\lambda - u} ≤ R_1$ et donc
  $R_1 - \abs{\lambda} ≤ \abs{u} ≤ R_1 + \abs{\lambda}$.
\end{proof}

\begin{proposition}
  $u$ est convergente vers $\lambda$ si et seulement si $(\Re u)$ et
  $(\Im u)$ sont convergentes vers $\Re \lambda$ et $\Im \lambda$. De
  plus $\conj{u}$ converge vers $\conj{\lambda}$.
\end{proposition}
\begin{proof}
  \begin{description}
    \item[$\lim u = \lambda$]
      $\forall \varepsilon > 0,\  \exists n_0 \in \N, \ n ≥ n_0 \implies
      \sqrt{(\abs{\Re u - \Re \lambda})^2 + (\abs{\Im u - \Im
      \lambda})^2} ≤ \varepsilon$. Or $\abs{a} ≤ \sqrt{a^2 + b^2}$. On
      obtient bien les limites des suites $\Re u$ et $\Im u$.
    \item[réciproque] $\lim \Re u = x$ et $\lim \Im u = y$. Posons
      $\lambda = x + iy$. On a, à partir d'un certain rang, $\abs{\Re u
      - \Re \lambda} ≤ \frac1{\sqrt{2}}\varepsilon$ et $\abs{\Im u - \Im
      \lambda} ≤ \frac1{\sqrt{2}}\varepsilon$ d'où $\abs{u - \lambda} ≤
      \sqrt{ \brk*{\frac1{\sqrt{2}}\varepsilon}^2 +
      \brk*{\frac1{\sqrt{2}}\varepsilon}^2} = \varepsilon$.
  \end{description}
  Pour la suite des conjugués, il suffit de remarquer que $\abs{u -
  \lambda} = \abs{\conj{u} - \conj{\lambda}}$.
\end{proof}

\begin{remarque} La continuité des applications linéaires $\Re$, $\Im$,
  $\conj{\cdot}$ et $(x,y) \mapsto x + iy$ permet de conclure.
\end{remarque}

\begin{proposition}
  Soient $(u,v) \in \brk*{\C^{\N}}^2,\ (\lambda, \mu) \in \C^2$, $\lim
  \lambda u + \mu v = \lambda \lim u + \mu \lambda v$ et $\lim (uv) =
  \lim u \lim v$.
\end{proposition}
\begin{proof}
  Pour la somme, on utilise l'inégalité triangulaire.

  Pour le produit également en faisant apparaître un terme «correctif»
  qu'on annule.
\end{proof}

\begin{proposition}
  Si $u \in (\C)^{\N}$ converge vers $\lambda \in \C^*$,
  $\brk*{\frac1u}_{(n≥n_0)}$ est définie et converge vers $\frac1{\lambda}$.
\end{proposition}
\begin{proof}
  Soit $\lambda ≠ 0$ tel que $\lim u = \lambda$. Prenons $\varepsilon =
  \frac12 \abs{\lambda}$.

  $0 \not \in \B(\lambda, \varepsilon)$ et par conséquent, pour $n ≥ n_0$,
  $\frac1u$ est bien définie.

  $\abs{\frac1u - \frac1{\lambda}} = \frac{\abs{u_n -
  \lambda}}{\abs{u_n}\abs{\lambda}}$. Or pour $n ≥ n_0,\ \frac12
  \abs{\lambda} ≤ \abs{u_n} ≤ \frac32 \abs{\lambda}$ et donc $\abs{\frac1u -
  \frac1{\lambda}}  ≤ \frac{2 \abs{u_n - \lambda}}{\abs{\lambda}^2} ≤
  \frac{2\varepsilon}{\abs{\lambda}^2}$. D'où le résultat attendu.
\end{proof}

\begin{theoreme}{Bolzano-Weierstrass}
  De toute suite bornée, on peut extraire une suite convergente.
\end{theoreme}
\begin{proof}
  Soit $u \in \C^{\N}$. $\forall n \in \N, u_n = x_n + iy_n$, $(x,y) \in
  \brk*{\R^{\N}}^2$.

  $u$ bornée $\implies x$ et $y$ bornées. On peut donc extraire
  $(x_{\varphi(n)})$ de $x$ et on note que $(y_{\varphi(n)})$ est toujours
  bornée.

  $(x_{\varphi(n)})$ (Bolzano-Weierstrass dans $\R$). Et de
  $(y_{\varphi(n)}$ on peut extraire une suite $(y_{\psi \circ \varphi(n)})$
  . $(x_{\psi \circ varphi(n)})$ converge comme suite extraite d'une suite
  convergente et $(y_{\psi \circ \varphi(n)})$ (Bolzano-Weierstrass dans
  $\R$). On a donc la convergence des deux suites $\Re u_{\psi \circ
  \varphi}$ et $\Im u_{\psi \circ \varphi}$ et donc la convergence de
  $u_{\psi \circ \varphi}$.
\end{proof}
