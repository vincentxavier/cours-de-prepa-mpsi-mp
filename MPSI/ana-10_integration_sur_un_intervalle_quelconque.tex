\chapter{Intégration sur un intervalle quelconque}

\section{Cas des fonctions continues à image dans $\R_+$}

Le chapitre \ref{integration_segment} a permis de définir l'intégrale des
fonctions continues sur un segment, notée $\int_a^b f =
\int_{\intv{a}{b}} f,\ a ≤ b$.

On s'intéresse désormais à l'intégration sur des intervalles quelconques de
$\R$ comme $\intv[l]{a}{b}$.

\begin{exemple}
  $f \colon \intv[o]{0}{+\infty} \to \R_+^*, x \mapsto \dfrac1{x^2}$ est
  continue sur son intervalle de définition, mais comment définir son
  intégrale sur celui-ci ?
\end{exemple}

On va se limiter dans un premier temps aux seules fonctions positives, c'est
à dire à image dans $\R_+$.

\subsection{Définition}

\begin{definition}
  On dit que la fonction $f$ continue positive à image dans $\R_+$ est
  intégrale sur $I$ lorsqu'il existe  $M \in \R_+$ tel que pour tout segment
  $J \subset I,\ \int_J f ≤ M$. On pose alors $
  \int_I f = \sup_{\substack{J \subset I\\J\ \text{segment}}}
  \int_J f$.
\end{definition}

\begin{exemple}
  \leavevmode
  \begin{itemize}
    \item $f \colon x \mapsto \dfrac1{x^2}$ et $I = \intv[r]{1}{+\infty}$.\\
      Soient $a$ et $b$ tels que $1≤ a < b$ et $J = \intv{a}{b}$.\\
      $\int_J f =  \int_a^b f = \dfrac{-1}b +
      \dfrac1a ≤ \dfrac1a ≤ 1$.\\
      $f$ est donc sommable sur $\intv[r]{1}{+\infty}$ car continue positive
      et les sommes sur les segments inclus sont bornées.
    \item $f$ sur $I = \intv[l]{0}{1}$.\\
      Soient $a$ et $b$ tels que $0<a≤b≤1$ et $J = \intv{a}{b}$.\\
      $\int_J f = \dfrac{-1}b +\dfrac1a ≤ \dfrac1a - 1 ≤
      +\infty$.\\
      La fonction $f$ est donc non sommable sur $I = \intv[l]{0}{1}$. car
      non bornée.
  \end{itemize}
\end{exemple}

On retrouve, pour les fonctions constantes sur les intervalles bornés, que
ces fonctions y sont sommables.

\begin{proposition}
  Soient $f$ une fonction continue positive et $I$ un intervalle de $\R$.\\
  S'il existe une suite $(J_n)_{n\in \N}$ de segments croissants pour
  l'inclusion ($\forall n \in \N,\ J_n \subset J_{n+1}$) dont la réunion est
  $I$ et un réel $M$ tel que $\forall n \in \N,\ \int_{J_n} f
  ≤ M$, alors $f$ est intégrable sur $I$ et de plus, \[ \int_I f = \lim_{n
  \to +\infty} \int_{J_n} f . \]
  Inversement, si $f$ est sommable sur $I$, pour toute suite croissante de
  segments $Jçn$ inclus et de réunion $I$, on a $ \int_I f =
  \sup_{\substack{J_n \subset I \\ n \in \N }}
  \int_{J_n} f = \lim_{n \to +\infty}  \int_{J_n}
  f$.
\end{proposition}
\begin{proof}
  On pose $u_n = \int_{J_n} f$. La suite $(u_n)_{n\in \N}$ est
  majorée par $M$ et croissante car $f$ est positive. Cette suite converge
  vers $\ell$ qui la borne supérieure de l'ensemble des $u_n$.

  Soit $\intv{\alpha}{\beta} \subset I$. Peut-on majorer $
  \in_{\alpha}^{\beta}$ ?\\
  $\exists (n_1,n_2) \in \N^2,\  \alpha \in J_{n_1}$ et $\beta \in J_{n_2}$.
  Pour $n = \max(n_1,n_2)$, $\intv{\alpha}{\beta} \subset J_n$.\\
  D'où $\int_{\alpha}^{\beta} ≤  \int_{J_n} f ≤
  M$. \\ Donc $f$ est sommable sur $I$.

  $\forall J \subset I,\ \int_J f ≤  \int_I$ donc
  $\forall n \in \N, \int_{J_n} f ≤  \int_I$,
  donc $\ell ≤ \int_I$.

  D'autre part, $\forall J \subset I,\ \int_{\alpha}^{\beta} ≤
  \int_{J_n} f ≤ \ell$, d'où, $\forall J,\
  \int_J f ≤ \ell$.\\
  Or $\int_I f = \sup  \int_J f ≤ \ell$. Il vient
  donc que $\int_I f ≤ \ell ≤  \int_I f$ et donc
  \[ \ell = \int_I f. \]

  Si $f$ est sommable sur $I$, alors $\int_I f$ majore toute
  suite de terme général $\int_{J_n} f$, où $J_n$ st une suite
  croissante de segment inclus dans $I$ et dont la réunion vaut $I$. Ainsi
  ces suites sont croissantes majorées et on peut conclure comme ci-dessus.
\end{proof}

\begin{exemple}
  $\int_{\intv[l]{0}{1}} \dfrac{1}{\sqrt{x}} \diff x$.

  $\dfrac{1}{\sqrt{x}}$ est continue sur $\intv[l]{0}{1}$ et est positive.
  Posons $I = \bigcup_{n \in \N} \intv*{\frac1{n+1}}{1}$. Soit $n \in \N,\
  \int_{\intv*{\frac1{n+1}}{1}} \dfrac{1}{\sqrt{x}} \diff x =
  2 - \dfrac{2}{\sqrt{n+1}}$ d'où $\int_{\intv[l]{0}{1}}
  \dfrac{1}{\sqrt{x}} \diff x = 2$
\end{exemple}

\subsection{Propriétés de l'intégrale}

\begin{proposition}[somme]
  Si $f$ et $g$ sont deux fonctions continues positives et sommable sur $I$,
  alors $f + g$ est sommable sur $I$ et $\int_I f + g =
  \int_I f +  \int_I g$.
\end{proposition}
\begin{proof}
  On sait qu'il existe $M_1$ et $M_2$ tels que pour tout $J \subset I,\
  \int_J f ≤ M_1$ et $ \int_J g ≤ M_2$.\\ On en
  déduit que $\int_J f + g ≤ M_1 + M_2$ et donc $f + g$ est
  intégrable sur $I$.

  On sait que pour toute suite croissante $(J_n)_{n\in \N}$ de segments de
  réunion $I$, on a :\\
  $\lim_{n\to + \infty}  \int_{J_n} f =
  \int_I f$\\
  $\lim_{n\to + \infty}  \int_{J_n} g =
  \int_I g$\\
  $\lim_{n\to + \infty}  \int_{J_n} f + g =
  \int_I f + g$\\
  Or $\lim_{n\to + \infty}  \int_{J_n} f + g =
  \lim_{n\to + \infty}  \int_{J_n} f +
  \lim_{n\to + \infty}  \int_{J_n} g =
  \int_I f +  \int_I g$.
\end{proof}

\begin{proposition}
  Si $f$ est intégrable sur $I$ et $\lambda \in \R_+$ alors $\lambda f$ est
  intégrable sur $I$ et $\lambda \int_I f =
  \int_I \lambda f$.
\end{proposition}
\begin{proof}
  Analogue à la démonstation précédente.
\end{proof}

Le théorème suivant est d'importance : par comparaison avec des fonctions de
«référence», on pourra juger de la sommabilité d'une fonction.

\begin{theoreme}\label{10ana:thm:comparaison}
  Si $f$ est continue, positive, majorée par $g$ continue, positive et
  inétgrable sur $I$, alors $f$ est intégrable sur $I$ et de plus
  $\int_I f ≤ \int_I g$.
\end{theoreme}
\begin{proof}
  Comme $g$ est intégrable, on sait que $\exists M \in \R_+,\ \forall J
  \subset I,\ \int_J g ≤ M$. Or $f ≤ g \implies \int_J f ≤ \int_J g$. On
  déduit donc que $ \int_J f ≤ M$ et donc que $f$ est intégrable sur $I$.

  Raisonnons par l'absurde : supposons que $\int_I f >
  \int_J g$.\\
  Alors $\sup_{J \subset I}  \int_J f >
  \int_I g$ et donc il existe $J_0$ tel que $
  \int_{J_0} f > \int_I g$.\\
  Or $\int_{J_0} g ≤  \int_I g$, on aurait alors
  $\int_{J_0} f >  \int_{J_0} g$, ce qui serait
  contraire à $f ≤ g$ sur $I$.
\end{proof}

\begin{proposition}[relation de Chasles]
  Soient $f$ une fonction continue positive sur $I$ et $a \in I$. On note
  $I_g(a) = I \cap \intv[l]{-\infty}{a}$ et $I_d(a) = I \cap
  \intv[r]{a}{+\infty}$.

  $f$ est intégrable sur $I$ si et seulement si elle est intégrable sur
  $I_g(a)$ et $I_d(a)$. Dans ce cas, $\int_I f =
  \int_{I_g(a)} f + \int_{I_d(a)} f$.
\end{proposition}
\begin{proof}
  Si $f$ est intégrable sur $I$, alors $\exists M \in \R,\ \forall J \subset
  I,\ \int_J f ≤ M$. Si $J' \subset I_g(a)$ et $J" \subset I_d(a)$, alors
  $\int_{J'} f ≤ M$ et $\int_{J"} f ≤ M$ donc $f$ est intégrable à gauche et
  à droite de $a$.

  Réciproquement, si $f$ est intégrable sur $I_g(a)$ et $I_d(a)$, alors
  $\exists M_1,\ \forall J_1 \subset I_g(a),\ \int_{J_1} f ≤ M_1$ et
  $\exists M_2,\ \forall J_2 \subset I_d(a),\ \int_{J_2} f ≤ M_2$.\\
  Soit $J \subset I$. $J$ s'écrit $J = (\underbrace{J \cap I_g(a)}_{J_g(a)})
  \cup (\underbrace{J \cap I_d(a)}_{J_d(a)})$ et donc $\int_J f =
  \int_{J_g(a)} f + \int_{J_g(a)} f ≤ M_1 + M_2$ et donc $f$ est intégrable
  sur $I$.

  On a $\int_I f = \lim_{n\to +\infty} \int_{J_n} f$ où $(J_n)_{n\in \N}$
  est une suite croissante de segments inclus dans $I$ et $\bigcup_{n\in \N}
  J_n = I$. En écrivant $J_n = J'_n \cup J"_n$ où $J'_n = J_n \cap I_g(a)$
  et $J"_n = J_n \cap I_d(a)$, on obtient que $(J'_n)$ et $(J"_n)$ sont deux
  suites croissantes de $I_g(a)$ et $I_d(a)$.\\
  Ainsi, on a
  \begin{align*}
    \int_I f = \lim_{n\to +\infty} \int_{J_n} f & = \lim_{n\to +\infty}
    \int_{J'_n} f + \int_{J"_n} f \\
    & = \lim_{n\to +\infty} \int_{J'_n} f + \lim_{n\to +\infty} \int_{J"_n}
    f \\
    & = \int_{I_g(a)} f + \int_{I_d(a)} f\\
  \end{align*}
\end{proof}

\begin{proposition}
  Si $f$ et $g$ sont deux fonctions continues positives sur
  $I = \intv[r]{a}{b},\ b \in \R \cup \set{+\infty}$ et si $f(x)
  \underset{b}{\sim} g(x)$, alors les deux fonctions sont intégrables ou non
  intégrables sur $I$.
\end{proposition}
\begin{proof}
  Supposons $f$ intégrable sur $I$ et $f(x) \underset{b}{\sim} g(x)$. Alors,
  il existe $M,\ \forall j \subset I, \int_J f ≤ M$ et $\forall \varepsilon
  > 0,\ \exists \alpha > 0,\ \forall x,\ b- \alpha ≤ x < b \implies f(x) (1-
  \varepsilon ≤ g(x) ≤ f(x) (1+ \varepsilon)$.\\
  $f (1+ \varepsilon)$ est intégrable sur $\intv[r]{b- \alpha}{b}$, donc $g$
  l'est aussi. Comme $g$ est continue sur $\intv{a}{b-\alpha}$, elle est
  intégrable dessus et donc $g$ est intégrable sur $I$.\\
  En échangeant les rôles de $f$ et $g$, on montre que $g$ intégrable
  entraîne $f$ intégrable et on a donc l'équivalence.
\end{proof}

\subsection{Intégrabilité et convervgence d'intégrale}

\begin{definition}
  Soit $f$ une fonction continue positive sur $I = \intv[r]{a}{b}$.\\
  On dit que l'intégrale $\int_a^b f(x) \diff x$ est \emph{convergente} en
  sa borne $b$ lorsque $\lim_{u \to b}\int_a^u f(x) \diff x$ existe.
\end{definition}

Cette définition est valable lorsque $b \in \R \cup \set{+\infty}$.

\begin{proposition}
  $f$ est intégrable sur $I = \intv[r]{a}{b}$ si et seulement si
  l'intégrale $\int_a^b f(x) \diff x$ est convergente.
\end{proposition}
\begin{proof}
  Si $f$ est intégrable sur $\intv[r]{a}{b}$, alors $u \mapsto \int_a^u f(x)
  \diff x$ est croissante, car $f ≥ 0$ et comme $\intv{a}{u} \subset I$, on
  sait que $\forall J \subset I,\ \int_J f ≤ \int_I f$ et donc $u \mapsto
  \int_a^u f(x) \diff x$ est croissante majorée d'où\\
  $\int_a^bf(x) \diff x$ existe comme limite à gauche.

  Si on suppose que $\int_a^bf(x) \diff x$ est convergente, alors
  \begin{align*}
    \int_a^bf(x) \diff x & = \lim_{u \to b}\int_a^u f(x) \diff x \\
                         & = \sup_{u \in \intv[r]{a}{b}} \int_a^u f(x) \diff
    x \\
  \end{align*}
  D'où $\forall J \subset I,\ \exists u \in \intv[r]{a}{b},\ J \subset
  \intv{a}{u}$ et donc $\int_J f(x) \diff x ≤ \int_a^u f(x) \diff x ≤
  \int_a^b f(x) \diff x$ et donc $f$ est sommable sur $\intv[r]{a}{b}$.
\end{proof}

\begin{corollaire}
  $\int_a^b f(x) \diff x = \int_{\intv[r]{a}{b}} f$
\end{corollaire}
\begin{proof}
  On a, pour $(u_n)_{n\in \N}$ suite croissante de limite $b$, $\lim_{n\to
  +\infty} \int_a^{u_n} f(x) \diff x = \int_a^b f(x) \diff x$. Or\\
  $\intv{a}{u_n} = J_n \subset I$ et $(J_n)$ est croissante et
  $\bigcup_{n\in \N} J_n = I$. D'où $\lim_{n \to +\infty} \int_{J_n} f =
  \int_{\intv[r]{a}{b}} f$.\\
  On a bien $\int_a^b f(x) \diff x = \int_{\intv[r]{a}{b}} f$.
\end{proof}

La définition, la proposition et son corollaire ont été données dans le
cadre d'un intervalle ouvert à droite, éventuellement non borné. On peut
adapter les propositions (et leurs preuves) au cas d'intervalle ouvert à
gauche, éventuellement non borné non plus.

Dans le cas où l'intervalle est ouvert des deux côtés, éventuellement non
borné, il faut «couper» en deux parties. Ainsi, si $f$ est une fonction
intégrable sur $\R$, on a $\int_a^b f(x) \diff x = \lim_{\substack{u \to a\\
u > a}} \lim_{\substack{v \to b\\v < b}} \int_u^v f(x) \diff x$. Il est
prudent de traiter ces deux limites séparement.

\begin{exemple}
  $\lim_{\alpha \to \frac{\pi}2} \int_{-\alpha}^{\alpha} \abs{\tan x} \diff
  x ≠ \lim_{\alpha \to -\frac{\pi}2} \lim_{\beta \to \frac{\pi}2}
  \int_{\alpha}^{\beta} \abs{\tan x} \diff x$.
\end{exemple}

Quelques exemples de références sont à connaître :
\begin{enumerate}
  \item $\int_a^{+\infty} t^{\alpha} \diff t,\ \alpha > 0$ converge si et
    seulement si $\alpha < -1$. Dans ce cas, $\int_a^{+\infty} t^{\alpha}
    \diff t = \frac1{\alpha +1}(-a)^{\alpha + 1}$.
  \item $\int_0^a t^{\alpha} \diff t,\ \alpha > 0$ converge si et seulement
    si $\alpha > -1$ Dans ce cas, $\int_0^a t^{\alpha} \diff t =
    \frac{a^{\alpha + 1}}{\alpha +1}$.
\end{enumerate}

\section{Fonctions continues à valeur dans $\C$}

\begin{definition}
  Soit $f$ continue sur $I \subset \R$ à valeurs dans $\C$.\\
  On dit que $f$ est sommable sur $I$ lorsque $\abs{f}$ est sommable sur
  $I$.
\end{definition}

\begin{exemple}
  \leavevmode
  \begin{itemize}
    \item $t \mapsto \cos \frac1t$ est majorée par $1$ en valeur absolue sur
      $\intv[l]{0}{1}$ donc la fonction est sommable.
    \item $x \mapsto \frac{e^{ix}}{x^2}$ est majorée en module par
      $\frac1{x^2}$ qui est sommable.
    \item $\lim_{x\to +\infty}\int_0^x \frac{\sin t}{t} \diff t$ existe,
      mais $\frac{\sin t}{t}$ n'est pas sommable sur $\R_+$.
  \end{itemize}
\end{exemple}

\begin{proposition}
  Si $f$ et $g$ sont sommables sur $I$ à valeurs dans $\C$, alors $\forall
  \lambda \in \C,\ f + \lambda g$ est sommable.
\end{proposition}
\begin{proof}
  $\abs{f + \lambda g} ≤ \abs{f} + \abs{\lambda} + \abs{g}$ et comme
  $\abs{f}$ et $\abs{g}$ sont sommables, en utilisant le théorème
  \ref{10ana:thm:comparaison}, on obtient le résultat.
\end{proof}

\begin{proposition}
  Soit $f$ une fonction et $I$ un intervalle de $\R$.
  \begin{itemize}
    \item Si $f$ est à valeur dans $\R$, alors $f$ est sommable si et
      seulement si $f^+$ et $f^-$ sont sommables. Dans ce cas, $\int_I f =
      \int_I f^+ - \int_I f^-$
    \item Si $f$ est à valeur dans $\C$, alors $f$ est sommable si et
      seulement si $\Re(f)$ et $\Im(f)$ sont sommables. Dans ce cas, $\int_I
      f = \int_I \Re(f) + i\int_I \Im(f)$.
  \end{itemize}
\end{proposition}
\begin{proof}
  $f = f^+ + f^-$ dans le cas réel, ce qui permet d'être dans le cas de la
  proposition précédente.

  Pour le cas complexe, il suffit de noter que si $f$ est sommable, alors
  $\conj{f}$ est sommable et d'utiliser le fait que $\Re(f) = \frac{f +
  \conj{f}}2$ et $\Im(f) = \frac{f - \conj{f}}{2i}$.
\end{proof}

\begin{proposition}
  $f \mapsto \int_I f$ est une forme linéaire sur l'espace de fonctions
  intégrables dans $\C$.
\end{proposition}
\begin{proof}
  La linéarité a déjà été montrée. Et comme le résultat est dans le corps
  des scalaires, c'est bien une forme linéaire.
\end{proof}

\begin{proposition}
  $\abs*{\int_I f} ≤ \int_I \abs{f}$.
\end{proposition}
\begin{proof}
  $\abs*{\int_I f} = \underbrace{e^{i\alpha} \int_I f}_{\in \R} = \Re\brk*{
  \int_I e^{i\alpha} f} ≤ \int_I e^{i\alpha} \abs{f} ≤ \int_I \abs{f}$.
\end{proof}

\begin{remarque}
  On peut conclure en mentionnant l'existence d'intégrales semi-convergente
  : l'intégrale converge, mais la fonction n'est pas intégrable.\\
  Un tel exemple est la fonction $\frac{\sin t}t$. Son intégrale converge
  car $\frac{\cos x}{x^2}$ (obtenu par intégration par parties) est
  intégrable. Mais on peut montrer que $\int_0^{n\pi} \abs*{\frac{\sin x}x}
  \diff x ≥ \frac{2}{\pi} ≥ \sum_{k=1}^n \frac1k$ qui diverge.
\end{remarque}
