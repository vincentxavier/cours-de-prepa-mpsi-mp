\chapter{L'ensemble $\M_{n,p}$ des matrices à $n$ lignes et $p$ colonnes}

\section{Matrices rectangulaires}

\subsection{Introduction}

Soient $E$ de dimension $p < + \infty$ et $F$ de dimension $n < +\infty$
deux espaces vectoriels sur $\K$ et $u \in \L(E,F)$ une application linéaire
de $E$ dans $F$. Soit $\B = (e_i)_{1≤i≤p}$ une base de $E$ et $\mathcal{C} =
(f_i)_{1≤i≤n}$ une base de $F$. D'après le chapitre précédent, $u$ est
entièrement déterminée par les vecteurs $u(e_i)$ pour $1≤i≤p$. Plus
précsiément, on a : $\forall i \in \llbracket 1, p \rrbracket,\ u(e_i) =
\sum_{j=1}^n a_{j,i} f_j$. Il vient donc que
\begin{align*}
  u(x) & = u\brk*{\sum_{i=1}^p x_i e_i} \\
       & = \sum_{i=1}^p x_i u(e_i)      \\
       & = \sum_{i=1}^p x_i \displaystyle\sum_{j=1}^n a_{j,i}
  f_j \\
       & = \sum_{i=1}^p \displaystyle\sum_{j=1}^n a_{j,i} x_i
  f_j \\
       & = \sum_{j=1}^n \brk*{ \displaystyle\sum_{i=1}^p
  a_{j,i} x_i} f_j \\
       & = \sum_{j=1}^n x'_j f_j
\end{align*}
où $\forall j \in \llbracket 1, n\rrbracket,\ x'_j =
\sum_{i=1}^p a_{j,i} x_i$.

On a donc \[ \left\{ \begin{array}{lclclclcl}
      x'_1 & = & a_{1,1} x_1 & + & a_{1,2} x_2 & + & … & + & a_{1,p} x_p \\
      x'_2 & = & a_{2,1} x_1 & + & a_{2,2} x_2 & + & … & + & a_{2,p} x_p \\
      \vdots & = &  &  &  &  &  &  & \\
      x'_n & = & a_{n,1} x_1 & + & a_{n,2} x_2 & + & … & + & a_{n,p} x_p \\
\end{array} \right. \]

On peut encore l'écrire \[ \begin{pmatrix} x'_1 \\ x'_2\\ \vdots \\ x'_n
    \end{pmatrix} = \begin{pmatrix} a_{1,1} & a_{1,2} & …                                             &  a_{1,p} \\
    a_{2,1}                                 & …       & …                                             &  \vdots  \\
    \vdots                                  &         &                                               &  \vdots  \\
    a_{n,1}                                 & …       & … & a_{n,p}
    \end{pmatrix} × \begin{pmatrix} x_1 \\ x_2 \\ \vdots \\ x_p
  \end{pmatrix} : X' = A⋅X
\]

$A$ est donc la matrice de $u$ relativement aux base $\B$ et $\mathcal{C}$.
On note parfois $A = \M_{\B}^{\mathcal{C}}(u)$.

C'est un tableau de coefficients à $n$ lignes et $p$ colonnes.

\begin{exemple}
  $\begin{pmatrix}0 & 1 \\ 3 & 1\\ 1 & -1 \end{pmatrix} × \begin{pmatrix}1
  \\ 2 \end{pmatrix} = \begin{pmatrix} 2 \\ 5 \\ -1 \end{pmatrix}$.

  $u \colon \R^3 \to \R^2,\ (x,y,z) \mapsto (x + y, y - z)$ a pour matrice
  par rapport aux bases canoniques $A = \begin{pmatrix} 1 & 1 0 \\ 0 & 1 &
    -1 \end{pmatrix}$ et on a $\begin{pmatrix} x' \\ y' \end{pmatrix} =
    \begin{pmatrix} 1 & 1 & 0 \\ 0 & 1 & -1 \end{pmatrix} × \begin{pmatrix}
  x \\y \\ z \end{pmatrix}$.

  Dans $\R_3[X]$, on consièdre l'application $D \colon P \mapsto P'$. La
  matrice de $D$ comme endomorphisme dans la base $(1,X,X^2,X^3)$ est
  $\begin{pmatrix} 0 & 1 & 0 & 0 \\ 0 & 0 & 2 & 0 \\ 0 & 0 & 0 & 3 \\ 0 & 0
  & 0 & 0 \end{pmatrix}$

  La matrice de l'identité, $\Id \colon E \to E,\ x \mapsto x$ exprimée dans
  la même base est $I_n = \begin{pmatrix}
    1      & 0      & …      & 0      \\
    0      & 1      & \ddots & \vdots \\
    \vdots & \ddots & \ddots & 0      \\
    0      & …      & 0      & 1      \\
  \end{pmatrix}$
\end{exemple}

Notons pour finir cette introduction que, à toute application linéaire $u
\in \L(E,F)$, et pour toute choix d'une base $\B$ de $E$ et d'une base
$\mathcal{C}$ de $F$, il existe une et une seule matrice
$\M_{\B}^{\mathcal{C}}(u) \in \M_{n,p}(\K)$ et réciproquement, pour toute
matrice $A \in \M_{n,p}(\K)$, il existe une et une seule application
linéaire $u \in \L(E,F)$ telle que $A = \M_{\B}^{\mathcal{C}}(u)$.

On dispose ainsi d'une bijection entre $\L(E,F)$ et $\M_{n,p}(\K)$.

\subsection{Structure de $\M_{n,p}(\K)$}

On définit les opérations usuelles comme l'addition et la soustraction en
utilisant fondamentalement la bijection mentionnée ci-dessus. Ainsi, on a
les définitions suivantes.

\begin{definition}
  Soient $A = \M_{\B}^{\mathcal{C}}(u)$ et $B = \M_{\B}^{\mathcal{C}}(v)$.
  On définit $A + B$ par $A + B = \M_{\B}^{\mathcal{C}}(u + v)$.
\end{definition}

Une conséquence directe de cette définition est que les colonnes de $A + B$
sont les $(u+v)(e_i) = u(e_i) + v(e_i)$ et donc si $A =
(a_{i,j})_{\substack{1≤i≤p\\1≤j≤p}}$ et $B =
(b_{i,j})_{\substack{1≤i≤p\\1≤j≤p}}$, alors $A + B$ est la matrice des
$(a_{i,j} + (b_{i,j})_{\substack{1≤i≤p\\1≤j≤p}}$.

\begin{exemple}
  $\begin{pmatrix} 1 & 3                                   \\ 2 &  -1 \end{pmatrix} + \begin{pmatrix} -1 & 1 \\
  0                  & 2 \end{pmatrix} = \begin{pmatrix} 0 &  4 \\ 2 &
1\end{pmatrix}$
\end{exemple}

On a même la proposition suivante qui complète la bijection entre $\L(E,F)$
et $\M_{n,p}(\K)$.

\begin{proposition}
  $(\M_{n,p}(\K),+)$ est isomorphe à $(\L(E,F),+)$.
\end{proposition}
\begin{proof}
  \begin{itemize}
    \item Le neutre $(0)$ est  la matrice de l'application nulle.
    \item La matrice de l'application $u - v$ est $A - B$.
  \end{itemize}
\end{proof}

\begin{definition}
  Soit $A = \M_{\B}^{\mathcal{C}}(u)$ et $\lambda \in \K$. $\lambda A$ est
  la matrice de $\lambda u$.
\end{definition}

\begin{proposition}\label{alg08:prop:isomorphisme_espaces_vect}
  $(\M_{n,p}(\K),+,×_{\text{ext}})$ est isomorphe à
  $(\L(E,F),+,×_{\text{ext}})$.
\end{proposition}
\begin{proof}
  Laissée au lecteur : il suffit de vérifier les axiomes.
\end{proof}

En écriture matricielle, on a $\lambda (a_{i,j})_{\substack{1≤i≤p\\1≤j≤p}}
= (\lambda a_{i,j})_{\substack{1≤i≤p\\1≤j≤p}}$.

\begin{definition}
  Soient $A = (a_{i,j})_{\substack{1≤i≤p\\1≤j≤n}} \in \M_{n,p}(\K)$ et $B =
  (b_{i,j})_{\substack{1≤i≤n\\1≤j≤m}}$ deux matrices.\\
  $A = \M_{\B}^{\mathcal{C}}(u),\ u \in \L(\underset{p}{E},\underset{n}{F})$
  $A = \M_{\mathcal{C}}^{\mathcal{D}}(v),\ v \in
  \L(\underset{n}{F},\underset{m}{G})$ de telle façon que $v \circ u \in
  \L(\underset{p}{E},\underset{m}{G})$.

  On pose alors $\underset{(m,n)}{B} × \underset{(n,p)}{A} =
  \underset{(m,p)}{\M_{\B}^{\mathcal{D}}(v \circ u)}.$
\end{definition}

On peut préciser la valeur des coefficients du produit $B × A$ en fonction
des coefficients $(a_{i,j})$ et $(b_{i,j})$.

$\forall k \in \llbracket 1 , p \rrbracket,\ u(e_k) =
\sum_{i=1}^n a_{i,k} f_i$.\\
$\forall l \in \llbracket 1 , n\rrbracket,\ v(f_l) =
\sum_{i=1}^m b_{i,l} g_i$.\\
$\forall k \in \llbracket 1 , p \rrbracket,\ v\circ u(e_k) =
\sum_{i=1}^m c_{i,k} g_i$.
\begin{align*}
  \forall k \in \llbracket 1 , p \rrbracket,\ v\circ u(e_k) = v(u(e_k)) & =
  v\brk*{ \sum_{i=1}^n a_{i,k} f_i} \\
   & = \sum_{i=1}^n a_{i,k} v(f_i) \\
   & = \sum_{l=1}^n a_{l,k} v(f_l) \\
   & = \sum_{l=1}^n a_{l,k} \displaystyle\sum_{i=1}^m b_{i,l}
  g_i \\
  & = \sum_{i=1}^m \brk*{ \displaystyle\sum_{l=1}^n a_{l,k}
b_{i,l} } g_i
\end{align*}
D'où $\forall (i,k) \in \llbracket 1 , m\rrbracket × \llbracket 1 , p
\rrbracket,\ c_{i,k} = \sum_{l=1}^n b_{i,l} a_{l,k}$

En pratique, on dispose les matrices de la façon suivante pour procéder au
calcul :
\begin{center}
  \begin{tikzpicture}
    \node at (0,0) { $
      \begin{pmatrix}
        0 & 1 & 0 & 1  \\
        1 & 0 & 0 & -1 \\
        0 & 0 & 1 & 1  \\
      \end{pmatrix}
    $ } ;
    \node at (-2.5,-1.5) { $
      \begin{pmatrix}
        1 & 1 & 4  \\
        2 & 3 & -1 \\
      \end{pmatrix}
    $ } ;
  \end{tikzpicture}
\end{center}

De cette façon, le résultat s'obtient en additionnant les produits des
lignes par les colonnes.

Sous réserve d'existence, on a les lois suivantes de distributivité et
d'associativité :
\begin{itemize}
  \item $C(A + B) = CA + CB$
  \item $(A+B)C = AC + BC$
  \item $C(BA) = (CB)A = CBA$.
\end{itemize}

\begin{definition}[transposition]
  Soit $A \in \M{n,p}(\K)$, $A = (a_{i,j})_{\substack{1≤i≤p\\1≤j≤p}}$. On
  définit $\transpose{A}$ par $\transpose{A} =
  (\lambda_{i,j})_{\substack{1≤i≤n,\\1≤j≤p}}$ où $\lambda_{i,j} = a_{j,i}$.
\end{definition}

\begin{exemple}
  $\transpose{\begin{pmatrix}
      1 & 1 & 4  \\
      2 & 3 & -1 \\
    \end{pmatrix}} = \begin{pmatrix}
    1 & 2  \\
    1 & 3  \\
    4 & -1 \\
  \end{pmatrix}$
\end{exemple}

\begin{proposition}
  \begin{itemize}
    \item $\forall A \in \M_{n,p}(\K),\ \transpose{\brk*{\transpose{A}}}$
    \item $\forall A \in \M_{n,p}(\K),\ \forall B \in \M_{p,q}(\K),\
      \transpose{A⋅B} = \transpose{B}⋅\transpose{A}$
  \end{itemize}
\end{proposition}
\begin{proof}
  Le premier point est évident, étudions le second point.

  $A = (a_{i,j})$ et $B = (b_{i,j}$. Ainsi, $AB = (c_{i,j}) =
  (\sum_{k=1}^p a_{i,k} b_{k,j})$.\\
  $\transpose{AB} = (c_{j,i}) = (\sum_{k=1}^p
  a_{j,k}b_{k,i}$.\\
  Ce dernier résultat n'est en fait autre que les coefficients de
  $\transpose{B}\transpose{A}$, d'où le résultat.

  En effet, $\transpose{B} \transpose{A} = (d_{i,j})$, avec $d_{i,j} =
  \sum_{k=1}^p \beta_{i,k} \alpha_{k,j} =
  \sum_{k=1}^p b_{k,i} a_{j,k}$.
\end{proof}

\begin{proposition}
  Soient $A$ et $B$ deux matrices de $\M_{n,p}(\K)$ et $\lambda \in \K$.

  $\transpose{A + \lambda B} = \transpose{A} + \lambda \transpose{B}$
\end{proposition}
\begin{proof}
  Preuve aisée en passant par les coordonnées de la matrice transposée.
\end{proof}

\section{Matrices carrés : l'ensemble $\M_n(\K)$}

On note désormais $\M_n(\K)$ l'ensemble $\M_{n,n}(\K)$ des matrices carrées
à $n$ lignes et $n$ colonnes.

\subsection{Matrices particulières}

On reprécise ici la notion de matrice nulle : $0_{\M_{n}(\K)} = (0)$.

On peut définir la matrice diagonale : $\diag(a_1,a_2,…,a_n) =
\begin{pmatrix}
  a_1    & 0      & …      & 0      \\
  0      & a_2    & \ddots & \vdots \\
  \vdots & \ddots & \ddots & 0      \\
  0      & …      & 0      & a_n    \\
\end{pmatrix}$. \\ En particulier, $I_n = \diag(\underbrace{1,1,…,1}_{n\
\text{fois}})$.

\begin{proposition}
  Si $A \in \M_n(\K)$ alors $\transpose{A} \in \M_n(\K)$.
\end{proposition}
\begin{proof}
  Aisée en considérant la définition de la transposée.
\end{proof}

\begin{definition}
  \begin{itemize}
    \item $A$ est dite \emph{symétrique} lorsque $\transpose{A} = A$.
    \item $A$ est dite \emph{antisymétrique} lorsque $\transpose{A} = -A$.
  \end{itemize}
\end{definition}

\begin{exemple}
  $\begin{pmatrix}
    0  & -1 & 2  \\
    1  & 0  & -3 \\
    -2 & 3  & 0  \\
  \end{pmatrix}$ est antisymétrique.\\
  $\begin{pmatrix}
    1 & 1 \\
    1 & 0 \\
  \end{pmatrix}$ est symétrique.
\end{exemple}

\begin{definition}
  \begin{itemize}
    \item La matrice $A$ est dite triangulaire supérieure lorsque $\forall
      i,j,\ i > j \implies a_{i,j} = 0$.
    \item La matrice $A$ est dite triangulaire inférieure lorsque $\forall
      i,j,\ i < j \implies a_{i,j} = 0$.
  \end{itemize}
\end{definition}

\begin{exemple}
  $A = \begin{pmatrix}
    1 & 2 & 3 \\
    0 & 1 & 0 \\
    0 & 0 & 4 \\
  \end{pmatrix}$ est triangulaire supérieure.
\end{exemple}

\begin{proposition}
  $\M_n(\K)$ est un $\K$-espace vectoriel de dimension $n^2$.
\end{proposition}
\begin{proof}
  $\M_n(\K)$ est isomorphe à $\L(E)$ (voir la proposition
  \ref{alg08:prop:isomorphisme_espaces_vect}) et le théorème
  \ref{alg07:prop:dimension_espace_applications_lineaires} permet de
  conclure, $\L(E)$ étant de dimension $n^2$.
\end{proof}

\begin{proposition}
  \begin{enumerate}
    \item L'ensemble $\mathcal{S}_n(\K)$ des matrices symétriques et
      l'ensemble $\mathcal{A}_n(\K)$ sont des sous-espaces vectoriels de
      $\M_n(\K)$.
    \item $\mathcal{S}_n(\K) \oplus \mathcal{A}_n(\K) = \M_n(\K)$.
  \end{enumerate}
\end{proposition}
\begin{proof}
  \begin{enumerate}
    \item Soient $A$ et $B$ deux matrices et $\lambda$ un scalaire.  Comme
      $\transpose{A + \lambda B} = \transpose{A} + \lambda \transpose{B}$,
      les espaces $\mathcal{S}_n(\K)$ et $\mathcal{A}_n(\K)$ sont stables
      par combinaisons linéaires. Comme ils contiennent tous deux
      $0_{\M_n(\K)}$, ce sont bien des sous-espaces vectoriels.
    \item Soit $A \in \mathcal{S}_n(\K) \cap \mathcal{A}_n(\K)$. On a donc
      $\transpose{A} = A$ et $\transpose{A} = -A$, d'où $A = -A$, ce qui
      entraîne $2A = (0)$ et donc comme $2 ≠ 0$, $A = (0)$. On a donc $\{
        (0) \} \subset \mathcal{S}_n(\K) \cap \mathcal{A}_n(\K) \subset \{
      (0) \}$, d'où l'égalité.

      Cherchons $U \in \mathcal{S}_n(\K)$ et $V \in \mathcal{A}_n(\K)$
      telles que $A = U + V$.\\
      Raisonnons par condition nécessaire :\\
      $\transpose{A} = \transpose{U} + \transpose{V} = U - V$. Ainsi $U =
      \dfrac12 \brk*{ A + \transpose{A}}$ et $V = \dfrac12 \brk*{ A -
      \transpose{A}}$ conviennent. Ces deux dernières étant définies de
      façon unique, la condition est suffisante. Ainsi, $\forall A \in
      \M_n(\K)\, \exists! (U,V) \in \mathcal{S}_n(\K) × \mathcal{A}_n(\K),\
      A = U + V$. On a donc $\mathcal{S}_n(\K) \oplus \mathcal{A}_n(\K) =
      \M_n(\K)$ et donc $\mathcal{S}_n(\K)$ et $\mathcal{A}_n(\K)$ sont
      supplémentaires dans $\M_n(\K)$.
  \end{enumerate}
\end{proof}
\begin{remarque}
  Une étude plus poussée permet de montrer que $\dim \mathcal{S}_n(\K) =
  \dfrac{n^2 + n}2 = \dfrac{n(n+1)}2$. En effet, en décomposant $A \in
  \mathcal{S}_n(\K)$ suivant une base de $\M_n(\K)$, par exemple $E_{i,j} =
  (\delta_{l,i}\delta{j,k})$, la condition $A \in \mathcal{S}_n(\K)$ s'écrit
  $A = \sum_{i=1}^n \lambda_i E_{i,i} +
  \sum_{i=1}^{n-1} \sum_{j=i+1}^n a_{i,j} (E_{i,j +
  E_{j,i}}$.\\
  La dimension de $\mathcal{A}_n(\K)$ se trouve, soit de façon analogue,
  soit en utilisant le résultat du théorème \ref{alg07:thm:dimension_somme}
  sur la dimension d'un supplémentaire.
\end{remarque}

\subsection{Trace d'une matrice carrée}

\begin{definition}[trace d'une matrice]
  Soit $A \in \M_n(\K)$. On définit la \emph{trace} d'une matrice comme la
  somme des éléments de la diagonale. Ainsi, si $A =
  (a_{i,j})_{\substack{1≤i≤n\\1≤j≤n}},\ \tr A = \sum_{i=1}^n
  a_{i,i}$.
\end{definition}

\begin{exemple}
  $A = \begin{pmatrix}
    \cos \theta & - \sin \theta & 0 \\
    \sin \theta & \cos \theta   & 0 \\
    0           & 0             & 1 \\
  \end{pmatrix}$ a pour trace $\tr A = 2 \cos \theta + 1$.
\end{exemple}

\begin{proposition}
  La trace est une forme linéaire sur $\M_n(\K)$.
\end{proposition}
\begin{proof}
  Aisée : il suffit d'écrire la définition.
\end{proof}

\begin{proposition}
  $\forall (A,B) \in \M_n(\K)^2,\ \tr(AB) = \tr(BA)$.
\end{proposition}
\begin{proof}
  On considère l'application $\tr \colon \M_n(\K) \to \K$.

  $AB = (c_{i,j}),\ c_{i,j} = \sum_{k=1}^n a_{i,k}b_{k,j}$ et
  donc $\tr(AB) = \sum_{i=1}^n c_{i,i} =
  \sum_{i=1}^n \displaystyle\sum_{k=1}^n a_{i,k}b_{k,i}$.\\
  $BA = (d_{i,j}),\ d_{i,j} = \sum_{k=1}^n b_{i,k}a_{k,j}$ et
  donc $\tr(BA) = \sum_{i=1}^n d_{i,i} =
  \sum_{i=1}^n \displaystyle\sum_{k=1}^n b_{i,k}a_{k,i}$.\\
  Or $\sum_{i=1}^n \displaystyle\sum_{k=1}^n b_{i,k}a_{k,i} =
  \sum_{i=1}^n \displaystyle\sum_{k'=1}^n b_{i,k'}a_{k',i} =
  \sum_{k=1}^n \displaystyle\sum_{k'=1}^n b_{k,k'}a_{k',k} =
  \sum_{k=1}^n \displaystyle\sum_{i=1}^n b_{k,i}a_{i,k} =
  \sum_{i=1}^n \displaystyle\sum_{k=1}^n b_{k,i}a_{i,k} =
  \tr(AB)$. D'où le résultat.
\end{proof}

On a vu qu'il y avait un isomorphisme de $\L(\K^n)$ dans $\M_n(\K)$ et si
les isomorphismes transportent les structures\footnote{mal dit et à
démontrer dans un chapitre d'algèbre générale}, on obtient que
$(\L(\K^n),+,\circ)$ est un anneau isomorphe à $(\M_n(\K),+,×)$. Comme le
premier est ni commutatif, ni intègre, le second ne l'est pas non plus.

Une conséquence est l'absence de formule générale du binôme de Newton. On
peut toutefois noter que pour deux matrices qui commutent, en particulier si
l'une des deux est l'identité, la formule est vraie. Ainsi, on a \[ (A +
I)^n = \sum_{k=0}^n \binom{n}{k} A^k  \] avec la convention $A^0 = I$.

\begin{proposition}
  $(\M_n(\K), +, ×_{\text{ext}}, ×_{\text{int}})$ est une $\K$-algèbre non
  commutative et non intègre.
\end{proposition}
\begin{proof}
  Un rapide calcul montre que $\lambda(AB) = (\lambda A)B = A(\lambda B)$ et
  ce $\forall \lambda \in\K,\ \forall (A,B) \in \M_n(\K)$.
\end{proof}

\subsection{Groupe des matrices carrées inversibles}

\begin{definition}
  On dit que $A \in \GL_n(\K)$ si et seulement si $A = \M_{\B}(u)$ et $u \in
  \GL(E\K^n)$.
\end{definition}

\begin{proposition}
  $A$ est inversible si et seulement si $u$ est un automorphisme.
\end{proposition}
\begin{proof}
  C'est une relecture de la définition précédente.
\end{proof}

\begin{exemple}
  $A = \begin{pmatrix}
    1 & 2 \\
    2 & 5 \\
  \end{pmatrix}$ est-elle inversible ?
\end{exemple}

\subsubsection{Méthodes des coefficients indéterminés}

On cherche $x,y,z$ et $t$ tels que \[
  \begin{pmatrix}
    1 & 2 \\
    2 & 5 \\
  \end{pmatrix} × \begin{pmatrix}
    x & y \\
    z & t \\
  \end{pmatrix} = \begin{pmatrix}
    1 & 0 \\
    0 & 1 \\
  \end{pmatrix} .\]

Cette méthode est assez lourde et conduit à un système de $n$ équations à
$n$ inconnues.

\subsubsection{Méthode de Gauss}

\begin{proposition}
  Si $B$ est inverse à droite de $A$ et $C$ inverse à gauche de $A$, alors
  $B = C$.
\end{proposition}
\begin{proof}
  $A B = I$ d'où $C(AB) = C$\\
  $C A = I$ d'où $(CA)B = C(AB) = C$\\
  Or $(CA)B = B$.\\
  Donc $B = C$.
\end{proof}

On considère ici l'inversion de l'application linéaire induite par la
matrice $A$ pour résoudre le système.

\subsubsection{Ultérieuemnt}

\begin{proposition}
  $(\GL_n(\K), ×)$ est un groupe non commutatif (isomorphe à
  $(\GL(\K^n),\circ)$)
\end{proposition}
\begin{proof}
  Voir le morphisme d'espace vectoriel, qui induit ce morphisme de groupe.
\end{proof}

\begin{corollaire}
  Si $A$ et $B$ sont inversibles, alors $AB$ est inversible et $(AB)^{-1} =
  B^{-1} A^{-1}$.
\end{corollaire}
\begin{proof}
  C'est une conséquence directe de la notion de groupe appliquée à
  $\GL_n(\K)$.
\end{proof}

\begin{question}
  Montrer que $A$ telle que $A^2 + 3A - 2I = (0)$ est inversible et donner
  son inverse.
\end{question}
\begin{solution}
  $A^2 + 3A - 2I = (0) \implies A(A + 3I) = 2I$ et donc $A$ est inversible
  et $A^{-1} = \dfrac{A + 3I}2$.
\end{solution}

On peut trouver la matrice inverse de $A$ en multipliant par les matrices
des opérateurs $D_k(\lambda)$ et $T_{i,j}(\alpha)$.

% Mériterait d'être détaillé par un exemple. Voir p. 16 de «Théorème du
% rang.pdf»

\subsection{Changement de base}

On considère $u  \in \L(E,F)$ et $A = \M_{\B}^{\mathcal{C}}(u)$ et $A' =
\M_{\B'}^{\mathcal{C'}}(u)$ deux matrices où $\B$ et $\B'$ sont des bases
de $E$ et $\C$ et $\C'$ des bases de $F$.

\subsubsection{Effet sur les vecteurs}

Dans $E$, on a $\B = (e_i)_{1≤i≤p}$ et $\B' = (e'_i)_{1≤i≤p}$ deux bases.
$\forall x \in E,\ x = \sum_{i=1}^p x_i e_i =
\sum_{i=1}^p x'_i e'_i$. Ainsi, $x$ peut être représenté dans
$\M_{p,1}$ aussi bien par $X = \begin{pmatrix} x_1 \\ x_2 \\ \vdots \\ x_p
\end{pmatrix}$ que par $X' = \begin{pmatrix} x'_1 \\ x'2 \\ \vdots \\ x'_p
\end{pmatrix}$.

En pratique, les vecteurs $e'_i$ de $\B'$ sont exprimés dans $\B$ et on a
$e'_j = \sum_{i=1}^p p_{i,j} e_i$.

On appelle $P = (p_{i,j})_{\substack{1≤i≤p\\1≤j≤p}}$ la matrice de passage
de $\B$ à $\B'$.

$\forall x \in E,\ x = \sum_{j=1}^p x'_j e'_j =
\sum_{j=1}^p x'_j \displaystyle\sum_{i=1}^p p_{i,j} e_i =
\sum_{i=1}^p \brk*{ \displaystyle\sum_{j=1}^p x'_j p_{i,j} }
e_i$.

$\forall i \in \llbracket 1; p \rrbracket,\ x_i = \sum_{j=1}^p
p_{i,j} x'_j$. Ainsi, $X = PX'$ et comme $P$ est inversible, $X' = P^{-1}
X$.

\begin{remarque}
  La matrice de passage $P$ est en fait $\M_{\B}^{\B'}(\Id)$.
\end{remarque}

\subsubsection{Effet sur les matrices d'applications linéaires}

Avec les mêmes notations, et en posant $Y$ et $Y'$ les matrices $\M_{1,n}$
telles que $Y = AX$ et $Y' = A'X'$, ainsi que $P$ la matrice de passage de
$\B$ à $\B'$ et $Q$ la matrice de passage de $\mathcal{C}$ à $\mathcal{C}'$,
on a $X = P X'$ et $Y = Q Y'$. Ainsi,
\begin{align*}
     & Y = AX      \\
\iff &             \\
     & QY' = AP X' \\
\iff &             \\
     & Y' = Q^{-1} AP X'
\end{align*}
permet de montrer que $A' = Q^{-1} AP$.

En appliquant ce résultat aux endomorphismes et en utilisant la même base
pour les antécédents et les images, on a $A' = P^{-1} A P$. Ainsi, $\tr(A')
= \tr(P^{-1}AP) = \tr(A(P^{-1}P)) = \tr(A)$. Ceci jsutifie le fait que la
trace d'une matrice ne dépend pas de la base choisie et donc :

\begin{definition}
  On appelle trace d'un endomorphisme et on note $\tr(u)$ la trace de la
  matrice associée à $u$ dans n'importe quelle base.
\end{definition}

\subsection{Rang d'une matrice}

\begin{definition}[rang d'une matrice]
  Soit $A \in \M_{n,p}(\K)$. On appelle \emph{rang} de la matrice $A$, noté
  $\rg A$, le rang du sous-espace vectoriel engendré par les vecteurs
  colonnes dans $\K^n$.
\end{definition}

\begin{proposition}
  Soit $u \in \L(E,F)$ telle que $A = \M_{\B}^{\mathcal{C}}(u)$. On a
  $\rg(u) = \rg(A)$.
\end{proposition}
\begin{proof}
  Par définition $\rg(A)$ est le rang du sous-espace vectoriel engendré par
  les vecteurs colonnes de $A$. Or, d'après la définition
  \ref{alg07:def:rang_application_lineaire} le rang est la dimension de $\Im
  u$, c'est à dire le rang d'une famille de vecteurs.
  % Pourrait être mieux rédiger
\end{proof}

\begin{definition}[matrices équivalentes]
  Soient $A$ et $B$ deux matrices de $\M_{n,p}(\K)$. On dit que $A$ et $B$
  sont équivalentes lorsqu'il existe deux matrices \emph{inversibles} $U \in
  \GL_n(\K)$ et $V \in \GL_p(\K)$ telles que $B = U A V$.
\end{definition}

\begin{exemple}
  Soient $u \in \L(E,F)$ et $A = \M_{\B}^{\mathcal{C}}$ et $A' =
  \M_{\B'}^{\mathcal{C}'}$. On a vu précédement que $A' = Q^{-1} A P$ et
  donc $A'$ est équivalente à $A$.
\end{exemple}

\begin{proposition}
  «$A$ est équivalente à $B$» est une relation d'équivalence.
\end{proposition}
\begin{proof}
  \begin{itemize}
    \item $\forall A \in \M_{n,p}(\K),\ A = I_n A I_p$, donc $A$ est
      équivalente à $A$ et la relation est réflexive.
    \item $\forall (A,B) \in \M_{n,p}(\K)^2 \mid A \ \text{équivalente à}\ B
      \implies \exists (U,V) \in \GL_n(\K) × \GL_p(\K) \ \mid B = U A V
      \iff A = U^{-1} B V^{-1}$. La relation est donc symétrique.
      \item $\forall (A,B,C) \in \M_{n,p}(\K)^3$ telles que $B = U A V$ et
        $C = S B W, \ (U,S) \in \GL_n(\K)^2,\ (V,W) \in \GL_p(\K)^2$. On a
        alors $C = S U A W V = \underbrace{SU}_{\in \GL_n(\K)} A
        \underbrace{WV}_{\in \GL_p(\K)}$. Ainsi, la relation est transitive.
  \end{itemize}
  La relation est donc bien une relation d'équivalence.
\end{proof}

\begin{remarque}
  Deux matrices équivalentes sont en fait l'expression du même morphisme
  dans des bases différentes de $E$ et de $F$. Ainsi, deux matrices
  équivalentes ont le même rang.
\end{remarque}

\begin{definition}
  On définit $I_{n,p}(r) = \begin{pmatrix}
    1      & 0      & \cdots & 0      & 0      & … & 0      \\
    0      & 1      & \ddots & \vdots & \vdots &   & \vdots \\
    \vdots & \ddots & \ddots & 0      & \vdots &   & \vdots \\
    0      & …      & 0      & 1      & 0      & … & 0      \\
    0      & …      & …      & 0      & 0      & … & 0      \\
    \vdots &        &        & \vdots & \vdots &   & \vdots \\
    0      & …      & …      & 0      & 0      & … & 0      \\
  \end{pmatrix}$
\end{definition}

\begin{remarque}
  \begin{itemize}
    \item Cette matrice est construite en «complétant» $I_r$, l'identité sur
      $\K^r$ en une matrice de dimension $n,p$.
    \item Cette matrice est de rang $r$.
  \end{itemize}
\end{remarque}

\begin{proposition}
  $A \in \M_{n,p}(\K)$ est de rang $r$ si et seulement si elle est
  équivalente à $I_{n,p}(r)$.
\end{proposition}
\begin{proof}
  Si $I_{n,p}(r)$ est équivalente à $A$, alors elles ont le même rang.

  Supposons que $\rg A = r$. Alors $A$ représente un morphisme $u \in
  \L(E,F)$ relativement à des bases, et $\dim \Im u = r$ et donc $\dim \ker
  u = p - r$.

  Considérons dans $E$ une base $(e_i)_{1≤i≤p}$ telle que $(e_i)_{r+1≤i≤p}$
  soit une base de $\ker u$. Les $(u(e_i))_{1≤i≤r}$ engendrent $\Im u$ et
  comme $\dim \Im u = r$, cette famille est une base de $\Im u$ et donc
  libre dans $F$. On peut ainsi la compléter en une base de $F$.

  On en déduit ainsi que la matrice de $u$ dans des bases «bien choisies»
  est $I_{n,p}(r) = \begin{pmatrix}
    1      & 0      & \cdots & 0      & 0      & … & 0      \\
    0      & 1      & \ddots & \vdots & \vdots &   & \vdots \\
    \vdots & \ddots & \ddots & 0      & \vdots &   & \vdots \\
    0      & …      & 0      & 1      & 0      & … & 0      \\
    0      & …      & …      & 0      & 0      & … & 0      \\
    \vdots &        &        & \vdots & \vdots &   & \vdots \\
    0      & …      & …      & 0      & 0      & … & 0      \\
  \end{pmatrix}$
\end{proof}

\begin{question}
  Déterminer le rang de $A = \begin{pmatrix}
    1 & 2  & 3  \\
    3 & 2  & 1  \\
    4 & 3  & 4  \\
    2 & -1 & -1 \\
  \end{pmatrix}$
\end{question}
\begin{solution}
  On a $A$ équivalente à $I_{4,3}(2)$.
\end{solution}

\begin{corollaire}
  Le rang d'une matrice est égal au rang de sa transposée.
\end{corollaire}
\begin{proof}
  Si $A$ est équivalente à $I_{n,p}(r)$, alors il existe $Q$ et $P$ telles
  que $Q^{-1} A P = I_{n,p}(r)$. Ainsi $\transpose{I_{n,p}(r)} =
  \transpose{\brk{Q^{-1} A P}} \iff I_{p,n}(r) = \transpose{P} \transpose{A}
  \transpose{Q^{-1}}$. Or $PP^{-1} = I \implies
  \transpose{P}\transpose{P^{-1}} = I$, donc $\transpose{P}$ et
  $\transpose{Q^{-1}}$ sont inversibles. On a donc que $\transpose{A}$ est
  équivalente à $I_{p,n}(r)$ et donc que la transposée de $A$ est aussi de
  rang $r$.
\end{proof}

\begin{question}
  Calculer $A^n$, avec $A = \begin{pmatrix}
    a + b & 0 & a     \\
    0     & b & 0     \\
    a     & 0 & a + b \\
  \end{pmatrix}$.
\end{question}
\begin{solution}
  On peut remarquer que $A = bI + aJ$, où $J = \begin{pmatrix}
    1 & 0 & 1 \\
    0 & 0 & 0 \\
    1 & 0 & 1 \\
  \end{pmatrix}$, et que $I$ et $J$ commuttent et appliquer la formule du
  binôme de Newton.
\end{solution}

\subsection{Interprétation du rang pour les systèmes de $n$ équations
  linéaires à $p$ inconnues}

On considère $S : \begin{cases}
  a_{1,1} x_1 + … + a_{1,p} x_p & = b_1    \\
  a_{2,1} x_1 + … + a_{2,p} x_p & = b_2    \\
  \vdots                        & = \vdots \\
  a_{n,1} x_1 + … + a_{n,p} x_p & = b_n    \\
\end{cases}$

Les familles $(a_{i,j})_{\substack{1≤i≤n\\1≤j≤p}}$ $(b_i)_{1≤i≤n}$ sont
données, la famille $(x_i)_{1≤i≤p}$ inconnue.

\subsubsection{Aspect matriciel}

Matriciellement, ce système s'écrit $AX = B$. Dans le cas particulier où $A$
est inversible, la solution existe et est unique : $X = A^{-1}B$.

\subsubsection{Aspect vectoriel}

$A$ est la matrice de $u \in \L(E,F)$, relativement à certaines bases. Si
$\vec{x} = (x_1, … , x_p)$ et $\vec{b} = (b_1, …, b_n)$ relativement aux
bases considérées, il faut résoudre $u(\vec{x}) = \vec{b}$. On dit que c'est
une équation linéaire.

Si $\vec{b} \notin \Im u$, alors l'ensemble des solutions est vide.

Si $\vec{b} \in \Im u$, alors il exite $\vec{x_0}$ tel que $u(\vec{x_0} =
\vec{b}$ et donc $u(\vec{x}) = \vec{b} \iff u(\vec{x}) = u(\vec{x_0}) \iff
u(\vec{x} - \vec{x_0}) = 0_E$, c'est -à-dire $\vec{x} - \vec{x_0} \in \ker
u$.

L'ensemble des solutions d'un tel système est soit vide, soit une
translation d'un sous-espace vectoriel.

\subsubsection{Aspect géométrique}

On peut voir un tel système comme une intersection d'hyperplans. Précisons
cette notion.

\begin{definition}
  Dans un espace vectoriel $E$, on appelle \emph{hyperplan} tout sous-espace
  vectoriel dont un supplémentaire est de dimension 1.
\end{definition}

\begin{remarque}
  Si $\dim E = n < \infty$, alors un hyperplan est un sous-espace vectoriel
  de dimension $n-1$.
\end{remarque}

\begin{proposition}
  Tout hyperplan de $E$, avec $\dim E = n < \infty$ est le noyau d'une forme
  linéaire non nulle et réciproquement.
\end{proposition}
\begin{proof}
  Si $\varphi \in E^*$ le dual de $E$ et $\varphi ≠ 0_{E^*}$. On a $\Im
  \varphi \subset \K$ et $\Im \varphi ≠ \{ 0_{\K} \}$, donc $\dim \Im
  \varphi = 1$ et $\Im \varphi = \K$.
  D'après le théorème du rang (\ref{alg07:thm:rang}), $\dim \ker \varphi =
  n - 1$.

  Réciproquement, soit $H$ un hyperplan de $E$. Il s'agit de construire
  $\varphi \in E^*$ telle que $\ker \varphi = H$.\\
  Soit $(e_i)_{1≤i≤n-1}$ une base de $H$. On peut la compléter en une base
  de $E$. Soit $x \in E,\  x = \sum_{i=1}^n x_i e_i$. On a donc
  $\varphi(x) = \sum_{i=1}^n x_i \varphi(e_i)$.\\
  Imposons $\forall i \in \llbracket 1 ; n -1 \rrbracket,\ \varphi(e_i) = 0$
  et $\varphi(e_n) = \lambda ≠ 0$\footnote{On aurait pu prendre $\lambda =
  1$.}.\\
  $\varphi(x) = \lambda x_n$ et donc $\varphi \in E^*\setminus \{ 0_{E^*}
  \}$ et $\ker \varphi = H$.
\end{proof}

La somme $a_{1} x_1 + … + a_{p} x_p$ peut être vue comme une application
linéaire $f$ non nulle si les $a_i$ sont non tous nuls. L'équation $a_{1}
x_1 + … + a_{p} x_p = 0$ caractérise l'appartenance au noyau de $f$, donc à
un hyperplan.

% il serait intéressant de revenir sur la méthode de Gauss et de mentionner
% le théorème de Rouché-Fontené : sans que le rôle de celui-ci soit central
% dans l'acquisition des connaissances des systèmes d'équations, il mérite
% cependant d'être mentionné.
