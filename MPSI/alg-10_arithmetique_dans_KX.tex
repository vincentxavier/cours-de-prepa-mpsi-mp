\chapter{Arithmétique dans l'anneau {$\K[X]$} des polynômes à une indéterminée}

\section{Idéaux de $\K[X]$}

Ici, $\K$ est un corps, le plus souvent un sous-corps de $\C$.

On rappelle que $\K[X]$ est un anneau commutatif intègre dont les
inversibles sont les polynômes constants non nuls.

\begin{definition}[Polynômes associés]
  On dit que deux polynômes $P$ et $Q$ sont \emph{associés} lorsque $P \mid
  Q$ et $Q \mid P$.
\end{definition}

\begin{proposition}
  Deux polynômes associés diffèrent d'une constante multiplicative non
  nulle.
\end{proposition}
\begin{proof}
  Par définition, on a $P = AQ$ et $Q = BP$, où $(A,B) \in \K[X]^2$. Ainsi,
  $P = ABP$, ou encore $P(1-AB) = 0_{\K[X]}$. Comme $\K[X]$ est intègre, on
  obtient que $AB = 1_{\K[X]}$. Donc $A$ et $B$ sont deux polynômes de degré
  0 non nuls.
\end{proof}

On rappelle que $\K[X]$ est euclidien, c'est-à-dire qu'on dispose de la
division euclidienne : $\forall (A,B) \in \K[X]^2, \exists! (Q,R) \in
\K[X]^2,\ A = B Q + R,\ \deg R < \deg B$.

On peut chercher les sous-groupes de $\K[X]$ : $G = \{ \lambda X^2 + \mu (X
+ 1) \mid (\lambda,\mu) \in \K^2 \}$ est un candidat : il est non vide, il
contient le polynôme nul. Pour que $G$ soit monogène, il faudrait que $G = P
\K[X]$, ainsi les éléments de $G$ serait divisble par $P$\footnote{Ça serait
  même le noyau de l'application associée à la classe d'équivalence de 0
dans la division par $P$.}. En particulier, $P$ diviserait $X + 1$, donc $
\in \K$ est impossible, donc $P$ est associé à $X+1$.\\
Prenons $A = 2X^2 + 3(X+1) \in G$. $A = (X+1)(2X+1) +2$ et donc n'est pas
divisible par $X+1$, donc $A \notin (X+1)\K[X]$.

Ce qui ressort de cetté étude rapide, c'est que le raisonnement utilisé dans
$\Z$ pour caractériser les sous-groupes ne suffit pas ici, il faut donc une
notion plus «puissante».

\subsection{Notion d'idéal d'un anneau}

\begin{definition}
  Soit $A$ un anneau commutatif. Une partie $J \subset A$ est un idéal
  lorsque $(J,+)$ est un sous-groupe de $(A,+)$ et $\forall a \in A,\
  \forall x \in J,\ ax \in J$.
\end{definition}

On peut écrire cette définition de façon équivalente par :
\begin{itemize}
  \item $J ≠ ø$
  \item $\forall (x,y) \in J^2,\ x - y \in J$
  \item $\forall a \in A,\ \forall x \in J,\ ax \in J$
\end{itemize}

On remarque aussi que dans $\Z$, les idéaux de $\Z$ sont les sous-groupes.

\begin{definition}[idéal principal]
  Un idéal est dit \emph{principal} lorsqu'il est de la forme $xA$.
\end{definition}

\begin{proposition}
  Dans $\K[X]$, tous les idéaux sont principaux.
\end{proposition}
\begin{proof}
  Soit $J = \{ 0_{\K[X]} \}$, alors $J = 0_{\K[X]} \K[X]$.\\
  Soit $J ≠ \{ 0_{\K[X]} \}$, alors il existe $A$ de degré minimum ($≠ -
  \infty$) dans $J$. Soit $B \in J$, divisons $B$ par $A$.\\
  $B = AQ + R,\ \deg R < \deg A$, donc $R = B - AQ$. $B \in J,\ A \in J$ et
  comme $J$ est un idéal, $AQ \in J$. Donc en particulier $R = B - AQ \in
  J$. Or $\deg R < \deg A$ qui est minimal, donc $\deg R = - \infty$ et donc
  $R = 0_{\K[X]}$. Ainsi, tout élément de $J$ est multiple de $A$ et donc $J
  = A\K[X]$ et l'idéal $J$ est principal.
\end{proof}

\begin{remarque}
  On dit même que l'anneau $\K[X]$ est principal.
\end{remarque}

\section{PGCD et PPCM}

\subsection{PGCD}

\begin{proposition}
  La somme de deux idéeaux de $\K[X]$ est un idéal de $\K[X]$.
\end{proposition}
\begin{proof}
  Soient $J_1$ et $J_2$ deux idéeaux de $\K[X]$. Comme ce sont des
  sous-groupes, leur somme est également un sous-groupe (voir la proposition
  \ref{09alg:prop:somme_sous-groupe}).\\
  Soient $A_1 \in J_1,\ A_2 \in J_2$ et $P\in \K[X]$.\\
  $P(A_1 + A_2) = \underbrace{\underbrace{PA_1}_{\in J_1} +
  \underbrace{PA_2}_{\in J_2}}_{\in J_1 + J_2}$.
\end{proof}

\begin{proposition}
  Pour $(A,B) \in \K[X]^2,\ J = A\K[X] + B\K[X]$ est un idéal de $\K[X]$ et
  il existe $D$ tel que $J = D\K[X]$.
\end{proposition}
\begin{definition}[PGCD de deux polynômes]
  Les polynômes $D$ tels que $D\k[X] = A\K[X] + B\K[X]$ sont appellés
  \emph{un PGCD} de $A$ et $B$.
\end{definition}
\begin{proof}
  Si $D_1$ et $D_2$ sont deux pgcd de $A$ et $B$, alors $D_1 \mid D_2$ et
  $D_2 \mid D_1$. Donc $D_1$ et $D_2$ sont associés et il existe $k \in
  \K[X], D_2 = kD_1$.
\end{proof}

\begin{remarque}
  On appelle \emph{le pgcd} le polynôme $D$ unitaire.
\end{remarque}

On peut reprendre les différentes propriétés du PGCD dans $\Z$ :

\begin{proposition}
  \begin{itemize}
    \item $\forall (A,B) \in \K[X]^2,\ A \wedge B = B \wedge A$
    \item $\forall A \in \K[X],\ A \wedge 0 = D$, un polynôme associé à $A$
      et $0 \wedge 0 = 0$
  \end{itemize}
\end{proposition}
\begin{proof}
  \begin{itemize}
    \item Trivial
    \item Il faut remarquer que $0\K[X] = \{ 0 \}$ et les résultats en
      découlent.
  \end{itemize}
\end{proof}

En partique, on utilise quelques propriétés faciles :
\begin{itemize}
  \item $A \wedge B = A_1 \wedge B_1$, où $A_1 = kA$ et $B_1 = k'B$, $k,k'$
    deux éléments non nuls de $\K$.
  \item $\forall P \in \K[X],\ (PA) \wedge (PB) = P_1(A \wedge B)$, où $P_1$
    est associé à $P$.
  \item Si $\Delta \mid A $ et $\Delta \mid B$, alors
    $\brk*{\frac{A}{\Delta} \wedge \frac{B}{\Delta} } = \frac1{\Delta_1}
    (a \wedge b)$, où $\Delta_1$ est un polynôme associé à $\Delta$.
  \item $D = A \wedge B \iff \begin{cases} A = DA_1,\ A_1 \in \K[X] \\ B = D
        B_1,\ B_1 \in \K[X] \\
    A_1 \wedge B_1 = 1 \end{cases}$
\end{itemize}

En terme de vocabulaire, on dit que $A$ et $A$ sont «premiers entre eux» ou
«étrangers» si $A \wedge B = k$.

\begin{theoreme}\label{10alg:thm:Bezout_polynomes}
  $A$ et $B$ sont étrangers si et seulement si $\exists (U,V) \in \K[X]^2,\
  AU + BV = 1$.
\end{theoreme}
\begin{proof}
  La preuve est analogue à celle du théorème \ref{09alg:thm:bezout}.
\end{proof}

\begin{corollaire}[Théorème de Gauss]\label{10alg:thm:Gauss_polynomes}
  Si $A \mid (BC)$ et $A \wedge B = 1$, alors $A \mid C$.
\end{corollaire}
\begin{proof}
  Comme $A \wedge B = 1$, il existe $(U,B) \in \K[X]^2,\ AU + BV = 1$. D'où
  $ACU + BCV = C$. $A \mid ACU$. Comme $A \mid BC$, $A \mid BCV$, donc $a
  \mid ACU + BCV $ ce qui s'écrit encore $A \mid C$.
\end{proof}



$U,V$ se trouvent en utilisant l'algorithme d'Euclide étendu :

\begin{tabular}{c*{3}{|c}}
  & $4X$ & $\frac16 X + \frac1{36}$ & \\ \hline
  $4X^3 +2X - 1$ & $X^2 - 1$ & $6X - 1$ & $\frac{-35}{36}$ \\ \hline
  1 & 0 & 1 & $\frac16 X - \frac1{36}$ \\ \hline
  0 & 1 & $-4X$ & $\frac23 X^2 + \frac19 X + 1$ \\ \hline
\end{tabular}

Rappellons un théorème admis (\ref{alg04:lemme:polesimple}) qui énonce
\begin{lemme}\label{10alg:lemme:polesimple}
  Soit $F = \dfrac{A}B$ une fraction rationnelle irréductible. On suppose que
  $\deg F < 0$ et que $B = P_1 P_2$ où $P_1$ et $P_2$ sont des polynômes
  premiers entre-eux. Alors, il existe de manière unique des polynômes $A_1$
  et $A_2$ tels que $F = \dfrac{A_1}{P_1} + \dfrac{A_2}{P_2}$ et $\deg A_1 <
  \deg P_1$ et $\deg A_2 < \deg P_2$.
\end{lemme}
\begin{proof}
  On veut montrer l'existence et l'unicité de $A_1$ et $A_2$ tels que $A_1
  P_2 + A_2 P_1 = A,\ \deg A_1 < \deg P_1,\ \deg A_2 < \deg P_2$, sachant
  que $\deg P_1 + \deg P_2 > \deg A$.\\
  Comme $P_1 \wedge P_2 = 1$, on sait qu'il existe $(U,V) \in \K[X]^2, \
  P_1U + P_2 V = 1$. On a donc $P_1 (AU) + P_2 (BV) = A$ et donc $P_1 B_2 +
  P_2 B_1 = A$. Divisons $B_1$ par $P_1$ : $B_1 = P_1 Q_1 + A_1,\ \deg A_1 <
  \deg P_1$. On a donc $P_1 B _2 + P_2 (P_1 Q_1 + A_1) = A$ ou encore $P_1
  (B_2 + P_2Q_1) + P_2 A_1 = A$.\\
  Vérifions que $A_2 = B_2 + P_2Q_1$ est le polynôme $A_2$ cherché. $P_1 A_2
  = -P_2 A_1 + A$. \\
  $\deg P_1 + \deg A_2 ≤ \max(\deg P_2 A_2, \deg A)$ et comme $\deg A < \deg
  P_1 + \deg P_2$, on obtient que $\deg A_1 < \deg P_1$, puis comme $\deg
  P_1 A_2 < \deg P_1 + P_2$, on en déduit que $\deg A_2 < \deg P_2$.

  Pour l'unicité : si $A = A_1 P_2 + A_2 P_1 = B_1 P_2 + A_2 P_1$, alors on
  aurait $(A_1 - B_1)P_2 = (B_2 - A_2)P_1$. Comme $P_1 \wedge P_2 =
  1$, d'après le théorème de Gauss (\ref{10alg:thm:Gauss_polynomes}), $P_1$
  divise $A_1 - B_1$, mais comme $\deg A_1 < \deg P_1$ et $\deg B_1 < \deg
  P_1$, la seule possibilité est $A_1 - B_1 = O_{\K[X]}$. On a donc $A_1 =
  B_1$ et donc $A_2 = B_2$.
\end{proof}

\begin{question}
  \begin{enumerate}
    \item Calculer $(X - a) \wedge (X - b)$
    \item Calculer $(X^n - 1) \wedge (X^m - 1)$
  \end{enumerate}
\end{question}
\begin{solution}
  \begin{enumerate}
    \item $(X - a) \wedge (X - b) = 1$
    \item $(X^n - 1) \wedge (X^m - 1) = X^{n \wedge m} - 1$
  \end{enumerate}
\end{solution}

\begin{proposition}
  Soit $P \in \K[X]$, $P ≠ 0_{\K[X]}$. $P$ est à racines simples dans $\C$
  si et seulement si $P \wedge P' = 1$.
\end{proposition}
\begin{proof}
  Si $P \wedge P' = 1$, alors soit $\alpha$ une racine de $P$ dans $\C$. $(X
  - \alpha)$ divise $P$ et si cette racine est au moins double, alors $(X -
  \alpha)^2 \mid P $ et $(X - \alpha) \mid P'$. En particulier, $X - \alpha$
  devrait diviser 1, ce qui est impossible. Donc $P$ est à racines simples
  dans $\C$.

  Si $P$ n'a que des racines simples, soit $D = P \wedge P'$.\\
  $D \in \K[X]$ et même $D \in \C[X]$. Si $\deg D ≥ 1$, alors d'après le
  théorème de D'Alembert-Gauss, il existe $\alpha$ tel que $(X - \alpha)$
  divise $D$. Dans ce cas, $P(\alpha) = 0$ et $P'(\alpha) = 0$ et donc
  $\alpha$ est racine double d'ordre supérieur ou égal à 2, ce qui est
  impossible. Donc $\deg D < 1$.
\end{proof}

\subsection{PPCM}

\begin{proposition}
  Si $J_1$ et $J_2$ sont deux idéaux de $\K[X]$, alors $J_1 \cap J_2$ est un
  idéal de $\K[X]$.
\end{proposition}
\begin{proof}
  On a déjà la proposition pour les sous-groupes avec la preuve de la
  proposition \ref{05alg:thm:intersection_ss_grpes}, il faut le montrer pour
  le caractère multiplicatif :\\
  Soit $A \in J_1 \cap J_2$, alors $A \in J_1$ et $A \in J_2$ et pour tout
  $P \in \K[X]$, $PA \in J_1$ et $PA \in J_2$, donc $PA \in J_1 \cap J_2$.\\
  Donc $J_1 \cap J_2$ est un idéal.
\end{proof}

\begin{definition}[PPCM de deux polynômes]
  Le \emph{PPCM de deux polynômes $A$ et $B$} est le générateur unitaire de
  l'idéal $A\K[X] \cap B\K[X]$. On le note $A \vee B$.
\end{definition}

\begin{exemple}
  $(X-1) \vee (X+2) = (X-1)(X+2)$
\end{exemple}

\begin{proposition}
  Soit $D = A \wedge B$ et $M = A \vee B$, alors $MD = AB$ modulo la
  relation d'association.
\end{proposition}
\begin{proof}\leavevmode
  \begin{description}
    \item[1\ier{} cas] $A \wedge B = 1$. Alors $M$ est multiple de $AB$ ($AB
      \mid M$, d'après le théorème de Gauss (\ref{10alg:thm:Gauss_polynomes}
      )) or $AB$ est multiple de $A$ et de $B$, donc $M \mid AB$ et donc $M$
      et $AB$ sont associés.
    \item[2\ieme{} cas] $A \wedge B = D$. On pose alors $A = A_1 D$ et $B =
      B_1 D$ et on a $A_1 \wedge B_1 = 1$.\\ $A \vee B = (DA_1 \vee DB_1) =
      D A_1 B_1$ d'après le 1\ier{} cas.\\
      $M = AB_1$ d'où $DM = ADB_1 = AB$.
  \end{description}
\end{proof}

Une conséquence est que l'algorithme d'Euclide permet de construire le PPCM
de deux polynômes.

\subsection{Décomposition en produit de facteurs irréductibles}

Les notions développées ici cherchent à étendre la notion de nombres
premiers à un anneau de polynôme.

\begin{definition}[polynôme irréductible]
  Un polynôme $P$ est dit \emph{irréductible} dans $\K[X]$ lorsque
  \begin{itemize}
    \item $\deg P ≥ 1$
    \item $P$ a pour seuls diviseurs ses polynômes associés et les polynômes
      constants non nuls (les inversibles de l'anneau)
  \end{itemize}
\end{definition}

Dans $\C[X]$, tous les polynômes sont scindés.\\
Dans $\R[X]$, les irréductibles sont les $(X - a),\ a\in \R$ et lmes $X^2 +
pX + q$, où $p^2 - 4q < 0$.

\begin{proposition}
  Tout polynôme non constant dans $\K[X],\ \K \in \{ \R, \C \}$ admet une
  décomposition unique (à l'ordre près et modulo l'association des
  polynômes) en produit de facteurs irréductibles.
\end{proposition}
\begin{proof}\leavevmode
  \begin{description}
    \item[existence]\leavevmode
      \begin{itemize}
        \item Si $P \in \K[X], \deg P ≥ 1,\ P$ irréductible, alors on
          s'arrête.
        \item Si $P \in \K[X], \deg P ≥ 1,\ P$ composé, alors $P = AB,\ 1 ≤
          \deg A < \deg P$. En un nombre fini de divisions, on trouve un
          facteur irréductible pour $P$.
      \end{itemize}
      $P = H_1 Q_1,\ \deg H_1 ≥ 1,\ \deg Q_1 < \deg P$. $Q_1$ admet au moins
      un diviseur irréductible d'où $P = H_1 × … × H_r$.\\
      En pratique, on suppose tous les polynômes unitaires et on regroupe
      les facteurs identiques et donc $P = k H_1^{\alpha_1} ×…×
      H_s^{\alpha_s}$
    \item[unicité] On suppose que $P$ (unitaire) admet deux décompositions
      (unitaires) $P = H_1^{\alpha_1} ×…× H_s^{\alpha_s}$ et $P =
      L_1^{\alpha_1} ×…× L_r^{\alpha_r}$.\\
      $H_1$ divise la première décomposition, donc la seconde, donc $H_1$
      divise $L_j$, par exemple, $H_1 \mid L_1$. Comme $H_1 \wedge L_1 = 1$
      on a $H_1^{\alpha_1} \mid L_1^{\beta_1}$ et donc $\alpha_1 ≤ \beta_1$
      et par symétrie $\beta_1 ≤ \alpha_1$, d'où l'égalité $\alpha_1 =
      \beta_1$.\\
      On est donc ramené à $H_2^{\alpha_2} ×…× H_s^{\alpha_s} =
      L_2^{\alpha_2} ×…× L_r^{\alpha_r}$. En procédant de la même façon on
      arrive à $1 = L_{s+1}^{\alpha_{s+1}} ×…× L_r^{\alpha_r}$ par exemple
      (ou le «contraire»), ce qui est impossible, d'où $r = s$ et l'unicité
      de la décomposition.
  \end{description}
\end{proof}
