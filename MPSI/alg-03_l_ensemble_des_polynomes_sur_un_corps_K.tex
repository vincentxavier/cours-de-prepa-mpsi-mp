\chapter{L'ensemble des polynômes sur un corps $\K$}

\section*{Introduction}

Jusqu'à présent on dispose d'une notion assez vague des polynômes ou en fait
plutôt des fonctions polynomiale.

Informatiquement, on peut ne s'intéresser qu'aux coefficients rangés dans
l'ordre des puissances croissantes ou décroissantes.

Par ailleurs, le corps $\K$ considéré ici est un sous-corps de $\C$, comme
$\Q$ ou $\Z_{/5\Z}$.

\section{Construction rapide de $\K[X]$}

\subsection{Définition}

\begin{definition}
  On appelle polynomiale sur $\K$ toute suite $P = (a_n)_{n\in \N}$ nulle à
  partir d'un certain rang, c'est à dire $\exists n_0 \in \N \mid \forall n
  \in \N,\ n > n_0 \implies a_n = 0$.

  On écrit $P \in \K^{(\N)}$ ou encore $P \in \K[X]$.
\end{definition}

\begin{exemple}
  La suite nulle est un polynôme. On dit même que c'est le polynôme nul et
  on le note $0_{\K[X]}$.
\end{exemple}

On appelle les éléments de la suite les coefficients du polynôme.

\begin{definition}[degré d'un polynôme]
  Soit $P$ un polynôme non nul. On appelle \emph{degré} dun polynôme $P$ et
  on note $\deg P$ le plus grand indice $n$ pour lequel $a_n ≠ 0$. Si $P$
  est le polynôme nul, on pose par convention $\deg 0 = - \infty$.

  On appelle \emph{valuation} du polynôme $P$ et
  on note valuation $P$ le plus petit indice $n$ pour lequel $a_n ≠ 0$. Si
  $P$ est le polynôme nul, on pose par convention que valuation de $0 = +
  \infty$.
\end{definition}

\subsection{Structure de $\K[X]$}

\subsubsection{Addition dans $\K[X]$}

\begin{definition}[somme de deux polynômes]
  Soit $P = (a_n)_{n\in \N}$ et $Q = (b_n)_{n\in \N}$, deux polynômes.

  On définit la \emph{somme} des deux polynômes comme le polynôme $P + Q =
  (a_n + b_n)_{n\in \N}$.
\end{definition}

\begin{proposition}
  Avec la notation ci-dessus, $P + Q$ est bien un polynôme et son degré est
  inférieur ou égal au $\max \brk{\deg P, \deg Q}$.
\end{proposition}
\begin{proof}
  En effet, pour $n ≥ \max \brk{\deg P, \deg Q}$, on $a = 0$ et $b_n =0$ et
  donc $a_n + b_n = 0$.
\end{proof}

\begin{theoreme}
  $\brk*{\K[X],+}$ est un groupe commutatif.
\end{theoreme}
\begin{proof}
  On peut le montrer directement en vérifiant que 0 est neutre, que la loi
  $+$ est associative, commutative et que tout polynôme admet un opposé.

  On peut aussi remarquer qu'il s'agit d'un sous-groupe du groupe des suites
  bornées ou même du groupe des suites.
\end{proof}

\subsubsection{Multiplication externe à opérateur dans $\K$}

On définit une multiplication externe par $\forall \lambda \in \K,\ \forall
P \in \K^{(\N)},\ P = (a_n),\ \lambda P = (\lambda a_n)$.

\begin{proposition}
  $\brk*{\K[X],+,\times_{\text{ext}}}$ est un $\K$ espace vectoriel.
\end{proposition}
\begin{proof}
  Il suffit de vérifier les propriétés liées à la multiplication externe.
\end{proof}

\subsubsection{Muliplication des polynômes}

\begin{definition}
  Soient $P \in \K^{(\N)}, P = (a_n)$ et $Q \in \K^{(\N)}, Q = (b_n)$ deux
  polynômes.

  On pose $PQ = (c_n)$ le polynôme \emph{produit} définit par $\forall n \in
  \N,\ c_n=\sum_{k+l = n} a_l b_k = \sum_{k=0^n a_k
  b_{n-k}}$.
\end{definition}

\begin{proposition}
  $\brk*{\K[X],+,\times_{\text{int}}}$ est un $\K$ anneau commutatif
  intègre.
\end{proposition}
\begin{proof}
  Aisée en vérifiant les points un à un.
\end{proof}

\begin{question}
  \begin{enumerate}
    \item Vérifier que $\K[X]$ est un anneau intègre.
    \item Quels sont les inversibles de $\K[X]$.
  \end{enumerate}
\end{question}
\begin{solution}
  \begin{enumerate}
    \item On suppose que $P ≠ 0_{\K[X]}$ et $Q ≠ 0_{\K[X]}$. Si $\deg P
      =n$ et $\deg Q = m$, il existe $a_n ≠ 0$ et $b_m ≠ 0$. Pour $ s >
      n + m$, le coefficient $c_s$ du produit $PQ = 0$. Pour $s = n +
      m$, le coefficient $c_s = a_n \times b_m ≠ 0$ et donc $PQ ≠ 0$.
    \item On cherche $P$ et $Q$ tels que $PQ = 1_{\K[X]}$. La condition
      nécessaire est que $\deg P + \deg Q = 0$ et donc -- voir la
      résolution de l'équation $x + y =0$ dans $\N$ -- $\deg P = \deg Q
      = 0$. L'ensemble des inversibles de $\K[X]$ est inclus dans
      l'ensemble des polynômes constants.

      Réciproquement, les polynômes constants non nuls sont inversibles.
  \end{enumerate}
\end{solution}

\subsection{Écriture usuelle}

On note $X$ le polynôme $(0,1,0,\dots 0) = (\delta_{1,n})_{n \in \N}$,
où $\delta_{i,j} = \left\lbrace \begin{array}{l} 1 \text{ si } i = j\\ 0
\text{ sinon} \end{array} \right.$ est le symbole de Kronecker.

On dit que c'est le polynôme indéterminé.

Pour $k \in \N^*,\ X^k = (\delta_{k,n})_{n \in \N}$.

On peut ensuite compléter en posant $X^0 = 1_{\K[X]}$.

On constate alors qu'un polynôme $P = (a_n)_{n\in \N}$ peut alors
s'écrire $P = \sum_{k = 0}^n a_k X^k$.

\subsection{Composition de deux polynômes}

\begin{definition} Le polynôme $P \circ Q$ est le polynôme obtenu en
  substituant $Q$ à $X$ dans $P$. Autrement dit, si $P = \sum_{k = 0}^n
  a_k X^k$ et $Q = \sum_{k = 0}^m b_k X^k$, alors \[ P \circ Q = \sum_{k
  = 0}^n a_k Q^k = \sum_{k = 0}^n a_k \brk*{\sum_{k = 0}^n b_k X^k} .\]
\end{definition}

\begin{remarque} Le terme dominant de $P\circ Q$ est $a_n b_m(X^m)^n =
  a_n b_m X^{nm}$. On a donc que $\deg P \circ Q = \deg P \times \deg
  Q$.
\end{remarque}

\begin{exemple}
  $P = X^2 + X + 1,\  Q = X^3 - 1$.

  $P \circ Q = (X^3 - 1)^2 + X^3 - 1 + 1 = X^6 - X^3 + 1$.
\end{exemple}

\section{Divisibilité dans $\K[X]$}

\subsection{Divisibilité et association de polynômes}

\begin{definition}
  On dit que $P_1 \in \K[X]$ \emph{divise} le polynôme $P_2 \in \K[X]$
  (dans $\K[X]$) lorsqu'il existe un polynôme $Q \in \K[X]$ tel que $P_2
  = Q \times P_1$.
\end{definition}

\begin{remarque} $A \mid B$ ($A$ divise $B$) définit une relation
  binaire dans $\K[X]^2$. Cette relation est
  \begin{itemize}
    \item réflexive
    \item transitive
  \end{itemize}
\end{remarque}
\begin{question}\label{03_alg:ex:association}
  Démontrer les deux assertions ci-dessus.

  La relation est-elle symétrique ou anti-symétrique ?
\end{question}
\begin{solution}
  La relation est réflexive. En effet, $\forall A \in \K[X],\ A =
  1_{\K[X]} A$.

  La relation est transitive. En effet, $\forall (A,B,C) \in \K[X]^3, A
  \mid B \wedge B \mid C \implies \exists (P,Q) \in \K[X]^2, A = PB
  \wedge B = QC \implies A = PQC \implies A \mid C$.

  La relation n'est clairement pas symétrique.
  Pour l'anti-symétrique. Supposons que $A \mid B$ et $B \mid
  A$, alors il existe $P,Q$ deux polynômes tels que $A = PB$ et $B =
  QA$. On a donc $A = PQ A$.
  \begin{itemize}
    \item Si $A = 0$, alors $B = 0$ (voir le résultat sur les diviseurs
      de 0.)
    \item Si $A ≠ 0_{\K[X]}$, alors $A(1 - PQ) = 0$ et donc $1 - PQ = 0$
      et donc $PQ = 1_{\K[X]}$ et donc $P$ et $Q$ sont deux polynômes
      constants non nuls.
  \end{itemize}
  La relation n'est pas anti-symétrique.
\end{solution}

\begin{definition}[association]
  On dit que $A$ et $B$ sont deux \emph{polynômes associés} lorsque $A
  \mid B$ et $B \mid A$.
\end{definition}

\begin{remarque}
  Deux polynômes associés sont tous les deux nuls ou alors à
  coefficients proportionnels.
\end{remarque}

\begin{proposition}
  L'association est une relation d'équivalence de $\K[X]$.
\end{proposition}
\begin{proof}
  Voir l'exercice \ref{03_alg:ex:association}
\end{proof}

\begin{exemple}
  $X - 1$ et $2X - 2$ sont associés.
\end{exemple}

\subsection{Division euclidienne dans $\K[X]$}

\begin{theoreme}
  Soient $A$ un polynôme de $\K[X]$ et $B$ un polynôme de $\K[X]
  \setminus \brk[c]*{ 0_{\K[X]}}$. Il existe un unique couple $(Q,R) \in
  \K[X]^2, \deg R < \deg P$ tel que \[ A = B Q + R . \]
\end{theoreme}
\begin{proof}
  \begin{description}
    \item[unicité] Supposons que $A = B Q_1 + R_1, \ \deg R_1 < \deg B$
      et $A = B Q_2 + R_2,\ \deg R_2 < \deg B$.

      Alors $B(Q_2 - Q_1) = R_1 - R_2$ et $\deg R_1 - R_2 ≤ \max (\deg
      R_1, \deg R_2) < \deg B$. Si $Q_2 - Q_1 ≠ 0$, alors $\deg B(Q_2 -
      Q_1) = \deg B \times \deg Q_2 - Q_1 ≥ \deg B$, ce qui est
      impossible. Donc $Q_1 = Q_2$ et donc $R_1 = R_2$.
    \item[existence] Soit $A = \sum_{k = 0}^n a_kX^k$ et $B =
      \sum_{k=0}^m b_k X^k, \ b_m ≠0$.
      \begin{itemize}
        \item Si $\deg A < \deg B$, alors $A = B \times 0_{\K[X]} + A$
          et donc le couple $(0,A)$ convient.
        \item Si $\deg A ≥ \deg B$, alors on pose $Q_1 =
          \frac{a_n}{b_m}X^{n-m}$ et $A_1 = A - BQ_1$. On a donc $\deg
          A_1 ≤ \deg A - 1$. Si $\deg A_1 < \deg B$, alors le couple
          $(Q_1,A_1)$ convient, sinon, on pose $A_1 = \sum_{k=0}^{n_1}
          a_k X^k$ et $Q_2 = \frac{a_{n_1}}{b_m} X^{n_1 - m}$ et puis
          $A_2 = A_1 - BQ_2$, avec $\deg A_2 ≤ \deg A_1 - 1$. Si $\deg
          A_2 < \deg B$, alors le $A = B(Q_1 + Q_2) + A_2$ et le couple
          $(Q_1 + Q_2, A_2)$ convient. Sinon, on recommencen par
          récurrence.
      \end{itemize}
      On construit donc, par un procédé récursif, le polynôme quotient.
  \end{description}
\end{proof}

\begin{exemple}
  $4X^6 + X^4 - 2X^3 + X - 1 \div 2X^4 + 1$

  $(2X^4 + 1)\times (2X^2) = 4X^6 + 2X^2$

  $4X^6 + X^4 - 2X^3 + X - 1 - (4X^6 + 2X^2 = X^4 - 2X^3 - 2X^2 + X + 1$

  $(2X^4 + 1)\times \frac12 = X^4 + \frac12$

  $X^4 - 2X^3 - 2X^2 + X + 1 - (X^4 + \frac12) = -2X^3 - 2X^2 + X
  -\frac32$

  Le quotient $Q = 2X^2 + \frac12$ et le reste $R = -2X^3 - 2X^2 + X -
  \frac32$.
\end{exemple}

\begin{question}
  Soient $n$ et $m$ deux entiers naturels positifs, $n ≥ m$ et $a$ un
  réel non nul.

  Montrer que le quotient et le reste de la division de $X^n - a^n$ par
  $X^m - a^m$ sont $Q = \sum_{k= 1}^q a^{k-1}X^{m - kn}$ et $R = a^{qn}
  X^r + a^{qn + r}$ où $(q,r)$ sont tels que $m = nq + r,\ 0 ≤ r < n$.
\end{question}
\begin{solution}
  Par récurrence sur $m$ à $n$ fixé.
\end{solution}

\section{Fonctions polynomiales, zéros d'un polynôme}

\subsection{Fonctions polynomiales}

\begin{definition}[fonction polynomiale]
  Soit $P = \sum_{k=0}^n a_k X^k \in \K[X]$ un polynôme. On définit la
  \emph{fonction polynomiale} $\tilde{P}$ par \[ \tilde{P} : \left
  \lbrace \begin{array}{l} \K \to \K, \\ x \mapsto \sum_{k=0}^n a_k x^k
  \end{array} \right. . \]
\end{definition}

Dans ce cadre, $0^0 = 1$.

\begin{remarque}
  La factorisation de Hörner est un moyen efficace de calcul : \[
    \tilde{P}(x) = \brk{\brk{\brk{a_n x + a_{n-1}} x + a_{n-2}} x +
    \cdots + a_1 } + a_0 \]
\end{remarque}

\begin{proposition}
  L'application $\Phi$ de $\K[X]$ dans $\K^{\K}$ qui à $P \mapsto
  \tilde{P}$ est un morphisme de $\K$-algèbre (en général ni injectif ni
  surjectif.)
\end{proposition}
\begin{proof}
  $\forall P,Q \in \K[X], \Phi(P+Q) = \Phi(P) + \Phi(Q)$, il suffit de
  le vérifier sur les coefficients et comme les sommes sont ici finies,
  on peut additionner sous le signe somme.

  Pour le produit, on peut procéder de la même façon.
\end{proof}

\begin{remarque}
  En prenant $\K = \Z_{/5\Z}$ on a un bon exemple d'absence
  d'injectivité.

  Pour la surjecitivé, il suffit de considérer une fonction comme $\cos$
  qui n'est pas polynomiale.
\end{remarque}

\subsection{Racines (zéros) d'un polynôme}

\begin{definition}[racine d'un polynôme]
  Soit $P \in \K[X],\ \alpha \in \K$. On dit que $\alpha$ est une
  \emph{racine} lorsque $\tilde{P}(\alpha) = 0$.
\end{definition}

\begin{proposition}
  Soit $P$ un polynôme. Si $\deg P ≤ n \in \N$ et si $P$ admet $n + 1$
  racines 2 à deux distinctes, alors $P = 0_{\K[X]}$.
\end{proposition}
\begin{proof}
  \begin{lemme}
    $\alpha$ est racine de $P$ si et seulement s'il existe $Q \in
    \K[X]$, tel que $P = (X - \alpha)Q$.
  \end{lemme}
  \begin{proof}
    Soit $\alpha$ une racine de $P$ et divisons $P$ par $X - \alpha$. On
    a donc $P = (X - \alpha)Q + k,\ \deg k < 1$. Comme $\alpha$ est
    racine de $P$, alors $\tilde{P}(\alpha) = 0$ et donc $0 = 0 \times Q
    + k$ et donc $k = 0_{\K[X]}$. Finalement, on obtient $P = (X -
    \alpha)Q$.

    Réciproquement, si $X - \alpha$ divise $P$, on a bien
    $\tilde{P}(\alpha) = 0$.
  \end{proof}
  Si $n = 0$, alors $P$ est un polynôme constant. S'il admet une racine,
  alors $P = 0_{\K[X]}$.

  Soit $n \in \N^*,\ \deg P ≤ n$ et $P$ admet $n+1$ racines. Supposons
  alors que $P = 0_{\K[X]}$.

  Soit $P_1$ de degré au plus $n+1$, admettant $n+2$ racines $\alpha_1,
  \dots, \alpha_{n+2}$. D'après le lemme, $P_1 = (X - \alpha_{n+2})Q$.

  On a $\deg Q ≤ n$ et d'autre part $Q$ admet $n+1$ racines $\alpha_1,
  \dots, \alpha_{n+1}$ et donc par hypothèse de récurrence, $Q =
  0_{\K[X]}$ et donc $P_1 = 0_{\K[X]}$.
\end{proof}

\begin{corollaire}
  Si $\K$ est un sous-corps de $\C$, alors l'application $\Phi \colon
  \left \lbrace \begin{array}{l} \K[X] \to \{\text{ens. fcts
    polynomiales}\} \\ P \mapsto \tilde{P} \end{array} \right.$ est une
    bijection.
\end{corollaire}
\begin{proof}
  \begin{description}
    \item[surjectif] C'est par construction, rien à prouver.
    \item[injectif] Soient $(P_1, P_2) \in \K[X]^2$ tel que $\tilde{P_1}
      = \tilde{P_2}$. On a alors $\tilde{P_1} - \tilde{P_2} = 0_{\K \to
      \K}$.

      On a $\forall x \in \K,\ \tilde{P_1 - P_2}(x) = 0$. $P_1 - P_2$
      admet donc une infinité de racines, donc en particulier, il en
      admet $\max(\deg P_1, \deg P_2) + 1$ et donc $P_1 - P_2 =
      0_{\K[X]}$ et ainsi $P_1 = P_2$.
  \end{description}
\end{proof}

\subsection{Polynôme dérivé}

\begin{definition}[Dérivation des polynômes]
  Soit $P = \sum_{k=0}^n a_k X^k \in \K[X]$. On pose $P' =
  \sum_{k = 0}^{n-1} (k+1)a_{k+1} X^k =
  \sum_{k=1}^n k a_k X^{k - 1}$.
\end{definition}

\begin{remarque}
  Cette définition est formelle et non déduite de l'analyse.
\end{remarque}

\begin{proposition}
  Soit $P \in \K[X]$ un polynôme non constant. Si $\deg P = n ≥ 1$,
  alors $\deg P' = n - 1$.
\end{proposition}
\begin{proof}
  Résulte de la définition : $\deg P = n \implies a_n ≠ 0$ et donc $n
  a_n ≠ 0$. Or c'est le coefficient dominant du terme $X^{n-1}$ dans
  l'expression du polynôme dérivé.
\end{proof}

\begin{proposition}
  \begin{enumerate}[label=(\alph*),itemsep=1.0pc]
    \item $\forall \alpha \in \K,\ \forall P \in \K[X],\ (\alpha P)' =
      \alpha P$
    \item $\forall (P,Q) \in \K[X]^2,\ (P + Q)' = P' + Q'$
    \item $\forall n \in \N,\ (X^n)' = nX^{n-1}$
    \item $\forall (P,Q) \in \K[X]^2,\ (PQ)' = P'Q + PQ'$
  \end{enumerate}
\end{proposition}
\begin{proof}
  Les 3 premiers points résultent de la définition ou d'un calcul aisé.

  Prouvons le 4ème point. Soit $P = \sum_{k=0}^n a_k X^k$.
  \begin{align*}
    (PX^q)' & = \brk*{\sum_{k=0}^n a_k X^{k+q}}' \\
    & = \sum_{k=0}^{n-1} (k + q) a_k X^{k+q - 1} \\
  \end{align*}
  \begin{align*}
    P' X^q + P qX^{q -1} & = \brk*{\sum_{k=1}^n a_k X^{k-1}}X^q +
    \sum_{k=0}^n a_k q X^{k+q -1} \\
    & = \sum_{k=0}^n (ka_q +q a_q) X^{k+q-1}
  \end{align*}
  d'où on peut conclure en utilisant les points 1 et 2.
\end{proof}

\begin{definition}
  On note le polynôme dérivé $n$-ième $P^{(n)}$. Il est défini par
  récurrence en posant $P^{(0)} = P$ et pour tout $n \in \N, \ P^{(n+1)}
  = \brk*{P^{(n)}}'$.
\end{definition}

\begin{proposition}[Formule de Leibniz]
  $\forall n \in \N, \forall (P,Q) \in \K[X]^2, \ (PQ)^{(n)} =
  \sum_{k=0}^n \binom{n}{k} P^{(k)} Q^{(n-k)}$.
\end{proposition}
\begin{proof}
  Se démontre par récurrence de la même façon que la formule du binôme
  de Newton.
\end{proof}

\subsection{Formule de Taylor (1685- 1731)}

\begin{theoreme}
  Soit $P \in \K[X]$ et $a \in K$. Alors \[ P = \sum_{k=0}^{\deg P}
  \frac{P^{(k)}}{k!}(X - a)^k . \]
\end{theoreme}
\begin{proof}
  \begin{align*}
    P = \sum_{k=0}^n a_k X^k & = \sum_{k=0}^n a_k (X - a + a)^k \\
    & = \sum_{k=0}^n a_k \sum_{l=0}^k \binom{k}{l} (X - a)^l a^{k - l} \\
    & = \sum_{k=0}^n \sum_{l=0}^k a_k \binom{k}{l} (X - a)^l a^{k - l} \\
    & = \sum_{l=0}^n \sum_{k=l}^n \brk*{a_k \frac{k!}{l!(k-l)!} a^{k-l}} (X - a)^l \\
    & = \sum_{k=0}^n \sum_{l=k}^n \brk*{a_k \frac{l!}{k!(l-k)!} a^{l-k}} (X - a)^k \\
    & = \sum_{k=0}^n \sum_{l=k}^n \brk*{a_k \frac{l!}{(l-k)!} a^{l-k}} \frac{(X - a)^k}{k!} \\
    & =  \sum_{k=0}^n \frac{P^{(k)}}{k!} (X- a)^k \\
  \end{align*}
\end{proof}

\begin{exemple}
  $P = X^2 +2X + 1$
  $P' = 2X + 2$
  $P" = 2$.

  $P = 0 \times 1 + 0(X+1) + \frac{2}{2!}(X+1)^2$.
\end{exemple}
\begin{question}
  Trouver les polynômes de $\C[X]$ tels que $P\circ X - P \circ (X - 1)
  = X^2$.
\end{question}
\begin{solution}
  D'après l'égalité de Taylor sur les polynômes, on a $\forall a \in
  \C,\ P = \sum_{k=0}^n \frac{P^{(k)}}{k!} (X- a)^k$. En particulier,
  $P\circ (X + a) = \sum_{k=0}^n \frac{P^{(k)}}{k!} X^k$.

  On a donc, $\forall x\in \C,\ \forall a \in \C,\ \tilde{P}(x +a) =
  \sum_{k=0}^n \frac{\tilde{P}^{(k)}}{k!} x^k$, ce qui est vrai en
  particulier pour $x = -1$ et donne \[ P\circ (X - 1) = P - P' +
  \frac{P"}{2} - \frac{P"'}{3!} \dots \] L'équation se transforme et
  devient $P - (P - P' + \frac{P"}{2} + \dots = X^2$ et donc $P' = X^2 +
  \alpha X + \beta$ et $P" = 2X + \alpha$ et $P^{(3)} = 2$. On a donc
  $X^2 + \alpha X + \beta -X - \frac{\alpha}2 + \frac13 = X^2$ d'où on
  tire $\alpha = 1$ et $\beta = \frac16$. Les polynômes qui conviennent
  sont donc les $P = P(0) + \frac16 X + X^2 + \frac13 X^3$.
\end{solution}

\subsection{Multiplicité des racines}

\begin{definition}[racine multiple]
  Soit $a \in \K$ et $P \in \K[X]$, $a$ une racine du polynôme $P$ dans
  $\K$. On dit que $a$ est \emph{racine multiple} d'ordre $k≥1$ de $P$
  lorsque $(X - a)^k \mid P$ et $(X -a)^{k+1} \nmid P$.
\end{definition}

\begin{exemple}
  $X^3 - 7X^2 + 16X -12 = (X-2)(X^2-5X+6) = (X-2)^2(X-3)$ possède 2
  comme racine double et 3 comme racine simple.
\end{exemple}

\begin{proposition}
  $a$ est racine multiple d'ordre $k$ de $P$ si et seulement si
  $\tilde{P^{(i)}}(a) = 0$ pour $i \in \llbracket 0,k-1\rrbracket$ et
  $\tilde{P^{(k)}}(a) ≠ 0$.
\end{proposition}
\begin{proof}
  Si $\tilde{P^{(i)}}(a) = 0$ pour $i \in \llbracket 0,k-1\rrbracket$ et
  $\tilde{P^{(k)}}(a) ≠ 0$. Alors la formule de Taylor en $a$ donne $P =
  \sum_{i=k}^{+\infty} \frac{\tilde{P^{(i)}}(a)}{i!} (X - a)^i = (X -
  a)^k \sum_{i=k}^{+\infty} \frac{\tilde{P^{(i)}}(a)}{i!} (X - a)^{i-k}
  = (X - a)^k Q$.

  Comme $a$ est racine d'ordre au plus $k$, on a $\tilde{Q}(a) =
  \frac{P^{(k)}}{k!} ≠ 0$ et donc $(X - a)^{k+1}$ ne divise pas $P$.

  Réciproquement, si $a$ est racine d'ordre $k$ de $P$, $P = (X - a)^k
  P_1$ et $\tilde{P_1}(a) ≠ 0$.

  On a donc $P^{(i)} = \sum_{j=0}^i \binom{i}{j} \brk*{(X -a)^k}^{(j)}
  P_1^{(i-j)}$ pour $i < k$ et $\tilde{P^{(i)}}(a) = 0$ pour $i = k$. On
  a donc $\tilde{P^{(k)}}(a) = \binom{k}{k} k! \tilde{P_1}(a) ≠ 0$.
\end{proof}

\begin{proposition}
  Si $P$ admet les racines $\alpha_i$ avec les multiplicités $m_i$,
  alors $\sum m_i ≤ \deg P$.
\end{proposition}
\begin{proof}
  $P = (X - \alpha_1)^{m_1} (X - \alpha_2)^{m_2} \cdots (X -
  \alpha_s)^{m_s} \times Q$, d'où $\deg P = \sum m_i + \deg Q$ et donc
  l'inégalité.
\end{proof}

\begin{definition}[polynôme scindé]
  On dit que $P$ est \emph{scindé} sur $\K$ lorsque la somme des
  multiplicités de ses racines est égale à son degré.
\end{definition}

\begin{remarque}Un corps dans lequel tous les polynômes sont scindés est
  dit algébriquement clos.
\end{remarque}

\begin{theoreme}[d'Alembert-Gauss (hors programme MPSI)]
  $\C$ est algébriquement clos.
\end{theoreme}

\begin{remarque}
  Dans $\C[X]$ tous les polynômes sont scindés.
\end{remarque}

\subsection{Relation entre coefficients et racines d'un polynôme scindé}

Soit $P$ un polynôme s'écrivant $P = a_nX^n + a_{n-1} X^{n-1} + \cdots +
a1X + a_0$ admettant les racines $\alpha_1,\alpha_2,\dots,\alpha_n$.

On peut écrire $P$ sous la forme $P = k(X - \alpha_1)(X - \alpha_2)
\cdots (X - \alpha_n)$.

Par identification, on a $k = a_n$, $-k \sum \alpha_i = a_{n-1}$, \dots.

De façon plus générale, on peut introduir les polynômes symétriques pour
traiter de cette relation.

\begin{question}
  Trouver une condition nécessaire et suffisante sur 3 nombres complexes
  $a$, $b$ et $c$ pour que les 3 racines du polynôme $Z^3 + aZ^2 + bZ +
  c$ soient les affixes des sommets d'un triangle équilatéral.
\end{question}
\begin{solution}
  Soient $\alpha_1$, $\alpha_2$ et $\alpha_3$ les 3 racines. Elles sont
  les affixes des sommets d'un triangle équilatéral si et seulement si
  elle s'écrivent $\alpha_1 = \omega + \varphi$, $\alpha_2 = \omega +
  j\varphi$ et $\alpha_3 = \omega + j^2 \varphi$, avec $1+j+j^2 = 0$.

  Les relations entre les coefficients et les racines donnent $\sigma_1
  = 3\omega = - \alpha$, $\sigma_2 = 3 \omega^2 = b$ et $\sigma_3 =
  \omega^3 + \varphi^3 = -c$. On obtient $a^2 = 3b$ et donc les
  polynômes sont nécessairement de la forme $Z^3 + aZ^2 + \frac13a^2 Z +
  c$.

  Vérifions si cette condition est suffisante.

  Si $P = Z^3 + aZ^2 + \frac13a^2 Z + c$, alors $P$ peut se mettre sous
  la forme $P = \brk*{Z + \frac{a}3}^3 + c - \frac{a^3}{27}$. Les
  racines de $P$ sont telles que $\brk*{Z + \frac{a}3}^3 =
  \frac{a^3}{27} -c = \varphi^3$.

  Les solutions sont ici $z = \frac{-a}{3} + \varphi, \ z = \frac{-a}3 +
  \varphi^2 $ et $z = \frac{-a}{3} = \varphi^2$.

  La condition $b = \frac{a^2}3$ est donc nécessaire et suffisante.
\end{solution}

\begin{theoreme}[admis]
  Toute expression rationnele symétrique des racines de $P$ supposé
  scindé est fonction rationnelle des coefficients de $P$.
\end{theoreme}

\section{Décomposition en produit de facteurs irréductibles}

Ici, $\K \in \{ \R, \C\}$.

\subsection{Polynôme irréductible}

\begin{definition}[polynôme irréductible]
  Un polynôme $P \in \K[X]$ est \emph{irréductible} sur $\K$ lorsque $P$
  est non constant et admet pour seuls diviseurs les polynômes constants
  non nuls et les polynômes associés.
\end{definition}

\begin{exemple}
  Si $a \in \K$, $X-a$ est irréductible sur $\K$.

  Si $X - a$ admet un diviseur, alors $X - a = PQ$ avec $\deg PQ = \deg
  P + \deg Q = 1$, d'où $\deg P = 0$ et $\deg Q = 1$. Ainsi, $P$ est
  constant et $Q$ associé.
\end{exemple}

\subsection{Polynômes de $\C[X]$}

\begin{proposition}
  Dans $\C[X]$ les polynômes irréductibles sont les $X -a$ et leurs
  associés.
\end{proposition}
\begin{proof}
  $\lambda(X -a)$ est irréductible pour $\lambda ≠ 0$.

  Si $P$ est irréductible, alors $\deg P ≥ 1$ et d'après le théorème de
  D'Alembert, il existe $a \in \C$ qui est racine de $P$ et donc $P = (X
  -a)Q$. Si $Q$ n'est pas constant, alors il existe $b ≠ a$ tel que $P$
  est divisible par $(X -a)(X-b)$. $X - a$ n'es pas constant et n'est
  pas associé à $P$ et donc $Q$ est constant, ce qui est impossible.
  D'où $P = \lambda (X -a)$.
\end{proof}

\begin{theoreme}[décomposition en facteurs irréductibles (partiellement
  admis)]
  Tout polynôme $P \in \C[X]$ non constant admet une décomposition en
  produit de facteurs irréductibles unique (à l'ordre près) et \[ P =
  \lambda \prod_{i=1}^s \brk{X - a_i}^{m_i} . \]
\end{theoreme}
\begin{proof}
  L'unicité est admise pour l'instant.

  Par récurrence sur $n = \deg P$ et utilisant le théorème de
  D'Alembert.
\end{proof}

\subsection{Polynômes de $\R[X]$}

\subsubsection{Polynômes conjugés de $P$ dans $\C[X]$}

Si $P = (a_k)_{k\in \N} \in \C^{(\N)}$, alors $\conj{P} =
(\conj{a_k})_{k\in \N} \in \C^{(\N)}$.

\begin{proposition}
  $P \in \R[X]$ si et seulement si $P = \conj{P}$.
\end{proposition}
\begin{proof}
  La preuve repose sur le fait que $a = \conj{a} \iff a \in \R$.
\end{proof}

Soit $P \in \R[X]$, $\deg P ≥ 1$. $\exists \alpha \in \C$ tel que
$\tilde{P}(\alpha) = 0$ (d'après le théorème de D'Alembert.)

Or $\tilde{P}(\alpha) = 0$ alors $\tilde{\conj{P}}(\alpha) = 0$ et
$\conj{\tilde{P}(\alpha)} = \conj{\tilde{P}}(\conj{\alpha}) =
\tilde{P}(\alpha) = 0$.

Il vient donc que soit $\alpha$ est réel, soit si $\alpha \in \C
\setminus \R$, alors $P$ admet aussi la racine $\conj{\alpha}$
distinctes de $\alpha$.

\begin{proposition}
  Si $P \in \R[X]$ admet la racine $\alpha \in \C \setminus \R$ avec la
  multiplicité $m$, alors $P$ admet la racine $\conj{\alpha}$ avec la
  même multiplicité.
\end{proposition}
\begin{proof}
  $P \in \R[X] \implies P^{(k)} \in \R[X]$ pour $1 ≤ k ≤ m$. Comme
  $\alpha$ est racine de multiplicité $m$, on a, pour tout $ 0 ≤ k ≤ m$,
  $\tilde{P^{(k)}}(\alpha) = 0$ et donc
  $\tilde{P^{(k)}}(\conj{\alpha})$.
\end{proof}

\begin{theoreme}
  Les polynômes irréductibles de $\R[X]$ sont les $X -a$, où $a \in \R$
  et les $X^2 + pX + q$, $p^2 - 4q < 0$ et leurs associés.
\end{theoreme}
\begin{proof}
  Les polynômes $X - a$ sont irréductibles.

  Les polynômes $X^2 +pX + q,\ p^2 - 4q < 0$ sont aussi irréductibles
  car n'ont pas de racines réelles.

  Si $P$ est irréductible dans $\R[X]$, alors si $\deg P = 1,\ P =
  \lambda(X - a), (\lambda,a) \in \R^*\times\R$.

  Si $\deg P = = 2, \ P = \lambda(X^2 + pX + q),\ p^2 + 4q < 0$.

  Si $\deg P ≥ 3$, alors $\exists \alpha \in \C,\ \alpha$ racine de $P$.
  Si $\alpha \in \R$, alors $X - \alpha$ divise $P$ et $P$ n'est donc
  pas irréductible. Si $\alpha \in \C \setminus \R$, alors $X - \alpha$
  et $X - \conj{\alpha}$ divisent $P$ et donc $(X - \alpha)(X -
  \conj{\alpha})$ divise $P$. Or $(X - \alpha)(X - \conj{\alpha})$ peut
  s'écrire $X - (\alpha + \conj{\alpha})X + \alpha\conj{\alpha}$ qui
  divise donc $P$. On a donc un polynôme à coefficient réel qui divise
  $P$ et donc $P$ n'est pas irréductible dans $\R[X]$.
\end{proof}

\begin{theoreme}[décomposition en facteurs irréductibles (partiellement
  admis)]
  Tout polynôme $P \in \R[X]$ non constant admet une décomposition en
  produit de facteurs irréductibles unique (à l'ordre près) et \[ P =
  \lambda \prod_{i=1}^s \brk{X - a_i}^{m_i} \prod_{j=1}^r \brk*{X + p_jX
  + Q_j}^n_j,\ p_i^2 + 4q_j < 0 . \]
\end{theoreme}
\begin{proof}
  L'unicité est admise pour l'instant.

  Par récurrence sur $n = \deg P$ et utilisant le théorème de
  D'Alembert et les facteurs conjugés.
\end{proof}
