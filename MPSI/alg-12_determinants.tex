\chapter{Déterminants}

\section{Applications $n$-linéaires}

\subsection{Définition}

\begin{definition}
  Soient $E$ et $F$ deux $\K$ espace vectoriels, $\K \in \{ \Q, \R, \C \}$,
  de dimension $n \in N^*$ (le plus souvent $n ≥ 2$)

  $\varphi \colon E^n \to F$ est dite $n$-linéaire lorsque les
  $n$-applications partielles sont linéaires.
\end{definition}

Les applications partielles ici sont les $n$ applications $\varphi_k \colon
x \mapsto \varphi(x_1,…,x_{k-1},x,x_{k+1},…,x_n)$.

\begin{exemple}
  \leavevmode
  \begin{itemize}
    \item Dans $E = \M_2(\R)$ et $F = \R$, avec $n = 2$, on peut définir
      $\varphi \colon A,B \mapsto \tr(AB)$.
    \item Dans $E = \Cl^0(\R, \R)$ et $F = \R$, avec $n = 2$, on peut
      considérer $\varphi \colon (f,g) \mapsto \int_0^1 f(t)
      g(t) \diff t$.
  \end{itemize}
\end{exemple}

\begin{remarque}
  Si $\varphi$ est $n$-linéaire, $\varphi(\lambda v_1, …, \lambda v_n) =
  \lambda^n \varphi(v_1, …, v_n)$.
\end{remarque}

\begin{definition}
  L'ensemble des applications linéaires est noté $\L_n(E,F)$.
\end{definition}

\begin{proposition}
  L'ensemble $\L_n(E,F)$ est un espace vectoriel.
\end{proposition}
\begin{proof}
  On va utiliser un résultat plus fort : l'ensemble des applications
  $\mathscr{A}(X,F)$ où $F$ est un $\K$-espace vectoriel est un espace
  vectoriel.

  Ainsi, $\L_n(E,F) \subset \mathscr{A}(X,F)$ et $\L_n(E,F) ≠ ø$, il suffit
  de montre la stabilité :
  \begin{align*}
    (\varphi + \lambda \psi)(x_1,…,x_n) & = \varphi(x_1,…,x_n) +
    \lambda\psi(x_1,…,x_n) \\
    & = \varphi(x_1 + \alpha x'_1,…,x_n) + \lambda\psi(x_1 + \alpha
    x'_1,…,x_n) \\
    & = \varphi(x_1,…,x_n) + \alpha\varphi(x'_1,…,x_n) + \lambda
    \psi(x_1,…,x_n) + \lambda\alpha\psi(x'_1,…,x_n) \\
    & = \varphi(x_1,…,x_n) + \lambda \psi(x_1,…,x_n) +
    \alpha\varphi(x'_1,…,x_n) + \lambda\alpha\psi(x'_1,…,x_n) \\
  \end{align*}
  Ainsi, $\varphi + \lambda \psi$ est linéaire sur la première composante et
  on peut raisonner de même sur toutes les composantes et donc on a la
  stabilité par combinaison linéaire.
\end{proof}

\subsection{Alternance}

\begin{definition}
  Soit $\varphi \colon E^n \to F$. On dit que $\varphi$ est alternée lorsque
  $\forall (i,j) \in \llbracket 1 ; n \rrbracket^2,\ (i ≠ j, x_i = x_j)
  \implies \varphi(x_1,…,x_i,…,x_j,…,x_n) = 0$.
\end{definition}

\begin{exemple}
  $(\R^2)^2 \to \R,\ ((x,y),(x',y')) \mapsto xy' - x'y$ est une application
  bilinéaire alternée.
\end{exemple}

\subsection{Antisymétrie}

\begin{definition}
  Soit $\varphi \colon E^n \to F$. On dit que $\varphi$ est antisymétrique
  (resp symétrique) lorsque pour toute permutation $\sigma \in \S_n$ et pour
  tout $(x_1,…,x_n) \in E^n$, $\varphi(x_{\sigma(1)},…,x_{\sigma(n)}) =
  \varepsilon(\sigma) \varphi(x_1,…,x_n)$ (resp
  $\varphi(x_{\sigma(1)},…,x_{\sigma(n)}) = \varphi(x_1,…,x_n)$.)
\end{definition}

\begin{exemple}
  L'application de l'exemple précédent est antisymétrique.
\end{exemple}

\begin{proposition}
  Soit $\varphi \in \L_n(E,F)$. $\varphi$ est alternée si et seulement si
  $\varphi$ est antisymétrique.\\Cette proposition n'est vraie que pour $\K
  \in \{\Q,\R,\C\}$.
\end{proposition}
\begin{proof}
  Soit $\varphi \in \L_n(E,F)$ une application multi-linéaire alternée.

  Supposons $\varphi$ alternée. \\ On a donc
  $\varphi(x_1,…,x_i+x_j,…,x_i+x_j,…,x_n) = 0$, ce qui entraîne que \\
  $\underbrace{\varphi(x_1,…,x_i,…,x_i,…,x_n) + }_{=0}
  \varphi(x_1,…,x_i,…,x_j,…,x_n) + \varphi(x_1,…,x_j,…,x_i,…,x_n) +
  \underbrace{\varphi(x_1,…,x_j,…,x_j,…,x_n)}_{=0} = 0$ et donc \\
  $\varphi(x_1,…,x_i,…,x_j,…,x_n) = - \varphi(x_1,…,x_j,…,x_i,…,x_n)$. Or la
  signature de la transposition $(i j)$ est $-1$, d'où $\varphi$ est
  anti-symétrique.

  Réciproquement, supposons $\varphi$ anti-symétrique. \\ On a donc
  $\varphi(x_1,…,x_i,…,x_j,…,x_n) = - \varphi(x_1,…,x_j,…,x_i,…,x_n)$. \\ En
  prenant $x_i = x_j = x$ on peut écrire $2\varphi(x_1,…,x,…,x,…,x_n) = 0$
  et les sous corps de $\C$ étant de caractéristique\footnote{à préciser
  plus tard} différente de 2, on obtient alors que $\varphi$ est alternée.
\end{proof}

\begin{proposition}\label{11-alg:prop:famille_liee_det_nul}
  Si $\varphi$ est une application $n$-linéaire alternée, alors l'image
  d'une famille liée est 0.
\end{proposition}
\begin{proof}
  Soit $(x_1,…,x_n)$ une famille liée. Quitte à réordonner, on peut écrire
  $x_n = \sum_{k=1}^{n-1} \alpha_k x_k$. On a donc
  \begin{align*}
    \varphi(x_1,…,x_n) & = \varphi(x_1,…,x_{n-1},
    \sum_{k=1}^{n-1} \alpha_k x_k \\
    & = \sum_{k=1}^{n-1} \alpha_k \varphi(x_1,…,x_{n-1}, x_k)
    \\
    & = \sum_{k=1}^{n-1} \alpha_k 0 \\
    & = 0
  \end{align*}
\end{proof}

On vérifie de la même façon que les applications qui conservent le caractère
libre d'une famille conservent le caractère non nul.

\section{Déterminant suivant une base}

On considère ici $\varphi$ une forme $n$-linéaire alternée et $\dim E = n$.

\subsection{Développement de $\varphi(v_1,…,v_n)$}

De façon générale, les vecteurs $v_i$ sont des combinaisons linéaires des
vecteurs de la base $\B = (e_k)_{1≤k≤n}$. On a donc $v_i =
\sum_{k=1}^n x_{k,i} e_k$ et donc \\
$\varphi(v_1,…,v_n) = \varphi(\sum_{k=1}^n x_{k,1} e_k, …,
\sum_{k=1}^n x_{k,n} e_k)$.

A priori, un tel développement est constitué de $n^n$ termes, mais en raison
de l'alternance, les termes non nuls sont les $\brk*{
\prod_{i=1}^n x_{\sigma(i),i}} \varphi(e_{\sigma(1)},…,e_{\sigma(n)})$ et
donc \\
$\varphi(v_1,…,v_n) = \sum_{\sigma \in \S_n} \brk*{
  \brk*{\prod_{i=1}^n x_{\sigma(i),i} }
\varphi(e_{\sigma(1)},…,e_{\sigma(n)})} = \sum_{\sigma \in
\S_n} \brk*{ \brk*{\varepsilon(\sigma) \prod_{i=1}^n
x_{\sigma(i),i} } \varphi(e_1,…,e_n)}$.

En remarquant que $\varepsilon(\sigma) = \varepsilon(\sigma^{-1})$ et comme
la somme a lieu sur toutes les permutations, on obtient l'expression \[
  \varphi(v_1,…,v_n) = \sum_{\sigma \in \S_n} \varepsilon(\sigma)
\prod_{i=1}^n x_{i,\sigma(i)} \varphi(e_1,…,e_n) . \]

\subsection{L'espace $\A_n(E),\ \dim E = n < +\infty$}

\begin{proposition}
  L'espace $\A_n(E),\ \dim E = n < +\infty$ est l'ensemble des formes
  $n$-linéaires alternées et c'est un $\K$ espace vectoriel.
\end{proposition}
\begin{proof}
  Il suffit de montrer que c'est un sous-espace vectoriel de $\L_n(E,\K)$,
  ce qui est aisé.
\end{proof}

% à revoir, mais $\A_n(E)$ est le dual de l'espace des matrices $n×n$.

% revoir ce qui est écrit dans le fichier Applications multi-linéaires 2.pdf

\begin{theoreme}
  $\dim A_n(E) = 1$.
\end{theoreme}
\begin{proof}
  Notons $\B = (e_i)_{1≤i≤n}$ une base de $E$ et $\varphi_{\B}$ une forme
  $n$-linéaire construite en développant suivant cette base $E$.

  $\varphi_{\B}(\B) = \sum_{\sigma \in \S_n} \varepsilon(\sigma)
  \prod_{i=1}^n \delta_{i,\sigma(i)} = 1$. De plus, $\varphi_{\B}$ est un
  vecteur non-nul de $\L_n(E,\K)$.

  Vérifions que $\varphi_{\B}$ est anti-symétrique.\\
  Soit $\alpha \in \S_n$ et $(v_1,…,v_n)$ une famille de vecteurs de $E$.
  \begin{align*}
    \varphi_{\B}(v_{\alpha(1),…,\alpha(n)} & = \sum_{\sigma \in \S_n}
    \varepsilon(\sigma) \prod_{i=1}^n x_{\alpha(i), \sigma(\alpha(i))} \\
    & = \sum_{\sigma \in \S_n}
    \dfrac{\varepsilon(\alpha\sigma)}{\varepsilon(\alpha)} \prod_{i=1}^n
    x_{i,\sigma(\alpha(i))} \\
    & = \varepsilon(\alpha) \sum_{\sigma \in \S_n} \varepsilon(\sigma\alpha)
    \prod_{i=1}^n x_{i,\sigma(\alpha(i))} \\
    & = \varepsilon(\alpha) \sum_{\sigma \in \S_n} \varepsilon(\sigma)
    \prod_{i=1}^n x_{i,\sigma(i)} \\
    & = \varepsilon(\alpha) \varphi_{\B}(v_1,…,v_n) \\
  \end{align*}
  On a ainsi que $\varphi_{\B}$ est anti-symétrique et même que
  $\varphi(v_1,…,v_n) = \varphi_{\B}(v_1,…,v_n) \varphi(e_1,…,e_n)$.
\end{proof}

\begin{definition}[déterminant dans une base]
  $\varphi_{\B}$ s'appelle le \emph{déterminant dans la base $\B$}.

  On le note $\det_{\B}$ et $\det_{\B}(v_1,…,v_n) = \begin{vmatrix}
    x_{1,1} & … & x_{1,n} \\
    \vdots  &   & \vdots  \\
    x_{n,1} & … & x_{n,n} \\
  \end{vmatrix}$.
\end{definition}

Le déterminant dans une base $\B$ est l'unique forme $n$-linéaire alternée
qui prend la valeur 1 pour $\B$.

\begin{proposition}
  La famille $(v_1,…,v_n)$ est liée si et seulement si $\det_{\B}(v_1,…,v_n)
  = 0$.
\end{proposition}
\begin{proof}
  On a déjà vu que si la famille est liée, alors le déterminant est nul
  (proposition \ref{11-alg:prop:famille_liee_det_nul}). Inversement,
  supposons que $\det_{\B}(v_1,…,v_n) = 0$. Raisonnons par contraposée et
  montrons que $(v_1,…,v_n)$ libre entraine que $\det_{\B}(v_1,…,v_n) ≠
  0$.\\
  Supposons $\F = (v_1,…,v_n)$ libre. Alors c'est une base de $E$. Donc il
  existe $\lambda \in \K^*, \det_{\F} = \lambda \det_{\B}$. Ainsi
  $\det_{F}(\F) = \lambda \det_{\B}(\F)$ et donc $1 = \lambda \det_{\B}(\F)$
  et $\lambda = \dfrac{1}{\det_{\B}(\F)}$.
\end{proof}

\begin{corollaire}
  $\det_{\B_2} = \det_{\B_2}(\B_1) \det_{\B_1}$.
\end{corollaire}

\section{Déterminant d'un endomorphisme}

\subsection{Étude de $\det_{B}(f(\B))$}

Soit $\B$ une base de $E,\ \dim E = n < +\infty,\ \B = (e_i)_{1≤i≤n}$ et $f
\in \L(E)$.

Comparons, pour $\B_1$ et $\B_2$ deux bases de $E$, $\det_{\B_1}(f(\B_1))$
et $\det_{\B_2}(f(\B_2))$.\\ Or $\det_{\B_2}(f(\B_2)) = \det_{\B_2}(\B_1)
\det_{\B_1}(f(\B_2))$.\\ D'où $\det_{\B_1}(\B_2) \det_{\B_2}(f(\B_2)) =
\det_{\B_1}(f(\B_2))$. Ainsi, le résultat ne dépend pas de la base choisie.

\begin{definition}[Déterminant d'un endomorphisme]
  On appelle \emph{déterminant d'un endomorphisme $f$} le nombre
  $\det_{\B}(f(\B))$ où $\B$ est une base de $E$.
\end{definition}

\begin{exemple}
  $\det \Id = \det_{\B}(\B) = 1$.
\end{exemple}

\subsection{Propriétés}

\begin{proposition}
  $\forall (v_i)_{1≤i≤n},\ \det_{\B}(f(v_i)_{1≤i≤n}) = \det(f)
  \det_{\B}(v_i)_{1≤i≤n}$.
\end{proposition}
\begin{proof}
  \begin{align*}
    \det_{\B}(f(v_i)_{1≤i≤n}) & = \det_{\B}(f(\B)) \det_{\B}(v_i)_{1≤i≤n} \\
                              & = \det(f) \det_{\B}(v_i)_{1≤i≤n} .        \\
  \end{align*}
\end{proof}

\begin{proposition}
  $\forall (f,g) \in \L(E)^2,\ \det(f\circ g) = (\det f) \times (\det g)$.
\end{proposition}
\begin{proof}
  \begin{align*}
    \det(f\circ g) = \det_{\B}(f\circ g(\B)) & = \det f \det_{\B}g(\B) \\
                                             & = \det f \det g \det_{\B}\B
    \\
    & = \det f \det g . \\
  \end{align*}
\end{proof}

\begin{proposition}
  $f \in \GL(E)$ si et seulement si $\det f ≠ 0$.
\end{proposition}
\begin{proof}
  Si $f$ est bijective, alors $f(\B)$ est une base de $E$ et donc
  $\det_{\B}(f(\B)) ≠ 0$.

  Réciproquement, si $f$ est non bijective, alors $f(\B)$ n'est pas libre et
  donc $\det_{\B}(f(\B)) = 0$, d'où $\det f = 0$.
\end{proof}

\begin{corollaire}
  $\det \colon (\GL(E),\circ) \to (\K^*,×)$ est un morphisme de groupe.
\end{corollaire}
\begin{proof}
  Ce sont les trois propositions précédentes.
\end{proof}

\begin{corollaire}
  Soit $f$ une application linéaire bijective.\\
  $\det f^{-1} = \dfrac1{\det f}$.
\end{corollaire}
\begin{proof}
  C'est une conséquence immédiate du morphisme de groupe précédent.
\end{proof}

Le noyau de ce morphisme est l'ensemble des $f \in \GL(E)$ telles que $\det
f = 1$. On a donc $\ker \det = \SL(E)$ le groupe spécial linéaire. En tant
que noyau d'une morphisme, $\SL(E)$ est un sous-groupe
distingué\footnote{notion à préciser}.

\section{Déterminant d'une matrice}

Ici, $M \in \M_n(\K), M = (a_{i,j})_{\substack{1≤i≤n\\1≤j≤n}}$.

\begin{definition}[Déterminant d'une matrice]
  $\det M = \sum_{\sigma \in \S_n} \varepsilon(\sigma)
  \prod_{i=1}^n a_{\sigma(i),i}$
\end{definition}

\begin{proposition}
  $\det M = \det \transpose{M}$.
\end{proposition}
\begin{proof}
  Il suffit de réécrire la définition avec $\sigma^{-1}$
\end{proof}

On remarque, en utilisant la définition, que si on échange deux colonnes
d'une matrice, le déterminant est multiplié par $-1$.

\begin{proposition}
  $\det (M_1 M_2) = (\det M_1)(\det M_2)$.
\end{proposition}
\begin{proof}
  Voir plus tard.
\end{proof}

\begin{proposition}
  Si $M = (a_{i,j})_{\substack{1≤i≤n\\1≤j≤n}}$ est triangulaire, alors $\det
  M = \prod_{i=1}^n a_{i,i}$
\end{proposition}
\begin{proof}
  Dans le développement des produits, dès qu'il existe $i \in \llbracket 1 ;
  n \rrbracket$ tel que $\sigma(i) ≠ i$, alors, au moins un des
  $a_{i,\sigma(i)}$ est nul et donc le produit est nul. Ainsi, la seule
  permutation comportant des éléments non-nuls est $\sigma = \Id$.
\end{proof}

\begin{exemple}
  $\begin{vmatrix}
    1 & 4 & 7 \\
    2 & 1 & 0 \\
    3 & 0 & 1 \\
    \end{vmatrix} = \begin{vmatrix}
    -20 & 4 & 7 \\
    2   & 1 & 0 \\
    0   & 0 & 1 \\
    \end{vmatrix} = \begin{vmatrix}
    -28 & 4 & 7 \\
    0   & 1 & 0 \\
    0   & 0 & 1 \\
  \end{vmatrix} = 28$.
\end{exemple}

Long : insérer ici le développement suivant les lignes (ou les colonnes)
ainsi que la notion de cofacteurs et de mineurs.

Enchainer sur la règle de Sarrus

Finir sur la formule de Cramer
