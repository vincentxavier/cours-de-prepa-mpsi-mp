\chapter{Groupe symétrique d'ordre $n$ : $\S_n$}

\section{Généralités}

$E$ désignbe un ensemble. On note souvent $\mathfrak{S}(E)$ l'ensemble des
bijections de $E$ dans $E$. On peut montrer que $(\mathfrak{S}(E),\circ)$
est un groupe.

De façon plus ciblée, si $E$ est de cardinal $n$, on note $\mathfrak{S}(E) =
\S_n$ et c'est un groupe de cardinal $n!$, en bijection avec le groupe des
permutations de $\N_n$ dans $\N_n$.

\begin{exemple}
  \leavevmode


  \begin{tabular}{c*{6}{|c}}
    $\circ$ & $i$   & $c_1$ & $c_2$ & $t_1$ & $t_2$ & $t_3$ \\ \hline
    $i$     & $i$   & $c_1$ & $c_2$ & $t_1$ & $t_2$ & $t_3$ \\ \hline
    $c_1$   & $c_1$ & $c_2$ & $i$   & $t_1$ & $t_2$ & $t_3$ \\ \hline
    $c_2$   & $c_2$ & $i$   & $c_1$ & $t_2$ & $t_3$ & $t_1$ \\ \hline
    $t_1$   & $t_1$ & $t_2$ & $t_3$ & $i$   & $c_1$ & $c_2$ \\ \hline
    $t_2$   & $t_2$ & $t_3$ & $t_1$ & $c_2$ & $i$   & $c_1$ \\ \hline
    $t_3$   & $t_3$ & $t_1$ & $t_2$ & $c_1$ & $c_2$ & $i$   \\
  \end{tabular}
  \begin{tabular}{c|*{3}{c}}
          & 1 & 2 & 3 \\ \hline
    $i$   & 1 & 2 & 3 \\ \hline
    $c_1$ & 2 & 3 & 1 \\ \hline
    $c_2$ & 3 & 1 & 2 \\ \hline
    $t_1$ & 2 & 1 & 3 \\ \hline
    $t_2$ & 1 & 3 & 2 \\ \hline
    $t_3$ & 3 & 2 & 1 \\ \hline
  \end{tabular}
\end{exemple}

On peut noter les sous-groupes de $\S_3 : \set{i, c_1, c_2}, \set{i, t_1},
\set{i,t_2}$ et $\set{i,t_3}$.

\section{Transposition}

\begin{definition}
  On appelle \emph{transposition} dans $\S_n$ toute permutation qui échange
  deux éléments et laisse fixe tous les autres : $t_{i,j}(k) = \begin{cases}
  k & k \notin \set{i,j} \\ i & k = j \\ j & k = i \end{cases}$.
\end{definition}

On peut décomposer une permutation en un produit de transposition (mais il
n'y a pas unicité de la décomposition.) D'ailleurs, ce point fera l'objet
d'une démonstration.

\begin{exemple}
  \leavevmode
  $\sigma = \begin{pmatrix}
    1 & 2 & 3 & 4 & 5 & 6 & 7 \\
    4 & 2 & 1 & 3 & 7 & 6 & 5
  \end{pmatrix}\ t_{4,1} =  \begin{pmatrix}
    1 & 2 & 3 & 4 & 5 & 6 & 7 \\
    1 & 2 & 4 & 3 & 7 & 6 & 5
  \end{pmatrix}$. De proche en proche, on trouve $\sigma = t_{7,5} t_{4,3}
  t_{1,3}$.
\end{exemple}

\begin{proposition}
  Les tranpositions engendrent $\S_n$ pour $n ≥ 2$ et toute permutation est
  la composée de transpositions.
\end{proposition}
\begin{proof}
  Par récurrence sur $n$.

  Pour $n = 2, \S_2 = \set{i,t_{1,2}}$ et $i=t_{1,2}t_{1,2}$ donc le
  résultat est vrai.

  Soit $n ≥ 2$ on suppose que toute permutation de $\N_n$ est composée de
  transpositions.\\
  Soit $\varphi$ une permutation d'ordre $n+1$.
  \begin{itemize}
    \item Si $\varphi(n+1) = n+1$, alors $\varphi_{|_{\N_n}} \in \S_n$ et on
      a le résultat en appliquant l'hypothèse de récurrence.
    \item Si $\varphi(n+1) ≠ n+1$, alors $\varphi(n+1) = p \in \N_n$ et
      $t_{n+1,p}\varphi \in \S_{n+1}$ et même $t_{n+1,p}\varphi(n+1) =
      n+1$.\\
      On en déduit, d'après le point précédent que $t_{n+1,p}\varphi$ est
      une composée de transpositions et donc $\varphi$ aussi.
  \end{itemize}
  Conclusion : toute permutation est la composée de transposition et donc
  les transpositions engendrent bien $\S_n,\ n ≥ 2$.
\end{proof}

\section{Cycles}

\begin{definition}[orbite]
  Soit $\sigma \in \S_n$. On définit une relation $\mathcal{R}$ sur $\N_n$
  par $x \mathcal{R} y$ lorsqu'il existe $k \in \N,\ y = \sigma^k(x)$.

  Cette relation est une relation d'équivalence et les classes
  d'équivalences sont les \emph{orbites} suivant $\sigma$.
\end{definition}

\begin{exemple}
  Pour la transposition $\sigma$ de l'exemple précédent, on a $\O(1) =
  \set{1,3,4},\ \O(2) = \set{2},\ \O(3) = \O(4) = \O(1),\ \O(5) = \set{5,7}
  = \O(7)$ et $\O(6) = \set{6}$.
\end{exemple}

\begin{definition}[cycles]
  On appelle \emph{cycle} de $\S_n$ toute permutation de $\S_n$ dont
  l'orbite est de cardinal strictement supérieur à 1. Cette orbite, si elle
  existe, est appellée \emph{support du cycle}, noté $\supp(\sigma)$ et son
  cardinal est la \emph{longueur du cycle}.
\end{definition}

\begin{remarque}
  Les transpositions sont des cycles de longueur 2.
\end{remarque}

\begin{exemple}
  Dans la permutation ci-dessus, on peut dégager le cycle $\varphi =
  \begin{pmatrix}1 & 4 & 3\end{pmatrix}$.
\end{exemple}

\begin{proposition}
  Deux cycles à supports disjoints commutent.
\end{proposition}
\begin{proof}
  Soient $\sigma_1$ et $\sigma_2$ deux cycles de supports disjoints.

  $\sigma_1 \sigma_2(x) = x = \sigma_2 \sigma_1(x)$ si $x \notin
  \supp(\sigma_1) \cup \supp(\sigma_2)$\\
  Si $x \in \supp(\sigma_1)$, alors $\sigma_1\sigma_2(x) = \sigma_1(x)$ et
  $\sigma_2 \sigma_1(x) = \sigma_1(x)$.\\
  Si $x \in \supp(\sigma_2)$, alors $\sigma_1\sigma_2(x) = \sigma_2(x)$ et
  $\sigma_2 \sigma_1(x) = \sigma_2(x)$.\\
  Dans tous les cas, on a donc bien $\sigma_1\sigma_2(x) =
  \sigma_2\sigma_1(x)$ et donc les deux permuatations commutent.
\end{proof}

\begin{theoreme}
  Toute permuation se décompose de manière unique, à l'ordre des facteurs
  près, en un produit de cycle à supports disjoints.
\end{theoreme}
\begin{proof}
  Soit $\sigma$ une permutation. Étudions les orbites de $\sigma$. Celles-ci
  étant des classes d'équivalences, elles forment une partition et donc on
  peut trouver des orbites disjointes.\\
  Soient $S_i$ les orbites non réduites à un élément. On peut donc
  construire $\sigma_i$ un cycle qui laisse invariant les autres éléments.
  Ces cycles permuttent en vertu du théorème précédent.\\
  Soit $\sigma' = \prod \sigma_i$. Comparons $\sigma$ et
  $\sigma'$.\\
  Pour $x \notin \bigcup S_i$, $\sigma(x) = x = \sigma'(x)$.
  Pour $x \in S_i$, $\sigma'(x) = \sigma_i(x)$, mais $\sigma_i(x) =
  \sigma(x)$. Ainsi, pour tout $x,\ \sigma'(x) = \sigma(x)$.
\end{proof}

\section{Signature d'une permutation}

\begin{definition}[signature d'une permutation]
  Soit $\sigma \in \S_n$. On définit la \emph{signature d'une permutation}
  par $\varepsilon(\sigma) = (-1)^{n-m}$ où $m$ est le nombre d'orbite selon
  $\sigma$.
\end{definition}

\begin{exemple}
  \leavevmode
  \begin{itemize}
    \item $\varepsilon(i) = (-1)^{n-n} = 1$
    \item $\varepsilon(t_{i,j}) = (-1)^{n-(n-1)} = -1$
    \item Soit $c$ un cycle de longueur $l$. $\varepsilon(c) =
      (-1)^{n-(n-l+1)} = (-1)^{l-1}$.
  \end{itemize}
\end{exemple}

\begin{theoreme}
  Pour toute transposition $\tau$ et pour toute permutation $\sigma$,
  $\varepsilon(\sigma\tau) = - \varepsilon(\sigma)$.
\end{theoreme}
\begin{proof}
  On va comparer le nombre d'orbites de $\sigma$ et $\sigma\tau$. Pour cela,
  posons $\tau = t_{i,j}$.
  \begin{itemize}
    \item $i$ et $j$ sont dans la même orbite suivant $\sigma$. $\tau$
      n'affecte pas les autres orbites. Il existe $(p,q) \in \N^2,\ i =
      \sigma^p(j)$ et $j = \sigma^q(i)$. $\tau(i) = j$ et donc
      $\sigma\tau(i) = \sigma(j)$. En composant par $\sigma$ à droite, on
      obtient $\sigma\tau\sigma^{q-1}(i) = \sigma^q(j) = i$. On a ainsi
      construit une orbite $\setcond{i,\sigma^k(j)}{i\in \llbracket 1, q -1
      \rrbracket}$ et donc le normbre d'orbite de $\sigma\tau$ est le nombre
      d'orbite de $\sigma + 1$.
    \item $i$ et $j$ ne sont pas dans la même orbite. Dans ce cas, il existe
      $(p,q) \in \N^2, \sigma^p(i) = i$ et $\sigma^q(j) = j$. En composant
      par $\sigma$ à droite, on montre que les deux orbites sont réunies et
      donc que le nombre d'orbites de $\sigma\tau$ est le nombre d'orbites
      de $\sigma -1$.
  \end{itemize}
  On a donc dans les deux cas $\varepsilon(\sigma\tau) = -
  \varepsilon(\sigma)$.
\end{proof}

\begin{corollaire}
  Si $\sigma$ est la composée de $k$ transpositions, alors
  $\varepsilon(\sigma) = (-1)^k$.
\end{corollaire}
\begin{proof}
  Par récurrence en utilisant le théorème précédent.
\end{proof}

\begin{corollaire}
  $\varepsilon$ est un morphisme surjectif de $(\S_n,\circ) \to (\set{-1,1},
  ×)$.
\end{corollaire}
\begin{proof}
  En utilisant le corollaire ci-dessus et en décomposant les permutations en
  produits de $k$ transpositions.
\end{proof}

\begin{definition}[groupe alterné]
  Le noyau de la signature est le \emph{groupe alterné d'ordre $n$}. C'est
  l'ensemble des permutations de signature 1.
\end{definition}
