\chapter{Les fractions rationnelles}

\section{Corps des fractions d'un anneau intègre}

Soit $\mathcal{A}$ un anneau intègre et commutatif. \emph{a priori} seuls
quelques éléments possèdent un inverse.

\begin{exemple}
  \begin{itemize}
    \item dans $\Z$, -1 et 1
    \item dans $\K[X]$, les éléments de $\K^*$
  \end{itemize}
\end{exemple}

On cherche donc à construire un ensemble $E$, les «plus petit possible»,
contenant $\mathcal{A}$ sur le quel on peut prolonger + et $\times$ de sorte
que $(E,+,\times)$ soit un corps.

\underline{Méthode} On considère $P = \mathcal{A} \times \mathcal{A}
\setminus \{ 0_{\mathcal{A}} \}$.

On munit $P$ d'une relation $\mathcal{R}$ définie par $(a,b) \mathcal
(a',b') \iff ab' = a'b$. Il s'agit d'une relation d'équivalence sur $P$ et
qui est compatible avec $+$ définie par $(a,b) + (a',b') = (ab' + a'b,bb')$
et $\times$ définie par $(a,b) \times (a',b') = (aa', bb')$.

On construit ensuite $Q = P_{/\mathcal{R}}$ l'ensemble quotient des classes
d'équivalences de couples et on note souvent $\dfrac{a}{b}$ les
représentants. Ainsi construit, $Q$ est un anneau commutatif intègre, qui
possède un inverse $\frac{b}{a}$ pour tous les éléments $\frac{a}{b}$ de
$Q$. Donc $Q$ est bien un corps.

Enfin, on peut considérer $\varphi \colon \mathcal \to Q, a \mapsto
\frac{a}1$. $\varphi$ est un morphisme injectif d'anneau et permet de
plonger $\mathcal{A}$ dans $Q$ et donc de l'identifier à une sous partie.

\underline{Application :} construction de $\Q$.

On va ici construire $K(X)$, le corps de fractions rationnelles.

\begin{definition}
  On dit que $(p,q)$ (ou $\frac{p}q$) est un représentant irréductible
  lorsque $p$ et $q$ n'ont pour seuls divisieurs communs que les inversibles
  de $\mathcal{A}$.
\end{definition}

En pratique, dans le corps des fractions rationnelles, il s'agit des
polynômes n'ayant pas de racinces communes.

\section{L'ensemble $\K(X)$}

\subsection{Degré d'une fraction rationnelle}

Intuitivement, on souhaiterai que $\deg \frac{A}B = \deg A - \deg B$

\begin{theoreme}
  Si $(A,B) \mathcal{R} (A_1,B_1)$ alors $\deg A - \deg B = \deg A_1 - \deg
  B_1$.
\end{theoreme}
\begin{proof}
  Si $\frac{A}B = \frac{A_1}{B_1}$, alors $AB_1 = A_1B$ et donc $\deg A +
  \deg B_1 = \deg A_1 + \deg B$. Par conséquent, si $A ≠ 0 \wedge A_1 ≠ 0$,
  alors $\deg A - \deg B = \deg A_1 - \deg B_1$.

  Si $A = 0_{\K[X]}$, alors $A_1 =  0_{\K[X]}$ et l'égalité reste vraie.
\end{proof}

\begin{definition}[degré d'une fraction rationnelle]
  On définit le \emph{degré d'une fraction rationnelle} $\deg \frac{A}B =
  \deg A - \deg B$.
\end{definition}

\begin{exemple}
  $\deg \frac{X}{X^2 + 1} = -1$ ; $\deg \frac{X^2 - 1}{X^2 + 1} = 0$.
\end{exemple}

Cette définition est compatible avec les propositions sur le degré d'une
somme et d'un produit de polynômes.

\begin{proposition}
  Soient $(F_1,F_2) \in \K(X)^2$
  \begin{itemize}
    \item $\deg F_1F_2 = \deg F_1 \deg F_2$
    \item $\deg F_1 + F_2 ≤ \max (\deg F_1, \deg F_2)$
  \end{itemize}
\end{proposition}
\begin{proof}
  $F_1 = \frac{A}B,\ F_2 = \frac{C}D$ donc $F_1F_2 = \frac{AC}{BD}$ et $\deg
  F_1F_2 = \deg AC - \deg BD = \deg A + \deg C - \deg B - \deg D = \deg A -
  \deg B + \deg C - \deg D = \deg F_1 + \deg F_2$.

  $F_1 + F_2 = \frac{AD + BC}{BD}$ et donc $\deg{F_1 + F_2} = \deg{AD + BC}
  - \deg{BD}$. Or $\deg{AD + BC} ≤ \max(\deg A + \deg D, \deg B + \deg C)$

  D'où $\deg{F_1 + F_2} ≤ \max(\deg A + \deg D, \deg B + \deg C) - \deg BD =
  \max(\deg A - \deg B, \deg C - \deg D)$.
\end{proof}

\subsection{Dérivation des fractions rationnelles}

\begin{definition}
  Si $F = \frac{A}B$, alors $F' = \frac{A'B - AB'}{B^2}$.
\end{definition}

On peut vérifier les propriétés usuelles de la dérivation.

\subsection{Notion de fonctions rationnelles}

\subsubsection{Pôle et racine d'une fraction rationnelle}

\begin{definition}[pôle et racine d'une fraction rationnelle]
  Soit $\frac{A}B \in \K(X)$ un représentation irréductible d'une fraction
  rationnelle.

  On appelle \emph{pôles} de $\frac{A}B$ les racines de $A$ et
  \emph{racines} de $\frac{A}{B}$ les racines de $A$.
\end{definition}

\begin{remarque} Pôles et racines d'une fraction rationelle ont le même
  ordre de multiplicité que les racines du dénominateur et du numérateur.
\end{remarque}

\begin{exemple}
  $F = \frac{(X+2)(X-3)^4(X+3)}{X(X-1)^2}$ admet -2 et -3 comme racines
  simples, -3 comme racine avec la multiplicité 4. 0 est un pôle simple et 1
  est un pôle double.
\end{exemple}

\begin{definition}[ensemble de définition]
  On appelle \emph{ensemble de définition} de $F$ sur $\K$, noté
  $\mathcal{D}{\K}(F)$ l'ensemble $K$ privé des pôles de $F$ sur $\K$.
\end{definition}

\subsubsection{Fonctions rationnelles}

\begin{definition}
  Soit $F \in \K(X)$, on note $\tilde{F}$ l'application de
  $\mathcal{D}_{\K}(F) \to \K,\ x \mapsto \tilde{F}(x) = \frac{\tilde{A}(x)}
  {\tilde{B}(x)}$.
\end{definition}

\begin{theoreme}
  Soient $\K$ un sous corps de $\C$, $(F_1,F_2) \in \K(X)^2$.

  Si $\forall x \in \mathcal{D}_{\K}(F_1) \cap \mathcal{D}_{\K}(F_2), \
  \tilde{F_1}(x) = \tilde{F_2}(x)$, alors $F_1 = F_2$.
\end{theoreme}
\begin{proof}
  $F_1$ et $F_2$ n'ayant qu'un nombre fini de pôles, $\mathcal{D}_{\K}(F_1)
  \cap \mathcal{D}_{\K}(F_2) = \K \setminus \{ \text{pôles de } F_1 \text{
  et } F_2 \}$ est infini.

  L'égalité $\tilde{F_1}(x) = \tilde{F_2}(x)$ conduit à $A_1(x) B_2(x) =
  A_2(x) B_1(x)$ et donc en utilisant le fait que deux polynômes égaux sur
  une infinité de points sont égaux on en déduit $A_1 B_2 = A_2 B_1$ et donc
  $F_1 = F_2$.
\end{proof}

\section{Décomposition des fractions en éléments simples}

\subsection{La théorie}

\subsubsection{Partie «entière»}

\begin{lemme}\label{alg04:lemme:partent}
  Soit $F$ une fraction rationnelle. Il existe un unique polynôme $E \in
  \K[X]$ et une unique fraction rationnelle $G \in \K(X)$ telle que \[ F = E
  + G \ \text{et}\ \deg G < 0 . \]
\end{lemme}
\begin{proof}
  Soit $F$ une fraction rationnelle s'écrivant $F = \frac{A}B$ sous forme
  irréductible.

  En divisant $A$ par $B$, on a $A = BQ + R$ et $\deg R < \deg B$ et donc
  $\frac{A}{B} = Q + \frac{R}B$ et $\deg \frac{R}B < 0$. Il existe donc une
  solution.

  Supposons que $E_1 + G_1 = E_2 + G_2$, avec $\deg G_1 < 0$ et $\deg G_2 <
  0$. On peut aussi écrire $E_1 + \frac{A_1}{B_1} = E_2 + \frac{A_2}{B_2}$
  avec $\deg A_1 < \deg B_1$ et $\deg A_2 < \deg B_2$. En multipliant, on
  obtient $E_1 B_1 B_2 + A_1 B_2 = E_2 B_1 B_2 + A_2 B_1$ et donc $B_1 B_2
  (E_1 - E_2) = A_2 B_1 - A_1 B_2$.

  Si $E_1 ≠ E_2$, alors $\deg (B_1 B_2 (E_2 - E_1) ) ≥ \deg B_1 + \deg B_2$.
  Or $\deg (A_2 B_1 - A_1 B_2) ≤ \max( \deg A_1 + \deg B_2, \deg A_2 + \deg
  B_2) < \deg B_1 + \deg B_2$. L'hypothèse est donc absurde et donc $E_1 =
  E_2$ et enfin $G_1 = G_2$.
\end{proof}

\begin{remarque}
  La démonstration est constructive.
\end{remarque}

\subsubsection{Pôle simple}

\begin{lemme}\label{alg04:lemme:polesimple}
  Soit $F = \frac{A}B$ une fraction rationnelle irréductible. On suppose que
  $\deg F < 0$ et que $B = P_1 P_2$ où $P_1$ et $P_2$ sont des polynômes
  premiers entre-eux. Alors, il existe de manière unique des polynômes $A_1$
  et $A_2$ tels que $F = \frac{A_1}{P_1} + \frac{A_2}{P_2}$ et $\deg A_1 <
  \deg P_1$ et $\deg A_2 < \deg P_2$.
\end{lemme}
\begin{proof}
  La preuve peut être provisoirement admise. Cependant, elle repose sur le
  théorème de Bezout pour les polynômes (\ref{10alg:thm:Bezout_polynomes})
  qui sera vu plus tard. On peut en avoir la preuve avec la réécriture de ce
  lemme \ref{10alg:lemme:polesimple}.

  D'après le théorème de Bezout, comme $P_1$ et $P_2$ sont premiers
  entre-eux, il existe deux polynômes $(U,V) \in \K[X]^2$ tels que $UP_1 +
  VP_2 = 1$. On a donc $AUP_1 + AVP_2 = A$ puis en divisant par $P_1 P_2$,
  on obtient $\frac{AU}{P_2} + \frac{AV}{P_2} = \frac{A}B$.

  On remarque que $AUP_1 + AVP_2 = A \implies \max( \deg AU + \deg P_1, \deg
  AV + \deg P_2) ≤ \deg A$ et donc $\deg \frac{AU}{P_2} < \deg \frac{A}B <
  0$.

  Pour l'unicité, supposons que les couples $(A_1,A_2)$ et $(A_1^*, A_2^*)$
  conviennent. On a donc $\frac{A_1}{P_1} + \frac{A_2}{P_2} =
  \frac{A_1^*}{P_1} + \frac{A_2^*}{P_2}$, ce qui conduit à $\frac{A_1 -
  A_1^*}{P_1} = \frac{A_2^* - A_2}{P_2}$, ce qui s'écrit $P_1(A_2^2 - A_2) =
  P_2(A_1 - A_1^*)$. Comme $P_1$ et $P_2$ sont premiers entre-eux, d'après
  le théorème de Gauss, $P_1$ divise $A_1^* - A_1$. Or $\deg \frac{A_1 -
  A_1^*}{P_1} < 0$, donc $A_1 = A_1^*$ et de même pour $A_2$ et $A_2^*$.
\end{proof}
\begin{remarque}
  Cet énoncé s'étend par récurrence au cas où $B = P_1 \cdots P_s$, les
  $P_i$ étant deux à deux premiers.
\end{remarque}

\begin{exemple}
  $\frac{1}{(X-1)(X+1)} = \frac{\frac{1}{2}}{X-1} + \frac{\frac{-1}{2}}{X+1}$
\end{exemple}

\subsubsection{Pôle de multiplicité > 1}

\begin{lemme}\label{alg04:lemme:polemultiple}
  Soit $F \in \K(X), \ \deg F < 0$ et $F = \dfrac{A}{P^{\alpha}},\ \alpha \in
  \N^*$. Alors il existe une suite unique de polynômes tels que $F =
  \dfrac{A_1}{P} + \dfrac{A_2}{P^2} + \cdots +
  \dfrac{A_{\alpha}}{P^{\alpha}}$ où $\forall i \in \llbracket 1 ; \alpha
  \rrbracket,\ \deg A_i < \deg P$.
\end{lemme}
\begin{proof}
  Raisonnons par récurrence sur $\alpha$.

  Si $\alpha = 1$, $F = \frac{A}{P}$ et $\deg F < 0 \implies \deg A < \deg
  P$.

  Supposons que pour toute fraction $\frac{A}{P^{\alpha}}$ où $\deg A < \deg
  P$ la proposition soit vérifiée. On considère alors la fraction $G =
  \frac{B}{P^{\alpha+1}},\ \deg G < 0$.

  Divisons $B$ par $P$. On a donc $B = PA + R,\ \deg R < \deg P$ et donc $G
  = \frac{PA + R}{P^{\alpha +1}} = \frac{A}{P^{\alpha}} + \frac{R}{P^{\alpha
  +1}}$. On peut donc appliquer l'hypothèse de récurrence et conclure.

  Supposons que $\sum_{i=1}^{\alpha} \frac{A_i}{P^i} = \sum_{i=1}^{\alpha}
  \frac{B_i}{P^i}$, avec $\deg A_i < \deg P$ et $\deg B_i < \deg P$. Alors
  $(A_1 P^{\alpha - 2} + A_2 P^{\alpha - 3} + \cdots + A_{\alpha -1}) P +
  A_{\alpha} = (B_1 P^{\alpha - 2} + B_2 P^{\alpha - 3} + \cdots + B_{\alpha
  -1}) P + B_{\alpha}$. Alors, en interprétant ces deux membres comme des
  divisions euclidiennes, l'unicité de celle-ci permet d'écrire que
  $A_{\alpha} = B_{\alpha}$, puis d'en déduire l'égalité des autres
  polynômes deux à deux.
\end{proof}

\begin{exemple}
  $F = \frac{X}{(X+1)^2} = \frac{1}{X+1} + \frac{-1}{(X+1)^2}$.
\end{exemple}

\begin{theoreme}[de décomposition en élements simples]
  Pour toute fraction $F = \dfrac{N}D \in \K(X)$, où $D =
  \prod_{i=1}^q P_i^{\alpha^i}$, les $P_i$ étant deux à deux
  premiers entre-eux. Alors il existe une unique décomposition $F = E +
  \sum_{i=1}^q \sum_{j=1}^{\alpha_i} \frac{A_{i,j}}{P_i^j}
  \ \forall (i,j) \deg A_{i,j} < \deg P_i$.
\end{theoreme}
\begin{proof}
  On trouve $E$ de façon unique par le lemme \ref{alg04:lemme:partent}. On
  a donc $F = E + G,\ \deg G < 0$. Le lemme \ref{alg04:lemme:polesimple}
  permet d'écrire $G = \dfrac{A}{\prod_{i=1}^q P_i^{\alpha^i}}$
  sous la forme $G = \sum_{i=1}^q \frac{A_i}{P^{\alpha_i}},\
  \deg A_i < \deg P$. Enfin le lemme \ref{alg04:lemme:polemultiple} donne
  $\frac{A_i}{P^{\alpha_i}} = \sum_{j=1}^{\alpha_i
  \frac{A_{i,j}}{P_i^j} }\ \deg A_{i,j} < \deg P_i$.
\end{proof}

\subsection{La pratique dans $\C(X)$}

\subsubsection{Forme \textit{a priori} de la décomposition}

On suppose que $F = \dfrac{N}{\prod_{i=0}^q (X - a_i)^{\alpha_i}} \in
\C(X)$. On a déjà précisé que $E$ s'obtenait par division euclidienne.

\begin{proposition}
  Si $a$ est un pôle simple de $F = \dfrac{A}{B}$, alors il se décompose en
  $\dfrac{\tilde{A}(a)}{\tilde{B'}(a)(X - a)}$
\end{proposition}
\begin{proof}
  Soit $F = \dfrac{A}{B}$. $a$ étant un pôle simple de $F$, il existe $C \in
  \K[X],\ B = (X - a)C$ et donc $F = \frac{\alpha}{X - a} + \frac{R}{C}$ et
  $\tilde{C}(a) ≠ 0$. On a $(X-a) \frac{A}{(X - a)C} = \alpha + \frac{R}{C}
  (X -a)$ et donc $\dfrac{\tilde{A}(a)}{\tilde{C}(a)} = \alpha$.

  Il faut désormais déterminer $\tilde{C}(a)$. Mais, $B' = (X - a)C' + C$ et
  donc $\tilde{C}(a) = \tilde{B'}(a)$.
\end{proof}

\begin{exemple}
  $F = \dfrac{X^3 + 2}{(X+1)^2(X - 1)}$

  $F = 1 + \frac{a}{X - 1} + \frac{b}{X + 1} + \frac{c}{(X+1)^2}$
\end{exemple}

\begin{question} Décomposer en éléments simples $F = \dfrac{X^2 + 2}{X^4
  -1}$
\end{question}
\begin{solution}
  $F = \frac{a}{X -1} + \frac{b}{X+1} + \frac{c}{X - i} + \frac{d}{X + i}$

  Comme $\tilde{F}$ est paire, on a $\forall x\in \mathcal{D}_F,\
  \tilde{F}(x) = \tilde{F}(-x)$ et donc $a = -b$ et $c = -d$. Comme de plus
  $F$ est à coefficient réels, $F = \conj{F}$, on $a = \conj{a}$ et $c = -
  \conj{c}$.
\end{solution}

\begin{question}
  Décomposer
  \begin{enumerate}
    \item $F = \frac{X}{X^2 + 1}$
    \item $F = \frac{X + 1}{X^3 - 1}$
  \end{enumerate}
\end{question}
\begin{solution}
  \begin{enumerate}
    \item $F = \dfrac{\frac12}{X - i} + \dfrac{\frac12}{X + i}$
    \item $F = \dfrac2{3(X - 1)} + \dfrac{1}{3(X - j)} + \dfrac{1}{3(X -
      j^2)}$
  \end{enumerate}
\end{solution}

Lorsque la décomposition \textit{a priori} est «longue», on abaisse le
nombre d'inconnue en prenant quelques valeurs particulières. En pariculier,
un pôle $a$ de multiplicité $k$ peut s'annuler en prenant la valeur en
$\tilde{(X - a)^k F}(a)$. De même, on peut utiliser $\tilde{X F}(+ \infty)$
pour déterminer des égalités sur les éléments simple de multiplicité 1.

Pour finir, on procède par identification.

\begin{exemple}
  $F = \dfrac{X + 2}{(X + 1)^2(X+3)^2}$

  $F = \frac{a}{X+1} + \frac{b}{(X+1)^2} + \frac{c}{X + 3}^2 +
  \frac{d}{(x+3)^2}$.

  $\tilde{X F}(+ \infty) = 0 \implies a + c = 0 \implies c = -a$

  $\tilde{(X + 1)^2 F}(-1) = \frac14 = b$ et $\tilde{(X + 3)^2 F}(-3) =
  \frac{-1}4 = d$

  On a donc $F = \frac{a}{X+1} + \frac{1}{4(X+1)^2} + \frac{-a}{X + 3}^2 +
  \frac{-1}{4(x+3)^2}$. En prenant la valeur en 0, on a $\frac29 = a +
  \frac14 - \frac{a}{3} - \frac1{36}$ et donc $a =0$
\end{exemple}

\begin{question}
  Décomposer $F = \dfrac{X^4}{(X+1)^3}$
\end{question}
\begin{solution}
  $F = X - 3 + \frac{6}{X+1} - \frac4{(X + 1)^2} + \frac{1}{(X+1)^3}$
\end{solution}

\subsection{Décomposition en éléments simples dans $\R(X)$}

On peut effectuer la décomposition dans $\C(X)$ et «regrouper les pôles
conjugués.

\begin{question}
  Décomposer $F = \frac{X + 1}{X^3 - 1}$ dans $\R(X)$
\end{question}
\begin{solution}
  $F = \dfrac2{3(X - 1)} + \frac{-2X - 1}{3(X^2 + X + 1)}$
\end{solution}

\begin{remarque}
  $F = \frac{X}{(X^2 + 1)^3}$ est un élément simple (de deuxième espèce.)
\end{remarque}
