\chapter{Arithmétique dans $\Z$}

\section{Rappels sur $\Z$}

On rappelle que
\begin{itemize}
  \item $(\Z,+,×)$ est un anneau commutatif intègre
  \item $(\Z,+)$ est un groupe commutatif de neutre 0
  \item $(\Z,×)$ est à loi interne, associative, de neutre 1, distributive
    sur + et commutative.
\end{itemize}

En particulier une loi mutliplicative est intègre lorsque $ab = 0 \implies a
= 0 \vee b = 0$.

Une brève étude de $\Z$ montre d'ailleurs que les seuls inversibles de $\Z$
sont 1 et $-1$. Rappellons au passage que les inversibles d'un anneau
forment un groupe.

\begin{proposition}
  Tout élément de $\Z$ est simplifiable.
\end{proposition}
\begin{proof}
  Voir la preuve de la proposition \ref{01alg:prop:regulier}
\end{proof}

\begin{proposition}
  $\Z$ est muni de l'ordre qui prolonge l'ordre dans $\N$. De plus, toute
  partie non vide majorée admet un plus grand élément, et toute partie non
  vide minorée admet un plus petit élément.
\end{proposition}
\begin{proof}
  $\Z$ est construit comme $\N \cap (-\N)$ et ces propositions découlent de
  la construction de $\N$.
\end{proof}

\begin{proposition}
  Dans $\Z$, la division euclidienne s'écrit $\forall (a,b) \in \Z^2,\
  \exists ! (q,r) \in \Z^2,\ a = bq + r,\ 0 ≤ r < b$.
\end{proposition}
\begin{proof}
  La démonstration est analogue à celle de la proposition
  \ref{01alg:prop:unicite_division}.
\end{proof}

\subsection{Congruence dans $\Z$}

On rappelle la définition, étendue à $\Z$ de la notion d'équivalence.

\begin{definition}
  $\forall (a,b) \in \Z^2,\ \forall n \in \N^*,\ a \equiv b \mod n$ signifie
  que $b - a \in n\Z$, ou encore $\exists p \in \Z,\ b - a = np$.
\end{definition}

\subsubsection{Propriétés}

\begin{proposition}
  La relation d'équivalence modulo $n$ est une relation d'équivalence.
\end{proposition}
\begin{proof}
  \begin{itemize}
    \item Si $a \equiv b \mod n$, alors $a - b\in n\Z$ et donc, $b - a \in
      n\Z$. Ainsi $b \equiv a \mod n$.
    \item $a - a = 0 \in n\Z$ donc, $a \equiv a \mod n$.
    \item Si $a \equiv b \mod n \wedge b \equiv c \mod n$, alors $b - a \in
      n\Z \wedge c - b \in n\Z$. Ainsi, il existe $p\in \Z$ et $p' \in \Z$
      tels que $b - a = pn$ et $c - b = p'n$. On a donc, en sommant, $c -
      a = (p+p')n$ et donc $c - a \in n\Z$. Donc $a \equiv c \mod n$.
  \end{itemize}
\end{proof}

\begin{proposition}\label{09alg:prop:compatibilite}
  $\equiv$ est compatible avec les lois $+$ et $×$.
\end{proposition}
\begin{proof}
  Soient $a, b, a'$ et $b'$ quatre entiers relatifs tels que $a \equiv b
  \mod n$ et $a' \equiv b' \mod n$. Il existe $p$ et $q$ dans $\Z$ tels que
  $b - a = pn$ et $b' - a' = qn$. Ainsi, on a $b + b' - (a + a') = (p+q)n$
  et donc $a + a' \equiv b + b' \mod n$.\\
  $bb' - aa' = bb' - ab' + ab' - aa' = (b-a)b' + a(b' - a') = npb' + anq =
  n(pb' + qa)$, donc $bb' - aa' \in n\Z$ et $aa' \equiv bb' \mod n$.
\end{proof}

\begin{proposition}
  Si $b \mid a$ ($b$ divise $a$), alors $a = bq$ et $a \equiv 0 \mod b$.
\end{proposition}
\begin{proof}
  Il suffit de remarquer que $0 - a = -bq$.
\end{proof}

\begin{definition}[Anneau quotient $\EnsembleQuotient{\Z}{n\Z}$]
  $\EnsembleQuotient{\Z}{n\Z}$ est l'ensemble des classes d'équivalences de
  $\Z$ pour la relation d'équivalence modulo $n$.
\end{definition}

On peut noter que $\EnsembleQuotient{\Z}{n\Z} = \{ \dot{0}, \dot{1}, …,
\dot{n-1} \}$.

\begin{proposition}
  $\EnsembleQuotient{\Z}{n\Z}$ est un anneau commutatif.
\end{proposition}
\begin{proof}
  $(\Z,+,×)$ est un anneau. On définit les lois $\dot{+}$ et $\dot{×}$ par
  $\dot{a} \dot{+} \dot{b} = \dot{\overline{a + b}}$ et $\dot{a} \dot{×}
  \dot{b} = \dot{\overline{a × b}}$, ce qui est possible grâce à la
  proposition \ref{09alg:prop:compatibilite}. Ainsi, les lois induites
  garantissent les propriétés de l'anneau.\\
  En revanche, un tel anneau n'est en général pas intègre : $\dot{2} \dot{×}
  \dot{2} = \dot{0}$ dans $\EnsembleQuotient{\Z}{4\Z}$ alors que $\dot{2} ≠
  \dot{0}$.
\end{proof}

\begin{question}
  Construire les tables des opérations de $\EnsembleQuotient{\Z}{5\Z}$.
\end{question}
\begin{solution}
  \begin{tabular}{c|*{5}{c}}
    + & 0 & 1 & 2 & 3 & 4 \\ \hline
    0 & 0 & 1 & 2 & 3 & 4 \\
    1 & 1 & 2 & 3 & 4 & 0 \\
    2 & 2 & 3 & 4 & 0 & 1 \\
    3 & 3 & 4 & 0 & 1 & 2 \\
    4 & 4 & 0 & 1 & 2 & 3 \\
  \end{tabular}

  \begin{tabular}{c|*{5}{c}}
    × & 0 & 1 & 2 & 3 & 4 \\ \hline
    0 & 0 & 0 & 0 & 0 & 0 \\
    1 & 0 & 1 & 2 & 3 & 4 \\
    2 & 0 & 2 & 4 & 1 & 3 \\
    3 & 0 & 3 & 1 & 4 & 2 \\
    4 & 0 & 4 & 3 & 2 & 1 \\
  \end{tabular}
\end{solution}

\section{Notions de PGCD et de PPCM}

\subsection{PGCD}

\begin{proposition}\label{09alg:prop:somme_sous-groupe}
  Soient $G_1$ et $G_2$ deux sous-groupes de $\Z$. Alors $G_1 + G_2$ est un
  sous-groupe de $\Z$.
\end{proposition}
\begin{proof}
  $0 = 0 + 0$, donc $G_1 + G_2 ≠ ø$. Soient $x$ et $y$ deux éléments de $G_1
  + G_2$. $y - x = g_1^y + g_2^y - g_1^x - g_2^x =
  \underbrace{\underbrace{g_1^y - g_1^x}_{\in G_1} + \underbrace{g_2^y -
  g_2^x}_{\in G_2}}_{\in G_1 + G_2}$.  Donc $G_1 + G_2$ est un sous-groupe
  de $\Z$.
\end{proof}

\begin{corollaire}
  Soient $a$ et $b$ deux entiers relatifs non nuls. $a\Z + b\Z$ est un
  sous-groupe de $\Z$.
\end{corollaire}
\begin{proof}
  On applique la proposition précédente à $G_1 = a\Z$ et $G_2 = b\Z$ qui
  sont des sous-groupes de $\Z$.
\end{proof}

\begin{proposition}
  Soient $a$ et $b$ deux entiers relatifs non nuls. Il existe unique $d \in
  \N^*$ tel que $a\Z + b\Z = d\Z$.
\end{proposition}
\begin{proof}
  $a\Z + b\Z$ est un sous-groupe de $\Z$, or, d'après la proposition
  \ref{05alg:prop:sousgroupes_Z}, ceux ci sont de la forme $d\Z$, on a
  l'existence. L'unicité découle du fait que si $d\Z$ et $d'\Z$ sont le même
  sous-groupe de $\Z$, alors $d \mid d'$ et $d' \mid d$, ce qui n'est
  possible que si $d = d'$, car $d \in \N^*$.
\end{proof}

\begin{definition}[PGCD]
  Le \emph{PGCD (plus grand commun diviseur)}  de $a$ et $b$ est l'entier
  $d$ tel que $d\Z = a\Z + b\Z$. On note $d = a \wedge b$.
\end{definition}

\begin{exemple}
  $2 \wedge 3 = $ ?\\
  $2\Z + 3\Z = \{ 2x + 3y \mid (x,y) \in \Z^2 \}$. On a $1 = 2×(-1) + 3×1$,
  donc $1 \in 2\Z + 3\Z$. Ainsi, $\Z \subset 2\Z + 3\Z \subset \Z$ et donc
  $2 \wedge 3 = 1$.
\end{exemple}

\begin{remarque}
  \begin{itemize}
    \item On trouve parfois la notation $\gcd(a,b)$.
    \item Le nom sera explicité plus tard.
    \item La définition pourra s'étendre au pgcd de $n$ nombres.
  \end{itemize}
\end{remarque}

\begin{proposition}
  \begin{itemize}
    \item $\forall (a,b) \in \Z^2,\ a \wedge b = b \wedge a$
    \item $\forall a \in \Z,\ a \wedge 0 = \abs{a}$ et $0 \wedge 0 = 0$
    \item Si $d = a\wedge b $ et $d ≠ 0$, alors $d \mid a$ et $d \mid b$ et
      $d$ est le maximum de l'ensemble des diviseurs.
  \end{itemize}
\end{proposition}
\begin{proof}
  \begin{itemize}
    \item Trivial
    \item Il faut remarquer que $0\Z = \{ 0 \}$ et les résultats en
      découlent.
    \item Posons $d\Z = a\Z + b\Z$. On a $a \in a\Z$ donc en particulier $a
      \in a\Z + b\Z = d\Z$ et donc $d \mid a$. De même, $d \mid b$.\\
      Si $\delta \mid a$ et $\delta \mid b$, alors $\delta \mid ax + by$ et
      donc $\delta$ divise tous les éléments de $d\Z$, donc en particulier
      $\delta \mid d$. Donc $d$ est le plus grand diviseur de $a$ et $b$.
  \end{itemize}
\end{proof}

En partique, on utilise quelques propriétés faciles :
\begin{itemize}
  \item $a \wedge b = \abs{a} \wedge \abs{b}$
  \item $\forall m \in \Z,\ (ma) \wedge (mb) = \abs{m}(a \wedge b)$
  \item Si $\delta \mid a $ et $\delta \mid b$, alors
    $\brk*{\frac{a}{\delta} \wedge \frac{b}{\delta} } = \frac1{\abs{\delta}}
    (a \wedge b)$
  \item $d = a \wedge b \iff \begin{cases} a = da',\ a' \in \Z \\ b = b'd
      ,\ b' \in \Z \\
    a' \wedge b' = 1 \end{cases}$
\end{itemize}

En terme de vocabulaire, on dit que $a$ et $b$ sont «premiers entre eux» ou
«étrangers» si $a \wedge b = 1$.

\begin{theoreme}[Bachet de Mezériac (1581-1638) -- Bezout(1730-1783)]
  \label{09alg:thm:bezout}
  $a$ et $b$ sont étrangers si et seulemnt s'il existe $(u,v) \in \Z^2$ tels
  que $au + bv = 1$.
\end{theoreme}
\begin{proof}
  Si $a \wedge b = 1$, alors l'égalité d'ensemble $a\Z + b\Z = \Z$ donne
  directement l'existence de $(u,v)$ tels que $au+bv=1$.\\
  Réciproquement, s'il existe $(u,v) \in \Z^2,\ au + bv = 1$, alors $1 \in
  a\Z + b\Z$ et donc $\Z \subset a\Z + b\Z \subset \Z$, d'où $\Z = a\Z +
  b\Z$ et $a \wedge b = 1$.
\end{proof}

\begin{exemple}
  Calcul de $\np{1983} \wedge \np{2018}$. On cherche $u$ et $v$ entiers
  relatifs tels que $1983u + 2018v = 1$.\\
  Raisonnons modulo \np{1983}.\\
  $1983u + 2018v \equiv 1 \mod 1983 \iff 35v \equiv 1 \mod 1983 \iff 35v - 1
  $ est divisible par 1983. On trouve $v = 170$, puis $u = (1 -
  2018×170)/1983$. On vérifie : $-173×1983 + 2018×170 = 1$.
\end{exemple}

\begin{corollaire}[Théorème de Gauss]\label{09alg:thm:Gauss}
  Si $a \mid (bc)$ et $a \wedge b = 1$, alors $a \mid c$.
\end{corollaire}
\begin{proof}
  Comme $a \wedge b = 1$, il existe $(u,v) \in \Z^2,\ au + bv = 1$. D'où
  $acu + bcv = c$. $a \mid acu$. Comme $a \mid bc$, $a \mid bcv$, donc $a
  \mid acu + bcv $ ce qui s'écrit encore $a \mid c$.
\end{proof}

\begin{exemple}
  Application à la résolution de $1983u = 2018v = 1$.\\
  On connaît la solution $u_0 = -173$ et $v_0 = 170$. Ainsi, l'équation est
  équivalente à $1983u + 2018v = -173×1983 + 2018×170$ soit encore $1983(u +
  173) = 2018(170 -v)$. Comme $1983 \wedge 2018 = 1$, 1983 divise $170 -v$.
  On peut donc écrire cette dernière sous la forme $k1983 = 170 - v$ et donc
  $v = 170 - k1983$. Pour ce $v$ là, on peut trouver $u$ et l'ensemble des
  solutions est donc $\{ (u,v) \in \Z^2 \mid u = -173 + 2018k, v = 170 -
  1983k, k \in \Z \}$.
\end{exemple}

\begin{proposition}
  Si $a \mid c$ et $b \mid c$ et $a \wedge b = 1$ alors $ab \mid c$.
\end{proposition}
\begin{proof}
  $c = ak$ et $c = bk'$. On a donc $ak = bk'$ Comme $a \wedge b = 1$ et que
  $a \mid a k$, on a que $a \mid b k'$, mais d'après le théorème de Gauss
  (\ref{09alg:thm:Gauss}) $a \mid k'$, c'est-à-dire $k' = ak"$ et donc $bk'
  = bak"$, d'où $c = bak"$ et donc $ab \mid c$.
\end{proof}

\begin{proposition}
  Si $a \wedge b =1 $ et $a \wedge c = 1$, alors $a \wedge bc = 1$.
\end{proposition}
\begin{proof}
  $\exists (u,v,u',v') \in \Z^4$ tels que $a u + bv = 1$ et $au' + cv' = 1$,
  d'où $(a u + bv)(au' + cv') =1 \implies a^2uu' + abvu' + aucv' + bucv' = 1
  \implies a(auu' + bvu' +cv'u) + (bc)uv' = 1$ et donc $a \wedge (bc) = 1$.
\end{proof}

\begin{corollaire}
  Si $a \wedge b = 1$, alors, $\forall (m,n) \in \N^2,\ a^m \wedge b^n = 1$.
\end{corollaire}
\begin{proof}
  Démonstration par récurrence sur $m$, puis sur $n$ en utilisant le
  théorème précédent.
\end{proof}

On peut présenter l'algorithme d'Euclide qui constitue une méthode pratique
pour le caclul du PGCD. De plus, cet algorithme peut-être facilement étendu
pour calculer également les coefficients $(u,v)$ de Bezout.\\
Cet algorithme repose principalement sur la proposition qui suit.

\begin{proposition}
  $\forall (a,b,c) \in \Z,\ a \wedge b = a \wedge (b - ac)$.
\end{proposition}
\begin{proof}
  Si $m \in a\Z + b\Z$, alors il existe $(u,v) \in \Z^2, a u + b v = m$ et
  donc $m = au + (b-ac)v + v ac = a(u +cv) + (b -ac)v \in a\Z + (b -
  ac)\Z$.\\
  Si $m \in a\Z + (b - ac)\Z$, alors $(u,v) \in \Z^2, a u + (b - ac)v = m$
  et donc $m = a u + bv - acv = a(u-cv) + bv \in a\Z + b\Z$.\\
  Ainsi, $a\Z + b\Z = a\Z + (b - ac)\Z$.
\end{proof}

\begin{exemple}Un tel algorithme peut s'écrire en Python :\\
  \begin{verbatim}
def pgcd(a,b):
    if b == 0 :
        return a
    else:
        return pgcd(b,a % b)
  \end{verbatim}
\end{exemple}

\begin{exemple}
  Dans le cas du calcul de $2002 \wedge 1983$ :\\
  \begin{tabular}{l|*{7}{c|}c}
    quotient &  & 1 & 104 & 2 & 1 & 2 & & \\ \hline
    reste & 2002 & 1983 & 19 & 7 & 5 & 2 & 1 & 0 \\
  \end{tabular}
\end{exemple}

On peut étendre l'algorithme ci-dessus, à une version qui permet le calcul
rapide des coefficients $u$ et $v$ de l'égalité de Bézout. On pose ainsi,
$u_{-1} = 1, u_{0} = 0$ et $v_{-1} = 0, v_{0} = 1$ et pour chaque itération,
$u_{n+1} = u_{n-1} - q_{n} u_{n}$ et $v_{n+1} = v_{n-1} - q_n v_n$.

\begin{exemple}
  Dans le cas du calcul de $2002 \wedge 1983$ :\\
  \begin{tabular}{l|*{7}{c|}c}
    $q_n$ &      & 1    & 104 & 2    & 1    & 2    &      &    \\ \hline
    $r_n$ & 2002 & 1983 & 19  & 7    & 5    & 2    & 1    &  0 \\ \hline
    $u_n$ & 1    & 0    & 1   & -104 & 209  & -313 & 835  &    \\ \hline
    $v_n$ & 0    & 1    & -1  & 105  & -211 & 316  & -843 &    \\
  \end{tabular}
\end{exemple}

Justification :
On pose $a = r_{-1}$ et $b = r_0$. $a = bq_0 + r_1,\ 0≤ r_1 < b$ et $b = r_1
q_1 + r_2,\ 0≤ r_2 < r_1$ et de façon générale $r_{k-2} = r_{k-1} q_{k-1} +
r_{k},\ 0 ≤ r_k < r_{k-1}$.\\
Ces restes peuvent s'exprimer comme $r_{-1} = 1 a + 0 b$, $r_0 = 0a + b$,
$r_1 = r_{-1} + r_0q_0$, …, $r_k = r_{k-2} - r_{k-1}q_{k-1}$, ce qui peut
s'écrire, avec les notations $u_n$ et $v_n$, $r_j = u_j a + v_j b$ et
$r_{j+1} = r_{j-1} - r_j q_j = u_{j-1}a + v_{j-1} b - q_j (u_j a + v_j b) =
(u_{j-1} - q_j u_j) a + (v_{j-1} - q_j v_j) b$.

\subsection{PPCM}

\subsubsection{Propiétés}

\begin{proposition}
  L'intersection de deux sous-groupes de $\Z$ est un sous-groupe de $\Z$.
\end{proposition}
\begin{proof}
  C'est un cas particulier du théorème
  \ref{05alg:thm:intersection_ss_grpes}.
\end{proof}

\begin{definition}[ppcm]
  L'intersection des sous-groupes $a\Z$ et $b\Z$ est un sous-groupe de $\Z$
  et on a donc $a\Z \cap b\Z = m\Z$. $m$ est le \emph{ppcm} de $a$ et $b$ et
  on le note $m = a \vee b$.
\end{definition}

\begin{proposition}
  \begin{itemize}
    \item $a \vee 0 = 0$
    \item $a \vee b = b \vee a = \abs{a} \vee \abs{b}$
    \item $\forall k \in \Z,\ (ka) \vee (kb) = \abs{k} a \vee b$
    \item $\delta \mid a \vee \delta \mid b \implies \frac{a}{\delta} \vee
      \frac{b}{\delta} = \frac{a \vee b}{\abs{\delta}}$
  \end{itemize}
\end{proposition}
\begin{proof}
  Démonstration aisée laissée au lecteur.
\end{proof}

\begin{remarque}
  Si $d = a \wedge b$, on a $a = da'$ et $b = db'$ avec $a' \wedge b' = 1$.
  Alors $a \vee b = d(a' \vee b')$, on ramène le calcul au calcul du ppcm
  dans le cas om les nombres sont premiers entre eux.
\end{remarque}

\begin{theoreme}
  Si $a \wedge b = 1$, alors $a \vee b = ab$.
\end{theoreme}
\begin{proof}
  Si $m = a \vee b$, alors $m \in a\Z$ et $m \in b\Z$. Ainsi, il existe
  $(k_a,k_bà \in \Z^2$ tels que $m = k_a a$ et $m = k_b b$.\\
  $a \mid k_a a$ donc $a \mid k_b b$ et comme $a \wedge b = 1$, alors $a
  \mid k_b$, donc il existe $k \in \Z,\ k_b = ak$ et $m = k ab$. Donc $m \in
  ab\Z$.\\
  Or $ab \in a\Z \cap b\Z$, donc $ab \in m\Z$, donc $k =1$ est la plus
  petite valeur et donc $a \vee b = ab$.
\end{proof}

\begin{corollaire}
  $\forall (a,b) \in \N^2,\ (a \vee b)(a \wedge b) = ab$.
\end{corollaire}
\begin{proof}
  Si $a \wedge b = 1$, alors on applique le théorème ci-dessus.\\
  Sinon, $a \wedge b = d$ et on utilise la remarque sur le calcul du ppcm en
  se ramenant au cas où le pgcd vaut 1. On a donc $(a \vee b) = d(a' \vee
  b') = a'b' $ et donc $d(a \vee b) = da' db'$ d'où le résultat.
\end{proof}

\par{Anneau $\EnsembleQuotient{\Z}{n\Z}$}
\label{09alg:ex:inversible_anneau_quotient}

Cherchons les inversibles de $\EnsembleQuotient{\Z}{n\Z}$. Soit
$\overline{a} \in \ZnZ{n}$. Si $\overline{a}$ est inversible, alors il
existe $b \in\Z$ tel que \begin{align*}
  \overline{a}\overline{b} & = \overline{1} \\
  \overline{ab} & = \overline{1} \\
  ab & \equiv 1 \mod n \\
\end{align*}
Ainsi, $\exists k \in \Z,\ ab - 1 = kn \iff ab - kn = 1$. D'après le
théorème de Bachet-Bezout (\ref{09alg:thm:bezout}) on a $a \wedge n = 1$.
Autrement dit, si $a$ est inversible dans $\ZnZ{n}$, alors il est premier
avec $n$.

Réciproquement, si $a \wedge n = 1$, alors, toujours d'après le théorème de
Bezout, on a $\exists (u,v) \in \Z,\ au + nv = 1$, soit encore $au - 1 =
-nv$. On a donc $au \equiv 1 \mod n$ et donc $\overline{au} = \overline{1}$,
ce qui s'écrit $\overline{a} × \overline{b} = \overline{1}$. Donc
$\overline{a}$ est inversible dans $\ZnZ{n}$.

Finalement, pour qu'un élément soit inversible dans $\ZnZ{n}$, il faut qu'il
soit premier avec $n$ et cette condition est suffisante.

\section{Nombres premiers}

\subsection{Généralités}

\begin{definition}[nombre premier]
  Un entier naturel $p$ est dit \emph{premier} lorsqu'il possède exactement
  deux diviseurs distincts : 1 et $p$.

  Un nombre nom premier est dit \emph{composé}
\end{definition}

\begin{exemple}
  2, 3, 17,257, 65537, 1234567891 sont premiers
\end{exemple}

Les nombres premiers ont fait l'objet de nombreuses recherches par le passé
: Fermat, Mersenne, Euler, Gauss, … En particulier Fermat a émis l'hypothèse
qu'un nombre de la forme $F_n = 2^{(2^n)} + 1$ est premier. Cette hypothèse
se vérifie sur les 4 premiers nombres, mais Euler a montré que $F_5 =
\np{4294967297} = \np{641}×\np{67004117}$ n'est pas premier. Montrons que
$2^{32} + 1 \equiv 0 \mod 641$.
\begin{align*}
  641 & = 5×128 +1  \\
      & = 1 + 5×2^7 \\
      & = 2^4 + 5^4 \\
\end{align*}
En particulier, on a $5 × 2^7 \equiv -1 \mod 641$ et $5^4 \equiv -2^4 \mod
641$, d'où $(5 × 2^7)^4 \equiv 1 \mod 641$ et donc $-2^4 × 2^{28} \equiv 1
\mod 641$, on a donc $-2^{32} \equiv 1 \mod 641$ et donc $0 \equiv 2^{32} +
1 \mod 641$.

Pour des nombres plus grand comme $F_6$, on peut passer par un calcul
informatique. En revanche, ceux-ci ont montré que $2^{859433} - 1$ est
premier.

\subsection{Ensemble $\mathcal{P}$ des nombres premiers}

\begin{theoreme}
  L'ensemble $\mathcal{P}$ des nombres premiers est infini.
\end{theoreme}
\begin{proof}
  $2 \in \mathcal{P}$, donc $\mathcal{P} ≠ ø$\\
  On suppose que $\mathcal{P}$ est fini et qu'il admet donc un plus grand
  élément $p_k$. On construit $N = \prod_{i=1}^k p_i + 1$. $N >
  p_k$. $N$ est composé donc il existe $A$ et $B$ tel que $N = AB$. Si $A$
  est premier, alors $A$ est l'un des $p_i$ et donc $p_1 × … × p_k + 1 = p_i
  B$, ce qui est impossible, car $1 \not\equiv 0 \mod p_i$, donc $A$ est
  composé $A = A_1B_1$. En un nombre fini d'itérations, on trouve un
  diviseur premier de $N$ et alors $p_1 × … × p_k + 1 = p_i Q$ et donc $1
  \equiv 0 \mod p_i$ ce qui est impossible. Donc $N$ n'est pas composé, il
  est donc premier. Comme $N > p_k$, on en déduit que $\mathcal{P}$ n'a pas
  de plus grand élément.
\end{proof}

\par{Crible d'Ératosthène (-276 -- -194)}

\begin{proposition}
  Soit $n \in \N,\ n ≥ 3, n\ \text{impair}$. Si $\forall k \in \N,\ 3≤k<
  \sqrt{n} \implies k \nmid n$, alors $n$ est premier.
\end{proposition}
\begin{proof}
  $n = pq$ et $p < q$. alors $p < \sqrt{n}$ et $q ≥ \sqrt{n}$.
\end{proof}

On peut par exemple construire le crible d'Ératosthène :

\begin{tikzpicture}[scale=0.7]
  \foreach \i in {1,...,6} {
    \foreach \j in {1,...,17} {
      \pgfmathsetmacro{\t}{ int(\i + 6*(\j-1))}
      \draw (\i,-\j) node { \t } ;
    }
  }
  \draw (1,-1) node [draw,cross out] {} ;
  \draw (2,-1) node [draw,circle] {} ;
  \draw (2,-2) -- (2,-17) ;
  \draw (4,-1) -- (4,-17) ;
  \draw (6,-1) -- (6,-17) ;
  \draw (3,-1) node [draw,circle] {} ;
  \draw (3,-2) -- (3,-17) ;
  \draw (5,-1) node [draw,circle] {} ;
  \draw (4,-2) -- (1,-5) ;
  \draw (6,-5) -- (1,-10) ;
  \draw (6,-10) -- (1,-15) ;
  \draw (6,-15) -- (4,-17) ;
  \draw (1,-2) node [draw,circle] {} ;
  \draw (2,-3) -- (6,-7) ;
  \draw (1,-9) -- (6,-14) ;
  \draw (1,-16) -- (2,-17) ;
\end{tikzpicture}

Les nombres qui ne sont pas rayés dans ce crible sont des nombres premiers.

\begin{theoreme}[Décomposition en facteurs
  premiers]\label{09alg:thm:decomposition_facteurs_premiers}
  Pour tout entier naturel $n$, il existe une unique suite $(\nu_k)_{k\in
  \N} \in \N^{(\N)}$ telle que \[ n = \prod_{k \in \N} p_k^{\nu_k} \] où
  $(p_k)_{k\in \N}$ est la suite des nombres premiers.
\end{theoreme}
\begin{proof}
  On a vu que tout entier $n ≥ 2$ admet au moins un facteur premier.\\
  Pour $n = 1$, on peut poser $\nu_k = 0 \forall k \in \N$ et donc
  $\prod_{k \in \N} p_k^0 = 1$.\\
  Pour $n = pn_1,\ n_1 < n$, soit $n_1$ est premier, soit composé. Dans ce
  cas, on réitère le processus et en un nombre fini d'étapes, on obtient
  $n_k = p^k q $ ou $q = 1$ ou $q$ est impair. Ainsi, on a l'existence de la
  décomposition en facteurs premiers.

  Supposons qu'il existe $(\alpha_k)_{k\in\N}$ et $(\beta_k)_{k\in \N}$ tels
  que $\prod_{k \in \N} p_k^{\alpha_k} = \displaystyle\prod_{k
  \in \N} p_k^{\beta_k}$. Prenons le premier indice $k_0$ tel que
  $\alpha_{k_0} ≠ 0$. On a même $\alpha_{k_0} ≥ 1$, alors
  $p_{k_0}^{\alpha_{k_0}}$ divise le membre de gauche, donc le membre de
  droite. Or $p_{k_0}$ est étrangers à tous les $p_k$ sauf $p_{k_0}$, par
  conséquent $p_{k_0}^{\alpha_{k_0}}$ divise $p_{k_0}^{\beta_{k_0}}$ et donc
  $\alpha_{k_0} ≤ \beta_{k_0}$. Par symétrie, on obtient que $\alpha_{k_0} ≥
  \beta_{k_0}$ et donc $\alpha_{k_0} = \beta_{k_0}$. En épuisant tous les
  $p_{k_0}^{\alpha_{k}}$, on constate que les deux décompositions ont les
  mêmes facteurs, avec les mêmes exposants.
\end{proof}

\begin{remarque}
  Ce théorème est fondamental : il dit que tout nombre premier se décompose
  de façon unique, à l'ordre près des facteurs, en un produit de nombres
  premiers.
\end{remarque}

\begin{exemple}
  $120 = 2^3 × 3 × 5$.
\end{exemple}

\begin{remarque}
  Si $d$ divise $n$, alors sa décomposition en facteurs premiers est $d =
  \prod_{k \in \N} p_k^{\beta_k}$, avec $\forall k\in \N,
  \beta_{k} ≤ \alpha_{k}$.\\
  Le nombre de diviseurs de $n$ est $(1+\alpha_1)(1+\alpha_2)…(1+\alpha_k)$.
\end{remarque}

\begin{proposition}
  L'anneau $\ZnZ{p}$ est un corps si et seulement $p$ est premier.
\end{proposition}
\begin{proof}
  L'étude des inversibles de l'anneau quotient
  \ref{09alg:ex:inversible_anneau_quotient} a montré que $\overline{n}$
  était inversible dans $\ZnZ{p}$ si et seulement si $n \wedge p = 1$.\\
  Si $p \in \mathcal{P},\ \forall k \in \llbracket 1; p- 1\rrbracket,\ k
  \wedge p = 1$, d'où $\overline{k}$ est inversible et donc $\ZnZ{p}$ est un
  corps.

  Si $\ZnZ{p}$ est un corps, alors tous ses éléments non nuls sont
  inversibles, donc $p$ est premier avec tous les nombres $k$ de 1 à $p-1$.
  Il n'a donc d'autres diviseurs que $1$ et lui même et donc $p$ est
  premier.
\end{proof}

\begin{theoreme}[de Fermat (1600?--1665)]\label{09alg:thm:fermat}
  Si $p$ est premier et $a$ n'est pas un multiple de $p$, alors $a^{p-1}
  \equiv 1 \mod p$.
\end{theoreme}
\begin{proof}
  Si $n$ est premier, alors $\binom{n}{k} \equiv 0 \mod n$. En effet, pour
  $n$ premier, on a, pour $1 ≤ k ≤ n - 1, n \wedge k = 1$ et donc $n \wedge
  k! = 1$. Or, comme $\binom{n}{k} \in \N,\ k!$ divise $n(n-1)…(n-k +1)$ et
  divise $(n-1)…(n-k)$. Ainsi, en écrivant $\binom{n}{k} = n
  \underbrace{\frac{(n-1)…(n-k)}{k!}}_{\in \N} \equiv 0 \mod n$.

  Soit $a \in \llbracket 1 ; p - 1\rrbracket$ et $p$ premier. Si $a = 1,\
  a^{p-1} \equiv 1 \mod p$. Sinon, $a^p \equiv ((a-1) + 1)^p \equiv (a-1)^p
  + 1 \mod p$ car les coefficients de la formule du binôme de Newton sont
  copngrus à 0 modulo $p$.\\
  À une translation de l'unité, on obtient $(a-1)^p \equiv (a - 2)^p + 1
  \mod p$ et par «descente itérative»  $2^p \equiv 1^p + 1 \mod p$ et $1^p
  \equiv 1 \mod p$. Ainsi, on a $a^p \equiv a \mod p$ en «remonant», d'où
  $a^p - a \equiv 0 \mod p$. On peut écrire cette dernière égalité sous la
  forme $p \mid a(a^{p-1} - 1)$ et comme $a\wedge p = 1$, $p$ divise
  $a^{p-1} - 1$. On a donc $a^{p-1} - 1 \equiv 0 \mod p$ d'où $a^{p-1}
  \equiv 1 \mod p$.
\end{proof}

\begin{theoreme}[de Wilson (1741 -- 1793)]
  Soit $p \in \N, p ≥ 2$. $p$ est premier si et seulement si $(p-1)! \equiv
  -1 \mod p$.
\end{theoreme}
\begin{proof}
  On considère dans $\ZnZ{p}[X]$ le polynôme $X^{p-1} - 1$.\\ Pour $k \in
  \llbracket 1 ; p - 1\rrbracket,\ \overline{(X^{p-1} - 1)}(\overline{k}) =
  \overline{k}^{p-1} - 1 = \overline{0}$. Donc $X^{p-1} - 1$ admet $k - 1$
  racines distinctes $k, \ 1 ≤ k ≤ p - 1$.\\
  $X^{p-1} - 1 = \prod_{k=1}^{p-1}(X - k)$, le coefficient
  dominant étant 1, à gauche et à droite. En évaluant ce polynôme en
  $\overline{p}$, on obtient $\overline{-1} =
  \prod_{k=1}^{p-1}(\overline{p} - \overline{k})$, ce qui
  s'écrit $(p - 1)! \equiv -1 \mod p$.

  Réciproquement, si $(p - 1)! \equiv -1 \mod p$, alors il existe $q \in
  \Z,\ (p-1)! + qp = -1$. Écrivons $(p-1)!$ sous la forme $ku$, $k \in
  \llbracket 1 ; p - 1 \rrbracket$. Ainsi, $(p-1)! + qp = -1$ s'écrit $uk +
  qp = -1$, ce qui est équivalent à $k \wedge p = 1$ d'après le théorème de
  Bezout. Donc $p$ est premier.
\end{proof}
