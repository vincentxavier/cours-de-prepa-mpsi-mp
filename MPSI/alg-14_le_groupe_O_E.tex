\chapter{Le groupe $\O(E)$}

$E$ désigne ici un espace euclidien, $\braket{⋅}{⋅}$ est le produit scalaire
et l'espace est éventuel orienté. $\dim E = n < +\infty$ et même $n ≤ 3$ le
plus souvent.

\section{Généralités}

\subsection{Définition}

\begin{definition}
  Un endomorphisme $\varphi$ de $E$ est dit orthogonal lorsqu'il conserve le
  produit scalaire : $\forall (u,v) \in E^2,\
  \braket{\varphi(u)}{\varphi(v)} = \braket{u}{v}$.
\end{definition}

Si $\varphi$ est orthogonal, alors $\varphi$ conserve la norme et les
distances associées à la norme.

\begin{exemple} Les symétries orthogonales sont des endomorphismes
  orthogonaux.
\end{exemple}

\begin{proposition}
  Les endomorphismes orthogonaux sont les automorphismes de $E$. Ils
  transforment les bases orthonormales en bases orthonormales.\\
  Inversement, tout automorphisme de $E$ qui transforme une base
  orthonormale en une base orthonormale est un élément de $\O(E)$.
\end{proposition}
\begin{proof}
  Si $\varphi \in \O(E),\ \forall (i,j) \in \llbracket 1 ; n \rrbracket,\
  \braket*{\varphi(e_i)}{\varphi(e_j)} = \braket{e_i}{e_j} =
  \delta_{i,j}$.\\
  $\Im \varphi = \Vect \varphi(e_i)_{1≤i≤n} = E$ donc $\varphi$ est
  surjective.\\
  $\dim \ker \varphi = \dim E - \rg \varphi = 0$ donc $\varphi$ est
  injective.\\
  $\varphi \in \GL(E)$.

  Si $\varphi \in \GL(E)$ et $\varphi(\B)$ est aussi une base orthonormale.
  $\B = (e_i)_{1≤i≤n}$.\\
  $u = \sum_{i=1}^n u_i e_i$ et $u' = \sum_{i=1}^n u'_i e_i$.\\
  Dans la base $\varphi(\B)$, $\braket*{\varphi(u)}{\varphi(u')} =
  \sum_{i=1}^n u_i u'_i$.\\
  Dans la base $\B$, $\braket{u}{u'} = \sum_{i=1}^n u_i u'_i$.
  On a ainsi l'égalité et donc $\varphi$ est orthogonal.
\end{proof}

\begin{proposition}
  $\varphi \in \L(E)$ est orthogonal si et seulement s'il conserve la norme.
\end{proposition}
\begin{proof}
  Le sens direct est évident.

  Réciproquement, soit $\varphi \in \L(E)$. On a $\forall u \in E,\
  \norm{\varphi(u)} = \norm{u}$.\\
  Soit $(u,v) \in E$,
  \begin{align*}
    \braket{\varphi(u)}{\varphi(v)} & = \frac12 \brk*{
      \norm{\varphi(u)+\varphi(v)}^2 - \norm{\varphi(u)}^2 -
    \norm{\varphi(v)}^2 } \\
    & = \frac12 \brk*{ \norm{\varphi(u+v)}^2 - \norm{u}^2 - \norm{v}^2 } \\
    & = \frac12 \brk*{ \norm{u+v}^2 - \norm{u}^2 - \norm{v}^2 } =
    \braket{u}{v} \\
  \end{align*}
\end{proof}

\begin{proposition}
  $(\O(E),\circ)$ est un groupe non abélien.
\end{proposition}
\begin{proof}
  $\O(E) \subset \GL(E)$ d'après la proposition précédente. Comme $\Id_E \in
  \O(E)$, celui est non vide.\\
  Soit $\varphi_1$ et $\varphi_2$ deux éléments de $\O(E)$. On a $\varphi_1
  \circ \varphi_2 \in \GL(E)$ et $\forall (u,v) \in E^2,\ \braket{\varphi_1
  \circ \varphi_2(u)}{\varphi_1 \circ \varphi_2(v)} =
  \braket{\varphi_2(u)}{\varphi_2(v)} = \braket{u}{v}$.\\
  Soit $\varphi \in \O(E)$. Comme $\varphi \in \GL(E)$, $\varphi$ est
  bijective et $\varphi^{-1} \in \GL(E)$.\\
  $\forall u \in E,\ \norm{\varphi(u)} = \norm{u}$. En écrivant $ u=
  \varphi^{-1}(v)$, on a $\norm{v} = \norm{\varphi^{-1}(v)}$ et donc en
  utilisant la proposition précédente, $\varphi^{-1} \in \O(E)$.
\end{proof}

On montre d'ailleurs que l'application $\det$ est un morphisme de
$(\O(\R^n), \circ) \to (\set{-1,1},×)$.

\begin{proposition}
  L'ensemble des automorphismes de déterminant $+1$ est un sous-groupe de
  $\O(E)$. On le note $\O^+(E)$ ou $\SO(E)$.
\end{proposition}
\begin{proof}
  Il s'agit du noyau du morphisme précédent.
\end{proof}

\section{Classification de $\O(\R^n),\ n ≤ 3$}

\subsection{Quelques résultats généraux}

\begin{proposition}
  Si $f \in \O(E)$, les seuls vecteurs colinéaires à leurs images sont
  (éventuellements) les vecteurs invariants (ou changés en leur opposé).
\end{proposition}
\begin{proof}
  Soit $f \in \O(E)$ et supposons que $v \in E$ soit colinéaire à son image.
  Alors $\exists \lambda \in \R \mid f(v) = \lambda v$, ce qui entraîne que
  $\norm{v} = \norm{f(v)} = \norm{\lambda v} = \abs{\lambda} \norm{v}$ et
  donc $\abs{\lambda} = 1$, ce qui s'écrit $\lambda = ± 1$.
\end{proof}

\begin{proposition}
  Soit $F$ un sous-espace vectoriel de $E$. Si $F$ est globalement invariant
  ($f(F) = F$) par $f$, alors $f_{|_F} \in \O(F)$ et de plus, $F^{\perp}$
  est aussi globalement invariant et $f_{|_F^{\perp}} \in \O(F^{\perp})$.
\end{proposition}
\begin{proof}
  On sait que $f(F) = F$ et $f_{|_F} \in \L(F)$. En outre, $\forall v\in F,
  \norm{f_{|_F}(v)} = \norm{f(v)} = \norm{v}$ et donc $f_{|_F} \in \O(F)$.

  Montrons que $F^{\perp} = f(F^{\perp})$.\\
  $f$ conserve la dimension des sous-espaces vectoriels.\\
  Soit $y \in f(F^{\perp})$, alors $y = f(x)$ et $x \in F^{\perp}$.\\
  Soit $v \in F,\ \braket{v}{y} = \braket{f(x)}{v} = \braket{f(x)}{f(u)} =
  \braket{x}{u} = 0$.\\
  Ainsi, $\forall v \in F,\ \braket{y}{v} = 0$ et donc $f(F^{\perp}) \subset
  F^{\perp}$.\\
  Comme $\dim f(F^{\perp}) = \dim F^{\perp}$, on en déduit l'égalité.
\end{proof}

\begin{proposition}
  Si $f \in \O(E)$, alors $\ker(f - \Id) = (\Im (f - \Id))^{\perp}$
\end{proposition}
\begin{proof}
  Si $v \in \ker(f - \Id)$, alors $f(v) = v$. Soit $y \in \Im (f - \Id),\
  \exists x \in E,\ y = f(x) - x$.
  \begin{align*}
    \braket{v}{y} & = \braket{v}{f(x) - x}             \\
                  & = \braket{v}{f(x)} - \braket{v}{x} \\
                  & = \braket{v}{x} - \braket{v}{x}    \\
                  & = 0                                \\
  \end{align*}
  On a donc $\underbrace{\ker(f - \Id)}_{\Lambda} \subset \underbrace{(\Im
  (f - \Id))^{\perp}}_{\Omega^{\perp}}$.\\
  On a $\Omega \oplus \Omega^{\perp} = E$ et $\dim \Lambda + \dim \Omega =
  \dim E$ d'où $\dim \Lambda = \dim \Omega^{\perp}$ et donc l'égalité
  annoncée.
\end{proof}

\begin{theoreme}
  Soient $a$ et $b$ deux vecteurs distincts de $E$, $\norm{a} = \norm{b}$.\\
  Il existe une unique réflexion $s$ qui échange $a$ et $b$.
\end{theoreme}
\begin{proof}
  \leavevmode
  \begin{description}
    \item[Condition nécessaire] $s(a) = b$ et $s(b) = a$, donc $s(a + b) = a
      + b$ et $s(a-b) = b - a$. Posons $a + b \in H$ et $a - b \in
      H^{\perp}$.\\
      Il y a donc au plus une solution : la symétrie par rapport à $\set{b -
      a}^{\perp}$.
    \item[Condition suffisante] $a + b \perp a - b$ ? En effet, $\braket{a +
      b}{a-b} = \norm{a}^2 - \norm{b}^2 = 0$.\\
      $a + b \in H$, $s(a) = s\brk*{ \frac12 \brk*{ (a+b) + (a-b)}} =
      \frac12 \brk*{ (a+b) - (a-b)} = b$ et $s(b) = s\brk*{ \frac12 \brk*{
      (a+b) - (a-b)}} = \frac12 \brk*{ (a+b) + (a-b)} = a$.\\
      Donc $s$ convient.
  \end{description}
\end{proof}

\subsection{$\dim E = 1$}

$\O(E) = \set{\Id, -\Id}$ et $\SO(E) = \set{\Id}$.

\subsection{$\dim E = 2$}

On trouve ici les matrices orthogonales de $E$

\begin{theoreme}
  Les réflexions engendrent $\O(\R^2)$.\\
  De plus, toute application orthogonale de $\R^2$ dans lui-même admet
  une décomposition en au plus deux réflexions.
\end{theoreme}
