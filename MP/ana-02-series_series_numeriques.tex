Dans tout le chapitre, on suppose que $E$ est un $\K$ espace vectoriel
normé \textbf{complet}, avec $\K = \R$ ou $\C$.\\
Cette hypothèse sera vérifiée si $E$ est un espace vectoriel réel ou
complexe de dimension finie. On rappelle que, dans ce cas tous les
normes sont équivalentes sur $E$, et que de ce fait la plus part des
propriétés énoncées sont indépendantes de la norme choisie.

\section{Généralités}

\subsection{Définitions et premières propriétés}

\subsubsection{Définition et premières propriétés}

\paragraph{Définition}

Soit $(u_n)_{n\in\N}$ une suite d'éléments de $E$. On lui associe la
suite $(U_n)_{n\in\N}$ de terme général $U_n = \sum_{k=0}^n u_k$.

\begin{definition}
  \leavevmode
  \begin{itemize}
    \item $U_n$ est appelée \textbf{somme partielle} d'ordre $n$.
    \item $(U_n)_{n\in\N}$ est la suite des sommes partielles.
    \item On appelle série de terme général $u_n$ et on note $\sum u_n$
      la couple de deux suites $\brk*{ (u_n)_{n\in\N}, (U_n)_{n\in\N}}
      \in \brk*{E^{\N}}^2$.
  \end{itemize}
\end{definition}

L'étude de la série $\sum u_n$ est par définition l'étude de la suite
$(U_n)_{n\in\N}$.

\paragraph{Suite et série}

Ainsi, à toute suite $(u_n)_{n\in\N}$ on fait correspondre la suite des
sommes partielles $(U_n)_{n\in\N}$ de la série $\sum u_n$.\\
Réciproquement, si $(v_n)_{n\in\N}$ est une suite d'élements de $E$, il
existe une unique série $\sum u_n$ dont la suite $(v_n)_{n\in\N}$ est la
suite des sommes partielles.\\
Il suffit pour cela de poser $\begin{cases} u_0 = v_0 \\ u_n = v_n -
v_{n-1},\ \forall n \in \N^* \end{cases}$ de sorte que $u_0 + u_1 + … +
u_n = v_n$.

\paragraph{Troncature}

Si $\sum u_n$ est une série et $n_0 \in \N^*$, on définit une nouvelle
série \emph{par troncature au rang $n_0$} en posant $\begin{cases} v_n =
  0 \ \text{pour}\ n < n_0 \\ v_n = u_n \ \text{pour} \ n ≥
n_0\end{cases} .$ Dans ce cas, la série $\sum v_n$ se note $\brk*{\sum
u_n}_{n ≥ n_0}$.\\
Réciproquement, si une suite $(u_n)_{n\in\N}$ n'est définie que pour $n
≥ n_0$, on peut définir la suite $\sum u_n$ en posant $u_n = 0$ pour
tout $n < n_0$.\\
Par la suite, nous adopterons cette convention.

\begin{exemple}
  $\sum \frac1n$ est la série dont le terme général $u_n$ est défini par
  $u_0 = 0$ et $u_n = \frac1n$ pour tout $n ≥ 1$.\\
  Elle est appelée \emph{série harmonique}.
\end{exemple}

\subsubsection{Convergence d'une série}

\paragraph{Définition}

\begin{definition}
  La série $\sum u_n$ est dite \emph{convergente} lorsque la suite
  $(U_n)_{n\in\N}$ des sommes partielles est convergente. La limite $U$ de
  $(U_n)$ est appelée \emph{somme} de la série $\sum u_n$ et est notée $U =
  \sum_{k=0}^{\infty} u_k$.\\
  Lorsque la série $(U_n)$ est divergente, on dit que la série est
  divergente.
\end{definition}

\paragraph{Reste d'une série}

Cette notion n'a de sens que pour les séries convergentes.

\begin{definition}
  Si $\sum u_n$ est une série convergente de somme $U$, on appelle reste
  d'ordre $n$ de la série la différence $R_n = U - U_n = \lim{p\to \infty}
  \sum_{k=n+1}^{n+p} u_k$ que l'on peut noter $R_n = \sum_{k = n+1}^{\infty}
  u_k$, puisqu'il s'agit de la troncature de la série obtenue à partir de
  $\sum u_n$ par troncature au rang $n+1$.
\end{definition}

\paragraph{Exemples}

Il s'agit ici de séries dont il est possible de calculer les sommes
partielles ou à défaut de les encadrer, ce qui permet d'en déduire la
nature.

\begin{itemize}
  \item $\sum \frac{1}{n!}$ ; $\sum (-1)^n$
  \item série géométrique
  \item série harmonique
\end{itemize}

\begin{exemple}
  $\sum \frac1{n(n+2)}$ et $\sum \ln(1 + \frac1n)$\\
  $\sum \ln(1 + \frac1{n^2})$ et $\sum \frac{1}{\sqrt{n}}$
\end{exemple}

\paragraph{Condition nécessaire de convergence}

\begin{theoreme}
  Si la série $\sum u_n$ converge, alors $u_n \underset{n \to
  \infty}{\longleftarrow} 0$.
\end{theoreme}

\begin{contreexemple}[La réciproque est fausse]
  La série harmonique diverge alors que $\frac1n$ converge vers 0.
\end{contreexemple}

Une série dont le terme général ne tend pas vers zéro est dite
\emph{grossièrement divergente}.

Dans toute la suite, on utilisera les notations suivantes : si $u = (u_n)$,
alors $\sum u_n$ est la série associée ; $(U_n)$ la suite des sommes
partielles ; en cas de convergence $U$ la somme de la série et $R_n(u)$ le
reste d'ordre $n$ de $\sum u$.

\paragraph{Séries composantes}

On suppose dans ce paragraphe que $E$ est un $\K$ espace vectoriel de
dimension finie $p$.\\
Soit $(e_k)_{1≤k≤p}$ une base de $E$.

Soit $(u_n)$ est une suite d'éléments de $E$ et $(u_{n,k})_{n\in \N}$ pour
$1≤k≤p$ les suites composantes telle que $u_n = \sum_{k=1}^p u_{n,k} e_k$.

\begin{theoreme}
  La série $\sum u_n$ converge si et seulement si les $p$ séries composantes
  convergent et de plus $U = \sum_{n=0}^{\infty} \brk*{ \sum_{k=1}^p
  u_{n,k}e_k} = \sum_{k=1}^p \brk*{ \sum_{n=0}^{\infty} u_{n,k}} e_k$.
\end{theoreme}

On peut noter le cas particuliers des séries complexes.

\begin{remarque}
  Étudier une série dans un $\K$ espace vectoriel revient à étudier
  simultanément $p$ séries à termes réels ou complexes.
\end{remarque}

\subsubsection{Opérations sur les séries}

\paragraph{Somme}

\begin{definition}
  La somme de deux séries $\sum u_n$ et $\sum v_n$ est la série de terme
  général $u_n + v_n$.
\end{definition}

\begin{proposition}
  Si les séries $\sum u_n $ et $\sum v_n$ sont convergentes, alors leur
  somme est aussi convergente.
\end{proposition}

\textbf{Application : partitions des indices}\\
Soit $\sum u_n$ une série et une partition de $\N$ en deux parties infinies
$I$ et $J$. On pose $a_n = u_n$ si $n \in I$ et 0 sinon et $b_n = u_n $ si
$n \in J$ et 0 sinon.\\
On a clairement $u_n = a_n + b_n$.\\
On peut conclure quant à la nature de $\sum u_n$ si les deux séries sont
convergentes ou si l'une des deux seulement est convergente. De façon
classique $I = 2\N$ et $J = \setcond{2n + 1}{n \in \N}$.

\begin{exemple}
  Étude de $\sum u_n$ avec $u_n = k^{(2 + (-1)^n)n},\ k\in \R_+^*$.
\end{exemple}

\paragraph{Produit par un scalaire}

\begin{definition}
  Le produit par un scalaire $\lambda \in \K$ de la série $\sum u_n$ est la
  série de terme général $\lambda u_n$. Elle sera notée $\sum \lambda u_n$.
\end{definition}

\begin{theoreme}
  Si la série $\sum u_n$ converge, alors $\forall \lambda \in \K$, la série
  $\sum \lambda u_n$ converge et sa somme vaut $\lambda U$.
\end{theoreme}

\begin{remarque} La réciproque est vraie si $\lambda ≠ 0$.
\end{remarque}

\textbf{Conclusion}\\
Compte tenu des deux paragraphes précédents, on peut munir l'ensemble des
séries convergentes d'une structure de $K$-espace vectoriel et l'application
qui, à une série convergente, fait correspondre sa somme est linéaire.

\paragraph{Produit}(de Cauchy)

Cela suppose que $\sum u_n$ soit à termes dans une algèbre de Banach.

\begin{contreexemple}[Ce n'est pas le produit terme à terme]
  Il est défini par analogie avec le calcul des coefficients d'un polynôme
  et sera justifiée par l'étude des séries entières.
\end{contreexemple}

\begin{definition}
  Soient $\sum u_n$ et $\sum v_n$ deux séries pris dans cet ordre. Le
  produit de Cauchy est la série de terme général $w_n$ avec \[ w_n =
  \sum_{k=0}^n u_k * v_{n-k} = \sum_{p+q = n} u_p * v_q. \] On la note $\sum
  u_n * \sum v_n$ et on l'appelle série produit.
\end{definition}

\begin{remarque}
  \leavevmode
  \begin{itemize}
    \item Ce produit n'est commutatif que si l'algèbre est commutative.
    \item Le produit de deux séries convergentes n'est pas toujours
      convergent.
  \end{itemize}
\end{remarque}

\subsubsection{Critère de Cauchy -- Absolue convergence}

\paragraph{Critère de Cauchy}

$E$ étant supposé être un espace de Banach, $\sum u_n$ est une série
convergente si et seulement si la suite $(U_n)$ est une suite de Cauchy.

\begin{proposition}[Critère de Cauchy pour les séries]
  $\sum u_n$ converge $\iff \forall \varepsilon \in \R_+^*,\ \exists N \in
  \N,\ \forall (n,p) \in \N^*,\ n ≥ N \implies \norm*{\sum_{k=n+1}^{n+p}
  u_k} ≤ \varepsilon $.
\end{proposition}

Conséquence pratique : pour montrer qu'une séruie $\sum u_n$ converge, par
application du critère Cauchy, on peut essayer de majorer
$\norm*{\sum_{k=n+1}^{n+p} u_k}$ \emph{indépendamment de $p$} par une suite
de limite nulle.

\begin{contreexemple}[Faux si $E$ n'est pas complet]
\end{contreexemple}

\paragraph{Absolue convergence}

$\norm*{\sum_{k=n+1}^{n+p} u_k} ≤ \sum_{k=n+1}^{n+p} \norm*{ u_k}$, on en
déduit que si la série $\sum_{k=n+1}^{n+p} \norm*{ u_k}$ converge, alors la
série $\sum u_n$ converge.

\begin{definition}
  On dit que la série $\sum u_n$ est absolument convergente si la série
  $\sum \norm{u_n}$ est convergente.
\end{definition}

\begin{theoreme}
  Si la série $\sum u_n$ est absolument convergente, alors elle est
  convergente et $\norm*{U} ≤ \sum_0^{\infty} \norm*{u_n}$.
\end{theoreme}

\begin{remarque}
  \leavevmode
  \begin{itemize}
    \item Faux si $E$ n'est pas un espace complet.
    \item la dénomination «absolue convergence» vient du fait que la notion
      a été introduite au départ pour les séries à termes réels, la norme
      étant la valeur absolue.
    \item Il existe des séries convergentes non absolument convergentes : la
      série harmonique alternée.
  \end{itemize}
\end{remarque}

\subsection{Théorèmes généraux}

\subsubsection{Convergence des séries et convergence de suites}

\paragraph{Théorème}

\begin{theoreme}
  Soit $(a_n)_{n\in\N}$ une suite d'éléments de $E$. On pose $u_n = a_{n+1}
  - a_n\ \forall n \in \N$. Alors $\sum u_n$ et $(a_n)_{n\in\N}$ sont de
  même nature.\\
  En cas de convergence, $U = \lim_{n\to\infty} a_n - a_0$
\end{theoreme}

\paragraph{Exemples et remarques}


