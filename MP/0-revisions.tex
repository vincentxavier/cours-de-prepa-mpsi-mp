\section{Révisions d'algèbre générale}

\subsection{Théorie des ensembles}

\subsubsection{Applications}

\paragraph{$f \colon E \to F$}

\begin{description}
  \item[$f$ injective] $\iff (\forall (x,x') \in E^2,\ x≠x' \implies
    f(x)≠f(x'))$\\
    $\iff (\forall (x,x') \in E^2,\ f(x) = f(x') \implies x = x')$
  \item[$f$ surjective] $\iff \forall y \in F,\ \exists x \in E,\ y =
    f(x)$\\
    $\iff \Im f = F$
  \item[$f$ bijective] $\iff \forall y \in F,\ \exists ! x \in E,\ y = f(x)$\\
    $\iff f$ injective et $f$ surjective
\end{description}

\paragraph{Image directe et réciproque}

\begin{itemize}
  \item $A \subset E,\ f(A) = \setcond{y \in F}{\exists x\in A\ \text{et}\ y
    = f(x)}$ ; $y \in f(A) \iff \exists x \in A \mid y = f(x)$
  \item $B \subset F,\ \overset{-1}{f}(B) = \setcond{x \in E}{f(x) \in B}$ ;
    $x \in \overset{-1}{f}(B) \iff f(x) \in B$.
\end{itemize}

\paragraph{Relation d'ordre}

\begin{itemize}
  \item Une relation $\mathcal{R}$ définie sur $E×E$ est une relation
    d'ordre si et seulement si elle est réflexive, transitive et
    anti-symétrique.
  \item $\mathcal{R}$ est un ordre total si deux éléments sont
    comparables.\\ $\mathcal{R}$ est un ordre partiel s'il existe deux
    éléments non comparables.
\end{itemize}

\paragraph{Relation d'équivalence}

\begin{itemize}
  \item Une relation $\mathcal{R}$ définie sur $E×E$ est une relation
    d'équivalence si et seulement si elle est réflexive, symétrique et
    transitive.
  \item La classe d'équivalence de $x$ est l'ensemble des éléments qui sont
    en relation avec $x$.
  \item L'ensemble des classes d'équivalence forme une partition de $E$.
\end{itemize}

\subsection{Structures fondamentales}

\subsubsection{Groupe}

$(G,*)$ est un groupe si et seulement si $*$ est une loi de composition
interne définie sur $G$, associative, possédant un élément neutre et pour
laquelle tout élément de $G$ est symétrisable.\\
Si, de plus, $*$ est commutative, on dit $(G,*)$ est un groupe commutatif ou
abélien.

$H \subset G$ est un sous groupe de $G$ si et seulement si $H ≠ ø,\ \forall
x \in H,\ \forall y \in H,\ x*y^{-1} \in H$ si et seulement si $H ≠ ø,\
\forall x \in H,\ \forall y \in H,\ x*y \in H,\ y^{-1} \in H$.

\subsubsection{Anneau}

\paragraph{$(A,+,×)$} est un anneau si et seulement si
\begin{enumerate}
  \item $(A,+)$ est un groupe abélien
  \item $×$ est une loi de composition interne associative et distributive
    sur l'addition
  \item il existe un élément neutre pour $×$, généralement noté $1_A$.
\end{enumerate}
Si $×$ est commutative, on dit que l'anneau est commutatif.

$B \subset A$ est un sous anneau si et seulement si $B ≠ ø,\ \forall (x,y)
\in B^2,\ x -y \in B\ x×y \in B,\ 1_A \in B$.

\paragraph{Compléments sur les anneaux}

Soit $(A,+,×)$ un anneau commutatif.
\begin{itemize}
  \item $x \in A,\ x ≠ 0_A$ est un diviseur de zéro si et seulement si
    $(\exists y ] 0_A,\ x×y = 0_A)$.
  \item $(A,+,×)$ est un anneau intégre si et seulement s'il est sans
    diviseur de zéro.
  \item Les corps $\Z,\ \R,\ \C,\ \kk$ sont des anneaux intègres.
  \item $\F(\R,\R),\ \L(E),\ \M_n(\K)$ sont des anneaux (et même des
    algèbres) non intègres.
\end{itemize}

\paragraph{Sommes classiques -- Identités remarquables}

\begin{itemize}
  \item $\sum_{k=1}^n k = 1 + 2 + … + n = S_{1,n} = \frac{n(n+1)}2$ ;
    $\sum_{k=1}^n k^2 = S_{2,n} = \frac{n(n+1)(2n+1)}6$ ; $S_{3,n} =
    \sum_{k=1}^n k^3 = \frac14 n^2 (n+1)^2 = S_{1,n}^2$
  \item Les identités remarquables qui suivent sont valables dans un anneau
    commutatif ou s'appliquent à deux éléments $a$ et $b$ d'un anneau non
    commutatif qui commutent, ce qui est le cas lorsque $a = 1_A$ ou $b =
    1_A$
    \begin{enumerate}
      \item $a^2 - b^2 = (a-b)(a+b)$ ; $a^3 - b^3 = (a-b)(a^2 + ab + b^2)$ ;
        $a^n - b^n = (a-b)(a^{n-1} + a^{n-2}b + … + ab^{n-2} + b^{n-1}) =
        (a-b)\sum_{k=0}^{n-1} a^{n-k-1}b^k$
      \item $a^3 + b^3 = (a+b)(a^2 - ab + b^2)$ ; $a^{2n+1} - b^{2n+1} =
        (a+b) \sum_{k=0}^{2n} (-1)^k a^{2n-k} b^k$
      \item Formule du binôme : $(x+y)^n = \sum_{k=0}^n \binom{n}{k} x^k
        y^{n-k} = \sum_{k=0}^n \binom{n}{k} x^{n-k} y^k$ avec $\binom{n}{k}
        = \frac{n!}{k!(n-k)!} \in \N$ ; $\binom{n}{k} = \binom{n}{n-k},\ 0 ≤
        k ≤ n$ ; $\binom{n}{k} = \binom{n-1}{k-1} + \binom{n-1}{k},\ 1 ≤ k ≤
        ≤ n - 1$.
    \end{enumerate}
\end{itemize}

\subsubsection{$\K$ espace vectoriel ; $\K$ algèbre}

\paragraph{$\K$ espace vectoriel} (voir le cours pour la définition)\\
$F \subset E$ est un sous espace vectoriel de $E$ si et seulement si $F ≠ ø,
\ \forall (x,y) \in F^2,\ \forall \lambda \in \K,\ x + \lambda y \in \F$.

\paragraph{$\K$ algèbre}

$(A,+,×,*)$ est une $\K$ algèbre si et seulement si
\begin{enumerate}
  \item $(A,+,×)$ est un anneau
  \item $(A,+,*)$ est un $\K$ espace vectoriel
  \item $\forall (\lambda,x,y) \in \K×A^2,\ (\lambda * x)×y = \lambda * (x
    ×y) = x × (\lambda *y)$
\end{enumerate}
$B \subset A$ est une sous algèbre si et seulement si $B ≠ ø,\ \forall
(\lambda,x,y) \in \K×B^2,\ (x+ \lambda y) \in B,\ x×y \in \B$.

\section{$\N$ -- Ensembles finis ou dénombrables}

\subsection{$\N$ : ensemble des entiers naturels}

\subsubsection{$(\N,+,×,≤)$}

$+$ est associative, commutative et 0 est son élément neutre.\\
$×$ est associative, commutative et 1 est son élément neutre.\\
0 est le seul élément inversible pour l'addition et 1 est le seul élément
inversible pour la multiplication.

\subsubsection{Étude de la relation d'ordre}

\begin{itemize}
  \item $≤$ relation d'ordre total compatible avec l'addition et la
    multiplication.
  \item Toute partie non vide majorée admet un plus grand élément.\\
    Toute partie non vide de $\N$ (donc $\N$ en particulier) admet un plus
    petit élément.
  \item Principe de récurrence :\\
    \begin{proposition}
      Soit $A$ une partie de $\N$ vérifant $\begin{cases} 0 \in A \\ \forall
      n \in \N,\ n \in A \implies n+1 \in A \end{cases}$ alors $A = \N$.
    \end{proposition}
    Cette propriété de $\N$ est le \emph{principe} et la
    \emph{justification} des démonstrations dites par récurrence
    \begin{itemize}
      \item Soit $P(n)$ une propriété indexée par $\N$ telle que
        \begin{enumerate}[label=(\roman*),labelsep=0.7pt]
          \item $P(0)$ soit vraie
          \item $\forall n \in \N, (P(n)\ \text{vraie} \implies P(n+1)\
            \text{vraie})$
        \end{enumerate}
        alors $P(n)$ est vraie $\forall n \in \N$.
      \item Variantes classiques
        \begin{enumerate}
          \item Remplacer 0 par $n_0$. La conclusion devient vraie pour $n ≥
            n_0$.
          \item Récurrence forte : la deuxième condition devient $\forall p
            ≤n, P(p)\ \text{vraie} \implies \ P(n+1) \text{vraie}$.
        \end{enumerate}
    \end{itemize}
\end{itemize}

\subsubsection{Division euclidienne dans $\N$}

\begin{theoreme}
  $\forall (a,b) \in \N×\N^*,\ \exists ! (q,r) \in \N^2 \mid a = bq + r$
  avec $0 ≤ r < b$. $q$ et $r$ sont respectivement le quotient et le reste
  de la division euclidienne de $a$ par $b$.
\end{theoreme}

\begin{definition}
  Si $r = 0$, on dit que «$b$ divise $a$».
\end{definition}

\begin{proposition}
  La relation «divise» est une relation d'ordre partiel dans $\N^*$.
\end{proposition}

\subsection{Ensembles finis, dénombrables}

\subsubsection{Équipotence}

\begin{definition}
  Soit $E$ et $F$ deux ensembles. On dit que «$E$ est \emph{équipotent} à
  $F$» si et seulement s'il existe $f \colon E \to F$ bijective.

  On note $E \sim F$.
\end{definition}

\begin{theoreme}
  La relation ainsi définie est une relation d'équivalence.
\end{theoreme}
\begin{proof}
  Soient $E,\ F,\ G$ trois ensembles.
  \begin{itemize}
    \item $E \sim E$ : en effet l'identité convient ;
    \item si $E \sim F$, alors la bijection $f^{-1}$ envoie les éléments de
      $F$ sur $E$ et donc $F \sim E$ ;
    \item si $E \sim F$ et $F \sim G$, alors il existe $f \colon E \to F$ et
      $g \colon F \to G$ bijectives et donc $g \circ f$ étant bijective de
      $E \to G$, on a $E \sim G$
  \end{itemize}
\end{proof}

\begin{theoreme}
  Soit $(E_i)_{i \in I}$ et $(F_i)_{i\in I}$ deux familles d'ensembles. Si
  $\forall i \in, E_i \sim F_i$ alors $\prod_{i\in I} E_i \sim \prod_{i\in
  I} F_j$.
\end{theoreme}
\begin{proof}
  $\forall i \in I,\ E_i \sim F_i$ donc $\forall i \in I,\ \exists f_i
  \colon E_i \to F_i$ bijective. On va construire $f \colon \prod_{i\in I}
  E_i \to \prod_{i\in I} F_i$ à partir de ces $f_i.$
  Soit $(x,y) \in \brk*{\prod_{i\in I} E_i}^2\ x = (x_i)_{i \in I}$ et $y =
  (y_i)_{i \in I}$.
  \begin{align*}
    f(x) = f(y) & \implies \forall i \in I,\ f_i(x_i) = f_i(y_i) \\
                & \implies \forall i \in I,\ x_i = y_i           \\
                & \implies x = y                                 \\
  \end{align*}
  Soit $y \in \prod_{i\in I} F_j,\ y = (y_i)_{i \in I}$. $\forall i \in I,\
  \exists x_i \in E_i,\ y_i = f(x_i)$ d'où $\exists x \in \prod_{i\in I}
  E_i,\ y = f(x)$.
\end{proof}

\begin{theoreme}[de Bernstein]
  Soient $E$ et $F$ deux ensembles, $f$ une injection de $E$ dans $F$ et $g$
  une injection de $F$ dans $E$, alors $E \sim F$
\end{theoreme}
\begin{proof}
  On considère $h \colon E \to E$ définie par $h = g \circ f$ et on pose $G
  = E \setminus g(F)$. On appelle $\F$ le sous ensemble de $\mathscr{P}(E)$
  constituées des parties $X$ telles que $G \cup h(X) \subset X$.

  $\F$ est non vide car il contient $G$. Soient $X$ et $Y$ deux éléments de
  $\F$, c'est-à-dire que $G \cup h(X) \subset X$ et $G \cup h(Y) \subset Y$.
  On a alors $G \cup h(X \cap Y) \subset X \cap Y$.\\
  Montrons que si $X \in \F,\ G \cup h(X) \in \F$.

  On pose $A = \bigcap_{X \in \F}$ et $B = E \setminus A$, $A' = f(A)$ et
  $B' = \overset{-1}{g}(B)$. Montrons que $A' \cap B' = ø$ et $A' \cup B' =
  F$.

  Montrons pour finir que $\varphi \colon E \to F$ définie par $\begin{cases}
    \varphi(x) = f(x) & x \in A \\
    \varphi(x) = g(x) & x \in B \\
  \end{cases}$ est une bijection de $E$ sur $F$.
\end{proof}

\begin{theoreme}
  Pour tout ensemble $E$, $E$ et $\mathscr{P}(E)$ ne sont pas équiotents.\\
  Autrement dit, il n'existe pas de bijection de $E$ dans $\mathscr{P}(E)$.
\end{theoreme}
\begin{proof}
  Si $E = ø$, alors $\card E = 0$ et $\mathscr{P}(E) = \set{ø}$ et donc son
  cardinal est 1.

  Si $E ≠ ø$, on suppose qu'il existe $f \colon E \to \mathscr{P}(E)$
  surjective. Soit $A = \setcond{x \in E}{x \not\in f(x)}$. $A \in
  \mathscr{P}(E)$ et il existe $a \in E,\ A = f(a)$.\\
  Si $a \in A, a \not\in f(a)$ ce qui est impossible.\\
  Si $a \not\in A,\ a \in f(a) = A$, ce qui est impossible.
\end{proof}

\subsubsection{Sous ensemble de $\N$}

\paragraph{} Soit $(p,q) \in \N^2, p ≤ q$ on note $\llbracket p, q
\rrbracket = \setcond{x \in \N}{p ≤ x ≤ q}$ et $\rrbracket p, q
\rrbracket = \setcond{x \in \N}{p < x ≤ q}.$\\
Si $p = q,\ \rrbracket p, q \rrbracket = ø$\\
Si $p < q,\ \rrbracket p, q \rrbracket = \llbracket p+1, q \rrbracket$.

\paragraph{Propriétés}

\begin{theoreme}
  Soit $(m,n) \in \N^2$,
  \begin{enumerate}[label=(\roman*),labelsep=.9pt]
    \item s'il existe une injection de $\llbracket 1, n\rrbracket$ dans
      $\llbracket 1, m\rrbracket$, alors $n ≤ m$ ;
    \item s'il existe une bijection de $\llbracket 1, n\rrbracket$ dans
      $\llbracket 1, m\rrbracket$, alors $n = m$.
  \end{enumerate}
\end{theoreme}
\begin{proof}
  Admis
\end{proof}

\begin{theoremeetdefinition}
  On dit qu'un ensemble $E$ non vide est fini s'il existe $n \in \N$ tel que
  $E$ et $\llbracket1,n\rrbracket$ sont équipotents. $n$ est unique et
  s'appelle cardinal de $E$. On note $\card E$.
\end{theoremeetdefinition}
\begin{proof}
  Le point à démontrer ici est l'unicité de $n$. Celle-ci découle du
  théorème précédent.
\end{proof}

\begin{theoreme}
  Tout sous-ensemble de $\llbracket 0,n\rrbracket$ est fini.
\end{theoreme}
\begin{proof}
  Se démontre par récurrence sur $n$.
\end{proof}

\begin{theoreme}\label{00revision:thm:fini-ssi-majore}
  Un sous-ensemble de $\N$ est fini si et seulement s'il est majoré.
\end{theoreme}
\begin{proof}
  Soit $A$ un sous-ensemble de $\N$. S'il est majoré, alors il est inclus
  dans un ensemble de la forme $\llbracket 0,n\rrbracket$ et donc il est
  fini d'après le théorème précédent.\\
  S'il est fini, alors il possède un plus grand élément, qui est son
  majorant.
\end{proof}

\begin{definition}
  On appelle ensemble infini un ensemble qui n'est pas fini.
\end{definition}

On déduit du théorème~\ref{00revision:thm:fini-ssi-majore} que :
\begin{itemize}
  \item un sous-ensemble de $\N$ est infini si et seulement s'il n'est pas
    majoré ;
  \item $\N$ est infini.
\end{itemize}

\subsubsection{Propriétés des ensembles finis}

\begin{theoreme}
  Si $E$ et $F$ sont équipotents et $E$ est fini, alors $F$ l'est aussi et
  $\card E = \card F$.
\end{theoreme}
\begin{proof}
  Admis
\end{proof}

\begin{theoreme}
  Si $E$ est fini, alors tout sous-ensemble $F$ de $E$ est fini et $\card F
  ≤ \card E$.
\end{theoreme}
\begin{proof}
  Admis
\end{proof}

\begin{corollaire}
  Toute intersection d'ensembles finis est finie.
\end{corollaire}
\begin{proof}
  C'est un sous ensemble d'ensembles finis.
\end{proof}

\begin{corollaire}
  Si $F \subset E$ et $F$ infini, alors $E$ est infini.
\end{corollaire}
\begin{proof}
  C'est la contraposée du dernier théorème.
\end{proof}

\begin{theoreme}
  Si $A$ et $B$ sont finis, alors $A \cup B$ aussi et $\card A \cup B =
  \card A + \card B - \card A \cap B$.
\end{theoreme}
\begin{proof}
  Admis
\end{proof}

\begin{corollaire}
  Toute réunion finie d'ensembles finis est finie.
\end{corollaire}
\begin{proof}
  On applique la formule du théorème précédent.
\end{proof}

Le corollaire peut se noter, pour $I$ une famille finie et $A_i$ des
ensembles finis, $\bigcup_{i\in I} A_i$ est finie.

\begin{contreexemple}
  Faux pour une réunion quelconque : $\N = \bigcup_{n \in \N} \set{n}$.
\end{contreexemple}

\begin{corollaire}
  Soit $A \subset B$, $B$ fini et $\card A = \card B$. Alors $A = B$.
\end{corollaire}

\begin{contreexemple}
  Faux si on supprime l'hypothèse $B$ fini : $\N \subset \Z$, $\card \N =
  \card \Z$ et pourtant $\N ≠ \Z$.
\end{contreexemple}

\begin{theoreme}
  Soit $E$ et $F$ deux ensembles finis, alors $E × F$ est fini et $\card E×F
  = \card E × \card F$.
\end{theoreme}
\begin{proof}
  Admis
\end{proof}

\begin{theoreme}
  Soient $E$ un ensemble fini et $f \colon E \to F$ une application. Alors
  $f(E)$ est fini et $\card f(E) ≤ \card E$ avec égalité si et seulement si
  $f$ est injective.
\end{theoreme}
\begin{proof}
  Admis
\end{proof}

\begin{theoreme}
  Soient $E$ un ensemble fini et $f \colon E \to F$ une application. Si $f$
  est surjective, alors $F$ est fini et $\card F ≤ \card E$ avec égalité si
  et seulement si $f$ est bijective.
\end{theoreme}
\begin{proof}
  Admis
\end{proof}

\begin{theoreme}
  Soient $E$ et $F$ deux ensembles \emph{finis} de \emph{même cardinal} et
  $f \colon E \to F$ une application. Les trois propriétés sont
  équivalentes :
  \begin{enumerate*}[label=(\roman*)]
    \item $f$ injective ; \item $f$ surjective ; \item $f$ bijective.
  \end{enumerate*}
\end{theoreme}
\begin{proof}
  Si $f$ est surjective, alors $f(E) = F \implies \card f(E) = \card F =
  \card E$ et donc $f$ est injective.\\
  Si $f$ est injective, alors $f(E) = \card E = \card F \implies \card f(E)
  = \card E$ et $f(E) \subset F$ et donc $f$ est bijective, donc surjective.
\end{proof}

\begin{contreexemple}
  Faux pour les ensembles infinis : \begin{itemize}
    \item $f \colon \N \to \N, n \mapsto n^2$ est injective non surjective.
    \item $f \colon \N \to \N, n \mapsto n-1, 0 \mapsto 0$ est surjective
      non injective.
  \end{itemize}
\end{contreexemple}

\subsubsection{Ensembles dénombrables}

\paragraph{Définition}

\begin{definition}
  Un ensemble équipotent à $\N$ est dit dénombrable. Un ensemble est dit au
  plus dénombrable s'il est fini ou dénombrable, c'est à dire équipotent à
  une partie de $\N$.
\end{definition}

Si $E$ est dénombrable, l'existence d'une bijection $f \colon \N \to E$
permet de «numéroter» les éléments de $E$ en les notant $a_n$ pour $f(n)$.

\paragraph{Exemples d'ensembles dénombrables et non dénombrables}

\begin{enumerate}
  \item $2\N$ et $2\N+1$, $\Z$.
  \item $\N×\N$ et plus généralement (récurrence) $\N^p,\ p \in \N^*$. Par
    conséquent, tout produit fini d'ensemble dénombrables est dénombrable.
  \item $\mathscr{P}(\N)$ est infini non dénombrable.
  \item $\R$ est infini (il contient $\N$) non dénombrable (\emph{admis})
\end{enumerate}

\subsubsection{Propriétés des ensembles dénombrables}

\begin{theoreme}
  Soit $E$ un ensemble dénombrable et $A$ une partie de $E$. Alors, $A$ est
  au plus dénombrable.
\end{theoreme}
\begin{proof}
  Admis
\end{proof}

\begin{theoreme}
  Soient $E$ un ensemble dénombrable et $f \colon E \to F$ une application
  surjective. Alors $F$ est au plus dénombrable.
\end{theoreme}
\begin{proof}
  Admis
\end{proof}

\begin{corollaire}
  $\Q$ est dénombrable.
\end{corollaire}
\begin{proof}
  $\Z$ est dénombrable, $\N^*$ aussi, donc $\Z×\N^*$ l'est. On peut trouver
  une surjection de $\Z×\N^* \to \Q$, ce qui achève la démonstration.
\end{proof}

\begin{theoreme}
  Soit $(A_n)_{n\in\N}$ une famille dénombrable d'ensemble dénombrable.
  Alors $A = \bigcup_{n\in\N} A_n$ est dénombrable.
\end{theoreme}
\begin{proof}
  Démonstration par récurrence sur $n$.
\end{proof}

\begin{corollaire}
  Toute réunion \emph{finie} d'ensembles \emph{dénombrables} est
  \emph{dénombrable}.
\end{corollaire}
\begin{proof}
  Une réunion finie est dénombrable.
\end{proof}

\begin{corollaire}
  Toute réunion \emph{dénombrable} d'ensembles \emph{finis} est \emph{au
  plus dénombrable}.
\end{corollaire}

\begin{theoreme}
  Si $P$ est une partie infinie de $\N$, alors il existe une et une seule
  bijection strictement croissante de $\N$ sur $P$.
\end{theoreme}
\begin{proof}
  Admis.
\end{proof}

\begin{theoreme}
  Un ensemble $I$ est au plus dénombrable si et seulement s'il existe une
  suite croissante $(J_n)_{n\in \N}$ de parties finies de $I$ dont la
  réunion vaut $I$.
\end{theoreme}
\begin{proof}
  Si $I = \bigcup_{n\in\N} J_n$, alors $I$ est au plus dénombrable.

  Si $I$ est au plus dénombrable.\\
  Si $I$ est fini, on pose $J_n = I,\ \forall n \in \N$.\\
  Si $I$ est infini, alors il existe $f$ une bijection de $N$ sur $I$. En
  écrivant $N = \bigcup_{n\in\N} \llbracket 0,n\rrbracket$, on peut définir
  $I_n = f\brk*{\llbracket 0,n\rrbracket}$ et on a bien $I_{n} \subset
  I_{n+1}$ pour tout $n \in \N$.\\
  $f(\N) = I = \bigcup_{n\in\N} f\brk*{\llbracket 0,n\rrbracket} =
  \bigcup_{n\in\N} f(I_n)$.
\end{proof}

\section{$\Z,\ \Q,\ \R$}

\subsection{$\Z$ anneau des entiers relatifs}

\subsubsection{Structure}

\begin{itemize}
  \item $(\Z,+,×,≤)$ est un anneau totalement ordonné.\\
    $\N \subset \Z$ et les opérations dans $\Z$ prolongent celles de $\N$.\\
    $\Z$ est intègre ; $±1$ sont les seuls éléments inversibles.
  \item Dans $\Z$, il n'y a ni plus petit élément, ni plus grand élément.\\
    Soit $A \subset \Z,\ A ≠ ø$ \begin{itemize}
      \item $A$ admet un plus grand élément $\iff A$ majorée.
      \item $A$ admet un plus petit élément $\iff A$ minorée.
      \item $A$ finie $\iff A$ bornée.
    \end{itemize}
\end{itemize}

\subsubsection{Division euclidienne dans $\Z$}

\paragraph{Théorème}

\begin{theoreme}
  $\forall (a,b) \in \Z×\Z^*,\ \exists ! (q,r) \in \Z^2,\ a = b q + r,\ 0 ≤ r
  < \abs{b}$.
\end{theoreme}
\begin{remarque}
  \leavevmode
  \begin{enumerate}
    \item Il se déduit immédiatement de la division euclidienne dans $\N$.
    \item Si on impose seulement $0 ≤ \abs{r} ≤ \abs{b}$, il n'y a pas
      toujours unicité.
  \end{enumerate}
\end{remarque}

\paragraph{Sous-groupes de $\Z$}

\textbf{Très important}

\begin{theoreme}
  $\forall a \in \Z,\ a\Z$ est un sous-groupe de $\Z$.
\end{theoreme}
\begin{proof}
  \begin{itemize}
    \item $0 \in a\Z$, donc $a\Z ≠ ø$.
    \item Soient $(x,y) \in (a\Z)^2,\ \exists (p,q) \in \Z^2,\ x=ap,\ y
      =aq$.\\
      $x - y = a(p - q)$ or, $p - q \in \Z$, d'où $x - y \in a \Z$.
  \end{itemize}
\end{proof}
\begin{theoreme}
  Soit $H$ un sous-groupe de $\Z$ et $a \in H$. Alors $a\Z \subset H$.
\end{theoreme}
\begin{proof}
  Soit $H$ un sous-groupe de $\Z,\ a \in H$. Montrons que $a\N \subset H$.\\
  $0 = 0 × a \in H$.\\
  Soit $n$ tel que $an \in H$. Alors $a(n+1) = an + a \in H$.\\
  $\forall n \in \N,\ an \in H$.\\
  Pour $n \in \Z\-^*,\ -n \in \N$ et donc $-an \in H$ et donc $an \in H$.
  Finalement $a\Z \in H$.
\end{proof}
\begin{theoreme}
  Soit $H$ un sous-groupe de $\Z$ alors $\exists a \in H \mid H = a\Z$.
\end{theoreme}
\begin{proof}
  Si $H = \set{0}$, alors $H = 0\Z$.\\
  Sinon, si $H ≠ \set{0},\ \exists x \in H, x ≠ 0$.\\
  $x$ ou $-x \in \N$ et donc on peut construire $A = H \cap \N^*$, qui est
  une partie non vide de $\N$ et donc qui, à ce titre, admet un plus petit
  élément $a$.\\
  Or $a \in H$, donc $a\Z \subset H$. De plus, pour $x \in H,\ \exists !
  (p,r) \in \Z^2,\ x = ap +r,\ 0≤ r < a$. Comme $ap \in H$ et $x \in H$, on
  en déduit que $r \in H$
\end{proof}

\paragraph{Idéaux de $\Z$}

\begin{definition}
  Soit $A$ un anneau commutatif. On dit que $\I$ est un idéal de $A$ si et
  seulement si
  \begin{enumerate}
    \item $\I$ est un sous-groupe de $A$ ;
    \item $\forall a \in A,\ \forall x \in \I, ax \in \I$.
  \end{enumerate}
\end{definition}

\begin{theoreme}
  $\I \subset \Z$ est un idéal de $\Z \iff \I$ est un sous-groupe de $\Z
  \iff \exists a \in \I \mid \I = a\Z$.
\end{theoreme}

\begin{remarque} Tous les idéaux de $\Z$ étant de la forme $aA$ (on dit
  \emph{principaux}), $\Z$ est dit \emph{anneau principal}.
\end{remarque}

\subsection{$\Q$ : corps des nombres rationnels}

\begin{itemize}
  \item $(\Q,+,×,≤)$ est un corps totalement ordonné archimédien.\\
    $\N \subset \Z \subset \Q$ et les opérations dans $\Q$ prolongent celles
    de $\Z$.\\
  \item $\Q$ présent deux graves lacunes :
    \begin{enumerate}
      \item il existe des sous-ensembles de $\Q$ non vide majorés
        n'admettant pas de borne supérieure.
      \item Soit $a \in \Q_+$, l'équation $x^2 = a$ n'a pas toujours de
        solution dans $\Q$.
    \end{enumerate}
\end{itemize}

\subsection{$R$ : le corps des réels}

\subsubsection{Définition}

\begin{definition}
  $(E,≤)$ vérifie la propriété de la bore supérieure si et seulement si
  $\forall A \subset E$, $A$ non vide et majorée, $A$ admet une borne
  supérieure.
\end{definition}

\begin{definition}
  Il existe un corps $(\R,+,×,≤)$ totalement ordonné archimédien et
  vérifiant la propriété de la borne supérieure. Il est unique à un
  isomorphisme de corps strictement croissant.
\end{definition}

\begin{proposition}
  $\R$ vérifie la propriété de la borne supérieure.
\end{proposition}
\begin{proof}
  Admis.
\end{proof}

\subsubsection{Propriétés}

\paragraph{Structure de $\R$}

\begin{proposition}
  $\R$ est archimédien, c'est-à-dire $\forall x \in \R,\ \forall a \in
  \R_+^*,\ \exists n \in \N \mid na ≥ x$.
\end{proposition}
\begin{proof}
  Par l'absurde, supposons que $\forall n \in \N, na ≤ x$. Construisons $A =
  \set{na,\ n \in \N}$. $A$ est un sous-ensemble de $\R$ non vide et majoré
  par $x$. Il admet donc une borne supérieure $M$.\\
  $\forall n \in \N,\ na ≤ M \implies \forall n \in \N^*,\ (n-1)a ≤ M - a$
  et donc $M -a$ est encore un majorant de $A$, ce qui contredit la
  définition de $M$.
\end{proof}

\begin{proposition}
  $\R$ n'est pas dénombrable.
\end{proposition}
\begin{proof}
  Admis.
\end{proof}

\begin{definition}
  Soit $E$ un ensemble et $A \subset E$ une partie de $E$. On dit que $A$
  est dense dans $E$ quand …
\end{definition}

\begin{proposition}
  $\Q$ et $\R\setminus \Q$ sont denses dans $\R$.
\end{proposition}
\begin{proof}
  %TODO
\end{proof}

\paragraph{Ensemble $\overline{\R}$ (droite achevée)}

\begin{definition}
  $\overline{\R} = \R \cup \set{+ \infty, -\infty}$ et $\forall x \in \R,\ -
  \infty < x < +\infty$.
\end{definition}

On perd la structure de corps de $\R$ ($0 × +\infty$ et $\infty - \infty$ ne
sont pas définis.) Mais toute partie non vide de $\overline{\R}$ admet une
borne supérieure et une borne inférieure.

\subsubsection{Suites de réels}

\begin{proposition}
  $\forall n \in \N$, on pose $I_n = \intv{a_n}{b_n}$ avec $I_{n+1} \subset
  I_n$. On suppose de plus que $\lim_{n \infty} b_n - a_n = 0$. Alors
  $\bigcup_{n \in \N} I_n = \set {\ell}$ avec de plus $\ell = \lim_{n
  \infty} a_n = \lim_{n \infty} b_n$.
\end{proposition}
\begin{proof}
  Il nécessite le théorème~\ref{thm:suites_adjacentes} : $(a_n)$ et $'b_n)$
  sont adjacentes, donc convergentes et de limite $\ell$. On a donc $\forall
  n \in \N,\ a_n ≤ \ell ≤ b_n$, donc $\ell \in \bigcup_{n \in \N} I_n$.\\
  Soit $\ell' \in \bigcup_{n \in \N} I_n\ \abs{\ell' - \ell} ≤ b_n - a_n
  \forall n \in \N \implies \ell = \ell'$.
\end{proof}

\begin{proposition}
  $\R$ est complet, c'est-à-dire que toute suite de Cauchy de réels est
  convergente.
\end{proposition}
\begin{proof}
  Admis
\end{proof}

\begin{theoreme}[Bolzano-Weierstrass]
  De toute suite bornée de réels, on peut extraire une suite convergente.
\end{theoreme}
\begin{proof}
  Admis
\end{proof}

\subsubsection{Sous-groupes additifs de $\R$}

\begin{theoreme}
  Les sous-groupes additifs de $\R$ sont de deux formes :
  \begin{enumerate}
    \item les sous-groupes discrets de la forme $a\Z,\ a \in \R$ ;
    \item les sous-groupes partout denses.
  \end{enumerate}
\end{theoreme}
\begin{proof}
  Soit $G$ un sous-groupe additif de $\R$, $G ≠ \set{0}$ (sinon $G =
  0\Z$.)\\
  $G \cap \R_+^* ≠ ø$. En effet, pour $x \in G \setminus \set{0}$, $x$ ou
  $-x \in G \cap \R_+^*$. On peut donc considérer $a = \inf{G \cap
  \R_+^*}$.\\
  \begin{itemize}
    \item 1\ier{} cas : $a > 0$ \\
      $a < 2a$ donc $\exists x \in G \cap \R_+^* \mid a ≤ x < 2a$. Si $x ≠
      a\ \exists y \in G \cap \R_+^* \mid a < x < y < 2a$ d'où $0 < x - y <
      a$, avec $x - y \in \R_+^*$ ce qui contredit $a = \inf{G \cap \R_+^*}$
      donc finalement $x = a$ et $a \in G$.\\
      On démontre $a\Z \subset G$.\\
      Soit $z \in G$. $n = \left\lfloor \frac{z}a \right\rfloor \implies na
      ≤ z < (n+1)a \implies 0 ≤ z - an < a$ avec $z - an \in G \cap \R_+^*$
      donc $z - an = 0$ et donc $z = an$. D'où l'inclusion $G \in a\Z$.
    \item 2\ieme{} cas : $a = 0$ ($\inf{G \cap \R_+^*} = 0$) \\
      Soit $(x,y) \in \R^2$ avec $x < y$. $y - x > 0 \implies \exists z \in
      G \cap \R_+^*\ 0 < z < y - x$. Soit $n = \left\lfloor \frac{x}z
      \right\rfloor\ nz ≤ x < (n+1)z = nz + z <  x + y - x = y$. Donc
      $(n+1)z \in G \cap \intv[o]{x}{y}$.
  \end{itemize}
\end{proof}

\section{$\C$ : corps des complexes}

\subsection{Propriétés algébriques}

\subsubsection{Définition}

\begin{definition}
  $\C = \setcond{a + ib}{(a,b) \in \R^2,\ i^2 = -1}$ est un corps
  commutatif.\\
  $z = a + ib,\ (a,b) \in \R^2,\ a = \Re(z) \ b = \Im(z)$.
\end{definition}

\subsubsection{Propriétés}

On ne peut munir $\C$ d'une structure de corps ordonné, car tout nombre est
un carré (cf \ref{B4} et Annexe.)

\begin{definition}
  $\conj{z} = a - ib$ est le nombre complexe conjugué.
\end{definition}

\begin{proposition}
  $z \in \R \iff z = \conj{z}$ et $z \in i\C \iff \conj{z} = -z$.
\end{proposition}
\begin{proof}
  Aisée.
\end{proof}

\begin{proposition}
  Soit $z \in \C, z\conj{z} ≥ 0$.
\end{proposition}
\begin{proof}
  Soit $z \in \C$. $z$ s'écrit $z = a + ib, (a,b) \in \R^2$.\\
  $z \conj{z} = (a + ib)(a - ib) = a^2 + b^2 ≥ 0$.
\end{proof}

\begin{definition}
  $\abs{z} = \sqrt{z\conj{z}}$ est le module du nombre complexe $z$.
\end{definition}

\begin{definition}
  On appelle formes polaires (trigonométrique ou exponentielle) les formes,
  pour tout nombre $z$ complexe non nul $z = \abs{z}e^{i\theta}$ et $z =
  \abs{z}(\cos \theta + i\sin \theta)$ où $\theta$ est un nombre réel.
\end{definition}

\subsection{Applications des nombres complexes à l'algèbre}

\subsubsection{Racines $n$-ièmes}

\begin{definition}
  On appelle racine $n$-ième du nombre complexe $A$ toute solution de
  l'équation $z^n = A$.
\end{definition}

Pour $A ≠ 0$, posons $A = re^{i\alpha}$ et $z = \rho e^{i\theta}$.\\
$z^n = A \iff z = z_k = \sqrt[n]{r} e^{i\frac{\alpha + 2k\pi}n},\ k \in
\Z$.\\
On constate ainsi qu'il existe $n$ racines $n$-ièmes distinctes. On les
obtient en faisant varier $k$ dans $\llbracket0,n-1\rrbracket$ ou
$\llbracket1,n\rrbracket$.\\
Les images des solutions forment les sommets dun polygone régulier de $n$
côtés inscrit dans le cercle $\Gamma$ de centre $O$ et de rayon
$\sqrt[n]{r}$.

\subsubsection{Racines $n$-ièmes de l'unité}

\begin{definition}
  On appelle racine $n$-ième de l'unité toute racine de l'équation $z^n =
  1$.
\end{definition}

Il y a $n$ racines $n$-ièmes de l'unité distinctes $z_k =
e^{\frac{2ik\pi}n},\ k \in \llbracket0,n-1\rrbracket$.\\
Les racines $n$-ièmes de $A$ s'obtiennent en multipliant l'une
d'entres elles par les racines $n$-ièmes de l'unité.\\
L'ensemble des racines $n$-ièmes de l'unité est un groupe cyclique engendré
par $z_1$ ou plus généralement par $z_k, \ k \wedge n = 1$.

\subsubsection{Racines carrées d'un nombre complexe}

\begin{lemme}
  $z = z' \iff \begin{cases} \abs{z} = \abs{z'} \\ \Re z = \Re z' \\
  \Im z × \Im z' ≥ 0 \end{cases}$
\end{lemme}
\begin{proof}
  $\abs{z} = \abs{z'} \implies \sqrt{x^2 + y^2} = \sqrt{x'^2 + y'^2}
  \implies x^2 + y^2 = x'^2 + y'^2$\\
  $\Re z = \Re z' \implies x = x'$\\
  On a donc $y^2 = y'^2 \implies y = ± y'$, mais comme $\Im z × \Im z' ≥ 0$,
  on a $y = y'$.
\end{proof}

Ce lemme est utile pour résoudre $z^2 = A$. On pose $z = x + iy$, $A = a +
ib$ et l'équation devient $\begin{cases} x^2 + y^2 = \sqrt{a^2 + b^2} \\ x^2
- y^2 = a \\ xyb ≥ 0 \end{cases}$.

\subsubsection{Équation du second degré à coefficients dans $\C$}

\begin{definition}
  On considère le polynôme $P = aZ^2 + bZ + c$ dans $\C_2[X]$, avec $a ≠ 0$.
\end{definition}

$P = a\brk[s]*{\brk*{Z + \frac{b}{2a}}^2 + \frac{4ac - b^2}{4a^2}}$ permet
d'affirmer que trouver les racines du polynôme est équivalent à résoudre
l'équation $\brk*{Z + \frac{b}{2a}}^2 = \frac{b^2 - 4ac}{4a^2}$. Posons
$\Delta = b^2 - 4ac$ et comme on est dans $\C$, on pose $\delta$ tel que
$\delta^2 = \Delta$. Ainsi, on est ramené à l'équation $Z + \frac{b}{2a} =
\frac{\varepsilon \delta}{2a}$ avec $\varepsilon \in \set{-1,1}$.\\
On distingue donc deux cas : $\Delta = 0$ qui entraîne que la racine est
double et est $\frac{-b}{2a}$. et le cas $\Delta ≠ 0$ où les racines sont
$\frac{-b - \delta}{2a}$ et $\frac{-b + \delta}{2a}$.

\begin{remarque}
  Les relations sur la somme et le produit des racines restent vraies.
\end{remarque}

\subsection{Application des nombres complexes à la trigonométrie}

\subsubsection{Linéarisation de $(\sin \theta)^n$ et $(\cos \theta)^n$}

L'utilisation des formules d'Euler permet de linéariser $(\sin \theta)^n$ et
$(\cos \theta)^n$, c'est à dire de les écrire sous la forme d'une
combinaison linéaire de sinus et cosinus.\\
Pour établir la formule générale, il convient de distinguer la parité de
$n$.

\subsubsection{Multiplication des arcs}

La formule de Moivre (qui n'est autre chose que $\brk*{e^{i\theta}}^m =
e^{im\theta}$) permet d'écrire $\cos m\theta$ et $\sin m\theta$ sous la
forme d'un polynôme des deux variables $\cos \theta$ et $\sin \theta$. Ce
polynôme s'obtient en écrivant $\cos m\theta + i\sin m\theta = \brk{\cos
\theta + i\sin \theta}^m$ et en développant suivant la formule du binôme.
