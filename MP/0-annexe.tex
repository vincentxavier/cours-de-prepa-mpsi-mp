\section{Anneaux totalement ordonnés}

\subsection{Définition d'un anneau totalement ordonné}

\begin{definition}
  Soit $(A,+,×)$ un anneau commutatif et $≤$ une relation d'ordre sur $A$.\\
  $(A,+,×,≤)$ est un anneau totalement ordonné si et seulement si
  \begin{enumerate}
    \item $≤$ est une relation d'ordre total
    \item $≤$ est compatible avec $+$ ($\forall (a,b,c) \in A^3,\ a ≤ b
      \implies a + c ≤ b + c$)
    \item $\forall (a,b) \in A^2,\ a≥ 0$ et $b≥ 0 \implies ab ≥ 0$
  \end{enumerate}
\end{definition}

\subsection{Éléments positifs, négatifs}

\begin{definition}
  On appelle \emph{élément positif} tout élement supérieur ou égal à 0 et
  \emph{élement négatif} tout élément inférieur ou égal à 0.\\
  On note $A_+ = \setcond{x \in A}{0 ≤ x}$ et $A_- = \setcond{x \in A}{x ≤
  0}$.
\end{definition}

\begin{proposition}
  On a $A = A_+ \cup A_-$ et $A_+ \cap A_- = \set{0_A}$.
\end{proposition}

\begin{remarque}
  $x \in A_+ \iff -x \in A_-$.
\end{remarque}

\subsubsection{Règles de calcul et propriétés}

$\forall (a,b,c,d) \in A^4,\ a ≤ b$ et $c ≤ d \implies a + c ≤ b + d$. En
particulier, $A_+ + A_+ \subset A_+$ et $A_- + A_- \subset A_-$.\\
Par récurrence, on obtient que $\forall n \in \N,\ a ≤ b \implies na ≤
nb$.\\
On montre également que si $x > 0$, alors $nx > 0$. On en déduit alors que
si $m ≠ n$, alors $mx ≠ nx$ et donc que tout anneau totalement ordonné est
infini.

L'axiome 3 des anneaux totalement ordonnés permet d'établir la règle des
signes. $\forall (x,y) \in A$
\begin{itemize}
  \item $x ≥ 0,\ y ≥ 0 \implies xy ≥ 0$
  \item $x ≤ 0,\ y ≥ 0 \implies xy ≤ 0$ ($-x ≥ 0 \implies -xy ≥ 0 \implies
    xy ≤ 0$)
  \item $x ≤ 0,\ y ≤ 0 \implies xy ≥ 0$
\end{itemize}
En particulier, $\forall x \in A,\ x^2 ≥ 0$.

\begin{itemize}
  \item $a ≤ b$ et $0 ≤ c \implies ac ≤ bc$
  \item $a ≤ b$ et $c ≤ 0 \implies bc ≤ ac$
\end{itemize}

$\C$ ne peut-être muni d'une structure de corps totalement ordonné. En
effet, $1_{\C}^2 = 1_{\C} > 0 \implies -1_{\C} < 0$ et l'existence de $i^2 =
-1 < 0$ contredit le fait que tous les carrés sont positifs.

\subsubsection{Valeur absolue dans un anneau totalement ordonné}

\begin{definition}
  Soit $x \in A$. On pose
  \begin{itemize}
    \item $\abs{x} = x $ si $x \in A_+$
    \item $\abs{x} = -x$ si $x \in A_-$.
  \end{itemize}
\end{definition}

\begin{proposition}
  \leavevmode
  \begin{itemize}
    \item $\forall x \in A,\ \abs{x} \in A_+$ et $\abs{x} = \abs{-x}$.
    \item $\forall (x,y) \in A^2,\ \abs{xy} = \abs{x}\abs{y}$ et $\abs{x+y}
      ≤ \abs{x} + \abs{y}$.
    \item $\forall (x,y) \in A^2,\ \abs{\abs{x} - \abs{y}} ≤ \abs{x - y}$.
  \end{itemize}
\end{proposition}
\begin{proof}
  $x$ et $y$ de même signe, il y a égalité. Supposons $x ≥ 0$ et $y ≤ 0$.\\
  $x + y = x - \abs{y} = \abs{x} - \abs{y}$. On a donc $\abs{x + y} =
  \abs{x} - \abs{y}$ si $\abs{x} ≥ \abs{y}$ et $\abs{x + y} = \abs{y} -
  \abs{x}$ si $\abs{x} ≤ \abs{y}$, mais dans les deux cas, $\abs{x + y} =
  \abs{x} + \abs{y}$.
\end{proof}

\subsubsection{Anneau archimédien}

\begin{definition}
  Un anneau totalement ordonné est dit archimédient si $\forall a > 0,\ 
  \forall b ≥ 0,\ \exists n \in \N \mid na > b$.
\end{definition}

\begin{exemple}
  $\Z$ et $\Q$ sont archimédiens.
\end{exemple}

\begin{question}
  Soit $P \subset A$ ($(A,+,×)$ un anneau commutatif) vérifiant $P + P
  \subset P,\ P×P \subset P,\ P\cap(-P) = \set{0_A}$ et $P \cup (-P) = A$.
  On pose $x \R y \iff y - x \in P$. Montrer que $\R$ est une relation
  d'ordre total qui munit $A$ d'une structure d'anneau totalement ordonné et
  que $P = A_+$.
\end{question}

\section{Borne supérieure}

\subsection{Définition}

\begin{definition}
  Soient $(E,≤)$ un ensemble ordonné et $A \subset E$ une partie de $E$. On
  dit que $M$ est la borne supérieure de $A$ si et seulement si $M$ est le
  plus petit des majorants.
\end{definition}

\begin{remarque}
  Seuls les ensembles majorés peuvent avoir une borne supérieure. Sous
  réserve d'existence, celle-ci est unique.
\end{remarque}

\subsection{Caractérisation}

\begin{proposition}
  Soit $(E,≤)$ un ensemble ordonné. $M = \sup A \iff \begin{cases} \forall x
  \in A,\ x ≤ M \\ \forall M' \ \text{majorant}\ M ≤ M' \end{cases}$
\end{proposition}
\begin{proof}
  C'est la reformulation de la définition.
\end{proof}

\begin{proposition}
  Soit $(K,+,×,≤)$ un corps totalement ordonné. $M = \sup A \iff
  \begin{cases} \forall x \in A,\ x ≤ M \\ \forall \varepsilon > 0,\ 
  \exists x \in A,\ M - \varepsilon < x ≤ M \end{cases}$.
\end{proposition}
\begin{proof}
  %TODO: rédiger la preuve
\end{proof}
