\section{Généralités sur les suites}

\subsection{Généralités}

\begin{definition}
  Soit $E$ un ensemble. On appelle suite d'éléments de $E$ toute application
  de $\N$ dans $E$. On note $\begin{array}{ll}u \colon & \N \to E \\ & n \to
  u_n \end{array}$. $u_n$ est aussi bien le terme général de la suite que le
  terme de rang $n$.\\
  On note $E^{\N}$ l'ensemble des suites d'éléments de $E$.
\end{definition}

\begin{contreexemple}[Ne pas confondre]
  $u = (u_n)_{n\in \N}$ la suite et $u(\N)$, l'ensemble des valeurs prises
  par les termes de la suite.
\end{contreexemple}

Les deux définitions suivantes sont valables dans $E$ un ensemble totalement
ordonné.

\begin{definition}
  Soit $(u_n)_{n\in\N}$ une suite. On dit que $u$ est \emph{croissante}
  (resp. strictement croissante) si et seulement si $\forall (k,k') \in
  \N^2,\ k ≤ k' \implies u_k ≤ u_{k'}$ (resp. $\forall (k,k') \in \N^2,\ k <
  k' \implies u_k < u_{k'}$.)
\end{definition}

On définit de la même façon les suites décroissantes. Une suite croissante
ou décroissante est dite \emph{monotone}.

\begin{remarque}
  Si $E = \R$, pour étudier la monotonie, il suffit d'étudier le signe de
  $u_{n+1} - u_n$.
\end{remarque}

\begin{definition}
  Soit $(u_n)_{n\in\N}$ une suite. On dit que $u$ est
  \begin{itemize}
    \item majorée $\iff \exists M \in E \mid \forall k \in \N,\ u_k ≤ M$
    \item minorée $\iff \exists m \in E \mid \forall k \in \N,\ m ≤ u_k$
    \item bornée $\iff \exists (m,M) \in E^2 \mid \forall k \in \N,\ m ≤ u_k
      ≤ M$
  \end{itemize}
\end{definition}

\begin{theoreme}
  Soit $\varphi \colon \N \to \N$ \emph{strictement croissane}. Alors
  $\forall n \in \N, \varphi(n) ≥ n$
\end{theoreme}
\begin{proof}
  Par récurrence sur $n$.\\
  Pour $n = 0$, on a $\varphi(0) \in \N$ et donc $\varphi(0) ≥ 0$. La
  proposition est donc vraie au rang 0.\\
  Soit $n$ un entier naturel, supposons que $\varphi(n) ≥ n$.\\
  $n < n+1 \implies \varphi(n) < \varphi(n+1)$. Comme $\varphi(n) ≥ n$, on
  obtient que $\varphi(n+1) > n$ et donc $\varphi(n+1) ≥ n + 1$, ce qui
  achève la démonstration.
\end{proof}

\begin{definition}
  Soit $u$ une suite d'éléments de $E$ et $\varphi \colon \N \to \N$
  strictement croissante. $u \circ \varphi$ est dite \emph{suite extraite de
  $u$}.
\end{definition}

\subsection{Opérations sur les suites}

On se place dans $(E,+,⋅)$ un $\K$ espace vectoriel.

\begin{definition}
  \leavevmode
  \begin{itemize}
    \item On appelle somme de deux suites $(x_n)_{n\in\N}$ et
      $(y_n)_{n\in\N}$ la suite de terme général $x_n + y_n$.
    \item On aeppel produit de la suite $(x_n)_{n\in\N}$ par le scalaire
      $\lambda \in \K$ la suite de terme général $\lambda x_n$.
  \end{itemize}
\end{definition}

\begin{proposition}
  $E^{\N}$ muni de ces deux lois a une structure de $\K$ espace vectoriel.
\end{proposition}
\begin{proof}
  Aisée.
\end{proof}

On se place dans le cas où $(E,+,×)$ est un anneau commutatif.

\begin{definition}
  On appelle produit des deux suites $(x_n)_{n\in\N}$ et $(y_n)_{n\in\N}$ la
  suite de terme général $x_n × y_n$.
\end{definition}

\begin{proposition}
  $E^{\N}$ muni de l'addition et du produit a une structure d'anneau.
\end{proposition}
\begin{proof}
  Aisée.
\end{proof}

\begin{contreexemple}[Intégrité de $E^{\N}$]
  $E^{\N}$ n'est jamais intègre, que $E$ le soit ou non. Il suffit de
  considérer les suites $x$ et $y$ terme général $x_n = 1 + (-1)^n$ et $y_n
  = 1 - (-1)^n$ qui sont non nulles alors que leur produit est nul.
\end{contreexemple}

\section{Suites de réels}

\subsection{Convergence d'une suite}

\subsubsection{Définitions}

\begin{definition}
  Soient $(u_n)_{n\in\N} \in \R^{\N}$ et $\ell \in \R$. On dit que $\ell$
  est \emph{limite} de la suite $(u_n)_{n\in\N}$ si et seulement si $\forall
  \varepsilon \in \R_+^*,\ \exists N \in \N,\ \forall n \in \N,\ n ≥ N
  \implies \abs{u_n - \ell} < \varepsilon$.\\
  Si $\ell$ existe dans $\R$, on dit alors que $(u_n)_{n\in\N}$ est
  convergente.
\end{definition}

\begin{exemple}
  On peut écrire que $(u_n)_{n\in\N}$ ne converge pas vers $\ell$ ou que $u$
  est divergente.
\end{exemple}

\begin{definition}
  Soit $(u_n)_{n\in\N} \in \R^{\N}$. On dit que la suite $(u_n)_{n\in\N}$
  tend vers
  \begin{enumerate}
    \item $+\infty$ si et seulement si $\forall A \in \R,\ \exists N\in \N,\
      \forall n \in \N,\ n ≥ N \implies u_n ≥ A$.
    \item $-\infty$ si et seulement si $\forall A \in \R,\ \exists N\in \N,\
      \forall n \in \N,\ n ≥ N \implies u_n ≤ A$.
  \end{enumerate}
\end{definition}

\subsubsection{Définition de la convergence à l'aide des voisinages}

\begin{definition}
  Soit  $V \subset \overline{\R}$. On dit que $V$ est un voisinage de
  \begin{itemize}
    \item $\ell \in \overline{\R}$ si et seulement si $\exists k > 0,\
      \intv[o]{\ell - k}{\ell + k} \subset V$
    \item $+\infty \in \overline{\R}$ si et seulement si $\exists a \in \R,\
      \intv[o]{a}{+\infty} \subset V$
    \item $-\infty \in \overline{\R}$ si et seulement si $\exists a \in \R,\
      \intv[o]{-\infty}{a} \subset V$
  \end{itemize}
  Pour $\ell \in \overline{\R}$, on note $\V(\ell)$ l'ensemble des
  voisinages de $\ell$.
\end{definition}

\begin{proposition}
  $\forall (V_1,V_2) \in \V(\ell)^2,\ V_1 \cap V_2 \in \V(\ell)$
\end{proposition}
\begin{proof}
  Démonstration aisée.
\end{proof}

\begin{proposition}
  $\forall V \in \V(\ell),\ \forall W \mid V \subset W$, alors $W \in
  \V(\ell)$.
\end{proposition}
\begin{proof}
  Démonstration aisée.
\end{proof}

\begin{definition}
  Soit $(u_n)_{n\in\N} \in \R^{\N}$. On dit que $\ell \in \overline{\R}$ est
  une limite de la suite $\ell$ si et seulement si $\forall V \in \V(\ell),\
  \exists N \in \N,\ \forall n \in \N, n ≥ N \implies u_n \in V$
  où $\V(\ell)$ désigne les voisinages de $\ell$.
\end{definition}

\begin{theoreme}
  $\ell$ est unique ($\overline{\R}$ est séparé.)
\end{theoreme}
\begin{proof}
  $\forall (a,b) \in \overline{\R}^2,\ a≠b,\ \exists U \in \V(a),\ \exists V
  \in \V(b) \mid U \cap V = ø$. On démontre ensuite l'unicité de la limite
  par l'absurde : à partir d'un certain rang, les termes de $u$ devraient
  appartenir à deux voisinages d'intersection vide, ce qui est absurde.
\end{proof}

\subsection{Propriétés des suites convergentes dans $\R$ ou dans
$\overline{\R}$}

\textbf{On fera bien attention aux hypothèses de convergence dans $\R$ ou
dans $\overline{\R}$.}

\begin{definition}
  Soit $(u_n)_{n\in\N}$ une suite. On dit que $u$ est une \emph{suite de
  Cauchy} lorsque $\forall \varepsilon > 0,\ \exists N \in \N,\ \forall
  (n,p) \in \N^2,\ n ≥ N$ et $p ≥ N \implies \abs{u_n - u_p} < \varepsilon$.
\end{definition}

\begin{proposition}
  On peut également prendre $\forall \varepsilon > 0,\ \exists N \in \N,\
  \forall (n,p) \in \N^2,\ n ≥ N \implies \abs{u_{n+p} - u_n} < \varepsilon$
  comme définition.
\end{proposition}
\begin{proof}
  On pose $p' = n + p$ dans la première définition.
\end{proof}

\begin{theoreme}
  Les deux propositions suivantes sont équivalentes.
  \begin{enumerate}
    \item $(u_n)_{n\in\N}$ est une suite de Cauchy
    \item $(u_n)_{n\in\N}$ est une suite  convergente \emph{dans $\R$}.
  \end{enumerate}
\end{theoreme}
\begin{proof}
  Soit $\varepsilon > 0$\\
  $u_{n+p} - u_n = u_{n+p} - \ell + \ell - u_n$\\
  $\abs{u_{n+p} - u_n} ≤ \abs{u_{n+p} - \ell} + \abs{\ell - u_n}$\\
  Or $\exists N \in \N,\ \forall n \in \N, n ≥ N \implies \abs{u_n - \ell} ≤
  \frac{\varepsilon}2$ et donc \textit{a fortiori}, $\abs{u_{n+p} - \ell} ≤
  \frac{\varepsilon}2$.\\
  On en déduit que $\forall (n,p) \in \N^2, n ≥ N \implies \abs{u_{n+p} -
  u_n} ≤ \varepsilon$.
\end{proof}

\begin{theoreme}
  Toute suite convergente \emph{dans $\R$} est bornée.
\end{theoreme}
\begin{proof}
  Aisée.
\end{proof}
\begin{contreexemple}[La réciproque est fausse]
  $u_n = (-1)^n$ est divergente et bornée.
\end{contreexemple}

\begin{theoreme}
  De toute suite bornée, on peut extraire une suite convergente \emph{dans
  $\R$}.
\end{theoreme}
\begin{proof}
  Bolzano-Weierstrass.
\end{proof}

\begin{theoreme}
  Si la suite $(u_n)_{n\in\N}$ est convergente de limite dans
  $\ell \in \overline{\R}$ alors toute suite extraite est convergente de
  même limite.
\end{theoreme}
\begin{proof}
  Soit $(u_{\varphi(n)})_{n\in\N}$ la suite extraite.
  Soit $V \in \V(\ell),\ \exists N \in \N, \forall n \in \N, n ≥ N \implies
  u_n \in V$.\\
  Comme, $\forall n \in \varphi(n) ≥ n ≥ N$, on a $u_{\varphi} \in V$.
\end{proof}

\begin{remarque}
  Ce théorème est particulièrement utile pour démontrer la divergence d'une
  suite comme celle de terme général $(-1)^n$.
\end{remarque}

\begin{theoreme}
  Si les deux suites extraites $(u_{2n})_{n\in\N}$ et $(u_{2n+1})_{n\in\N}$
  sont convergente dans $\overline{\R}$ de même limite $\ell$, alors la
  suite $(u_n)_{n\in\N}$ est convergente de limite $\ell$.
\end{theoreme}
\begin{proof}
  On suppose que $(u_{2n})$ converge vers $\ell$ et $(u_{2n+1})$ converge
  vers $\ell$.\\
  Soit $V \in \V(\ell)$ fixé.\\
  $\exists N_1 \in \N, \forall n \in \N, n ≥ N_1 \implies u_{2n} \in V$ \\
  $\exists N_2 \in \N, \forall n \in \N, n ≥ N_2 \implies u_{2n+1} \in V$ \\
  Posons $N = \max(2N_1,2N_2 + 1)$.
  Si $n = 2p$, alors $n ≥ N \implies p ≥ N_1$ et $u_{2p} \in V \implies u_n
  \in V$.\\
  Si $n = 2p+1$, alors $n ≥ N \implies p ≥ N_2$ et $u_{2p+1} \in V \implies
  u_n \in V$.\\
\end{proof}

\begin{theoreme}
  Si la suite $(u_n)_{n\in\N}$ est convergente dans $\R\setminus\set{0}$
  alors tous les termes sont, à partir d'un certain rang, du signe de la
  limite et de plus $\exists (m,N) \in \R^*_+×\N$ tel que $\forall n \in \N\
  n ≥ N \implies u_n > m > 0$ ou $\forall n \in \N\ n ≥ N \implies u_n < -m
  < 0$.
\end{theoreme}

\begin{theoreme}[Suites monotones]
  Toute suite monotone est convergente dans $\overline{\R}$.
\end{theoreme}
\begin{proof}
  On suppose $(u_n)_{n\in\N}$ croissante par exemple.
  \begin{itemize}
    \item $(u_n)_{n\in\N}$ majorée\\
      $u(\N) = \setcond{u_n}{n\in\N} \subset \R$ est non vide et majorée.
      Elle admet donc une borne supérieure $L = \sup u(\N)$.\\
      $\begin{cases}
        \forall \varepsilon > 0,\ \exists N \in \N,\ L - \varepsilon < u_N ≤
      L \\ \forall n \in \N, u_n ≤ L \end{cases}$\\
      $\forall n \in \N,\ L - \varepsilon < u_N ≤ u_n ≤ L < L + \varepsilon$
      d'où $\forall \varepsilon > 0,\ \exists N \in \N,\ \forall n \in \N, n
      ≥ N \implies \abs{u_n - L} < \varepsilon$.
    \item $(u_n)_{n\in\N}$ non majorée
      $\forall A \in \R,\ \exists N \in \N, u_N ≥ A$ et $\forall n \in \N, n
      ≥ N  \implies u_n ≥ u_N > A$.
  \end{itemize}
\end{proof}

\begin{theoreme}
  Soit deux suites $(u_n)_{n\in\N}$ et $(v_n)_{n\in\N}$ telles que
  \begin{enumerate*}
    \item $(u_n)_{n\in\N}$ croissante
    \item $(v_n)_{n\in\N}$ décroissante
    \item $\lim_{n \infty} u_n - v_n = 0$
  \end{enumerate*}
  Alors ces deux suites sont convergentes de même limite $\ell \in \R$
\end{theoreme}
\begin{proof}
  \begin{enumerate}
    \item Montrons que $\forall n \in \N,\ u_n ≤ v_n$.\\
      Raisonnons par l'absurde et supposons qu'il existe $n_0 \in \N$, tel
      que $u_{n_0} > v_{n_0}$.\\
      $\forall n \in \N,\ n ≥ n_0 \implies v_n ≤ v_{n_0} < u_{n_0} ≤ u_n$ et
      donc $\forall n \in \N,\ n ≥ n_0 \implies u_n - v_n ≥ u_{n_0} -
      v_{n_0} = \varepsilon_0 > 0$, ce qui contredit l'hypothèse de la
      limite nulle pour la différence.
    \item $\forall n \in \N,\ u_0 ≤ u_n ≤ u_n ≤ u_0$. $u$ est croissante
      majorée donc convergente. $v$ est décroissante minorée donc
      convergente.
    \item $\lim_{n \infty} u_n - v_n = 0 \implies \ell_u = \ell_v$.
  \end{enumerate}
\end{proof}

\begin{theoreme}
  Si $(u_n)_{n\in\N}$ converge vers $\ell \in \overline{\R}$, alors
  $(\abs{u_n})_{n\in\N}$ converge vers $\abs{\ell}$.
\end{theoreme}
\begin{proof}
  On utilise le fait que $\abs{\abs{a} - \abs{b}} ≤ \abs{a - b}$.
\end{proof}

\begin{contreexemple}[La réciproque est fausse]
  $u_n = (-1)^n$
\end{contreexemple}

\subsection{Étude des suites convergentes dans $\R$}

\subsubsection{Théorèmes de comparaison}

\begin{theoreme}\label{01analyse:thm:majoration_suites}
  Si $(u_n)_{n\in\N}$ est convergente vers $\ell < b$, alors $\exists N \in
  \N \mid \forall n \in N, n ≥ N \implies u_n < b$
\end{theoreme}
\begin{proof}
  Choisions $\varepsilon > 0$ tel que $\ell + \varepsilon < b.\ \exists N
  \in \N \mid \forall n \in N, n ≥ N \implies  u_n < \ell + \varepsilon <
  b$.
\end{proof}

En particulier si la limite est positive tous les termes sont positifs à
partir d'un certain rang.

\begin{theoreme}\label{01analyse:thm:majoration_limite}
  Si, à partir d'un certain rang on a $u_n < \beta$ ($u_n ≤ \beta$) et si
  $(u_n)_{n\in\N}$ converge vers $\ell$, alors $\ell ≤ \beta$.
\end{theoreme}
\begin{proof}
  Par l'absurde : si $\ell > \beta$, alors à partir d'un certain rang $u_n >
  \beta$.
\end{proof}

\begin{contreexemple}[Passage à la limite dans les inégalités]
  Les inégalités strictes deviennent larges par passage à la limite.
\end{contreexemple}

\begin{theoreme}
  Si $(u_n)_{n\in\N}$ et $(v_n)_{n\in\N}$ admettent $\ell$ et $\ell'$ comme
  limite et si $\ell > \ell'$, alors à partir d'un certain rang, $u_n >
  v_n$.
\end{theoreme}
\begin{proof}
  On utilise $\lim_{n \infty} u_n - v_n = \ell - \ell' > 0$ et le
  théorème~\ref{01analyse:thm:majoration_suites}.
\end{proof}

\begin{theoreme}
  Si, à partir d'un certain rang, on a $u_n > v_n$ et si d'autre part
  $(u_n)_{n\in\N}$ et $(v_n)_{n\in\N}$ admettent $\ell$ et $\ell'$ comme
  limite, alors $\ell ≥ \ell'$.
\end{theoreme}
\begin{proof}
  On utilise $\lim_{n \infty} u_n - v_n = \ell - \ell' > 0$ et le
  théorème~\ref{01analyse:thm:majoration_limite}
\end{proof}

\begin{theoreme}[dit des «gendarmes»]
  Si d'une part, à partir d'un certain rang $v_n ≤ u_n ≤ w_n$ et d'autre
  part $(v_n)_{n\in\N}$ et $(w_n)_{n\in\N}$ admettent la même limite $\ell$,
  alors $(u_n)_{n\in\N}$ est convergente, de limite $\ell$.
\end{theoreme}
\begin{proof}
  Soit $\varepsilon > 0, \exists N_1  \in \N \mid n > N_1 \implies v_n ≤ u_n
  ≤ w_n$.\\
  $\exists N_2  \in \N \mid n > N_2 \implies l - \varepsilon < v_n < l +
  \varepsilon$.\\
  $\exists N_3  \in \N \mid n > N_3 \implies l - \varepsilon < w_n < l +
  \varepsilon$.\\
  $\implies \forall n \in \N,\ n > \sup(N_1,N_2,N_3) \implies l -
  \varepsilon < u_n < l + \varepsilon$.
\end{proof}

\begin{exemple}
  \leavevmode
  \begin{itemize}
    \item $u_n = \frac{n \sin n}{n^2 + 1}$
    \item $u_n = \sum_{p=1}^n \frac{n}{n^2 + p}$
  \end{itemize}
\end{exemple}

\subsubsection{Opérations sur les suites}

\paragraph{$\R$ espace vectoriel de suites convergentes dans $\R$}

\begin{theoreme}
  Si les suites $(u_n)_{n\in\N}$ et $(v_n)_{n\in\N}$ sont convergentes de
  limites $u$ et $v$, alors $\forall (\alpha,\beta) \in \R^2$ la suite
  $(\alpha u_n + \beta v_n)_{n\in\N}$ est convergente de limite $\alpha u +
  \beta v$.
\end{theoreme}

\begin{contreexemple}[Attention]
  la somme d'une suite convergente et d'une suite divergente est divergente,
  mais on ne peut rien dire sur la somme de deux suites divergentes.
\end{contreexemple}

\paragraph{Produit}

\begin{theoreme}
  Si la suite $(u_n)_{n\in\N}$ est bornée et si la suite $(v_n)_{n\in\N}$
  converge vers 0, alors la suite produit $(u_nv_n)_{n\in\N}$ converge vers
  0.
\end{theoreme}
\begin{proof}
  $\exists K > 0$ tel que $\forall n \in \N,\ \abs{u_n} ≤ K \implies
  \abs{u_n v_n} ≤ K \abs{v_n}$ …
\end{proof}

\begin{theoreme}
  Si les suites $(u_n)$ et $(v_n)$ convergent vers $u$ et $v$, alors la
  suite $(u_nv_n)$ converge vers $uv$.
\end{theoreme}
\begin{proof}
  $u_nv_n - uv = \underbrace{(u_n - u)}_{\to 0} ×
  \underbrace{v_n}_{\text{bornée}} + u\underbrace{(v_n - v)}_{\to 0}$
\end{proof}

\begin{theoreme}
  Si $(v_n)_{n\in\N}$ converge vers $v ≠ 0$ et si de plus, $\forall n \in
  \N,\ v_n ≠ 0$, alors la suite $\brk*{\frac1{v_n}}$ est convergente de
  limite $\frac1v$.
\end{theoreme}
\begin{proof}
  $\abs*{\frac1{v_n} - \frac1v} = \frac{\abs{v_n - v}}{\abs{v}\abs{v_n}} ≤
  \frac{\abs{v_n - v}}{m\abs{v}}$.
\end{proof}

\begin{remarque}
  Pour le quotient, on écrit $\frac{u_n}{v_n} = u_n × \frac1{v_n}$.
\end{remarque}

\begin{contreexemple}[Produit de suites divergentes]
  \begin{itemize}
    \item Le produit d'une suite divergente et d'une suite convergente n'est
      pas nécessairement divergent ou convergent.
    \item Le produit de deux suites divergentes peut-être convergent : $u_n
      = v_n = (-1)^n$.
  \end{itemize}
\end{contreexemple}

\subsection{Étude des suites convergentes dans $\overline{\R}$}

\subsubsection{Comparaison}

\begin{theoreme}
  Si $u_n ≤ v_n$ à partir d'un certain rang et si, de plus
  \begin{itemize}
    \item $\lim_{n\to \infty} u_n = +\infty$ alors $\lim_{n\to \infty}
      v_n = +\infty$
    \item $\lim_{n\to \infty} v_n = -\infty$ alors $\lim_{n\to \infty}
      u_n = +\infty$
  \end{itemize}
\end{theoreme}

\subsubsection{Somme}

\begin{theoreme}
  Si $u_n \to +\infty$ (resp. $u_n \to -\infty$) et si $(v_n)$ est
  bornée, alors $(u_n + v_n) \to +\infty$ (resp. $(-\infty)$).
\end{theoreme}

\begin{theoreme}
  Si $u_n \to \varepsilon \infty$ et $v_n \to \varepsilon \infty$, alors
  $u_n + v_n \to \varepsilon \infty$.
\end{theoreme}

\begin{contreexemple}[somme de suite divergentes]
  Attention «$+\infty + -\infty$» est indéterminé.
\end{contreexemple}

\subsubsection{Produit}

\begin{theoreme}
  So $u_n \to \varepsilon \infty$ et $v_n \to \ell,\ \ell ≠ 0$, alors
  $(u_nv_n)$ tend vers $\varepsilon \sgn(\ell) \infty$.
\end{theoreme}

\begin{theoreme}
  Si $u_n \to \varepsilon \infty$, alors $\frac{1}{u_n} \to 0$.\\
  Si $u_n \to 0$ et $u_n > 0$, alors $\frac1{u_n} \to +\infty$.\\
  Si $u_n \to 0$ et $u_n < 0$, alors $\frac1{u_n} \to -\infty$.
\end{theoreme}

\section{Suites à termes complexes}

\subsection{Généralités}

\begin{definition}
  On appelle suite de nombres complexes toute application de $\N$ dans
  $\C$, notée $(z_n)_{n\in\N}$.
\end{definition}

\begin{remarque}
  On pose, pour tout $n \in \N$,
  \begin{itemize}
    \item $z_n = x_n + i y_n$, $(x_n) \in \R^{\N}$ et $(y_n) \in
      \R^{\N}$ les suites des parties réelles et imaginaires ;
    \item $z_n = \rho_n e^{i \theta_n}$, $(\rho_n) \in \R^{\N}$ et
      $(\theta_n) \in \R^{\N}$ les suites des modules et arguments.
  \end{itemize}
\end{remarque}

$\C$ n'étant pas muni d'une relation d'ordre, les notions de suites
croissantes, décroissantes, majorées, minorées n'ont plus de sens.

On définit simplement la notion de suites bornées.

\begin{proposition}
  $(z_n)$ est bornée :
  \begin{itemize}
    \item si et seulement si $(\rho_n)$ est bornée ;
    \item si et seulement si $\exists M \in \R_+^* \mid \forall n \in
      \N,\ \abs{z_n} ≤ M$ ;
    \item si et seulement si les suites $(x_n)$ et $(y_n)$ sont bornées.
  \end{itemize}
\end{proposition}

\subsection{Suites convergentes}

\begin{definition}
  $(z_n)_{n\in\N} \in \C^{\N}$ est convergente de limite $\ell \in \C$
  si et seulement si $\forall \varepsilon > 0,\ \exists N \in \N,\
  \forall n \in \N,\ n ≥ N \implies \abs{z_n - \ell} < \varepsilon$.
\end{definition}

Cette limite, si elle existe, est unique.

\begin{definition}
  $(z_n)_{n\in\N} \in \C^{\N}$ est de Cauchy si et seulement si $\forall
  \varepsilon > 0,\ \exists N \in \N,\ \forall n \in \N,\ \forall p \in
  \N,\ n ≥ N \implies \abs{z_{n+p} - z_n} < \varepsilon$.
\end{definition}

\begin{lemme}
  $\forall z \in \C,\ z = x + iy,\ \begin{cases} \abs{x} ≤ \sqrt{x^2 +
    y^2} ≤ \abs{x} + \abs{y} \\ \abs{y} ≤ \sqrt{x^2 +
  y^2} ≤ \abs{x} + \abs{y} \end{cases}$.
\end{lemme}

\begin{theoreme}
  $(z_n)_{n\in\N} \in \C^{\N}$ est convergente dans $\C$ si et seulement
  si les suites $x_n$ et $y_n$ sont convergentes.
\end{theoreme}

\begin{theoreme}
  $(z_n)_{n\in\N} \in \C^{\N}$ est de Cauchy si et seulement si les
  suites $x_n$ et $y_n$ sont de Cauchy.
\end{theoreme}

La conséquence fondamental est que $\C$ est complet, c'est-à-dire qu'une
suite est convergente si et seulement si elle est de Cauchy.

\begin{proposition}
  \leavevmode
  \begin{enumerate}
    \item Toute suite extraite d'une suite convergente est convergente
      de même limite.
    \item Si les suites $(z_{2n})$ et $(z_{2n+1})$ sont convergentes de
      même limite $\ell$, alors $(z_n)$ est convergente de limite
      $\ell$.
    \item Toute suite convergente est bornée.
    \item Si $(z_n)$ converge vers $\ell$, alors $(\abs{z_n})$ converge
      vers $\abs{\ell}$
    \item Si $(z_n)$ converge vers $\ell ≠ 0$, alors la suite est
      minorée en module à partir d'un certain rang.
  \end{enumerate}
\end{proposition}

\section{Étude de suites classiques}

\subsection{Suites géométriques}

\begin{definition}
  On appelle suite géométrique toute suite récurrente définie par
  $u_{n+1} = qu_n,\ (u_0,q) \in \C × \C$ donnés. $q$ s'appelle la raison
  de la suite.
\end{definition}

On démontre par récurrence que $u_n = u_0q^n$. De plus, si $u_0$ ou $q =
0$, alors $\forall n \in \N^*,\ u_n =0$, ce qui est sans intérêt.

\begin{itemize}
  \item Si $\abs{q} > 1$, on pose $\abs{q} = h + 1,\ h > 0$ et donc
    $\abs{q}^n = (1 + h)^n ≥ nh$. La suite $(q^n)$ ne converge pas dans
    $\C$.
  \item Si $\abs{q} = 1$.\\
    Supposons que $u_n$ converge vers $\ell \in \C$. Or $u_{n+1} = qu_n$
    donc $\ell = q\ell$ et donc $\ell (q-1) = 0 \implies \begin{cases}
    \ell = 0 \\ q = 1 \end{cases}$.
  \item Si $\abs{q} < 1$, alors $\lim_{n\to +\infty} = 0$.
\end{itemize}

\subsection{Suites récurrentes de type $u_{n+1} = f(u_n)$}

On suppose que $f \colon A \to \R,\ A \subset \R$ et que $f(A) \subset
A$. On considère alors la suite définie par $u_{n+1} = f(u_n),\ \forall
n \in \N,\ u_0 \in A$.

\begin{theoreme}
  \leavevmode
  \begin{itemize}
    \item Si la fonction $f$ est croissante, alors la suite $(u_n)$ est
      monotone et son sens de variation est donné par les premiers
      termes.
    \item Si la fonction $f$ est décroissante, alors les suites
      $(u_{2n})$ et $(u_{2n+1})$ sont monotones et de sens contraires.
  \end{itemize}
\end{theoreme}

\begin{remarque} Si $A$ est une partie bornée de $\R$ la suite $(u_n)$
  est convergente et les suites $(u_{2n})$ et $(u_{2n+1})$ sont
  convergentes.
\end{remarque}

On suppose de plus que $f$ est continue sur $A$. Si $(u_n)$ converge
vers $\ell \in A$ alors $f(\ell) = \ell$.

\begin{remarque}
  \leavevmode
  \begin{enumerate}
    \item si $f(\ell) = \ell$ ne possède pas de solution dans $A$, alors
      $(u_n)$ ne peut pas converger dans $A$. Sinon, on ne peut pas
      conclure que $(u_n)$ converge.
    \item Dans tous les cas $\ell \in \overline{A}$ donc à $A$ si $A$
      est fermé.
    \item le même théorème vaut aussi pour les suites $(u_{2n})$ et
      $(u_{2n+1})$ lesquelles, si elles convergent dans $A$ convergent
      vers les limites $\ell$ et $\ell'$ solutions de $f\circ f(\ell) =
      \ell'$.
    \item Pour déterminer les solutions des l'équation $f \circ f(x) =
      x$ (qui sont les valeurs possibles des limites) on peut remarquer
      que $f(x) = x \implies f \circ f(x) = x$, ce qui permet certaines
      factorisation quand on a des équations polynomiales.
    \item Si on sait que $\lim_{n \to \infty} u_{2n} = \ell \in A$, on a
      $\lim_{n \to \infty} u_{2n+1} = f(\ell) \in A$. Il suffit donc de
      montrer la convergence d'une des deux suites.
  \end{enumerate}
\end{remarque}

Pour étudier ces suites, on peut suivre le plan suivant :
\begin{enumerate}
  \item Étudier la fonction $f$, faire un dessin et déterminer $A$ (en
    général un intervalle) tel que $f(A) \subset A$ ($A$ peut dépendre
    de $u_0$).
  \item Résoudre $f(\ell) = \ell$ ou même $f\circ f(\ell) = \ell$
  \item Si $f$ est croissante, on étudie le signe de $u_{n+1} - u_n =
    u_1 - u_0$. On étudie le signe de $u_n - \ell$ et en général, on
    choisit $\ell$ pour majorer ou minorer la suite. Ne pas oublier que
    $f(\ell) = \ell$, cela simplifie les calculs.
  \item Si $f$ est décroissante, alors $f^2$ est croissante. On étudie
    le signe de $u_{n+2} - u_n$. Les suites $(u_{2n})$ et $(u_{2n+1})$
    sont monotones de sens contraire.
    \begin{itemize}
      \item Étudier séparement les suites $(u_{2n})$ et $(u_{2n+1})$ et
        montrer qu'elles sont convergentes de même limite.
      \item Montrer que les suites $(u_{2n})$ et $(u_{2n+1})$ sont
        adjacentes en essayant de montrer que $f$ est lipschitzienne de
        rapport $k \in \intv[r]{0}{1}$.
    \end{itemize}
\end{enumerate}

\begin{definition}
  $f \colon A \to A$ est dit contractante, s'il existe $k \in \R$ tel
  que $k \in \intv[r]{0}{1}$ et pour tout $(x,y) \in A^2,\ \abs{f(y) -
  f(x)} ≤ k \abs{y - x}$.
\end{definition}

\begin{theoreme}
  Toute application contractante est uniformément continue sur $A$.
\end{theoreme}

\begin{contreexemple}{$k ≠ 1$}
  Une application $f$ vérifiant $\abs{f(y) - f(x)} ≤ \abs{y - x}$ n'est
  pas contractante et le théorème qui suit ne s'applique pas.
\end{contreexemple}

\begin{theoreme}[du point fixe]
  Soit $f \colon A \to A$, $A$ \textbf{fermée} et $f$ contractante.
  L'équation $f(x) = x$ possède alors une solution unique $\ell$. De
  plus, pour tout choix de $u_0 \in A$, la suite récurrente $u_{n+1} =
  f(u_n)$ est convergente vers $\ell$.
\end{theoreme}

En pratique :
\begin{itemize}
  \item pour prouver que $f$ est contractante, une méthode simple est de
    borner la dérivée de $f$ (si $f$ est dérivable) car on sait que
    $\abs{f(x) - f(y)} = \abs{x - y}\abs{f'(c)}$ (théorème des
    accroissements finis)
  \item souvent $f$ est seuelement contractante dans un voisinage $\V$
    fermé de la limite éventuelle de la suite $(u_n)$. Il reste à
    prouver qu'à partir d'un certain rang tous les $(u_n)$ sont dans
    $\V$.
\end{itemize}

\subsubsection{Suites homographiques}

\subsubsection{Suites $u_{n+1} = a u_n + b$}

\subsubsection{Récurrence linéaire dans $\K = \R$ ou $\C$}

\section{Comparaison des suites}

\subsection{Suite négligeable devant une autre}

\subsubsection{Définition}

\begin{definition}
  Soit $(u_n)$ et $(v_n)$ deux suites de $\K^{\N}$. On dit que la suite
  $(u_n)$ est négligeable devant la suite $(v_n)$ si et seulement si
  $\exists (\varepsilon_n)$ de limite nulle telle que $u_n =
  \varepsilon_n v_n$.\\
  On note $u_n = \po{v_n}$ (ou $u_n << v_n$).
\end{definition}

\begin{remarque}
  Si $v_n ≠ 0$ à partir d'un certain rang, $u_n = \po{v_n} \iff
  \lim{n\to \infty} \frac{u_n}{v_n} = 0$.
\end{remarque}

\subsubsection{Propriétés}

\begin{proposition}
  Si $u_n = \po{v_n}$ et $v_n = \po{w_n}$, alors $u_n = \po{w_n}$.
\end{proposition}

\begin{proposition}
  Soient $u,\ v$ et $w$ trois suites d'éléments de $\K$.\\
  Si $u_n \po{w_n}$ et $v_n = \po{w_n}$ alors, $\forall (\alpha,\beta)
  \in \K^2,\ \alpha u_n + \beta v_n = \po{w_n}$.
\end{proposition}

\subsubsection{Comparaison de suites classiques}

\begin{proposition}
  $\forall k \in \R,\ k^n = \po{n!}$
\end{proposition}

\begin{proposition}
  $\forall \alpha \in \R_+^*,\ \forall k \in \R,\ k > 1,\ n^{\alpha} =
  \po{k^n}$
\end{proposition}

\begin{proposition}
  $\forall (\alpha,\beta) \in \brk{\R_+^*}^2,\ \brk{\ln n}^{\beta} =
  \po{n^{\alpha}}$.
\end{proposition}

\subsection{Suites équivalentes}

\subsubsection{Définition}

\begin{definition}
  Soient $u$ et $v$ deux suites de $\K^{\N}$. La suite $u$ est
  équivalente à la suite $v$ si et seulement s'il existe une suite
  $\varphi \in \K^{\N}$ telle que $\forall n \in \N,\ u_n = \varphi_n
  v_n$ et $\lim_{n\to \infty} \varphi_n = 1$.\\
  On note $u \sim v$ ou $u_n \sim v_n$.
\end{definition}

\begin{remarque}
  Si $v_n ≠ 0$ à partir d'un certain rang, $u_n \sim v_n \iff
  \lim_{n\to\infty} \frac{u_n}{v_n} = 1$.
\end{remarque}

\subsubsection{Propriétés}

\begin{proposition}
  «$\sim$» est une relation d'équivalence dans $\K^{\N}$.
\end{proposition}

\begin{proposition}
  Si $u_n \sim v_n$ et si $(v_n)$ converge vers $\ell$ dans $\K$ ($\K =
  \R,\ C$ ou $\overline{\R}$), alors $(u_n)$ converge aussi vers $\ell$.
\end{proposition}
